<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
	<head>
		<style>
		html,body
		{
			height:100%;
 			min-height:100%;
 
		}
		.hearts
		{
			color: red;
			font-color:red;
		}
		.text
		{
			color:black;
			font-family: Verdana, Helvetica, Sans-Serif;
			padding:5px;
		}
		
		.profileBackground
		{
	    	color: black;
		}
		
		.profileBackground label
		{
			font-size:12px;
		}
		
		.profileBackground span
		{
			font-size:15px;
		}
		
		.profileBackground input
		{
			font-family: Verdana, Helvetica, Sans-Serif;
			border:2px solid #ECC23A;
		}
		
		.profileBackground a
		{
			color: black;
		}
		
		.profileBackground a:hover
		{
			color: #ECC23A;
		}
		
		.profileBackground select
		{
			font-family: Verdana, Helvetica, Sans-Serif;
			border:2px solid #ECC23A;
		}
		
		.menuBackground
		{
/* 			border: 1px solid #ffff99; */
/* 	    	box-: 0px 0px 20px #888888; */
	    	color: black;
		}
		
		.menu label
		{
			cursor:pointer;
			font-size:11px;
			color: black;
		}
		
		.menu a
		{
			font-size:10px;
			color: black;
			text-decoration: none;
			cursor:pointer;
		}
		
		.menu a:hover
		{
			color: #ECC23A;
		}
		
		.menu .menuHeader
		{
			font-weight:bold;
		}
		
		.menu .menuHeader:hover
		{
			font-weight:bold;
			color: #ECC23A;
		}
		
		.favoritesTitle:hover
		{
			color: #ECC23A;
		}
		
		.contains
		{
/* 			border: 1px solid #ffff99; */
/* 			font-size:.75em; */
	    	color: black;
		}
		
		.menu li {list-style-type: circle; color:black;}
		
/* 		#tablePaddingLayout2 td  */
/* 		{ */
/*    			padding: 0; */
/* 		} */
			
		@-moz-document url-prefix() {
   			.paddingLayout 
   			{
        		min-height:100%;
/*         		width:100%; */
    		}
    		#tablePaddingLayout 
   			{
         		min-height:100%;
        		width:100%;
    		}
    		.paddingLayout2
   			{
        		height:100%;
        		width:100%;
    		}
    		
    		#tablePaddingLayout2 
   			{
        		height:100%;
        		width:100%;
    		}
    		
/*     		#tablePaddingLayout2 td { */
/*    				padding: 0; */
/* 			} */
    		
		}
		</style>
	
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		
<%-- 		<title><tiles:insertAttribute name="title" /></title> --%>
		<link href="/MOA/resources/css/style.css" rel="stylesheet" type="text/css" />
		<link href="/MOA/resources/css/kendo.common.min.css" rel="stylesheet" type="text/css" />
	    <link href="/MOA/resources/css/kendo.default.min.css" rel="stylesheet" type="text/css" />
		<link href="/MOA/resources/css/colorbox.css" rel="stylesheet" type="text/css" />
		
		<link href="/MOA/resources/css/Metro.css" rel="stylesheet" type="text/css" />
		
	    <script src="/MOA/resources/js/jquery.min.js"></script>
	    <script src="/MOA/resources/js/kendo.web.min.js"></script>
	    <script src="/MOA/resources/js/jquery.colorbox-min.js"></script>
	    <script src="/MOA/resources/js/custom.js"></script>
	    <script src="/MOA/resources/js/jquery-ui-1.10.4/js/jquery-ui-1.10.4.custom.min.js"></script>
	</head>
	<body>
	
	<table border="0" style="width:100%; min-height:100%;border-collapse:collapse; max-width:100%; position:relative;" class="paddingLayout2" id="tablePaddingLayout2">
	<tr>
		<td height="1px" valign="top" style="padding-left:5px; min-width:10px; width:10px; cursor:pointer; background-color:white;" width="10px" onClick="toggleMenu()">	
			<div><b style="font-weight:bold; font-size:20px"></b></div>
		</td>	
		<td valign="top" style="width:1px;" width="1px" bgcolor="" rowspan="2">
		<div style="z-index:10; position:absolute; visibility:hidden; display:block; min-height:100%; border-width: 0px 2px 0px 0px; border-color: gray; border-style: solid; box-shadow: 10px 10px 5px #DDDDDD; background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */" id="divMenuToggle" class="paddingLayout">
		<table border="0" style="width:100%; min-height:100%;border-collapse:collapse; max-width:100%;" id="tablePaddingLayout">
			<tr>
				<td height="1px" style="width:260px; min-width:260px; background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */" class="text profile profileBackground" valign="top">
<!-- 					<div class="profileBackground" style="height:100%;"> -->
<%-- 						<tiles:insertAttribute name="profileMenu" /> --%>
<!-- 					</div>  -->
				</td>
			</tr>
			<tr>
				<td height="1px" style="background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */">
					&nbsp;
					<c:if test="${not empty UserLogin}">
<%-- 						<tiles:insertAttribute name="portalMenu" /> --%>
					</c:if>
				</td>
			</tr>
			<tr>
				<td height="1px" class="menu" style="background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */">
					&nbsp;
					<c:if test="${not empty UserLogin}">
<%-- 						<div id="favorite1"><tiles:insertAttribute name="favorite" /></div> --%>
					</c:if>
					
				</td>
			</tr>
			<tr>
				<td height="1px" style="background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */">
					<br/>&nbsp;
					<c:if test="${not empty UserLogin}">
						<input style="font-family: Verdana, Helvetica, Sans-Serif;border:2px solid #ECC23A;" type='text' placeholder="Search Module..." id="searchMenu" onkeypress="searchMenu(event)"/>
						<br/>
					</c:if>
				</td>
			</tr>
			<tr> 
				<td height="1px" valign="top" width="250px" class="text menu" style="background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */">
					<div style="overflow-x:hidden;" class="menuBackground">
						<table width="100%">
<%-- 							<tr><td style="max-width: 180px"><div id="menu1"><tiles:insertAttribute name="menu" /></div></td></tr> --%>
						</table>
					</div>
				</td>
				
	<!-- 			<td> -->
	<!-- 				<div id="popBubble" style="padding-top:30px;"><img src="/MOA/resources/images/bubble-beng.png" width="150px" style="opacity:0.9;"/></div> -->
	<!-- 				<div id="popBubble" style="padding-top:100px;padding-left:100px;"><img src="/MOA/resources/images/bubble-tph.png" width="150px" style="opacity:0.9;"/></div> -->
	<!-- 				<div id="popBubble" style="padding-top:150px;padding-left:50px;"><img src="/MOA/resources/images/bubble-zupper.png" width="150px" style="opacity:0.9;"/></div> -->
	<!-- 			</td> -->
			</tr>
			<tr> 
				<td valign="top" width="250px" class="text menu" style="background: #EFEFEF; /* For browsers that do not support gradients */
			  background: -webkit-linear-gradient(left, white , #BBBBBB); /* For Safari 5.1 to 6.0 */
			  background: -o-linear-gradient(right, white, #BBBBBB); /* For Opera 11.1 to 12.0 */
			  background: -moz-linear-gradient(left, white, #BBBBBB); /* For Firefox 3.6 to 15 */
			  background: linear-gradient(to right, white , #BBBBBB); /* Standard syntax */">
				&nbsp;
				</td>
			</tr>
		</table> 
		</div>
		</td>
		<td valign="top" style="padding-left:5px; max-width:0px;" onClick="toggleMenuContainer()" rowspan="2">	
			<div class="container contains" style="height:100%; overflow-x:auto;"> 
				<div>
<%-- 					<tiles:insertAttribute name="panelButton" /> --%>
					<br/>
					<tiles:insertAttribute name="body" />
					<br/>
<%-- 					<tiles:insertAttribute name="panelButton" /> --%>
				</div>
			</div>
		</td>
		</tr>
		<tr><td></td></tr>	
		</table>
	</body>
</html>