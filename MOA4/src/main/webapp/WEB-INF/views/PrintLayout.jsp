<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
	<head>
		<script src="/MOA2/resources/js/jquery.min.js"></script>
		<link href="/MOA2/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<input type=text id="txtHid" value="${data}" style="display:none;"/>
		<a href="${link}"><input type="button" class="btn btn-medium" value="BACK"/></a>
		<iframe id="reader" style="width:99%" height="95%">
<%-- 			<object data="${data}" type="application/pdf" width="100%" height="94%"></object> --%>
		</iframe>
	</body>
</html>

<script>
	$(document).ready(function(){
		document.getElementById("reader").src = document.getElementById("txtHid").value;
	});
</script>