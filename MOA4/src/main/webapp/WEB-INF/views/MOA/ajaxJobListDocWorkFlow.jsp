<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<c:url value="${readUrl}" var="readUrl" />

<kendo:grid name="gridHome" pageable="true" groupable="true" sortable="true" filterable="false" 
				selectable="true" scrollable="false" resizable="true" databound="databound()"> 

        <kendo:grid-columns> 
        	<kendo:grid-column title="Check" width="180px" field="docno" template="<input type='checkbox' name='check' id='check' value=#= docNo # />"/> 
        	<kendo:grid-column title="Doc No" width="180px" field="docNo" template="<a target='_blank' href='/MOA2/ShowStatus/?docNo=#= docNo #&moduleCode=#= moduleCode #'>#= docNo #</a>"/> 
            <kendo:grid-column title="Module" field="moduleName"/> 
            <kendo:grid-column title="last Processed By" field="lastApproveName" width="110px"/> 
            <kendo:grid-column title="last Processed Date" field="lastDate" width="120px"/>
            <kendo:grid-column title="Created By" field="creatorName" width="110px" /> 
            <kendo:grid-column title="Created Date" field="createdDate" width="100px" />
        </kendo:grid-columns>
        
        <kendo:dataSource pageSize="5">    
        	<kendo:dataSource-transport>
            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
				 <kendo:dataSource-transport-parameterMap>
                	<script>
  	                	function parameterMap(options,type) {  
  	                		return JSON.stringify(options); 
  	                	} 
                  	</script>  
                </kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport> 
       </kendo:dataSource>
	</kendo:grid>
	
	<c:forEach var="text" items="${text}">
	<table>
	<tr>
	<td><font color="blue">${text}</font></td>
	</tr>
	</table>
	</c:forEach>
	
	<input type="button" id="submit" value="Accept" onClick="aClick(this)"/>
	<input type="button" id="return" value="Return" onClick="aClick(this)"/>
	<img id="waitingLink" src="/MOA2/resources/css/images/loading.gif" height="30px" title="Loading" style="border:1px; display:none;"/>
	<div id="window"></div>
<script type="text/javascript">

$(document).ready(function(){
	var x = $("#module").val();
	
	if(x != 'ALL'){
		var grid = $("#gridHome").data("kendoGrid");
		grid.hideColumn("moduleName");
	}  
}); 

function databound(){
	kendo.ui.progress($("#gridHome"), false);
}

function aClick(e)
{
	var docNo="";
	var check=document.getElementsByName("check");
	var n=0;
	for(var i=0;i<check.length;i++)
	{
		if(check[i].checked==true)
		{
			if(n>0)
			{
				docNo=docNo+"_"+check[i].value;
			}
			else
			{
				docNo=docNo+check[i].value;
			}
			n=n+1;
		}
	}
	
	if(e.value=="Accept")
	{
		$("#waitingLink").css("display","block"); 
		try { $("#submit").css("display","none"); } catch(e) { }
		try { $("#return").css("display","none"); } catch(e) { }
		$("#window").load("/MOA2/JobListDocument/Action/?docNo="+docNo+"&n="+n+"&action="+$("#action").val()+"_"+$("#action").val()+"&module="+$("#module").val());
	}
	else
	{
		$("#waitingLink").css("display","block"); 
		try { $("#submit").css("display","none"); } catch(e) { }
		try { $("#return").css("display","none"); } catch(e) { }
		$("#window").load("/MOA2/JobListDocument/Action/?docNo="+docNo+"&n="+n+"&action=ReturnDoc_"+$("#action").val()+"&module="+$("#module").val());
	}
}
</script>	
	
