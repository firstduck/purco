<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div id="browse-wrapper">
 		<c:url value="/ShowStat/DataBrowse/?search=${search}" var="readUrl" /> 
	<div class="search display-for-form">Search <input type="text" name="search" id="search" /></div>
	<br/>
 
 	<kendo:grid name="gridBrowse" pageable="true" sortable="true">
	<kendo:grid-editable mode="inline" confirmation="Are you sure you want to remove this item?"/>
        <kendo:grid-columns>  
        	<kendo:grid-column title="Doc No" field="DOCNO" width="220px" template="<a target ='_blank' href='/MOA2/ShowStatus/?docNo=#= DOCNO #&moduleCode=#=MODULECODE#'>#= DOCNO #</a>"/> 
<%--         	<kendo:grid-column title="Doc No" field="DOCNO" width="220px" template="<input type='text' style='border : 0px;' readonly='readonly' name= '#= DOCNO #_#= WEBNAME #' id='#= DOCNO #_#= WEBNAME #' value='#= DOCNO #' onClick='aClick(this)'>"/>  --%>
			<kendo:grid-column title="Created By" field="NAME" width="100px"/>
			<kendo:grid-column title="Module Code" field="MODULECODE" width="100px"/>
			<kendo:grid-column title="Detail Link" field="DETAILLINK" width="220px"/>
			<kendo:grid-column title="Doc Reff" field="DOCREFF" width="220px"/>
			<kendo:grid-column title="Web Name" field="WEBNAME" hidden="true" width="220px"/>
        </kendo:grid-columns>
        
            <kendo:dataSource pageSize="20">
        	<kendo:dataSource-transport>
            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
			</kendo:dataSource-transport> 
            <kendo:dataSource-schema> 
               <kendo:dataSource-schema-model>
                   <kendo:dataSource-schema-model-fields>
                       <kendo:dataSource-schema-model-field name="DOCNO" type="string" />
                   </kendo:dataSource-schema-model-fields>
               </kendo:dataSource-schema-model>
           </kendo:dataSource-schema>
       </kendo:dataSource>
	</kendo:grid>

<script type="text/javascript">
	$('#search').change(function() { 
		var search = $("#search").val().split(' ').join('%20');		
// 		$('#browse-wrapper').load("/MOA2/MasterMaterial/AjaxBrowse/?search="+search);
		 $("#gridBrowse").data('kendoGrid').dataSource.data([]); //buat apus isi nya
		 	
		 var grid = $("#gridBrowse").data("kendoGrid");

		 grid.dataSource.transport.options.read.url ="/MOA2/ShowStat/DataBrowse/?search="+search;
		 grid.dataSource.read();  
		 grid.dataSource.page(1);
	});
	
</script>
</div>