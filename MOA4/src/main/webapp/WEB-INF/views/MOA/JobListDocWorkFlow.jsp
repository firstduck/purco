<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<script type="text/javascript">
$(document).ready(function(){
	$("#window").load("/MOA2/JobListDocument/AjaxJobListDocument/?module=ALL&action=ReceiveDoc_ReceiveDoc");
});
</script>

<table>
<tr>
<td>Module</td>
<td>:</td>
<td><kendo:dropDownList name="module" id="module" dataTextField="text" dataValueField="value"><kendo:dataSource data="${modules}"></kendo:dataSource></kendo:dropDownList>
</tr>
<tr>
<td>Action</td>
<td>:</td>
<td><kendo:dropDownList name="action" id="action" dataTextField="text" dataValueField="value"><kendo:dataSource data="${action}"></kendo:dataSource></kendo:dropDownList>
</tr>
</table>
<div id="window"></div>
	
<script>
$("#module").change(function(){
	$("#window").load("/MOA2/JobListDocument/AjaxJobListDocument/?module="+$("#module").val()+"&action="+$("#action").val()+"_"+$("#action").val());
});

$("#action").change(function(){
	$("#window").load("/MOA2/JobListDocument/AjaxJobListDocument/?module="+$("#module").val()+"&action="+$("#action").val()+"_"+$("#action").val());
});
</script>
