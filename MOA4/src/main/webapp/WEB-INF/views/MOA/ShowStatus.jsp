<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<h2>${title}</h2>
<style>
th 
{
	padding:10px;
}
table.status td
{
	padding: 5px;
}
.done
{
	color:blue;
}
.current
{
	color:green;
}
.reject
{
	color:red;
}
</style>

<script src="http://192.168.8.80:8080/MayWare/resources/js/socket/client-socket-push.js"></script>
<script>
	function parseMessage(from, data) {
		console.log("Push From: " + from);
		location.reload();
	}
</script>
<a href="${link}">Back</a>
<br/>
<h4 style="color:black"> Doc No : ${docStatus[0].docNo} </h4>
<input type="hidden" id="docNo" name="docNo" value="${docStatus[0].docNo}" />
<input type="hidden" id="base_code" value="${docNo}"/>
	
<table border=1 style="border-collapse:collapse;" width="100%" id="tableStatus" class="status">
	<tr style="background-color:#EEEEEE ;">

		<th>Workflow Name</th>
		<th>Line</th>
		<th>Activity</th>
		<th>Line Description</th>
		<th>PIC</th>
		<c:if test = "${!reject}">
			<th>Closed Date</th>
<!-- 			<th>Closed By</th> -->
		</c:if>
		<c:if test = "${reject}">
			<th>Reject Date</th>
			<th>Reject By</th>
		</c:if>
		
		
	</tr>
<c:forEach var="status" items="${docStatus}">
	<tr>
	<c:if test = "${not empty status.closeDate}">
		<c:if test = "${status.pic == status.closeBy}">
			<td><div class='done'>${status.workflowName}</div></td>
			<td><div class='done'>${status.workflowID}.${status.workflowLinesId}</div></td>
			<td><div class='done'>${status.activity}</div></td>
			<td><div class='done'>${status.descLines}</div></td>
			<td><div class='done'>${status.pic}</div></td>
			<c:if test = "${!reject}">
				<td><div class='done'><fmt:formatDate pattern="dd MMMM yyyy HH:mm:ss" value="${status.closeDate}"/></div></td>
<%-- 				<td><div class='done'>${status.closeBy}</div></td>  --%>
			</c:if>
			<c:if test = "${reject}">
				<td><div class='done'><fmt:formatDate pattern="dd MMMM yyyy HH:mm:ss" value="${status.rejectDate}"/></div></td>
				<td><div class='done'>${status.rejectByUsername}</div></td>
			</c:if>
		</c:if>
	</c:if>
	<c:if test = "${empty status.closeDate}">
		<c:set var="workflowLine" value="${status.workflowCode}.${status.workflowID}.${status.workflowLinesId}"/>
		<c:choose>
		<c:when test = "${fn:contains(current,workflowLine)}">
			<td><div class='current'>${status.workflowName}</div></td>
			<td><div class='current'>${status.workflowID}.${status.workflowLinesId}</div></td>
			<td><div class='current'>${status.activity}</div></td>
			<td><div class='current'>${status.descLines}</div></td>
			<td><div class='current'>${status.pic}</div></td>
			<c:if test = "${!reject}">
				<td><div class='current'><fmt:formatDate pattern="dd MMMM yyyy" value="${status.closeDate}"/></div></td>
<%-- 				<td><div class='current'>${status.closeBy}</div></td>  --%>
			</c:if>
			<c:if test = "${reject}">
				<td><div class='current'><fmt:formatDate pattern="dd MMMM yyyy" value="${status.rejectDate}"/></div></td>
				<td><div class='current'>${status.rejectByUsername}</div></td>
			</c:if>
		</c:when>
		<c:otherwise>
			<td><div id="workflowName">${status.workflowName}</div></td>
			<td><div id="workflowID">${status.workflowID}.${status.workflowLinesId}</div></td>
			<td><div id="activity">${status.activity}</div></td>
			<td><div id="groupWorkflowDesc">${status.descLines}</div></td>
			<td><div id="pic">${status.pic}</div></td>
			<c:if test = "${!reject}">
				<td><div id="statusDate"><fmt:formatDate  pattern="dd MMMM yyyy" value="${status.closeDate}"/></div></td>
<%-- 				<td><div id="statusName">${status.closeBy}</div></td> --%>
			</c:if>
			<c:if test = "${reject}">
				<td><div id="statusDate"><fmt:formatDate  pattern="dd MMMM yyyy" value="${status.rejectDate}"/></div></td>
				<td><div id="statusName">${status.rejectBy}</div></td>
			</c:if>
		</c:otherwise>
		</c:choose>
	</c:if>
	</tr> 
</c:forEach>
</table>

<br/>
<div id="ajaxShowStatus">
<script type="text/javascript">
$("#ajaxShowStatus").load("/MOA2/AjaxShowStatus/?docNo="+$("#docNo").val());
</script>
</div>
<br/>

<table>
<tr>
	<td class="current"><b>GREEN</b></td>
	<td class="current">:</td>
	<td class="current">Current</td>
</tr>
<tr>
	<td class="done"><b>BLUE</b></td>
	<td class="done">:</td>
	<td class="done">Done</td>
</tr>
<tr>
	<td class="reject"><b>RED</b></td>
	<td class="reject">:</td>
	<td class="reject">Return / Reject</td>
</tr>
<tr>
	<td class="pending"><b>GREY</b></td>
	<td class="pending">:</td>
	<td class="pending">Waiting</td> 
</tr>
</table>
<br/>
<a href="${link}">Back</a>

<c:if test = "${reject}">
<script>
	$(".status td div").addClass("reject");
</script>
</c:if>