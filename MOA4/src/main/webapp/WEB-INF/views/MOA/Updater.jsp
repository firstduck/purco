<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<script>
function onChange()
{
	$("#applicationHidden").val($("#application").val());
	if($("#contentID").val() == "")
		$("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val());
	else $("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val() + "&contentID="+$("#contentID").val());
	$("#current").css("padding-left","20px");
	$("#current").css("border-left","1px dashed black");
}
function checkAll()
{
	$("#resultLabel").css("visibility","hidden");
	if($("#applicationHidden").val() == "" || $("#version").val() == "")
	{
		if($("#applicationHidden").val() == "") $("#errorApplication").css("display","block");
		else $("#errorApplication").css("display","none");
		if($("#version").val() == "") $("#errorVersion").css("display","block");
		else $("#errorVersion").css("display","none");
	}
	else 
	{
		$("#loading").css("visibility","visible");
		$.ajax({
			type:"POST",
			url:"/MOA2/Updater/Action",
			data: $("#applicationForm").serialize(),
			success:function(data) 
			{
				$("#resultLabel").css("visibility","visible");
				if(data)
				{
					$("#resultLabel").html("Update success!");
					if($("#contentID").val() == "")
						$("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val());
					else $("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val() + "&contentID="+$("#contentID").val());
				}
				else
				{
					$("#resultLabel").html("Update failed!");
				}
				$("#loading").css("visibility","hidden");
			},
			error:function()
			{
				$("#loading").css("visibility","hidden");
			}
		});
	}
}

function antiNumeric(event) {
	var evt = event.charCode;
	if(((evt < 48) || (evt > 57)) && evt != 0  && evt != 46){
		event.preventDefault();
	}
}
</script>

<style>
.attach td 
{
	padding:10px;
}
</style>

<h2>Update Client Application</h2>
<form id="applicationForm" method="POST">
<table class="attach" border=0>
<tr>
<td valign="top"><label class="display-for-form">Application</label></td>
<td valign="top">:</td>
<td style="padding-right:20px">
<input type="hidden" name="applicationHidden" id="applicationHidden" value=""/>
<script>
$("#applicationHidden").val("");
</script>
	<kendo:comboBox style="width:250px;" name="application" dataTextField="applicationName" dataValueField="applicationCode" change="onChange">
		<kendo:dataSource data="${applicationList}"></kendo:dataSource>
	</kendo:comboBox><br/>
	<label class="error" id="errorApplication" style="display:none">Please Select Application</label>
</td>
<td rowspan=6 valign="top" id="current" width="100%"><div id="attachment" style="height:160px; margin-top:-25px"></div>
</td>
</tr>
<tr>
<td><label class="display-for-form">Content Type</label></td>
<td>:</td>
<td><kendo:comboBox style="width:250px;" name="contentID" dataTextField="contentType" dataValueField="contentID" change="onChange">
	<kendo:dataSource data="${contentList}"></kendo:dataSource>
</kendo:comboBox>
</td>
</tr>
<tr>
<td><label class="display-for-form">Version</label></td>
<td>:</td>
<td>
<input type="text" style="width:94%;" id="version" name="version" onkeypress="antiNumeric(event)"/><br/>
<label class="error" id="errorVersion" style="display:none">Please Input Version</label>
</td>
</tr>
<tr>
<td><label class="display-for-form">Content Name</label></td>
<td>:</td>
<td><input type="text" style="width:94%;" id="contentName" name="contentName"/></td>
</tr>
<tr>
<td valign="top"><label class="display-for-form">Notes</label></td>
<td valign="top">:</td>
<td><input type="text" style="width:94%;" id="notes" name="notes"/></td>
</tr>
<tr>
<td valign="top"><label class="display-for-form">Attachment</label></td>
<td valign="top">:</td>
<td>
<b>Max File Size : 500MB</b>
<c:url value="/Client/upload/save" var="saveUrl" />
<c:url value="/Client/upload/remove" var="removeUrl" />
	<kendo:upload name="files" style="width:350px; cursor:hand;"> 
	   <kendo:upload-async autoUpload="true" saveUrl="${saveUrl}" removeUrl="${removeUrl}"/>
	</kendo:upload>
	<input type="hidden" id="errorFileHidden"/> <label class="error" id="errorFile" style="visibility:hidden">Please Insert Attachment</label>
</td>
</tr>
</table> 
<br/>
<table border=0>
<tr>
<td><input type="button" value="Update" onclick="checkAll()"/></td>
<td><img src="/MOA2/resources/css/images/loading.gif" id="loading" style="visibility:hidden"/></td>
<td><label class="display-for-form" id="resultLabel" style="visibility:hidden"></label></td>
</table>
</form>