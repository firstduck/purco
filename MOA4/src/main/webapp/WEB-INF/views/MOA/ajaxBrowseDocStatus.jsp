<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
#tableAjaxCreate { border-collapse:collapse; }
#tableAjaxCreate td, th
{
	border: 1px solid black;
	padding-left : 5px;
	padding-right : 5px;
}


</style>            
          
<c:forEach var="list" items="${md}">
<table id="tableAjaxCreate">
<tr><td>
<input type="button" id="${fn:replace(list.docNo,'/','ggtq')}" value="+" onClick="aClick(this)" /></td><td><a href="${list.webName}/Detail/?docNo=${list.docNo}">${list.docNo}</a></td><td>${list.creatorName}</td><td>${list.createdDate}</td><td>${list.status}</td></tr>
</table>
<table>
<tr><td></td><td><div id="window_${fn:replace(list.docNo,'/','ggtq')}"></div></td></tr>
</table>
</c:forEach>


<script>
function aClick(e)
{
	if(e.value=="+")
	{
		e.value="-";
		$("#window_"+e.id).load("/MOA2/ShowDocStatus/ajaxBrowse/?docNo="+e.id.replace('ggtq','/').replace('ggtq','/').replace('ggtq','/').replace('ggtq','/'));
		$("#window_"+e.id).slideDown();
	}
	else
	{
		e.value="+";
		$("#window_"+e.id).slideUp();
		
	}
	
}
</script>  	