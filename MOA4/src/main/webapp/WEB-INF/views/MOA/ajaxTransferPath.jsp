<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.k-grid th.k-header,
	.k-grid-header
	{
	    background:#e61e26; 
	} 
	  
</style>
 
<kendo:grid name="gridWorkflowPath" resizable="true" sortable="true" selectable="true" pageable="true" filterable="true" scrollable="true" groupable="true">				 
	    <kendo:grid-editable mode="inline"/>     
	   			   		    
        <kendo:grid-columns>   		       	 			        	 
        	<kendo:grid-column width="10%" title="<label style='color:white;'>Workflow Path Id</label>" field="idWorkflowPath"/> 
            <kendo:grid-column width="15%" title="<label style='color:white;'>Workflow Code</label>" field="codeWorkflow"/>
             <kendo:grid-column width="10%" title="<label style='color:white;'>ID</label>" field="idPath"/> 
            <kendo:grid-column width="20%" title="<label style='color:white;'>Workflow Path Desc</label>" field="workflowPathDesc"/> 
            <kendo:grid-column width="15%" title="<label style='color:white;'>Field Id</label>" field="fieldId"/>
            <kendo:grid-column width="10%" title="<label style='color:white;'>Operator</label>" field="operator"/>
            <kendo:grid-column width="20%" title="<label style='color:white;'>Value</label>" field="value"/>
            <kendo:grid-column width="10%" title="<label style='color:white;'>Connector</label>" field="connector"/>
        </kendo:grid-columns>   
		      
		<kendo:dataSource pageSize="10" data="${workflowPathList}">   									         
       </kendo:dataSource>     
</kendo:grid>  