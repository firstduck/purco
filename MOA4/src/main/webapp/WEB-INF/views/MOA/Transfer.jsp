<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>


<h2>Transfer Group, Doc No ${docNo}</h2>

 
<form id="frmAssign" method="POST" action="${action}">
	<input type="hidden" id="docNo" name="docNo" value="${docNo}"/> 
	<input type="hidden" id="currentGroup" name="currentGroup" value="${groupWorkflowCode}"/> 
	
	<div id="loadDetail1"></div> 
		<table >
		
			<tr>
				<td>
					<label class="display-for-form" style="font-size:14px">Transfer Option</label>
				</td>
			</tr>
			
			<tr>
				<td>
					<kendo:dropDownList name="transferOption" style="width:250px"  dataTextField="text" dataValueField="value" >
						<kendo:dataSource data="${transferOption}"></kendo:dataSource>
					</kendo:dropDownList><div id="errorTransferOption" style="display:none;"><label class="error">Please Select Transfer Option</label></div>  
				</td>
				
				<td>
					<div id="currentPath" style="display:none">
						<label style="font-size:14px">${currentPath}</label>
					</div>
				</td>
			</tr>
			
			<tr>
				<td> 
					<div id="transferGroup" style="display:none">
					<label class="display-for-form" style="font-size:14px">Transfer Group To</label>
					</div>
				</td> 
			</tr>
			
			<tr>    
				<td>  
					<div id="transferGroupList" style="display:none">
					<kendo:dropDownList name="transferTo" style="width:250px"  dataTextField="text" dataValueField="value" >
						<kendo:dataSource data="${transferGroupList}"></kendo:dataSource>
					</kendo:dropDownList><div id="errorTransferGroup" style="display:none;"><label class="error">Please Select Transfer Group</label></div>
					</div>
				</td>
			</tr>
			
			<tr>
				<td> 
					<div id="transferPath" style="display:none">
					<label class="display-for-form" style="font-size:14px">Select Path ID</label>
					</div>
				</td> 
			</tr>
			 
			<tr>     
				<td colspan="2">     
					<div id="transferPathList" style="display:none">  
					<kendo:dropDownList name="nextWorkflowPathId" style="width:400px"  dataTextField="text" dataValueField="value" >
						<kendo:dataSource data="${nextPath}"></kendo:dataSource>
					</kendo:dropDownList><div id="errorTransferPath" style="display:none;"><label class="error">Please Choose Path ID</label></div>
					</div>
				</td>
			</tr> 
		</table>
		
	<div id="gridWorkflowPath"></div> 
		
	<div class="display-for-form">Transfer Notes</div>
	<div><textarea id="transferNotes" name="transferNotes" style="width:400px;height:250px;"></textarea></div>
	<div id="errorTransferNotes" style="display:none;"><label class="error">Please Input Transfer Notes</label></div>
		   
	<br></br>
		 
	<img id="waitingLink" src="/MOA2/resources/css/images/loading.gif" height="30px" title="Loading" style="border:1px; display:none;"/>
	<input type="button" value="Process Transfer" id="btnTransfer" onClick="transfer()"/>
	<input type="button" id="btnCancel" name="btnCancel" value="Cancel" onClick="cancel()">
	
	<!-- ada atau tidak hasil query nya -->
	<input type="hidden" id="dataPathID" name="dataPathID" value="${validateDataPath}">
	
	<input type="hidden" id="currentWorkflowCode" name="currentWorkflowCode" value="${workflowCode}">
	<input type="hidden" id="currentWorkflowPathId" name="currentWorkflowPathId">
</form> 

<script>
$("#transferOption").change(function(){
	$("#errorTransferOption").css("display","none");
	 
	if($("#transferOption").val()=="Transfer Group"){
		$("#transferGroup").css("display","block");
		$("#transferGroupList").css("display","block");
		$("#currentPath").css("display","none");
		$("#transferPath").css("display","none");
		$("#transferPathList").css("display","none");
	}else{  
		if($("#dataPathID").val()=="noData"){
			$("#transferGroup").css("display","none");
			$("#transferGroupList").css("display","none");
			$("#currentPath").css("display","none");
			$("#transferPath").css("display","none");
			$("#transferPathList").css("display","none");
			alert("There's no Path Setting for this ${currentPath}");
			
			$("#transferOption").data("kendoDropDownList").select(0);
		}else{
			$("#transferGroup").css("display","none");
			$("#transferGroupList").css("display","none");
			$("#currentPath").css("display","block");
			$("#transferPath").css("display","block");
			$("#transferPathList").css("display","block"); 
		}
	}	
});

$("#transferNotes").change(function(){
	$("#errorTransferNotes").css("display","none");
}); 

$("#transferTo").change(function(){
	$("#errorTransferGroup").css("display","none");
}); 

$("#nextWorkflowPathId").change(function(){
	if($("#nextWorkflowPathId").val()!=""){
		$("#errorTransferPath").css("display","none");
		$("#gridWorkflowPath").load("/MOA2/HelpDesk/AjaxTransferPath/?workflowPathId="+$("#nextWorkflowPathId").val()+"&workflowCode="+$("#currentWorkflowCode").val());
	}else{
		$("#errorTransferPath").css("display","block");
	}
}); 

function addComment()
{
	$.ajax( 
	{ 
		type:"POST",
		url:"/MOA2/HelpDesk/addComment",
		data:$("#commentForm").serialize(),
		success:function(){
			$("#loadDetail").load("/MOA2/HelpDesk/AjaxDetail/?docNo="+$("#docNo").val());
			$("#commentType").data("kendoComboBox").value("");
			$("#textAreaComment").val("");
		}
	}		
	);	
}

$("#loadDetail1").load("/MOA2/HelpDesk/AjaxDetail/?docNo="+$("#docNo").val());

function transfer(){
	if($("#transferOption").val()=="" ||$("#transferNotes").val()==""){
		
		if($("#transferOption").val()==""){
			$("#errorTransferOption").css("display","block");
		}
		if($("#transferNotes").val()==""){
			$("#errorTransferNotes").css("display","block");
		} 
	}else{
		if($("#transferOption").val()=="Transfer Group"){
			if($("#transferTo").val()==""){
				$("#errorTransferGroup").css("display","block");
			}else{ 
				$("#waitingLink").css("display","block");
		 		try { $("#btnCancel").css("display","none"); } catch(e) { }
		 		try { $("#btnTransfer").css("display","none"); } catch(e) { }
		 		$("#frmAssign").submit();
			}
		} 
		else{
			if($("#nextWorkflowPathId").val()==""){
				$("#errorTransferPath").css("display","block");
			}else{
				$("#waitingLink").css("display","block");
		 		try { $("#btnCancel").css("display","none"); } catch(e) { }
		 		try { $("#btnTransfer").css("display","none"); } catch(e) { }
		 		$("#frmAssign").submit();
			}
	 	}
	}
}

function cancel()
{
	window.location="/MOA2/HelpDesk/Detail/?docNo="+$("#docNo").val();
	 
	$("#waitingLink").css("display","block");
	try { $("#btnCancel").css("display","none"); } catch(e) { }
	try { $("#btnTransfer").css("display","none"); } catch(e) { }

}
</script>
