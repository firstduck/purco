<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.label2{
	font-weight: bold;
	color: #CB0000;
	padding:4px;
	width:30%;
}

.centerDiv{
	margin-left: auto;
	margin-right:auto;
	width:100%;
}

.button{
	width:50px;
	height:50px;
}
</style>

<div id="divDetail" style="width:99%;height:74%;overflow:auto" ></div>

<fieldset>
	<table style="width:80%">
		<tr>
			<td colspan="2">Reason<br/>
				<textarea id="areaReason" style="width:98%"></textarea>
			</td>
		</tr>
		<tr>
			<td align="center" colspan=2>
				
				<button id="btnShow" onClick="show()" class="primary">Show More</button>
				<button id="btnHold" onClick="action('Hold','${docNo}','${web}')" class="info">Hold</button>
				
				<button id="btnApprove" onClick="action('Approve','${docNo}','${web}')" class="success">Approve</button>
				<button id="btnReject" onClick="action('Reject','${docNo}','${web}')" class="danger">Reject</button>
				
				<button id="btnReturn" onClick="action('Return','${docNo}','${web}')" class="warning">Return</button>
				<button id="btnClose" class="info">Close</button>
			</td> 
		</tr>
	</table>
</fieldset>

	
<div style="display:none">
	<input type="text" id="txtDoc" value="${docNo}"/>
	<input type="text" id="txtWeb" value="${web}"/>
	<input type="text" id="txtWebServer" value="${webServer}"/>
	<input type="text" id="txtAct"/>
</div>
	 
<script>
var doc = "${docNo}";
var web ="${web}";
// alert(web);
var moduleCode = "${moduleCode}";
var WFcode = "${wfCode}";
var pathID = "${pathID}";
var lineID = "${lineID}";
var server = "${server}";

$("#divDetail").html(""); //tambahan Hardy
urlString = "/MOA2/Home/JobListDetail/?moduleCode="+moduleCode+"&docNo="+doc+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID+"&flag=no&server="+server;
$("#divDetail").load(urlString);

$(document).ready(function(){
	$("#btnClose").click(function (){
		windowsss.close();
	});
	
	if(moduleCode == 'TVE')
	{
		document.getElementById("btnShow").setAttribute("disabled","true");
		document.getElementById("btnShow").style.color = "gray";
	}
});

function show(){
	var x = $("#btnShow").html();
	var urlString = "";
	
	if(x == 'Show More'){
		
	
		if(web.substring(0,1) == "/") urlString = web +"/Detail/?joblist=1&docNo="+doc;
		else urlString = "Http://"+web+"/Detail/?joblist=1&docNo="+doc;
		console.log("show more link:"+urlString);
		$("#btnShow").html('Show Less');
	}else{
		urlString = "/MOA2/Home/JobListDetail/?moduleCode="+moduleCode+"&docNo="+doc+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID+"&flag=no"; 
		$("#btnShow").html('Show More');
	}
	
	$("#divDetail").html("");
	kendo.ui.progress($("#divDetail"), true);
	$("#divDetail").load(urlString);
}

function action(act,docNo,web){ 
	if(act == 'Review' || act == 'Detail')	window.location = web+"/"+act+"/?docNo="+docNo;
	else{
		var x = $("#areaReason").val();

		if((act == 'Reject' || act == 'Return') && x == ''){
			alert("Please fill reason");
		}else{ 
			if(act != 'Close')$("#txtAct").val(act);
			windowsss.close();
		} 
	}
}
	 
</script>
