<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
function deleteUpdate(application,contentID,version,updateAppID)
{
	$.ajax({
		type: "GET",
        url: "/MOA2/deleteUpdate/?application="+application+"&contentID="+contentID+"&version="+version+"&updateAppID="+updateAppID,
        success: function(data)
        {
        	if($("#contentID").val() == "")
				$("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val());
			else $("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val() + "&contentID="+$("#contentID").val());
        }
	});
}

function editUpdate(application,contentID,version,updateAppID)
{
	$.ajax({
		type: "GET",
        url: "/MOA2/deleteUpdate/?application="+application+"&contentID="+contentID+"&version="+version+"&updateAppID="+updateAppID,
        success: function(data)
        {
        	if($("#contentID").val() == "")
				$("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val());
			else $("#attachment").load("/MOA2/getAllUpdate/?application="+$("#application").val() + "&contentID="+$("#contentID").val());
        }
	});
}
</script>

<fieldset>
<legend><label class="display-for-form">Existing update</label></legend>
<div id="existingUpdate" style="overflow-y: auto; height:270px;">
<c:if test = "${fn:length(attachment)>0}">

<c:forEach var="attach" items="${attachment}">
<a href="/MOA2/getUpdate/?application=${attach.pk.app}&contentID=${attach.pk.contentID}&version=${attach.pk.version}&updateAppID=${attach.pk.updateAppID}" target="_blank" title="${attach.pk.app}_${attach.pk.contentID}">
<b><c:out value="${attach.fileName}"/></b></a>&nbsp;<a onClick="deleteUpdate('${attach.pk.app}','${attach.pk.contentID}','${attach.pk.version}','${attach.pk.updateAppID}')"><img src="/MOA2/resources/css/images/delete-icon.png" height="15px" title="Delete" style="cursor:pointer;"></img></a>&nbsp;- Version: <b>${attach.pk.version}</b> ID: <b>${attach.pk.updateAppID}</b>
<br/><br/>
</c:forEach>

</c:if>
<c:if test = "${fn:length(attachment) == 0}">
---
</c:if>
</div></fieldset>
<script>
document.getElementById("existingUpdate").scrollTop = document.getElementById("existingUpdate").scrollHeight;
</script>