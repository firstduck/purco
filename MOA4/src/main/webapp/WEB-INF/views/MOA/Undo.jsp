<h2>${title} Confirmation</h2>

<form method="post" action="${action}" name="formUndo" id="formUndo">
<table>
	<tr>
		<td>Reason</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="docNo" value="${docNo}" />
			<textarea name="reason" id="reason" rows="10" cols="50"></textarea>
		</td> 
	</tr>
	<tr>
		<td><input type="button" value="Submit" id="Submit"/></td>
	</tr>
</table>
</form>

<script>
$(document).ready(function(){ 
	
	$("#Submit").click(function(){
			$("#formUndo").submit();
	});
});

</script>