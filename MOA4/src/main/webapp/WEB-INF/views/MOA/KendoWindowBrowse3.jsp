<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="browse-wrapper">
<c:url value="${readUrl}" var="readUrl"/>

<div id="selectedCode" style="display:none"></div>
<div id="selectedValue1" style="display:none"></div>
<div id="selectedValue2" style="display:none"></div>

<div>
	<fieldset>
		<legend>Look Up</legend>
			<div class="search">Search <input type="text" name="search" id="search" onChange="searcChange()"/></div>
		</fieldset>
</div>
	<fieldset>
		<kendo:grid name="grid" groupable="false" sortable="true" filterable="true" selectable="true" height="280px" scrollable="true" pageable="false" >
	        <kendo:grid-columns> 
	            <kendo:grid-column title="${title1}" field="id" width="${width1}px"/>
	            <kendo:grid-column title="${title2}" field="value1" width="${width2}px"/>
	            <kendo:grid-column title="${title3}" field="value2" width="${width3}px"/>
	        </kendo:grid-columns>  

	        <kendo:dataSource>
	        	<kendo:dataSource-transport>
	            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
				</kendo:dataSource-transport>
	            <kendo:dataSource-schema>
	               <kendo:dataSource-schema-model id="id">
	                   <kendo:dataSource-schema-model-fields>
	                       <kendo:dataSource-schema-model-field name="id" type="string" />
	                       <kendo:dataSource-schema-model-field name="value1" type="string" />
	                       <kendo:dataSource-schema-model-field name="value2" type="string" />
	                   </kendo:dataSource-schema-model-fields>
	               </kendo:dataSource-schema-model>
	           </kendo:dataSource-schema>
	       </kendo:dataSource>
		</kendo:grid>
</fieldset>

<input type="button" value="Select" onClick="getRow()"/>
<input type="hidden" value="${url}" id="url" width="1000px"/>
<input type="hidden" value="${search}" id="search_hidden"/>
</div> 

<script type="text/javascript">
	function searcChange() {
// 		alert($('#url').val()+search);
		var search = $("#search").val().split(' ').join('%20');
		$('#browse-wrapper').load($('#url').val()+search);
	}
	
	function getRow(){
		var entityGrid = $("#grid").data("kendoGrid");
		var selectedItem = entityGrid.dataItem(entityGrid.select());
		
		$("#selectedCode").html(selectedItem.id);
		$("#selectedValue1").html(selectedItem.value1);
		$("#selectedValue2").html(selectedItem.value2);
	
		$("#window").data("kendoWindow").close();
	}
	
// 	$(document).keypress(function(e) {
// 	    if(e.which == 13 || event.keyCode == 13) {
// 	        getRow();
// 	    }
// 	});
	
	$("#grid").dblclick(function() {
			getRow();
		}
	);
	
	$(document).ready(function(){
		$("#search").focus();
		$("#search").val(document.getElementById('search_hidden').value);
	});
</script>