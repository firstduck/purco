<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.k-window-action
{
    visibility: hidden ;
}

.k-grid tr { 
	font-size:11px;
}
.approve
{
	color:green;
}
.return
{
	color:#EEC900;
}
.reject
{
	color:red;
}
</style>

<script>
	$(document).ready(function(){
		
	});

	function getLabel(docno, act, web){
// 		console.log( "/"+web.split("/")[1]);
		if(act == 'Assign'){
            var Web = "/"+web.split("/")[1];
//             var host = Web[0];
//             var webname = Web[1];
// console.log(Web);
            return "<a href='/MOA2/Assign/?docNo="+docno+"' target='_blank'>Assign</a>";

        }    
		if(act == 'Review' || act == 'ReviewApprove' || act == 'Assign'){ 
			return "<a href='"+web+"/Review/?docNo="+docno+"' target='_blank'>Review</a>";
		} 
		else if(act == 'Print'){ 
			return "<a href='"+web+"/Print/?docNo="+docno+"' target='_blank'>Print</a>";
		}else if(act == 'ap' || act == 'Approve'){
			return "Approve";
		}else if(act == 'ho' || act == 'Hold'){
			return "Hold";
		}else if(act == 're'){
			return "Reject";
		}
		return act;
	}
	
	
</script>

<c:url value="${readUrl}" var="readUrl" />

<div id="temp">
<form method="POST" id="formJoblist" action="/MOA2/Home/NewJobListProcess">
<div>
	System
	<kendo:dropDownList name="system" dataTextField="name" dataValueField="id" style="width:300px" value="${s }">
		<kendo:dataSource data="${system}"></kendo:dataSource>
	</kendo:dropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<br/>
<div>
	Module
	<kendo:dropDownList name="moduleDDL" dataTextField="text" dataValueField="value" style="width:300px" value="${value}">
		<kendo:dataSource data="${modules}"></kendo:dataSource>
	</kendo:dropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<br/>

<div style="align:center;">
<kendo:grid name="gridNewJobList"  pageable="true" selectable="true" change="onChange" dataBound="databound" >

        <kendo:grid-columns>   
        	<kendo:grid-column title="Doc No" width="200px" field="docNo" /> 
            <kendo:grid-column title="Module" field="moduleName"/>
            <kendo:grid-column title="ModuleCode" field="moduleCode"/>
            <kendo:grid-column title="Last Approved By" field="lastApproveName" width="150px"/> 
            <kendo:grid-column title="Last Approve Date" field="lastDate" width="120px"/>
            <kendo:grid-column title="Created By" field="creatorName" width="200px" />  
            <kendo:grid-column title="Created Date" field="createdDate" width="100px" />
            <kendo:grid-column title="Action" field="action" template="<label name='actLabel'>#=getLabel(docNo,action,webName)#</label>"/>
<%--         	    <kendo:grid-column title="test" field="action"/> --%>
        </kendo:grid-columns>
        
        <kendo:dataSource pageSize="15">
        	<kendo:dataSource-transport>
            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
			</kendo:dataSource-transport>      
       </kendo:dataSource>
	</kendo:grid>
<br/>
</div>

<textarea style="display:none" id="joblistdata" name="joblistdata"></textarea>
<table style="width:100%">
<tr>
	<td><input type="button" id="btnSubmit" value="Submit"/></td>
	<td style="padding-left:200px">
		<label class="approve"><b>GREEN</b> : Approve </label>| 
		<label class="return"><b>YELLOW</b> : Return </label>| 
		<label class="reject"><b>RED</b> : Reject</label>
	</td>
</tr>
</table>
	
</form> 

<c:forEach var="item" items="${errorList}">
		<br/>
		<div class="error"><c:out value="${item}"/></div>
</c:forEach> 
<br/>

<br/> 
<a href="/MOA2/Home/JobList">Old Job List</a> |
<a href="/MOA2/JobListDocument">Job List Document</a>

</div>

<div id="newJoblistWindow" ></div>


<script type="text/Javascript">
kendo.ui.progress($("#gridNewJobList"), true);

var windowsss;
var grid ;
var needReloadModule = true;
var lsJoblist;

function databound(){
	
	var grid = $("#gridNewJobList").data("kendoGrid");
	grid.hideColumn("moduleCode");
	
	kendo.ui.progress($("#gridNewJobList"), false);
	
	if(needReloadModule){
		var temp = $("#gridNewJobList").data("kendoGrid").dataSource.data();
	
		var arr = new Array();
		var item = {text: "ALL", value: "ALL"};
		arr[0] = item;
		for(var i=0; i< temp.length; i++){
			var check = true;
			
			item = {text: temp[i].moduleName, value: temp[i].moduleCode};
			for(var j=0; j< arr.length; j++){
				if(arr[j].text == item.text){
					check = false;
					break;
				}
			}
			if(check)
				arr[arr.length] = item;
		}
		
	 	$("#moduleDDL").kendoDropDownList({
	 	  dataSource: arr,
	 	  dataTextField: "text",
	 	  dataValueField: "value"
	 	});
	 	needReloadModule = false;
	}
}

$(document).ready(function(){
	
	var x = $("#moduleDDL").val();
	var y = $("#system").val();
	
	
	if(x != 'ALL'){
		grid = $("#gridNewJobList").data("kendoGrid");
		grid.hideColumn("moduleName");
	}
	
	$("#moduleDDL").change(function(){
		var x = $("#moduleDDL").val();
		var y = $("#system").val();
		
		kendo.ui.progress($("#gridNewJobList"), true);
		
		if(lsJoblist == undefined){
			lsJoblist = $("#gridNewJobList").data("kendoGrid").dataSource.data();
			console.log("tes undefined "+lsJoblist);
		}
		
		var temp = new Array();
		for(var i=0; i< lsJoblist.length; i++){
			if(lsJoblist[i].moduleCode == x || x == 'ALL'){
				temp[temp.length] = lsJoblist[i];
			}
		}
		
        $("#gridNewJobList").data("kendoGrid").dataSource.data(temp);
		
	});
	
	$("#btnSubmit").click(function(){
		var temp = lsJoblist;
		console.log("test "+temp);
		
		if(temp === undefined){
			temp = $("#gridNewJobList").data("kendoGrid").dataSource.data();
			
		}
		
		var arr = new Array();
		
		for(var i=0; i< temp.length; i++){
			try{ 
				if(temp[i].action == "ap" || temp[i].action == "re" || temp[i].action == "Return" ){
					var item = {docNo: temp[i].docNo, action: temp[i].action, reason : temp[i].reason, moduleCode : temp[i].moduleCode, activity : temp[i].activity, workflowCode : temp[i].workflowCode};
					arr[arr.length] = item;
				}
			}
			catch(e){}
		}
		$("#joblistdata").val(JSON.stringify(arr));
		$("#formJoblist").submit(); 
	});
	
	grid = $("#gridNewJobList").data("kendoGrid");
	grid.dataSource.bind("change", function (e) {
		grid.tbody.find('tr').each(function () {
		    var dataItem = grid.dataItem(this);

		    if (dataItem.action == 'ap') { 
		        $(this).css('background-color', '#BDFCC9'); 
		    }else if (dataItem.action == 're') { 
		        $(this).css('background-color', '#FFB6C1'); 
		    }else if (dataItem.action == 'Return') { 
		        $(this).css('background-color', '#FFF68F'); 
		    }
		});
	});
});

var count = 0;
function onChange(arg){ 

	console.log('onChange()');
	
	grid = $("#gridNewJobList").data("kendoGrid");

	$(grid.tbody).on("click", "td", function (e) {
	    var rowa = $(this).closest("tr");
	    var colIdx = $("td", rowa).index(this);
	
	 	var selectedItem = grid.dataItem(grid.select());
		var docNo = selectedItem.docNo; 
		var act = selectedItem.activity;
		var web = selectedItem.webName;
		var moduleCode = selectedItem.moduleCode; 
		var WFcode = selectedItem.workflowCode; 
		var pathID = selectedItem.pathID; 
		var lineID = selectedItem.linesID; 
		var row = 0, act2, shortAct = "";
			
   		if(act != "Review" && act != "ReviewApprove" && act != "Print" && act != "Assign"){
   			grid.select().each(function () {
       			row = $(this).closest("tr").index();
       		});
        		
			$("#newJoblistWindow").kendoWindow({
			   width: "1000px",
			   height: "70%",
	           title: "Detail",
	           modal: true,
	           actions: ["Close"],
	           open: function(e) { $("html, body").css("overflow", "hidden"); },
			           
	           close : function(){
	        		$("html, body").css("overflow", "visible");
					act2 = $("#txtAct").val();
	
		      		if(act2 != ''){
						if(act2 == 'Approve')shortAct = 'ap';
						else if(act2 == 'Return')shortAct = 'Return';
						else if(act2 == 'Reject')shortAct = 're';
	
		      			var idx = ((grid.dataSource.page()-1)*15) +row;
	
		      		 	grid.dataSource.data()[idx].set("action", shortAct);
		      			grid.dataSource.data()[idx].set("reason", $("#areaReason").val());
	      			}
	          	}
	       	});
  			$('#newJoblistWindow').load("/MOA2/Home/DetailNewJobList/?docNo="+docNo+"&act="+act+"&web="+web+"&moduleCode="+moduleCode+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID+"&server="+$("#system").val()); 
    			windowsss = $("#newJoblistWindow").data("kendoWindow");
    			windowsss.center().open(); 
   		}
	});
}

$("#system").change(function(){
	if($("#system").val()!="")
	{
		var y = $("#system").val();
		window.location = "/MOA2/Home/NewJobList/?m=ALL&s="+y;
	}
});
</script>