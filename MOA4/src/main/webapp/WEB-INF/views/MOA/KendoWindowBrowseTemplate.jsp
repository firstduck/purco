<%@page import="org.springframework.ui.Model"%>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="browse-wrapper">
<c:url value="${readUrl}" var="readUrl"/>

<% Integer columnCount = Integer.valueOf(request.getParameter("column")); %>

<input type="hidden" id="columnCount" value="<%=columnCount %>"/>
<c:forEach var="i" begin="1" end="<%=columnCount %>">
<div id="selectedValue${i}" style="display:none"></div>
</c:forEach>

<div>
	<fieldset>
		<legend>Look Up</legend>
			<div class="search">Search <input type="text" name="search" id="search" onChange="changeCuy()"/></div>
		</fieldset>
</div>
	<fieldset>
		<kendo:grid name="grid${windowID}" groupable="false" sortable="true" filterable="true" selectable="true" height="280px" scrollable="true" pageable="false">
	        <kendo:grid-columns> 
	        	<% for(int i=0;i<columnCount;i++) { %>
	        	<c:set var="i" value="<%=String.valueOf(i) %>"/>
	            	<kendo:grid-column title="${titleArr[i]}" field='<%="value"+(i+1) %>' width="${widthArr[i]}"/>
	            <% } %>
	        </kendo:grid-columns>  

	        <kendo:dataSource>
	        	<kendo:dataSource-transport>
	            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
				</kendo:dataSource-transport>
<%-- 	            <kendo:dataSource-schema> --%>
<%-- 	               <kendo:dataSource-schema-model id="id"> --%>
<%-- 	                   <kendo:dataSource-schema-model-fields> --%>
<%-- 	                       <kendo:dataSource-schema-model-field name="id" type="string" /> --%>
<%-- 	                       <kendo:dataSource-schema-model-field name="value1" type="string" /> --%>
<%-- 	                       <kendo:dataSource-schema-model-field name="value2" type="string" /> --%>
<%-- 	                   </kendo:dataSource-schema-model-fields> --%>
<%-- 	               </kendo:dataSource-schema-model> --%>
<%-- 	           </kendo:dataSource-schema> --%>
	       </kendo:dataSource>
		</kendo:grid>
</fieldset>

<input type="button" value="Select" onClick="getRow()"/>
<input type="hidden" value="${url}" id="url" />
<input type="hidden" value="${search}" id="search_hidden"/>
<input type="hidden" value="${windowID}" id="windowID"/>
</div> 

<script type="text/javascript">

	function changeCuy() {
		var search = $("#search").val().split(' ').join('%20');
		try { search = search.replace("+", "%2B"); } 
		catch(e) { }
		$('#browse-wrapper').load($('#url').val()+ search);
	}
	
	function getRow(){

		var entityGrid = $("#grid"+$("#windowID").val()).data("kendoGrid");
		var selectedItem = entityGrid.dataItem(entityGrid.select());
			
		var valueArr = $.map(selectedItem, function(value, index) {
			return value;
		});

		var columnCount = parseInt($("#columnCount").val());
		for(var i=1;i<=columnCount;i++)
			$("#selectedValue"+i).html(valueArr[i+1]);
		$("#"+$("#windowID").val()).data("kendoWindow").close();
	}
	
// 	$(document).keypress(function(e) {
// 	    if(e.which == 13 || event.keyCode == 13) {
// 	        getRow();
// 	    }
// 	});
	
	$("#grid"+$("#windowID").val()).dblclick(function() {
			getRow();
		}
	);
	
	$(document).ready(function(){
		$("#search").focus();
		$("#search").val(document.getElementById('search_hidden').value);
	});
</script>