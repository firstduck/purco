<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<h2>${title}</h2>
<style>
th {
padding:10px;
}
table.status td{
padding: 5px;}
.current
{
color:blue;  
}
</style>

<a href="${link}">Back</a>
<br/>
<h4 style="color:black"> Doc No : ${docStatus[0].docNo} </h4>

<form method="POST" action="${urlAction}"  method="POST">
<c:set var="i" value="0"/>
<c:set var="tempDistinct" value=""/>
<input type="hidden" name="hidDocNo" value="${docStatus[0].docNo}"/>

<table border=1 style="border-collapse:collapse;" width="100%" id="tableStatus" class="status">
	<tr style="background-color:#EEEEEE ;">
		
		<th>Select</th>
		<th>Workflow Name</th>
		<th>Line</th>
		<th>Group Workflow Description</th>
		<th>PIC</th>

	</tr> 
<c:forEach var="status" items="${docStatus}">
	<c:if test = "${not empty status.closeDate}">
		<c:set var="temp" value="${status.workflowCode}.${status.workflowID}.${status.workflowLinesId}.${status.idPararelLine}"/>
		<c:if test = "${tempDistinct != temp}">
			<tr>
				<td><input type="radio" id="returnDoc" name="returnDoc" value="${i}"/>
					<input type="hidden" name="hidWFCode_${i}" value="${status.workflowCode}"/>  
					<input type="hidden" name="hidID_${i}" value="${status.workflowID}"/>
					<input type="hidden" name="hidLine_${i}" value="${status.workflowLinesId}"/>
					<input type="hidden" name="hidPara_${i}" value="${status.idPararelLine}"/>
				</td>
				<td><div class='current'>${status.workflowName}</div></td>
				<td><div class='current'>${status.workflowID}.${status.workflowLinesId}</div></td>
				<td><div class='current'>${status.groupWorkflowDesc}</div></td>
				<td><div class='current'>${status.closeBy}</div></td>
			</tr>
			<c:set var="tempDistinct" value="${temp}"/>
		</c:if>
	</c:if>
<c:set var="i" value="${i+1}"/>
</c:forEach>
</table>

<br/>
<label class="display-for-form">Reason</label>
<br/>
<textarea name="reason" style="width:100%" rows="5">
</textarea><br/><br/>
<input type="Submit" value="Save"/>
</form>
<br/> 
<a href="${link}">Back</a>

<script>
$(document).ready(function(){
	var select = document.getElementsByName("returnDoc");	
	select[0].checked=true;
});
</script>
