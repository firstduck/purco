<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<textarea cols="50" rows="10" id="txtReason">${message}</textarea>
<br/><br/>
<input type="button" value="Save Reason" id="btnReason"/>
<c:if test="${action == 'Approve'}">
	<input type="button" value="Cancel" id="btnCancel"/>
</c:if>

<div id="selectedReason" style="display:none"></div>

<script type="text/Javascript">
$("#btnCancel").click(function(){
	$("#window").data("kendoWindow").close();
});

$("#btnReason").click(function(){
	if($("#txtReason").val() == ""){
		alert("Please fill reason");
		$("#txtReason").focus();
	}
	else{
		$("#selectedReason").html($("#txtReason").val());
		$("#window").data("kendoWindow").close();
	}
});

// $(document).keypress(function(e) {
//     if(e.which == 13 || event.keyCode == 13) {
//     	saveReason();
//     }
// });

$(document).ready(function()
{
	$("#txtReason").focus();
});
</script>