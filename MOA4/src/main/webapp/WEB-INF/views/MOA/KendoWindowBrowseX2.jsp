<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="browse-wrapper">
	<c:url value="${readUrl}" var="readUrl" />

	<div id="selectedCode" style="display: none"></div>
	<div id="selectedName" style="display: none"></div>

	<div>
		<fieldset>
			<legend>Look Up</legend>
			<div class="search">
				Search <input type="text" name="search" id="search" />
			</div>
		</fieldset>
	</div>
	<fieldset>

			<div id="grid"></div><script>$(
					function() {
						$("#grid")
								.kendoGrid(
										{
											"dataSource" : {
												"schema" : {
													"model" : {
														"id" : "id",
														"fields" : {
															"id" : {
																"type" : "string"
															},
															"value" : {
																"type" : "string"
															}
														}
													}
												},
												"transport" : {
													"read" : {
														"dataType" : "json",
														"contentType" : "application/json",
														"type" : "GET",
														"url" : "${readUrl}"
													}
												},
												"pageSize" : 25.0
											},
											"height" : "280px",
											"selectable" : "true",
											"filterable" : true,
											"pageable" : true,
											"sortable" : true,
											"columns" : [ {
												"field" : "id",
												"title" : "Code",
												"width" : "110px"
											}, {
												"field" : "value",
												"title" : "Description",
												"width" : "250px"
											} ],
											"scrollable" : true,
											"groupable" : false
										});
					})
		</script>
	</fieldset>

	<input type="button" value="Select" onClick="getRow()" id="btnsearch" />
	<input type="hidden" value="${url}" id="url" /> <input type="hidden"
		value="${search}" id="search_hidden" />
		<input type="hidden"
		value="${parammodel}" id="param_hidden" />
		<input type="hidden"
		value="${parammodel2}" id="param_hidden2" />
</div>

<script type="text/javascript">
	$('#btnsearch').button();

	$('#search').change(function() {
		var search = $("#search").val().split(' ').join('%20');
		var param = $("#param_hidden").val().split(' ').join('%20');
		var param2 = $("#param_hidden2").val().split(' ').join('%20');
// 		alert("PARAM1" + param);
// 		alert("PARAM2" + param2);
		
		$('#browse-wrapper').load($('#url').val() + search + "&param=" + param + "&param2=" + param2 );
		
	});

	function getRow() {
		var entityGrid = $("#grid").data("kendoGrid");
		var selectedItem = entityGrid.dataItem(entityGrid.select());

		$("#selectedCode").html(selectedItem.id);
		$("#selectedName").html(selectedItem.value);

		$("#window").data("kendoWindow").close();
	}

	// 	$(document).keypress(function(e) {
	// 	    if(e.which == 13 || event.keyCode == 13) {
	// 	        getRow();
	// 	    }
	// 	});

	$("#grid").dblclick(function() {
		getRow();
	});

	$(document).ready(function() {
		$("#search").focus();
		$("#search").val(document.getElementById('search_hidden').value);
	});
</script>