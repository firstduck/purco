<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form name="attForm" id="attForm" method="POST" action="${action}">
<fieldset> 
	<legend style="font-size:14px;font-weight:bold">Insert Attachement</legend>
	<table>
	<tr>
	<td class="display-for-form">Document Number</td>
	<td><input type="text" id="docNo" name="docNo" /></td>
	</tr>
	<tr>
	<td></td>
	<td><div id="uploadattach"></div></td>
	</tr>
	<tr>
	<td></td>
	<td><input type="button" name="uploadFile" id="uploadFile" value="Upload"/>
	</tr>
	</table>
</fieldset>
</form>

<script type="text/javascript">
$("#uploadattach").load("/MOA2/upload");

$("#uploadFile").click(function(){
	$("#attForm").submit();
});

</script>