<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="browse-wrapper">
<c:url value="${readUrl}" var="readUrl"/>

<div id="selectedCode" style="display:none"></div>
<div id="selectedName" style="display:none"></div>

	<div class="search">Search <input type="text" name="search" id="search"/></div>

	<fieldset>
		<kendo:grid name="grid" groupable="false" sortable="true" filterable="true" selectable="true" height="280px" scrollable="true" pageable="false" >
	        <kendo:grid-columns> 
	            <kendo:grid-column title="Code" field="id" width="110px"/>
	            <kendo:grid-column title="Description" field="value" width="250px"/>
	        </kendo:grid-columns>  

	        <kendo:dataSource>
	        	<kendo:dataSource-transport>
	            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
				</kendo:dataSource-transport>
	            <kendo:dataSource-schema>
	               <kendo:dataSource-schema-model id="id">
	                   <kendo:dataSource-schema-model-fields>
	                       <kendo:dataSource-schema-model-field name="id" type="string" />
	                       <kendo:dataSource-schema-model-field name="value" type="string" />
	                   </kendo:dataSource-schema-model-fields>
	               </kendo:dataSource-schema-model>
	           </kendo:dataSource-schema>
	       </kendo:dataSource>
		</kendo:grid>
</fieldset>

<input type="button" value="Select" onClick="getRow()"/>
<input type="hidden" value="${url}" id="url" />
<input type="hidden" value="${search}" id="search_hidden"/>
</div> 

<script type="text/javascript">
	$('#search').change(function() {
		var search = $("#search").val().split(' ').join('%20');
		$('#browse-wrapper').load($('#url').val()+search);
	});
	
	function getRow(){
		var entityGrid = $("#grid").data("kendoGrid");
		var selectedItem = entityGrid.dataItem(entityGrid.select());
		
		$("#selectedCode").html(selectedItem.id);
		$("#selectedName").html(selectedItem.value);

		$("#window").data("kendoWindow").close();
	}
	
// 	$(document).keypress(function(e) {
// 	    if(e.which == 13 || event.keyCode == 13) {
// 	        getRow();
// 	    }
// 	});
	
	$("#grid").dblclick(function() {
			getRow();
		}
	);
	
	$(document).ready(function(){
		$("#search").focus();
		$("#search").val(document.getElementById('search_hidden').value);
	});
</script>