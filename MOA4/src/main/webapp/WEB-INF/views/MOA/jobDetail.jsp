<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<fieldset>
	<c:set var="i" value="1"/>
	<table width="100%"> 
		<c:forEach var="item" items="${title}">
				<c:if test="${i%2 == 1}">
					<tr> 
				</c:if>
				<td width="150px" valign="top"><label class="display-label">${item.fieldAlias}</label></td>
				<td width="300px" valign="top"><label class="display-field">${item.fieldToShow}&nbsp;</label></td>
				<c:if test="${i%2 == 0}">
					</tr>
				</c:if>
				<c:set var="i" value="${i+1}"/>
		</c:forEach>
	</table>
	
	<c:if test="${button== 'yes'}">
		<center><input type="button" value="Close" onClick="closeWindow()"/></center>
	</c:if>
</fieldset>  

<c:if test="${showDetailMore == 1}">
<table border ="1" width="100%" style="border: 1px solid #cccccc; border-spacing: 0px; border-collapse: collapse;"> 
	<tr>
	<c:forEach var="col" items="${col}">
	<td align="center" style="padding: 10px;"><b>${col}</b></td>
	</c:forEach>
	</tr>
	
	<c:set var="i" value="1" />
	<c:set var="jumlah" value="${n}" />
	
	<tr>
	<c:forEach var="row" items="${row}">
	<td style="padding: 2px 5px;" align="center">${row}</td>
	
	<c:if test="${i==jumlah}">
	<tr><c:set var="i" value="0" /></tr>
	</c:if>
	
	<c:set var="i" value="${i+1}" />
	
	</c:forEach>
	</tr>
	
</table>
</c:if>

<script>
function closeWindow(){
	$("#window1").data("kendoWindow").close();
}
</script>
