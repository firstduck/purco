<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%-- <input type ="text" id="desc" value='<%= request.getParameter("desc") %>'/> --%>
<script>
function deleteAtt(e, docNo, index, desc)
{	
	$.ajax({ 
		url : "/MOA2/removeAttachment/?docNo="+docNo + "&index=" + index,
		type : "GET",
		success : function(data) { 
// 			alert("/MOA2/getAllAttachment/?mode=edit&docNo=" + docNo + "&desc="+desc);
			if(desc != "") $("#"+e.parentNode.id).load("/MOA2/getAllAttachment/?mode=edit&docNo=" + docNo + "&desc="+desc);
			else $("#"+e.parentNode.id).load("/MOA2/getAllAttachment/?mode=edit&docNo=" + docNo);
		}
	});
}
</script>
<c:if test = "${fn:length(attachment)>0}">
	<c:forEach var="attach" items="${attachment}">
		<c:if test = "${mode!='edit' && mode!='review'}">
		<a href="/MOA2/getAttachment?docNo=${attach.docNo}&index=${attach.index}" target="_blank" title="${attach.docNo}_${attach.index}
		Name: ${attach.fileName}
		Type: ${attach.mimeType}">
		<b><c:out value="${attach.fileName}"/></b></a> <br/>
		</c:if>
		<c:if test = "${mode=='edit' || mode=='review'}">
		<a href="/MOA2/getAttachment?docNo=${attach.docNo}&index=${attach.index}" target="_blank" title="${attach.docNo}_${attach.index}
		Name: ${attach.fileName}
		Type: ${attach.mimeType}">
		<b><c:out value="${attach.fileName}"/></b></a>
		&nbsp;<img title="Remove" src="/MOA2/resources/css/images/delete-icon.png" style="cursor:pointer" onClick="deleteAtt(this,'${attach.docNo}','${attach.index}', '<%= request.getParameter("desc") %>')" width="15px"/><br/>
		</c:if>
	</c:forEach>
</c:if>
<c:if test = "${fn:length(attachment) == 0}">
---
</c:if>
