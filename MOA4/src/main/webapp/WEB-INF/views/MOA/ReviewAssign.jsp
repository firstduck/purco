<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.assign td{
	padding:2px;
}

.k-grid th.k-header,
	.k-grid-header
	{
	    background:#e61e26; 
	} 
</style>

<script>
$(document).ready(function(){
	$("#detailForHelpDesk").load("/MOA2/HelpDesk/AjaxDetail/?docNo="+$("#docNo").val());
	
	$("#btnSaveSend").click(function(){
		
		if($("#groupworkflowcode").val()=="#SELF#"){
			if($("#userNotes").val()=="" || $("#goLiveDate").val()==""){
				if($("#userNotes").val()==""){
					$("#userNotes").css("border-color","red");
					$("#validateNotes").css("display","inline"); 
				}
				if($("#goLiveDate").val()==""){ 
					$("#validateGoLiveDate").css("display","inline"); 
				}
			}	
			else{ 
				$("#validateNotes").css("display","none");
				$("#validateGoLiveDate").css("display","none"); 
				 $("#action").val("savesend");
					$("#formReviewAssign").submit(); 
					  
					$("#waitingLink").css("display","block");
					try { $("#btnCancel").css("display","none"); } catch(e) { }
					try { $("#btnSave").css("display","none"); } catch(e) { }
					try { $("#btnSaveSend").css("display","none"); } catch(e) { }
					try { $("#btnReturn").css("display","none"); } catch(e) { }
			}
		}else{					
			 var dataGrid = $("#gridPIC").data().kendoGrid.dataSource.view();
			 $("#jsonGridPIC").val(JSON.stringify(dataGrid));	
			 var taskPic = document.getElementsByName("taskPic");
			 var dueDatePic = document.getElementsByName("dueDatePic");
			 
			 var taskTester = null;
			 var dueDateTester = null;
			 if($("#TESTERNAME").val()!=""){
				 var dataGrid = $("#gridTester").data().kendoGrid.dataSource.view();
				 $("#jsonGridTester").val(JSON.stringify(dataGrid));	
				 
				 taskTester = document.getElementsByName("taskTester");
				 dueDateTester = document.getElementsByName("dueDateTester");
			 }
			 
			 var flag =0;
			 for(var i=0;i<taskPic.length;i++){
				 
				 if((taskPic[i].value==null ||taskPic[i].value=="null")||taskPic[i].value==""){
					 alert("Task PIC Index "+ i +" Can't Be Empty");
				 }
				 if((dueDatePic[i].value==null ||dueDatePic[i].value=="null")||dueDatePic[i].value==""){
					 alert("Due Date PIC Index "+ i +" Can't Be Empty");
				 }
				  
				 if($("#TESTERNAME").val()!=""){
					 for(var y=0;y<taskTester.length;y++){
						 
						 if(((taskTester[y].value==null ||taskTester[y].value=="null")||taskTester[y].value=="")
							 ||((dueDateTester[y].value==null ||dueDateTester[y].value=="null")||dueDateTester[y].value=="")){
							 
							 if((taskTester[y].value==null ||taskTester[y].value=="null")||taskTester[y].value==""){
								 alert("Task Tester Index "+ y +" Can't Be Empty");
							 }
							 if((dueDateTester[y].value==null ||dueDateTester[y].value=="null")||dueDateTester[y].value==""){
		 						 alert("Due Date Tester Index "+ y +" Can't Be Empty");
							 }
						 }				
						 else{
							flag=1;
						 } 
					 }		
				 }else{
					 flag=1;
				 }
			 }		
			 if(flag==1){
				 $("#action").val("savesend");
					$("#formReviewAssign").submit(); 
					 
					$("#waitingLink").css("display","block");
					try { $("#btnCancel").css("display","none"); } catch(e) { }
					try { $("#btnSave").css("display","none"); } catch(e) { }
					try { $("#btnSaveSend").css("display","none"); } catch(e) { }
			 }
		}
		 
	}); 
	
	$("#btnSave").click(function(){
		if($("#groupworkflowcode").val()=="#SELF#"){
			 $("#action").val("save");		 
			 $("#formReviewAssign").submit(); 
			 
			$("#waitingLink").css("display","block");
			try { $("#btnCancel").css("display","none"); } catch(e) { }
			try { $("#btnSave").css("display","none"); } catch(e) { }
			try { $("#btnSaveSend").css("display","none"); } catch(e) { }		
			 
		}else{
			 var dataGrid = $("#gridPIC").data().kendoGrid.dataSource.view();
			 $("#jsonGridPIC").val(JSON.stringify(dataGrid));	
			 
			 var dataGrid = $("#gridTester").data().kendoGrid.dataSource.view();
			 $("#jsonGridTester").val(JSON.stringify(dataGrid));	
			 
			 
			 $("#action").val("save");		 
			 $("#formReviewAssign").submit(); 
			 
			$("#waitingLink").css("display","block");
			try { $("#btnCancel").css("display","none"); } catch(e) { }
			try { $("#btnSave").css("display","none"); } catch(e) { }
			try { $("#btnSaveSend").css("display","none"); } catch(e) { }		
		}
	});
});

function cancel(){
	$("#waitingLink").css("display","block");
	try { $("#btnCancel").css("display","none"); } catch(e) { }
	try { $("#btnSave").css("display","none"); } catch(e) { }
	try { $("#btnSaveSend").css("display","none"); } catch(e) { }
	window.location.replace('/MOA2/HelpDesk/Detail/?docNo=${data.docNo}');
};

function returned(){
	$("#waitingLink").css("display","block");
	try { $("#btnCancel").css("display","none"); } catch(e) { }
	try { $("#btnSave").css("display","none"); } catch(e) { }
	try { $("#btnSaveSend").css("display","none"); } catch(e) { }
	try { $("#btnReturn").css("display","none"); } catch(e) { }
	window.location.replace('/MOA2/HelpDesk/Return/?docNo=${data.docNo}');
};
</script>
   
<div id="detailForHelpDesk"></div> 
<br>
<c:if test="${groupworkflowcode!='#SELF#' && not empty TESTERNAME}">
<h2>Review Task For PIC & Tester, Doc No ${docNo}</h2>
</c:if>

<c:if test="${groupworkflowcode!='#SELF#' && empty TESTERNAME}">
<h2>Review Task For PIC, Doc No ${docNo}</h2>
</c:if> 

<c:if test="${groupworkflowcode=='#SELF#'}">
<h2>User Review, Doc No ${docNo}</h2>
</c:if>

<form id="formReviewAssign" method="POST" action="${action}">

	<c:if test="${groupworkflowcode!='#SELF#'}">
	<table>
		<tr>		
			<td> 
				<div id="STARTDATE"> 
				<label class="display-for-form" >Start Date</label>
				<kendo:datePicker name="startDate" id="startDate" value="${helpDesk.startDate}" format="dd/MM/yyyy"></kendo:datePicker>
				</div>
			</td> 
			
			<td>     
			 	<div id="ENDDATE"> 
				<label class="display-for-form" style="padding-left:10px;">End Date</label>
				<kendo:datePicker name="endDate" id="endDate" value="${helpDesk.endDate}" format="dd/MM/yyyy"></kendo:datePicker>
				</div>
			</td>  
		</tr> 					
	</table>  
 	 <br>
 	<table>
 	<tr>
			<td>
				<div class="display-for-form">System</div> 
				<div> <kendo:comboBox name="systemCode" style="width:280px" filter="contains" placeholder="Select Application..." suggest="true" 
				      dataTextField="systemName" dataValueField="systemCode" value="${data.systemCode}"><kendo:dataSource data="${listModule}"></kendo:dataSource>
			     </kendo:comboBox></div>
			</td>
			<td>
				<div class="display-for-form" id="divLabelApp">Application</div> 
				<div id="divAppCode"> <kendo:comboBox name="appCode" style="width:200px" filter="contains" placeholder="Select Application..." suggest="true" 
				      dataTextField="text" dataValueField="value"  value="${data.applicationCode}">
			     </kendo:comboBox></div>
			</td>
			<td>
				<div class="display-for-form" id="divLabelModule">Module</div> 
				<div id="divModule"> <kendo:comboBox name="moduleCode" style="width:200px" filter="contains" placeholder="Select Module..." suggest="true" 
				      dataTextField="text" dataValueField="value"  value="${data.moduleCode }">
			     </kendo:comboBox></div>
			</td>
		</tr>	 
 	</table>
 	<br>
	<table>
		<tr> 
			<td> 
				<label class="display-for-form" style="widht:auto">Requirement Analysis</label>				
			</td>				
		</tr>
		<tr>
			<td>
				<textarea id="requirement" name="requirement" style="height:80px;width:450px">${helpDesk.requirement}</textarea>
			</td>
		</tr>
	</table> 
	</c:if> 
	
	<c:if test="${groupworkflowcode=='#SELF#'}">
	<fieldset>
		<legend style="font-size: 14px;font-weight:700;">User's Request</legend>
		<table> 
			<tr>		
				<td style="width:150px">    
					<div id="STARTDATE"> 
						<label class="display-for-form">Project Start Date</label>	 				
					</div>
				</td> 
				<td>:</td>
				<td><kendo:datePicker disabled="disabled" name="startDate" id="startDate" value="${dHelpDesk.startDate}" format="dd/MM/yyyy"></kendo:datePicker></td>
				
				<td>       
				 	<div id="ENDDATE"> 
						<label class="display-for-form" style="padding-left:10px;">Project End Date</label>					
					</div>
				</td>  
				<td>:</td>
				<td><kendo:datePicker disabled="disabled" name="endDate" id="endDate" value="${dHelpDesk.endDate}" format="dd/MM/yyyy"></kendo:datePicker></td>
			</tr> 	 					
		</table>    
	 	 <br>
		<table> 
			<tr>  
				<td> 
					<label class="display-for-form">Requirement Analysis</label>				
				</td>	
				<td>:</td>
				<td>
					<label style="widht:auto">${dHelpDesk.requirement}</label>				
				</td>			
			</tr>
		</table> 	
	</fieldset>	
	</c:if>
	
	<br>
	
	<c:if test="${groupworkflowcode=='#SELF#'}">
	<fieldset>
	<legend style="font-size: 14px;font-weight:700;">TASK DONE</legend>
		<table>
			<tr>
				<td>
					<label class="display-for-form" style="widht:auto">DONE BY</label>
				</td>
			</tr>	 
			
			<tr>  
				<td>
					<label class="display-for-form" style="widht:100px;margin-left: 15px">- PIC </label>
				</td>
				<td><label class="display-for-form" style="width:auto">: ${data.picName}</label></td>
			</tr>
			<tr>  
				<td> 
					<label class="display-for-form" style="widht:100px;margin-left: 15px">- TESTER </label>
				</td>
				<td><label class="display-for-form" style="width:auto">: ${data.testerName}</label></td>
			</tr>				
		</table> 
		
		<kendo:grid name="gridPIC" resizable="true" sortable="true" selectable="true" pageable="true" filterable="true" scrollable="true" groupable="true">				 
			    <kendo:grid-editable mode="inline"/>     
			   			   		   
		        <kendo:grid-columns>   		       	 			        	 
		        	<kendo:grid-column width="auto" title="<label style='color:white;'>Task</label>" field="task"  template="#=task == null ?'' : task #"/> 
		            <kendo:grid-column width="150px" title="<label style='color:white;'>Due Date</label>" field="dueDate" template="#=dueDate == null ?'' : kendo.toString(dueDate,'dd/MM/yyyy') #"/>
		            <kendo:grid-column width="150px" title="<label style='color:white;'>Close Date</label>" field="closeDate" template="#=closeDate == null ?'' : kendo.toString(closeDate,'dd/MM/yyyy') #"/>
		            <kendo:grid-column width="auto" title="<label style='color:white;'>Notes</label>" field="notes"  template="#=notes == null ?'' : notes #"/>
		            <kendo:grid-column width="auto" title="<label style='color:white;'>DONE BY</label>" field="pic"  template="#=pic == null ?'' : pic #"/>	            
		        </kendo:grid-columns>   
				      
				<kendo:dataSource pageSize="10" data="${helpdesk}">   									         
		       </kendo:dataSource>     
		</kendo:grid> 
	</fieldset>
	<br> 
	
	<fieldset>
		<legend style="font-size: 14px;font-weight:700;">Go Live Planning</legend>
		<table>
			<tr>
				<td>       
				 	<div id="GOLIVEDATE"> 
						<label class="display-for-form">Go Live Date</label>					
					</div>
				</td>  
				<td>:</td>
				<td><kendo:datePicker name="goLiveDate" id="goLiveDate" value="${data.goLivePlan}" format="dd/MM/yyyy"></kendo:datePicker></td>
				<td><label id="validateGoLiveDate" style="color: red;font-weight: bold;display:none">&nbsp;&nbsp;*Must be Filled</label></td>
			</tr>
		</table>
		<table> 
			<tr>  
				<td> 
					<label class="display-for-form">Notes</label>				 
				</td> 						
			</tr>
			<tr>		  
				<td> 
					<textarea id="userNotes" name="userNotes" style="height:100px;width:450px"></textarea>
					<label id="validateNotes" style="color: red;font-weight: bold;display:none">&nbsp;&nbsp;*Must be Filled</label>
				</td>			
			</tr>		 		
		</table>  
	</fieldset>
	<br> 
	 
	</c:if>
		
	<c:if test="${groupworkflowcode!='#SELF#'}">
	<fieldset>
	<legend style="font-size: 14px;font-weight:700;">PIC Task</legend>
		<table>
			<tr>
				<td>
					<label class="display-for-form" style="widht:auto">PIC : ${data.picName}</label>
				</td>
			</tr>				
		</table>
		
		<kendo:grid name="gridPIC" resizable="true" style="height:250px" selectable="true" pageable="true" filterable="true" scrollable="true">				 
			    <kendo:grid-editable mode="inline"/>  
			   
			    <kendo:grid-toolbar>
				  <kendo:grid-toolbarItem name="create"/> 
				  <kendo:grid-toolbarItem name="save"/>  
				</kendo:grid-toolbar> 
		   
		        <kendo:grid-columns>   		       	 			        	 
		        	<kendo:grid-column width="auto" title="<label style='color:white;'>Task</label>" field="task"  template="#=task == null ?'' : task #<input type='hidden' style='width:130px' editable='false' name='taskPic' value='#=task#'/> " /> 
		            <kendo:grid-column width="150px" title="<label style='color:white;'>Due Date</label>" field="dueDate" template="#=dueDate == null ?'' : kendo.toString(dueDate,'dd/MM/yyyy') #<input type='hidden' style='width:130px' editable='false' name='dueDatePic' value='#=dueDate#'/> " />
		            
		            <kendo:grid-column title="&nbsp;" width="200px">
			           	<kendo:grid-column-command> 
			           		<kendo:grid-column-commandItem name="edit"/> 
			           		<kendo:grid-column-commandItem name="delete"/>
			           	</kendo:grid-column-command>
			          </kendo:grid-column>  
		        </kendo:grid-columns>   
				    
				<kendo:dataSource pageSize="10" data="${HELPDESKLISTPIC}">   									         
					<kendo:dataSource-schema >  
              		 <kendo:dataSource-schema-model id="task">
		                   <kendo:dataSource-schema-model-fields> 
		                   <kendo:dataSource-schema-model-field name="task" type="string" > 
<%-- 	                       		<kendo:dataSource-schema-model-field-validation required="true"/> --%>
	                       	</kendo:dataSource-schema-model-field> 
		                    <kendo:dataSource-schema-model-field name="dueDate" type="date">
<%-- 	                       		<kendo:dataSource-schema-model-field-validation required="true"/> --%>
	                       	</kendo:dataSource-schema-model-field>
		                   </kendo:dataSource-schema-model-fields>
		               </kendo:dataSource-schema-model>
		           </kendo:dataSource-schema>  
		       </kendo:dataSource>    
		</kendo:grid>
	</fieldset> 
    
  <input type="hidden" id="TESTERNAME" value="${data.testerName}"> 
  
  	<c:if test="${not empty data.testerName}">   
	<fieldset>
		<legend style="font-size: 14px;font-weight:700;">Tester Task</legend>
			<table>
				<tr>
					<td>
						<label class="display-for-form" style="widht:auto">Tester : ${data.testerName}</label>
					</td>
				</tr>				
			</table>
			
			<kendo:grid name="gridTester" resizable="true" style="height:250px" selectable="true" pageable="true" filterable="true" scrollable="true">				 
				    <kendo:grid-editable mode="inline"/>  
				   
				    <kendo:grid-toolbar>
					  <kendo:grid-toolbarItem name="create"/> 
					  <kendo:grid-toolbarItem name="save"/>  
					</kendo:grid-toolbar>
			
			        <kendo:grid-columns>   		       	 			        	 
			        	<kendo:grid-column width="auto" title="<label style='color:white;'>Task</label>" field="task"  template="#=task == null ?'' : task #<input type='hidden' style='width:130px' editable='false' name='taskTester' value='#=task#'/> " />  
			            <kendo:grid-column width="150px" title="<label style='color:white;'>Due Date</label>" field="dueDate" template="#=dueDate == null ?'' : kendo.toString(dueDate,'dd/MM/yyyy') #<input type='hidden' style='width:130px' editable='false' name='dueDateTester' value='#=dueDate#'/> " />
	
			            <kendo:grid-column title="&nbsp;" width="200px">
				           	<kendo:grid-column-command> 
				           		<kendo:grid-column-commandItem name="edit"/> 
				           		<kendo:grid-column-commandItem name="delete"/>
				           	</kendo:grid-column-command>
				          </kendo:grid-column>   
			        </kendo:grid-columns>  
					   
					<kendo:dataSource pageSize="10" data="${HELPDESKLISTTESTER}">    
						<kendo:dataSource-schema >  
	              		 <kendo:dataSource-schema-model id="task">
			                   <kendo:dataSource-schema-model-fields> 
			                   <kendo:dataSource-schema-model-field name="task" type="string" > 
	<%-- 	                       		<kendo:dataSource-schema-model-field-cvalidation required="true"/> --%>
		                       	</kendo:dataSource-schema-model-field> 
			                    <kendo:dataSource-schema-model-field name="dueDate" type="date">
	<%-- 	                       		<kendo:dataSource-schema-model-field-validation required="true"/> --%>
		                       	</kendo:dataSource-schema-model-field>
			                   </kendo:dataSource-schema-model-fields>
			               </kendo:dataSource-schema-model>
			           </kendo:dataSource-schema>  
			       </kendo:dataSource>    
			</kendo:grid>
		</fieldset>
		</c:if>
		<br> 
		</c:if>
		
	<c:if test="${tempForCommitteeReview==1}">	
	<fieldset>
		<legend>Committee Review</legend>
	
		<table>
			<tr>
				<td style="width:auto">
					<div class="display-for-form" id="divLabelPriority">Priority</div>
				</td>
				
				<td>
					<div>
						<kendo:comboBox name="priority" style="width:auto" value="${data.priority}" filter="contains" placeholder="Select Priority..." suggest="true" 
		 				      dataTextField="category" dataValueField="pk.ID"><kendo:dataSource data="${priorityList}"></kendo:dataSource>
					    </kendo:comboBox>
				     </div>			     
				</td>	
				<td><div id="errorPriority" style="display:none;"><label class="error">Please Select Priority</label></div></td>					
			</tr>
			
			<tr>
				<td>
					<div class="display-for-form" id="divLabelScale">Scale</div>
				</td> 
				<td>
					<div>
						<kendo:comboBox name="scale" style="width:auto" value="${data.scale}" filter="contains" placeholder="Select Scale..." suggest="true" 
		 				      dataTextField="category" dataValueField="pk.ID"><kendo:dataSource data="${scaleList}"></kendo:dataSource>
					    </kendo:comboBox>
				     </div>			     
				</td>
				<td><div id="errorScale" style="display:none;"><label class="error">Please Select Scale</label></div></td>
			</tr>		
		</table>
	</fieldset>
	</c:if>
	  
	<br>  
	
	<input type="hidden" id="tempSystem" name="tempSystem">
	<input type="hidden" id="tempApplication" name="tempApplication">
	<input type="hidden" name="assignNotes" value="${assignNotes}"/>
	<input type="hidden" name="tempForCommitteeReview" value="${tempForCommitteeReview}"/>
	<input type="hidden" name="docNo" id="docNo" value="${docNo}"/>
	<input type="hidden" name="groupPic" value="${GROUPPIC}"/> 
	<input type="hidden" name="groupTester" value="${GROUPTESTER}"/>
	<input type="hidden" name="pic" value="${data.picName}"/>
	<input type="hidden" name="tester" value="${data.testerName}"/>
	
 	<img id="waitingLink" src="/MOA2/resources/css/images/loading.gif" height="30px" title="Loading" style="border:1px; display:none;"/>
  
	 <c:if test="${groupworkflowcode=='#SELF#'}">
	 	<input type="button" value="Return" name="btnReturn" id="btnReturn" onClick="returned()"/>
	 </c:if>  
	<input type="button" value="Save" name="btnSave" id="btnSave"/> 
	<input type="button" value="Save & Send" name="btnSaveSend" id="btnSaveSend"/>
	<input type="button" value="Cancel" name="btnCancel" id="btnCancel" onClick="cancel()"/>
	
	<input type="hidden" name="action" id="action"/>
	
	<input type="hidden" name="groupworkflowcode" id="groupworkflowcode" value="${groupworkflowcode}"/>
	

	<textarea style="display:none" name="listGroup" id="listGroup">${tempGroup}</textarea>
	<textarea style="display:none" name="listPic" id="listPic">${tempPic}</textarea>
	
	<textarea style="display:none" name="jsonGridPIC" id="jsonGridPIC"></textarea>
	<textarea style="display:none" name="jsonGridTester" id="jsonGridTester"></textarea>
</form>
<script>
$("#systemCode").change(function(){
	$("#tempSystem").val($("#systemCode").val());
	
		$.ajax({
	        type: "GET",
	        url: "/MOA2/HelpDesk/getApp/?systemCode="+$("#systemCode").val(),
	     	
	        success: function(data)
	        {
	        	$("#appCode").data("kendoComboBox").select(0);
	        	$("#appCode").data("kendoComboBox").setDataSource();
	        	$("#appCode").data("kendoComboBox").value("");
	        	for(var i=0;i<data.length;i++){
	        		$("#appCode").data("kendoComboBox").dataSource.add({ text:data[i].text , value:data[i].value});
	        			
	        	}        	
	        },
			error: function(e){}							        
	      });	
});

$("#appCode").change(function(){
	
	$("#tempApplication").val($("#appCode").val());
	
	if($("#tempApplication").val()=="NEW"){
		$("#divLabelModule").css("display","none");
		$("#divModule").css("display","none");	
	} 
	else{
		$("#divLabelModule").css("display","block");
		$("#divModule").css("display","block");
		
		if($("#systemCode").val()!="" && $("#appCode").val()!="")
		{
			$.ajax({
		        type: "GET",
		        url: "/MOA2/HelpDesk/getModule/?systemCode="+$("#systemCode").val()+"&appCode="+$("#appCode").val()+"&type="+$("#tempType").val(),
		     	
		        success: function(data)
		        {
		        	$("#moduleCode").data("kendoComboBox").select(0);
		        	$("#moduleCode").data("kendoComboBox").setDataSource();
		        	$("#moduleCode").data("kendoComboBox").value("");		        	
		        	
		        	for(var i=0;i<data.length;i++){
		        		$("#moduleCode").data("kendoComboBox").dataSource.add({ text:data[i].text , value:data[i].value });
		        			
		        	}	        	
		        },
				error: function(e){}							        
		      });
		}
	}	 
});
</script>