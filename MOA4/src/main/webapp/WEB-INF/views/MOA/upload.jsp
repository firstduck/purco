<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@page import="java.util.HashMap"%>

<script>
var attachment=0;

function onSelect(){
	attachment++;	
}

function onRemove(){
	attachment--;
} 

</script>

<c:url value="/upload/save" var="saveUrl" />
<c:url value="/upload/remove" var="removeUrl" />
<b>Max File Size : 1MB</b>

<br/>
	<kendo:upload select="onSelect" remove="onRemove" name="files" style="width:350px; cursor:hand;"> 
	   <kendo:upload-async autoUpload="true" saveUrl="${saveUrl}" removeUrl="${removeUrl}"/>
	</kendo:upload> 