<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.k-window-action
{
    visibility: hidden ;
}

.k-grid tr { 
	font-size:11px;
}

</style>
   
<script>
function getAction(activity, webName, docNo, moduleCode){  
	if((activity == 'Review' || activity == 'Assign' || activity == 'BatchReview') || (activity == 'ReviewApprove' && moduleCode == 'FPA')) {
		return "<a href='"+webName+"/Review/?docNo="+docNo+"' target='_blank'>Review</a>";} 
	else if(activity == 'Print')
		return "<a href='"+webName+"/Print/?docNo="+docNo+"'>Print</a>";
	else if((activity == 'ReviewApprove' || activity == 'Approve') && moduleCode != 'FPA' && moduleCode != 'FPK' && moduleCode != 'PMK' && moduleCode != 'PMK-M') 
		return "<input type='radio' name='"+docNo+"' value='ho_"+docNo+"' onChange='setSes(1);setDisabled(true,1)' checked />Hold "
				+ "<input type='radio' name='"+docNo+"' value='ap_"+docNo+"' onChange='setSes(1);setDisabled(false,1)' />Approve "
				+ "<input type='radio' name='"+docNo+"' value='re_"+docNo+"' onChange='setSes(1);setDisabled(false,1);addComment(1)' />Reject "
				+ "<input type='radio' name='"+docNo+"' value='rt_"+docNo+"' onChange='setSes(1);setDisabled(false,1);addComment(1)'/>Return"
				+ "<br/><center><label id='err_"+docNo+"' style='display:none;font-weight:bold;'>Please fill reason for rejection/return!</label></center>"
				+ "<input type='hidden' name='listReason' id='rea_"+docNo+"'/>";
	return "---";
}
</script>
 
<c:url value="${readUrl}" var="readUrl" />

<div>
	Module
	<kendo:dropDownList name="moduleDDL" dataTextField="text" dataValueField="value" style="width:300px" value="${value}">
		<kendo:dataSource data="${modules}"></kendo:dataSource>
	</kendo:dropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="/MOA2/Home/NewJobList">Click Here to Go to New Job List</a>
</div>
<br/>

<form id="formJobList" method="POST" action="/MOA2/Home/JobListProcess">
<kendo:grid name="gridHome" pageable="true" groupable="true" sortable="true" filterable="false" 
				selectable="true" change="onChange" scrollable="false" resizable="true" dataBound="databound"> 

        <kendo:grid-columns> 
        	<kendo:grid-column title="Doc No" width="180px" field="docNo" template="<a target='_blank' href='#=webName#/Detail/?docNo=#= docNo #'>#= docNo #</a>"/> 
            <kendo:grid-column title="Module" field="moduleName"/> 
            <kendo:grid-column title="Last Approved By" field="lastApproveName" width="110px"/> 
            <kendo:grid-column title="Last Approve Date" field="lastDate" width="120px"/>
            <kendo:grid-column title="Created By" field="creatorName" width="110px" /> 
            <kendo:grid-column title="Created Date" field="createdDate" width="100px" />
	        <kendo:grid-column title="Action" width="270px" template=" #=getAction(activity, webName,docNo, moduleCode)#"/> 
			<kendo:grid-column title="Comment" template="<input type='button' name='buttontest' id='button_#= docNo #' value='Add' onClick='addComment(1)' disabled/>"/>
        </kendo:grid-columns>
        
        <kendo:dataSource pageSize="5">    
        	<kendo:dataSource-transport>
            	<kendo:dataSource-transport-read url="${readUrl}" dataType="json" type="GET" contentType="application/json"/>
				 <kendo:dataSource-transport-parameterMap>
                	<script>
  	                	function parameterMap(options,type) {  
  	                		return JSON.stringify(options); 
  	                	} 
                  	</script>  
                </kendo:dataSource-transport-parameterMap>
			</kendo:dataSource-transport> 
       </kendo:dataSource>
	</kendo:grid>
<br/>

<input type="button" onClick="submitForm()" value="Submit"/>
</form>

<c:forEach var="item" items="${errorList}">
		<br/>
		<div class="error"><c:out value="${item}"/></div>
</c:forEach>

<hr/>
<div id="window"></div>
<div id="batchDiv"></div>
<div id="detailView"></div>

<script type="text/Javascript">
kendo.ui.progress($("#gridHome"), true);

function databound(){
	kendo.ui.progress($("#gridHome"), false);
}

$(document).ready(function(){
	var x = $("#moduleDDL").val();
	
	if(x != 'ALL'){
		var grid = $("#gridHome").data("kendoGrid");
		grid.hideColumn("moduleName");
	}  
}); 

$("#moduleDDL").change(function(){
	var x = $("#moduleDDL").val();
	window.location = "/MOA2/Home/JobList/?m="+x;
});

function setSes(flag){
	
	var entityGrid;
	var selectedItem;
	if(flag == 1){
		entityGrid = $("#gridHome").data("kendoGrid"); 
		selectedItem = entityGrid.dataItem(entityGrid.select());
	}
	else{
		entityGrid = $("#gridBatch").data("kendoGrid");

		entityGrid.select().each(function () {
			
	    var row = $(this).closest("tr");
	    selectedItem = entityGrid.dataItem(row);
		});
	}

	
	var docNo = selectedItem.docNo;
	var moduleCode = selectedItem.moduleCode; 
	var activity = selectedItem.activity;
	var reason = document.getElementById("rea_"+docNo).value;
	var buffer = document.getElementsByName(docNo);
	var wfCode = selectedItem.workflowCode;
	
	for(var i=0;i<buffer.length;i++){
		if(buffer[i].checked){
			$.ajax({
		        type: "GET",
		        url: "/MOA2/Home/SetJobIntoSession/?docNo="+docNo+"&workflowCode="+wfCode+"&moduleCode="+moduleCode+"&activity="+activity+"&action="+buffer[i].value+"&reason="+reason.split(' ').join('%20'),
		        success: function(data)
		        {
		        	
		        },
				error: function(e) {
				}
		    });
		}
	}
}

function onChange(arg){ 

	
	if($("#window1").data("kendoWindow")){ //if already initialized
		$("#window1").data("kendoWindow").destroy();
	}
	
	var grid = $("#gridHome").data("kendoGrid"); 

	var selectedItem = grid.dataItem(grid.select());
	var docNo = selectedItem.docNo; 
	
	
	if(selectedItem.activity != 'BatchReview'){

		var moduleCode = selectedItem.moduleCode; 
		var WFcode = selectedItem.workflowCode; 
		var pathID = selectedItem.pathID; 
		var lineID = selectedItem.linesID; 

		$("#batchDiv").html("");
		$("#detailView").load("/MOA2/Home/JobListDetail/?moduleCode="+moduleCode+"&docNo="+docNo+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID+"&flag=no"); 
	}
	else if (selectedItem.activity == 'BatchReview'){
		$("#detailView").html("");
		$("#batchDiv").load("/MOA2/Home/BatchJobList/?batch="+docNo);
	}
} 

function batchOnChange(){
	var grid = $("#gridBatch").data("kendoGrid");

	grid.select().each(function () {
		
	    var row = $(this).closest("tr");
	  
	    var selectedRow = grid.dataItem(row);
	    var cellIndex = $(this).closest("td").index();
	    
	    if(cellIndex != 6 && cellIndex != 7 && cellIndex != 0){
			var moduleCode = selectedRow.moduleCode; 
			var docNo = selectedRow.docNo; 
			var WFcode = selectedRow.workflowCode; 
			var pathID = selectedRow.pathID; 
			var lineID = selectedRow.linesID;
			
			$("#window1").load("/MOA2/Home/JobListDetail/?moduleCode="+moduleCode+"&docNo="+docNo+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID+"&flag=yes"); 
	
			$("#window1").kendoWindow({
		        width: "480px",
		        height: "250px",
		        actions: ["Close"],
		        title: "Job Detail"
			});
	
			$("#window1").load("/MOA2/Home/JobDetailWindow/?moduleCode="+moduleCode+"&docNo="+docNo+"&workflowCode="+WFcode+"&pathID="+pathID+"&lineID="+lineID); 
			$("#window1").data("kendoWindow").center().open();
	    }
	});
}


function submitForm(){
	$("#formJobList").submit();
}


function addComment(flag){
	var entityGrid;
	var selectedItem;
	if(flag == 1){
		entityGrid = $("#gridHome").data("kendoGrid");
		selectedItem = entityGrid.dataItem(entityGrid.select());
	}
	else{
		entityGrid = $("#gridBatch").data("kendoGrid");
		entityGrid.select().each(function () {
			var row = $(this).closest("tr");	  
		    selectedItem = entityGrid.dataItem(row);
		});
	}
	
	var docNo = selectedItem.docNo;
	
	var x = document.getElementsByName(docNo); //x itu radio
	var act="";

	if(x[1].checked){
		act = "Approve";
	}
	else if(x[2].checked || x[3].checked){
		act = "Other";
	}	
	
	$("#window").kendoWindow({
        width: "480px",
        height: "250px",
        title: "Input reason",
        activate: function(){
            $("#txtReason").select();
         },
        close: function() {
        	document.getElementById("rea_"+docNo).value = $("#selectedReason").html();
//         	alert(document.getElementById("rea_"+docNo).value);
        	setSes(flag);
        }
	 });
	var reason = document.getElementById("rea_"+docNo).value;
	$('#window').load("/MOA2/Home/KendoWindowBrowse/?msg="+reason+"&act="+act); //panggil controller
	$("#window").data("kendoWindow").center().open();
	
}

function setDisabled(disable, flag)
{
	var entityGrid;
	var selectedItem;
	
	if(flag == 1){
		entityGrid = $("#gridHome").data("kendoGrid");
		selectedItem = entityGrid.dataItem(entityGrid.select());
	}
	else {
		entityGrid = $("#gridBatch").data("kendoGrid");
		entityGrid.select().each(function () {
			
		var row = $(this).closest("tr");	  
	    selectedItem = entityGrid.dataItem(row);
		});
	}
	
	var docNo = selectedItem.docNo;

	document.getElementById("button_"+docNo).disabled=disable;
	document.getElementById("rea_"+docNo).value="";
}
</script>