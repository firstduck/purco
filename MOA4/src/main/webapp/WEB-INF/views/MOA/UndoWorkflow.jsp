<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<h2>${title}</h2>

<a href="${link}">Back</a>
<br/><br/>
<h3>${undoResult}</h3>
<br/><br/>
<a href="${link}">Back</a>
