<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>

<style>
.assign td{
	padding:2px;
}

.k-grid th.k-header,
	.k-grid-header
	{
	    background:#e61e26; 
	} 
</style>

<h2>Assign PIC, Doc No ${docNo}</h2>

Description : ${desc}<br/><br/>

<c:set var="i" value="0"/>
  
<form id="frmAssign" method="POST" action="${action}">

<div id="loadDetail1"></div>

<table width="100%">
	<tr>
		<td width="25%" valign="top">  
			<c:forEach var="group" items="${data}">
				<table class="assign">
					<tr>
						<td> 
							${group.groupworkflowname}
							<input type="hidden" name="assignGroup" value="${group.groupworkflowcode}"/>
						</td> 
					</tr>
			 		
					<tr> 
						<td>
							<kendo:dropDownList name="assignPIC_${i}" style="width:250px" index="0" ondatabound="assignPIC(${i})" onChange="assignPIC(${i})" dataTextField="text" dataValueField="value" >
								<kendo:dataSource data="${group.kendoList}"></kendo:dataSource>
							</kendo:dropDownList>
							<input type="hidden" id="hiddenPIC_${i}" name="assignedPIC"/>
						</td>
					</tr>
				</table>
				<c:set var="i" value="${i+1}"/>
				<br/><br/>  
			</c:forEach> 
		</td>
		  
		<td width="75%" valign="top">
			<label class="display-label">Assign Notes</label><br></br>
			<textarea name="assignNotes" rows="10" cols="50"></textarea>
		</td>	
	</tr>
</table>
	
	<img id="waitingLink" src="/MOA2/resources/css/images/loading.gif" height="30px" title="Loading" style="border:1px; display:none;"/>
	<input type="hidden" id="docNo" name="docNo" value="${docNo}"/>
	<input type="hidden" name="hidden" value="${i}"/>
	<input type="button" value="Save & Send" id="btnSaveSend"/>
	<input type="button" id="btnReturn" name="btnReturn" value="Return" onClick="returnTo()">
	<input type="button" id="btnReject" name="btnReject" value="Reject" onClick="reject()">
</form>  
 
<script> 
$("#loadDetail1").load("/MOA2/${webName}/AjaxDetail/?docNo="+$("#docNo").val());

	function assignPIC(x){
		$("#hiddenPIC_"+x).val($("#assignPIC_"+x).val());
	} 
	
	function returnTo(){
		$("#waitingLink").css("display","block"); 
		try { $("#btnSaveSend").css("display","none"); } catch(e) { }
		try { $("#btnReturn").css("display","none"); } catch(e) { }
		try { $("#btnReject").css("display","none"); } catch(e) { }
		  
		window.location="/MOA2/${webName}/Return/?docNo="+$("#docNo").val(); 
	}
	 
	function reject(){
		$("#waitingLink").css("display","block");
		try { $("#btnSaveSend").css("display","none"); } catch(e) { }
		try { $("#btnReturn").css("display","none"); } catch(e) { }
		try { $("#btnReject").css("display","none"); } catch(e) { }
		
		window.location="/MOA2/${webName}/Reject/?docNo="+$("#docNo").val(); 
	}
	$("#btnSaveSend").click(function(){
		$("#frmAssign").submit();
		$("#waitingLink").css("display","block"); 
		try { $("#btnSaveSend").css("display","none"); } catch(e) { }
		try { $("#btnReturn").css("display","none"); } catch(e) { }
		try { $("#btnReject").css("display","none"); } catch(e) { }
	});
</script>