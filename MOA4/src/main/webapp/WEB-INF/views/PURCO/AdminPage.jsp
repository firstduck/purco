<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	legend{
		color:#d50000;
		font-size:20px;
		font-weight: bold;
	}
	.display-for-form{
		color:#d50000;
		font-size:12px;
		font-weight:bold;
		font-family: Verdana, Helvetica, Sans-Serif;
		
	}
	.text{
		color:black;
		font-family: Verdana, Helvetica, Sans-Serif;
		
	}
	input[type="button"] {
	background: rgb(213,0,0);
	-webkit-border-radius: 30px;
	-moz-border-radius: 30px;
	-ms-border-radius: 30px;
	-o-border-radius: 30px;
	border-radius: 30px;
	-webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
	-moz-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
	-ms-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
	-o-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
	box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
	border: 1px solid #D69E31;
	color: #fff;
	cursor: pointer;
	float: left;
	font: bold 15px Helvetica, Arial, sans-serif;
	height: 35px;
	text-shadow: 0 1px 0 rgba(255,255,255,0.5);
	width: 120px;
}
input[type="button"]:hover {
	background: rgba(255,82,82 ,1);
}
#btnVerified,#btnUnverified{
	width:100px;
}
p.b {
    font: 16px verdana;
    margin-top: 20px;
    color: gray;
}
#mayoraHeader{
	width: 124px;
	height:37px;
}

</style>
<link href="/MOA4/resources/MOA2/css/kendo.common.min.css" rel="stylesheet" type="text/css" />
<link href="/MOA4/resources/MOA2/css/kendo.default.min.css" rel="stylesheet" type="text/css" />
<link href="/MOA4/resources/MOA2/css/kendo.metro.min.css" rel="stylesheet" type="text/css" />
<link href="/MOA/resources/css/colorbox.css" rel="stylesheet" type="text/css" />

<br><br><br><br><br><br><br><br>
<script src="/MOA4/resources/MOA2/js/jquery.min.js"></script>
<script src="/MOA4/resources/MOA2/js/kendo.web.min.js"></script>

<table>
	<tr>
		<td>
			<img id="mayoraHeader" src='/MOA4/resources/images/mayora_header.png'/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<legend>Welcome, ${nameLogin}</legend>
	<br>
	
	<kendo:grid name="gridAllVendor" id="gridAllVendor" pageable="false" groupable="false" filterable="false">
		    <kendo:grid-columns>
				<kendo:grid-column title="VENDORCODE" field="VENDORCODE"/>
	            <kendo:grid-column title="VENDORNAME" field="VENDORNAME"/>
	            <kendo:grid-column title="COUNTRY" field="COUNTRY"/>
	            <kendo:grid-column title="VENDORADDRESS" field="VENDORADDRESS"/>
	            <kendo:grid-column title="OWNER" field="OWNER"/>
	            <kendo:grid-column title="TOTAL_EMPLOYEE" field="TOTAL_EMPLOYEE"/>
	            <kendo:grid-column title="STATUS" field="STATUS"/>
	            <kendo:grid-column title="" field="" template= '#=Verif(STATUS)#'/>
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="5" data="${lsVendor}"></kendo:dataSource>
	</kendo:grid>
	
	<br>
</fieldset>
<script>
$("#btnLogout").click(function(){
	window.location.href = '/MOA4/PUR-CO/LoginForm';
});

$("#gridAllVendor").on("click", "#btnVerified", function(){
	var tr = $(this).closest("tr");
	var kendo = $("#gridAllVendor").data("kendoGrid");
	kendo.select("tr:eq(" +(tr.index()) +")");
	var grid = kendo.dataItem(kendo.select());
	$.ajax({
		url:'/MOA4/PUR-CO/UpdateVendor/?vendorCode='+grid.VENDORCODE+'&status=Verified',
		type:'GET',
		success:function(data){
			kendo.dataSource.data(data);
			alert("Vendor "+grid.VENDORNAME+ " is successfully verified");
		}	
	});
});

$("#gridAllVendor").on("click", "#btnUnverified", function(){
	var tr = $(this).closest("tr");
	var kendo = $("#gridAllVendor").data("kendoGrid");
	kendo.select("tr:eq(" +(tr.index()) +")");
	var grid = kendo.dataItem(kendo.select());
	$.ajax({
		url:'/MOA4/PUR-CO/UpdateVendor/?vendorCode='+grid.VENDORCODE+'&status=Not%20Verified',
		type:'GET',
		success:function(data){
			kendo.dataSource.data(data);
			alert("Vendor "+grid.VENDORNAME+ " is successfully unverified");
		}	
	});
});

function Verif(e){
	if(e == "Verified") return '<input type="button" id="btnUnverified" value="Unverify">';
	else return '<input type="button" id="btnVerified" value="Verify">';
}
</script>