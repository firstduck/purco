<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
	body{
		overflow:hidden;
	}
	.hide{
		display:none;
	}
	 .k-grid tbody tr{
	    height: 30px;
	}
	
	#gridConfirm{
	    width: 900px;
	}
	.hideCheck{
		display:none;
	}
	#ceklist{
		color:#33691E;
		font-size: 25px;
	}
	.hideWrong{
		display:none;
	}
	.hide{
		display:none;
	}
	#wrong{
		color:#d50000;
		font-size: 25px;
	}
	p.b {
	    font: 16px verdana;
	    margin-bottom: auto;
	    color: gray;
	}
	tr{
		height: 30px;
	}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>
<div id="window"></div>

<fieldset>
	<form target="_blank" id="processGrid" method="POST" action="/MOA4/PUR-CO/AffectedProducts">
		<table>
			<tr style="margin-bottom: 50px"> 
			   	<td style="width: 200px" class="display-for-form">Document Number</td>
			   	<td class="display-for-form" style="width: 8px">:</td>
			   	<td style="height:20px">
			   		<label id="docno" class="text">${docNo}</label><input type='hidden' id="docNo" name="docNo" value="${docNo}">
			   	</td>
			</tr>
		</table>
		<kendo:grid name="gridProcess" id="gridProcess" pageable="true" groupable="false" selectable="true" editable="false" filterable="false" onclick="onClick()">
			   <kendo:grid-columns>
					<kendo:grid-column title="MATERIAL CODE" field="materialCode"/>  
		            <kendo:grid-column title="MATERIAL NAME" field="materialName"/>   
		          	<kendo:grid-column title="WEEK" field="week"/>
		          	<kendo:grid-column title="YEAR" field="year"/>
		          	<kendo:grid-column title="PLAN ORDER" field="materialOutstanding" template= '<div style="text-align:right;">#= kendo.toString(materialOutstanding, "n0") #</div>'/>
		          	<kendo:grid-column title="CONFIRM" field="materialConfirm" template= '<div style="text-align:right;">#= kendo.toString(materialConfirm, "n0") #</div>'/>
		          	<kendo:grid-column title="DONE" field="done" template="<label id='ceklist' class='hideCheck'>✔</label><label id='wrong'>✘</label>"/>
		          	<kendo:grid-column title="GENERATE LINK" field="generateLink" template= '<input type="button" class="k-button" id="btnGenerateLink" value="Generate Link"></button>'/>
		        </kendo:grid-columns>
		
		        <kendo:dataSource pageSize="10" data="${d_proc}"></kendo:dataSource>
			
		</kendo:grid>
		<textarea class="hide" id='jsonProcess' name='jsonProcess'></textarea>
		<textarea class="hide" id='sessionGridConfirm' name='sessionGridConfirm'></textarea>
	</form>
</fieldset>

<div id="divConfirm" class="hide">
	<fieldset>
		<table>
			<tr style="margin-bottom: 50px"> 
			   	<td style="width: 200px" class="display-for-form">Material Code</td>
			   	<td class="display-for-form" style="width: 8px">:</td>
			   	<td style="height:20px">
			   		<label id="kodeMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Material Name</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="namaMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Week</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="weekMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Year</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="yearMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr>
				<td colspan="3">
					<kendo:grid name="gridConfirm" id="gridConfirm" groupable="false" editable="true" filterable="false">
					<kendo:grid-editable mode="inline"/>
					<kendo:grid-toolbar>
			            <kendo:grid-toolbarItem name="create"/>
			        </kendo:grid-toolbar>	
					<kendo:grid-columns>
							<kendo:grid-column title="VENDOR CODE" field="vendorCode"  width='200px'/>  
							<kendo:grid-column title="VENDOR NAME" field="vendorName"  width='200px'/>  
				            <kendo:grid-column title="QUANTITY" field="quantity"   width='200px' template= '<div style="text-align:right;">#= kendo.toString(quantity, "n0") #</div>'/>
				           	<kendo:grid-column title="&nbsp;"  width="100px">
			          			<kendo:grid-column-command>
				            		<kendo:grid-column-commandItem name="edit"/>
				       			</kendo:grid-column-command>
				       		</kendo:grid-column>
				       		<kendo:grid-column title="&nbsp;"  width="100px" template="<button class='k-button' id='btnDelete'>Delete</button>"></kendo:grid-column>
				    </kendo:grid-columns>	
				    <kendo:grid-edit>
						<script>
							function grid_edit(e) {
								e.container.find("input[name=vendorCode]").click(function() {
									$("#window").kendoWindow({
										width: "600px",
										height: "450px",
										actions: ["Close"],
										title: "Browse", 
										close:function(){ 
											if($("#selectedValue1").html()!=""){
												e.container.find("input[name=vendorCode]").val( $("#selectedValue1").html()).change();
												e.container.find("input[name=vendorName]").val( $("#selectedValue2").html()).change();
											}
											this.destroy();
											var div = document.createElement("div");
											div.id = "window";
											if(document.body!=null){
												document.body.appendChild(div);
											}
										}
									 });
									 $("#window").load("/MOA4/PUR-CO/KendoWindowChooseVendor/?window=window&column=2");
									 $("#window").data("kendoWindow").center().open();
						    	});
								
								e.container.find("input[name=quantity]").val("");				
								e.container.find(".k-button.k-button-icontext.k-primary.k-grid-update").click(function() {
									var len = $("#gridConfirm").data("kendoGrid").dataSource.view().length;
								 	var dataConfirm = $("#gridConfirm").data("kendoGrid").dataSource.data();
									var isUpdate = true;
									if(e.container.find("input[name=vendorCode]").val().trim()==""||e.container.find("input[name=vendorCode]").val().trim()==null){
										isUpdate = false;
									}
									else if(e.container.find("input[name=vendorName]").val().trim()==""||e.container.find("input[name=vendorName]").val().trim()==null){
										isUpdate = false;
									}
									else if(e.container.find("input[name=quantity]").val().trim()==""||e.container.find("input[name=quantity]").val().trim()==null){
										isUpdate = false;
									}
									for(var i=0;i<len;i++){
										if(e.container.find("input[name=vendorCode]").val() == dataConfirm[i].vendorCode && e.container.find("input[name=vendorCode]").index() != i){
								 			isUpdate=false;
								 			break;		
								 		}
									}
									if(isUpdate==false)
									{
										alert("data sudah ada atau input harus di isi");
										e.container.find("input[name=vendorCode]").val("");
										e.container.find("input[name=vendorName]").val("");
										e.container.find("input[name=quantity]").val("");
									}else{
									 	var kendo = $("#gridProcess").data("kendoGrid");
									 	var processDataSource = kendo.dataSource.data();
										var processLen = kendo.dataSource.view().length;
								 		var row = kendo.select().index();
								 		var grid = kendo.dataItem(kendo.select());
									 	var sessionStorage = window.sessionStorage;
									 	sessionStorage.setItem("grid_data"+row, JSON.stringify($("#gridConfirm").data("kendoGrid").dataSource.data()));
									 	sessionStorage.setItem("gridProcess",JSON.stringify(kendo.dataSource.data()));
										var total=0;
										var length = $("#gridConfirm").data("kendoGrid").dataSource.view().length;
									 	var data = $("#gridConfirm").data("kendoGrid").dataSource.data();
									 	for(var i = 0;i<length;i++){
									 		total+=parseInt(data[i].quantity);
									 	}
									 	$("#totalMaterial").html(total);
									 	
										grid.set("materialConfirm",total);
										for(var j = 0;j<processLen;j++){
								 			kendo.select("tr:eq(" +(j) +")");
								 			var currKendo = kendo.select();
											if(processDataSource[j].materialConfirm >= processDataSource[j].materialOutstanding){
												currKendo.closest("tr").find("#ceklist").removeClass("hideCheck").change();
												currKendo.closest("tr").find("#wrong").addClass("hideWrong").change();
											}
											else{
												currKendo.closest("tr").find("#ceklist").addClass("hideCheck");
												currKendo.closest("tr").find("#wrong").removeClass("hideWrong");
											}
										}
										processDataSource = kendo.dataSource.data();
								 		kendo.select("tr:eq(" +(row) +")");
									}
								});
							}
						</script>
					</kendo:grid-edit>
					<kendo:dataSource>
						<kendo:dataSource-schema> 
			              	<kendo:dataSource-schema-model>
				              	<kendo:dataSource-schema-model-fields>
				              		<kendo:dataSource-schema-model-field name="vendorCode">
				              			<kendo:dataSource-schema-model-field-validation required='true'/>
				              		</kendo:dataSource-schema-model-field>
				              		<kendo:dataSource-schema-model-field name="vendorName">
					              		<kendo:dataSource-schema-model-field-validation required='true'/>
				              		</kendo:dataSource-schema-model-field>
				              		<kendo:dataSource-schema-model-field name="quantity">
				              		</kendo:dataSource-schema-model-field>
				            	</kendo:dataSource-schema-model-fields>
			            	</kendo:dataSource-schema-model>           	          	
			         	</kendo:dataSource-schema>
			         </kendo:dataSource>
				</kendo:grid>
				</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Total</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="totalMaterial" class="text"></label>
			   	</td>
			</tr>
		</table>
	</fieldset>
</div>

<br>
<div>
	<input type="button" class="k-button" id="nextProcess" value="Next"/>
</div>

<script>
	window.onload = function(){
	    sessionStorage.clear();
	    var kendo = $("#gridProcess").data("kendoGrid");
		var processLen = kendo.dataSource.view().length;
		for(var j = 0;j < processLen;j++){
	    	init2(j);
	    }
	};
	
	function init(){
		var kendo = $("#gridProcess").data("kendoGrid");
		var processLen = kendo.dataSource.view().length;
		var processDataSource = kendo.dataSource.data();
		for(var j = 0;j<processLen;j++){
				kendo.select("tr:eq(" +(j) +")");
				var currKendo = kendo.select();
			if(parseInt(processDataSource[j].materialConfirm) >= parseInt(processDataSource[j].materialOutstanding)){
				currKendo.closest("tr").find("#ceklist").removeClass("hideCheck");
				currKendo.closest("tr").find("#wrong").addClass("hideWrong");
			}
			else{
				currKendo.closest("tr").find("#ceklist").addClass("hideCheck");
				currKendo.closest("tr").find("#wrong").removeClass("hideWrong");
			}
		}
		kendo.select().removeClass("k-state-selected");
	}
	function init2(num){
		var sessionStorage = window.sessionStorage;
		$("#updateBtn").removeClass("hide");
		var kendo = $("#gridProcess").data("kendoGrid");
		var processDataSource = kendo.dataSource.data();
		var processLen = kendo.dataSource.view().length;
		var j = num;
		kendo.select("tr:eq(" +(j) +")");
		var grid = kendo.dataItem(kendo.select());
		var row = kendo.select().index();
		var total = 0;
		$("#kodeMaterial").html(grid.materialCode);
		$("#namaMaterial").html(grid.materialName);
		$("#weekMaterial").html(grid.week);
		$("#yearMaterial").html(grid.year);
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxGetDataVendor/?docNo='+$("#docNo").val()+"&week="+grid.week+"&year="+grid.year+"&materialCode="+grid.materialCode,
			success:function(data){
				if(data.length != 0){
					$("#gridConfirm").data("kendoGrid").dataSource.data(data);
					sessionStorage.setItem("grid_data"+row, JSON.stringify($("#gridConfirm").data("kendoGrid").dataSource.data()));
				}
				var length = $("#gridConfirm").data("kendoGrid").dataSource.view().length;
				var datas = $("#gridConfirm").data("kendoGrid").dataSource.data();
				for(var i = 0;i<length;i++){
					total+=parseInt(datas[i].quantity);
				}
				$("#totalMaterial").html(total);
				grid.set("materialConfirm",total);
				for(var k = 0;k<processLen;k++){
		 			kendo.select("tr:eq(" +(k) +")");
		 			var currKendo = kendo.select();
					if(parseInt(processDataSource[k].materialConfirm) >= parseInt(processDataSource[k].materialOutstanding)){
						currKendo.closest("tr").find("#ceklist").removeClass("hideCheck");
						currKendo.closest("tr").find("#wrong").addClass("hideWrong");
					}
					else{
						currKendo.closest("tr").find("#ceklist").addClass("hideCheck");
						currKendo.closest("tr").find("#wrong").removeClass("hideWrong");
					}
				}
				processDataSource = kendo.dataSource.data();
				kendo.select().removeClass("k-state-selected");
	
			}
		});
	}
	function onClick(){
		var sessionStorage = window.sessionStorage;
		$("#updateBtn").removeClass("hide");
		var kendo = $("#gridProcess").data("kendoGrid");
		var grid = kendo.dataItem(kendo.select());
		var row = kendo.select().index();
		var total = 0;
	
		$("#kodeMaterial").html(grid.materialCode);
		$("#namaMaterial").html(grid.materialName);
		$("#weekMaterial").html(grid.week);
		$("#yearMaterial").html(grid.year);
	
		if(!sessionStorage.getItem("grid_data"+row)){
			$("#gridConfirm").data("kendoGrid").dataSource.data([]);
		}
		else{
			var json = JSON.parse(sessionStorage.getItem("grid_data"+row));
			$("#gridConfirm").data("kendoGrid").dataSource.data(json);
		}
		
		$.ajax({
			type:'GET',
			url:'/MOA4//PUR-CO/AjaxGetDataVendor/?docNo='+$("#docNo").val()+"&week="+grid.week+"&year="+grid.year+"&materialCode="+grid.materialCode,
			success:function(data){
				if(data.length != 0){
					$("#gridConfirm").data("kendoGrid").dataSource.add(data);
				}
	
				var length = $("#gridConfirm").data("kendoGrid").dataSource.view().length;
				var datas = $("#gridConfirm").data("kendoGrid").dataSource.data();
	
				if(length > 0){
					for(var i = 0;i<length;i++){
						total+=parseInt(datas[i].quantity);
					}
					$("#totalMaterial").html(total);
				}
	
				$("#divConfirm").removeClass("hide");
			}
		});
	}
	
	$("#gridConfirm").on("click", "#btnDelete", function() {
			var total=0;
			var sessionStorage = window.sessionStorage;
			var kendo = $("#gridProcess").data("kendoGrid");
			var processDataSource = kendo.dataSource.data();
			var processLen = kendo.dataSource.view().length;
			var grid = kendo.dataItem(kendo.select());
			var row = kendo.select().index();
	    	var tr = $(this).closest("tr");
	        var gridTr = $("#gridConfirm").data("kendoGrid");
	
	        gridTr.removeRow(tr);
			$("#gridConfirm").data("kendoGrid").refresh();
			var length = $("#gridConfirm").data("kendoGrid").dataSource.view().length;
			var data = $("#gridConfirm").data("kendoGrid").dataSource.data();
			
			for(var i = 0;i<length;i++){
				total+=parseInt(data[i].quantity);
			}
			$("#totalMaterial").html(total);
			grid.set("materialConfirm",total);
	 		for(var j = 0;j<processLen;j++){
	 			kendo.select("tr:eq(" +(j) +")");
	 			var currKendo = kendo.select();
				if(processDataSource[j].materialConfirm >= processDataSource[j].materialOutstanding){
					currKendo.closest("tr").find("#ceklist").removeClass("hideCheck");
					currKendo.closest("tr").find("#wrong").addClass("hideWrong");
				}
				else{
					currKendo.closest("tr").find("#ceklist").addClass("hideCheck");
					currKendo.closest("tr").find("#wrong").removeClass("hideWrong");
				}
			}
	 		processDataSource = kendo.dataSource.data();
	 		kendo.select("tr:eq(" +(row) +")");
			sessionStorage.setItem("grid_data"+row, JSON.stringify($("#gridConfirm").data("kendoGrid").dataSource.data()));
	});
	
	$("#nextProcess").click(function(){
		var kendo = $("#gridProcess").data("kendoGrid");
		var processDataSource = kendo.dataSource.data();
		var length = kendo.dataSource.view().length;
		var json = JSON.stringify(processDataSource);
		var jsonSessionConfirm ="";
		for(var i=0;i<length;i++){
			jsonSessionConfirm+=window.sessionStorage.getItem("grid_data"+i);
			if(i!=length-1){
				jsonSessionConfirm+="#";
			}
		}
		$("#sessionGridConfirm").val(jsonSessionConfirm);
		$("#jsonProcess").val(json);
		$("#processGrid").submit();
	});
	
	$("#gridProcess").on("click", "#btnGenerateLink", function(){
		var tr = $(this).closest("tr");
		var kendo = $("#gridProcess").data("kendoGrid");
		kendo.select("tr:eq(" +(tr.index()) +")");
		var grid = kendo.dataItem(kendo.select());
		window.open("/MOA4/PUR-CO/GenerateLink/?docNo="+$("#docNo").val()+"&materialCode="+grid.materialCode+"&materialName="+grid.materialName+"&quantity="+grid.materialOutstanding+"&week="+grid.week+"&year="+grid.year);
	});
</script>