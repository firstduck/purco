<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
#bawah
{
	width:100%;
	height:200px;
	margin-top: 50px;
}
#kotak{
	width:200px;
	height:70px;
	background: #DC143C;
	float:left;
	margin-left:35px;
	text-align: center;
	border-style: solid;
	border-radius:10px;
}
.kotak2{
	width:200px;
	height:70px;
	background: #8BC34A;
	float:left;
	margin-left:35px;
	text-align: center;
	border-style: solid;
	cursor:pointer;
	border-radius:10px;
}
#kotak > h3{
	color:white;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
tr{
	height: 30px;
}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
<legend>Detail Document</legend>
	<table>
		<tr>
			<td><div class="display-for-form">No. Document</div></td>
			<td>:</td>
			<td>${browseList.DOCNO }<input type="hidden" id="docNo" value="${browseList.DOCNO }"></td>
			
			<td style="padding-left: 200px"><div class="display-for-form">Month</div></td>
			<td>:</td>
			<td>${browseList.MONTH }<input type="hidden" id="month" value="${browseList.MONTH }"></td>
		</tr>
	

		<tr>
			<td><div class="display-for-form">Created Date</div></td>
			<td>:</td>
			<td>${browseList.CREATED_DATE }</td>
			
			<td style="padding-left: 200px; padding-right: 50px"><div class="display-for-form">Year</div></td>
			<td>:</td>
			<td>${browseList.YEAR }<input type="hidden" id="year" value="${browseList.YEAR }"></td>
		</tr>
		<tr>
			<td><div class="display-for-form">Created By</div></td>
			<td>:</td>
			<td>${browseList.CREATED_BY }</td>
			
			<td style="padding-left: 200px"><div class="display-for-form">Status</div></td>
			<td>:</td>
			<td>${browseList.STATUS }</td>
		</tr>
		
		<tr>
			<td><div class="display-for-form">Last Run</div></td>
			<td>:</td>
			<td>${browseList.LAST_RUN }</td>
			
			<td colspan="3">
		</tr>
	</table>
</fieldset>
<fieldset>
<legend>Diagram</legend>
<div id="bawah">
	<c:set var="j" value="1"/>
	<% for(int i = 0; i < 6; i++) { %>
		<c:if test="${j <= browseList.LAST_RUN}">
			<c:if test="${j == 1}">
				<div id="kotak2${j}" class="kotak2"><h3>Next Process for Add Vendor</h3></div>
			</c:if>
			<c:if test="${j == 2}">
				<div id="kotak2${j}" class="kotak2"><h3>Material Usage Planning</h3></div>
			</c:if>
			<c:if test="${j == 3}">
				<div id="kotak2${j}" class="kotak2"><h3>FDIS vs Supply</h3></div>
			</c:if>
			<c:if test="${j == 4}">
				<div id="kotak2${j}" class="kotak2"><h3>FDIS Final</h3></div>
			</c:if>
			<c:if test="${j == 5}">
				<div id="kotak2${j}" class="kotak2"><h3>Additional FDIS</h3></div>
			</c:if>
			<c:if test="${j == 6}">
				<div id="kotak2${j}" class="kotak2"><h3>FDIS Final Report</h3></div>
			</c:if>
		</c:if>
		<c:if test="${j > browseList.LAST_RUN}">
			<c:if test="${j == 1}">
				<div id="kotak"><h3>Next Process for Add Vendor</h3></div>
			</c:if>
			<c:if test="${j == 2}">
				<div id="kotak"><h3>Material Usage Planning</h3></div>
			</c:if>
			<c:if test="${j == 3}">
				<div id="kotak"><h3>FDIS vs Supply</h3></div>
			</c:if>
			<c:if test="${j == 4}">
				<div id="kotak"><h3>FDIS Final</h3></div>
			</c:if>
			<c:if test="${j == 5}">
				<div id="kotak"><h3>Additional FDIS</h3></div>
			</c:if>
			<c:if test="${j == 6}">
				<div id="kotak"><h3>FDIS Final Report</h3></div>
			</c:if>
		</c:if>
	<c:set var="j" value="${j+1}"/>	
	<% } %>
</div>
</fieldset>
<script>
	$("#kotak21").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/DetailForBox1/?docNo="+$("#docNo").val());
	});
	$("#kotak22").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/DetailForBox2/?docNo="+$("#docNo").val());
	});
	$("#kotak23").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/DetailForBox3/?docNo="+$("#docNo").val());
	});
	$("#kotak24").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/RemainingMaterials/?docNo="+$("#docNo").val());
	});
	$("#kotak25").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/FinalForRemainingMaterial/?docNo="+$("#docNo").val());
	});
	$("#kotak26").click(function(e){
		console.log(e.currentTarget.childNodes[0].innerHTML);
		window.open("/MOA4/PUR-CO/ReportFinal/?docNo="+$("#docNo").val());
	});
</script>