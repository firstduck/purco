<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
body{
	overflow:hidden;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
</style>
<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>

<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>
<br>

<fieldset>
	<legend>Browse</legend>
<kendo:grid name="gridBrowse" id="gridBrowse" pageable="true" groupable="false">
	    <kendo:grid-columns>
			<kendo:grid-column title="DOCNO" field="DOCNO" template="#=linkRef(DOCNO)#"/>
            <kendo:grid-column title="CREATED DATE" field="CREATED_DATE" template= '#= kendo.toString(kendo.parseDate(CREATED_DATE), "dd/MM/yyyy")#'/>
            <kendo:grid-column title="CREATED BY" field="CREATED_BY"/>
            <kendo:grid-column title="LAST RUN" field="LAST_RUN"/>
        </kendo:grid-columns>
        <kendo:dataSource pageSize="10" data="${browseList}"></kendo:dataSource>
</kendo:grid>
</fieldset>
<script>
function linkRef(docNo){
	return '<a href="/MOA4/PUR-CO/DetailBrowse/?docNo='+docNo+'">'+docNo+'</a>';
}
</script>