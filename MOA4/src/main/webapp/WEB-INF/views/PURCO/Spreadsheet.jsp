<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
	body{
		overflow: hidden;
	}
	p.b {
	    font: 16px verdana;
	    margin-bottom: auto;
	    color: gray;
	}
	tr{
		height: 30px;
	}
	.hide{
		display: none;
	}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<legend>Purchasing Confirmation Spreadsheet</legend>
	
	<div id="formPilih">
		<div style="margin-left:5px;">
			<table>
				<tr>
					<td><div class="display-for-form">Year</div></td>
					<td>:
						<kendo:dropDownList  id="year" name="year" style="margin-left:5px;">
							<kendo:dataSource data="${year}"></kendo:dataSource>
						</kendo:dropDownList >
					</td>
				</tr>
				<tr>
					<td><div class="display-for-form">Month</div></td>
					<td>:
						<kendo:dropDownList  id="month" name="month" style="margin-left:5px;">
							<kendo:dataSource data="${month}"></kendo:dataSource>
						</kendo:dropDownList >
					</td>
				</tr>
				<tr>
					<td><div class="display-for-form">Company Name</div></td>
					<td>:
						<kendo:dropDownList  id="companyName" name="companyName" style="margin-left:5px;">
							<kendo:dataSource data="${companyName}"></kendo:dataSource>
						</kendo:dropDownList >
					</td>
				</tr>
			</table>
		</div>
		
		<br>
		<input type="button" class="k-button" id="sbmtBtn" value="Submit" onclick="ajaxSheet()"/>
	</div>
</fieldset>

<form method="POST" id="formtest" action="/MOA4/PUR-CO/SpreadSheet/SaveData" >
	<div id="spreadsheet" style="width: 100%"></div><br>
</form>

<div class="hide" id="btnNext">
	<input type="button" class="k-button" id="btnProcessNext" value="Process"/>
</div>
<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"  /></div>


 
<script>
	$(document).ajaxStart(function () {
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});
	
	$(document).ajaxStop(function () {
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	
	
	$("#btnProcessNext").click(function(){
		window.open("/MOA4/PUR-CO/LackingMaterials/?year="+$("#year").val()+"&month="+$("#month").val());
	});
	
	function ajaxSheet(){
		var year = $("#year").val();
		var month = $("#month").val();
		var companyName = $("#companyName").val();
		
		$("#spreadsheet").empty();
		$("#spreadsheet").show();
		
		var dataSource = new kendo.data.DataSource({
		    transport: {
		      read:  {
		        url: "/MOA4/PUR-CO/AjaxSpreadsheet?year="+year+"&month="+month+"&companyName="+companyName+"&type=PODetail",
		        dataType: "json"
		      }
		    }
		  });
		
		var dataSource2 = new kendo.data.DataSource({
		    transport: {
			    read:  {
			      url: "/MOA4/PUR-CO/AjaxSpreadsheet/?year="+year+"&month="+month+"&companyName="+companyName+"&type=PO",
			      dataType: "json"
			    }
			}
		});
		var dataSource3 = new kendo.data.DataSource({
		    transport: {
			    read:  {
			      url: "/MOA4/PUR-CO/AjaxSpreadsheet/?year="+year+"&month="+month+"&companyName="+companyName+"&type=PurchaseConfirm",
			      dataType: "json"
			    }
			}
		});
		var dataSource4 = new kendo.data.DataSource({
		    transport: {
			    read:  {
			      url: "/MOA4/PUR-CO/AjaxDataSpreadsheet/?year="+year+"&month="+month+"&companyName="+companyName+"&type=DataConfirm",
			      dataType:"json"
			    }
			},
		});
		var dataSource5 = new kendo.data.DataSource({
		    transport: {
			    read:  {
			      url: "/MOA4/PUR-CO/AjaxDataSpreadsheet/?year="+year+"&month="+month+"&companyName="+companyName+"&type=DataPO",
			      dataType:"json"
			    }
			},
		});
		var dataSource6 = new kendo.data.DataSource({
		    transport: {
			    read:  {
			      url: "/MOA4/PUR-CO/AjaxDataSpreadsheet/?year="+year+"&month="+month+"&type=DataPOAndConfirm",
			      dataType:"json"
			    }
			},
		});
		
		$("#spreadsheet").kendoSpreadsheet({
			rows:3000,
		  	  columns: 100,
		 	  toolbar: false,
              sheets: [{
                  name: "Plan Order Detail",
                  rows: [{
                      height: 40,
                      cells: [
                      {
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                          enable:false
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      }]
                  }],
                  columns: [
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 }
                  ]
              },
              {
                  name: "Plan Order",
                  rows: [{
                      height: 40,
                      cells: [
                      {
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                          enable:false
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      }]
                  }],
                  columns: [
                      { width: 145 },
                      { width: 315 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 200 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 }
                  ]
              },{
                  name: "Purchase Confirm",
                  rows: [{
                      height: 40,
                      cells: [
                      {
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                          enable:false
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      },{
                          bold: "true",
                          background: "#9c27b0",
                          textAlign: "center",
                          color: "white",
                      }]
                  }],
                  columns: [
                      { width: 315 },
                      { width: 315 },
                      { width: 315 },
                      { width: 145 },
                      { width: 145 },
                      { width: 200 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 },
                      { width: 145 }
                  ]
              },{
                  name: "Data Confirm",
                  rows: [{height: 40}],
                  columns: [
                            { width: 145},
                            { width: 315 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 }
                        ]
              },{
                  name: "Data Plan Order",
                  rows: [{height: 40}],
                  columns: [
                            { width: 145},
                            { width: 315 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 },
                            { width: 145 }
                        ]
              },{
                  name: "Data Plan Order and Confirm",
                  rows: [{height: 40}],
                  columns: [
                            { width: 160},
                            { width: 315 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 },
                            { width: 160 }
                        ]
              }]
		});
		
		var spreadSheet = $("#spreadsheet").data("kendoSpreadsheet");
		  
		dataSource.fetch(function(){
			  var sheet=spreadSheet.sheetByName("Plan Order Detail");
			  sheet.setDataSource(dataSource,["PLANORDER_NUM",
											  "MATERIALCODE",
		                                	  "TOTALQTY",
		                                	  "UOM",
		                                	  "STARTDATE",
		                                	  "ENDDATE",
		                                	  "PLANTCODE"]);
			  
			  var rows=dataSource.total()+1;
			  var range = sheet.range("A1:B"+rows);
			  var rangeStyle = sheet.range("A2:B"+rows);
			  var range2=sheet.range("D1:G"+rows);
			  var range3=sheet.range("C1:C"+rows);
			  var rangeStyle2=sheet.range("D2:G"+rows);
			  var enable = range.enable();
			  var enable2 = range2.enable();
			  
			  rangeStyle.color("black");
			  rangeStyle2.color("black");
			  range.enable(!enable);
			  range2.enable(!enable2);
			  range3.format("#,#");
			  range.bold(true);
			  range2.bold(true);
			  range3.bold(true);
			  
			  sheet.range("A1:G1").textAlign("center");
			  sheet.range("A1:G1").verticalAlign("center");
		});
		
		dataSource2.fetch(function(){
			  var sheet2=spreadSheet.sheetByName("Plan Order");
			  sheet2.setDataSource(dataSource2,["MATERIALCODE",
												"MATERIALNAME",
			                                	"TOTALQTY",
			                                	"UOM",
			                                	"COMPANYCODE",
			                                	"COMPANYNAME",
			                                	"STARTWEEK","MONTH","STARTYEAR"]);
			  
			  var rows2=dataSource2.total()+1;
			  var range = sheet2.range("A1:B"+rows2);
			  var rangeStyle = sheet2.range("A2:B"+rows2);
			  var range2=sheet2.range("D1:I"+rows2);
			  var range3=sheet2.range("C1:C"+rows2);
			  var rangeStyle2=sheet2.range("D2:I"+rows2);
			  var enable = range.enable();
			  var enable2 = range2.enable();
			  
			  
			  rangeStyle.color("black");
			  rangeStyle2.color("black");
			  range.enable(!enable);
			  range2.enable(!enable2);
			  range3.format("#,#");
			  range.bold(true);
			  range2.bold(true);
			  range3.bold(true);
			  
			  sheet2.range("A1:I1").textAlign("center");
			  sheet2.range("A1:I1").verticalAlign("center");
		});
		
		dataSource3.fetch(function(){
			var sheet3=spreadSheet.sheetByName("Purchase Confirm");
			sheet3.setDataSource(dataSource3,["DOCNO","MATERIALCODE","MATERIALNAME","QTYCONTRACT","UOM","OUTSTANDINGQTY","VALIDFROM2","VALIDUNTIL2","STARTWEEK","ENDWEEK","DIFFWEEK","QTYPERWEEK"]);
		  	
		  	var rows3=dataSource3.total()+1;
		  	var range = sheet3.range("A1:L"+rows3).enable(false);
		  	var range3=sheet3.range("D1:D"+rows3);
		  	var range4=sheet3.range("F1:F"+rows3);
		  	var range5=sheet3.range("L1:L"+rows3);
		  	var rangeStyle = sheet3.range("B2:L"+rows3);
		  	
		  	
		  	if (rangeStyle.isSortable()) {
				rangeStyle.sort();
	        }
		  	rangeStyle.color("black");
		  	rangeStyle.bold(true);
		  	
		  	sheet3.range("A2:A"+rows3).color("black");
		  	sheet3.range("A2:A"+rows3).bold(true);
			
			range3.format("#,#");
			range4.format("#,#");
			range5.format("#,#.###");
			
			sheet3.range("A1:L1").textAlign("center");
			sheet3.range("A1:L1").verticalAlign("center");
		});
		  
		dataSource4.fetch(function(){
			var sheet4=spreadSheet.sheetByName("DATA CONFIRM");
			sheet4.setDataSource(dataSource4);
			
			var newSheet4 = sheet4.toJSON();
			var lengthCell=newSheet4.rows[0].cells.length;
			var arrCol=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
			var range = sheet4.range("A2:"+arrCol[lengthCell-1]+""+dataSource4.total()+1);
			var range2 = sheet4.range("C2:"+arrCol[lengthCell-1]+""+dataSource4.total()+1);
			
			
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").background("#9c27b0");
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").color("white");
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").verticalAlign("center");
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").textAlign("center");
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").bold(true);
			sheet4.range("A1:"+arrCol[lengthCell-1]+"1").enable(false);
			
			range2.textAlign("right");
			range2.format("#,#.###");
			range.enable(false);
			range.color("black");
			range.bold(true);
			if (range.isSortable()) {
	            range.sort();
	        }
		});
		
		dataSource5.fetch(function(){
			var sheet5=spreadSheet.sheetByName("DATA PLAN ORDER");
			sheet5.setDataSource(dataSource5);
			
			var newSheet5 = sheet5.toJSON();
			
			try{
				var lengthCell=newSheet5.rows[0].cells.length;
				var arrCol=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").background("#9c27b0");
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").color("white");
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").verticalAlign("center");
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").textAlign("center");
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").bold(true);
				sheet5.range("A1:"+arrCol[lengthCell-1]+"1").enable(false);
				var range = sheet5.range("A2:"+arrCol[lengthCell-1]+""+dataSource5.total()+1);
				var range2 = sheet5.range("C2:"+arrCol[lengthCell-1]+""+dataSource5.total()+1);
				range2.textAlign("right");
				range2.format("#,#.###");
				range.enable(false);
				range.color("black");
				range.bold(true);
				if (range.isSortable()) {
		            range.sort();
		        }
			}catch(e){
				alert("Data not found!");
				$("#btnNext").addClass("hide");
				$("#spreadsheet").hide();
				$("#wait").hide();
			}
		});
		
		dataSource6.fetch(function(){
			var sheet6=spreadSheet.sheetByName("DATA PLAN ORDER and CONFIRM");
			sheet6.setDataSource(dataSource6);
			
			var newSheet6 = sheet6.toJSON();
			var lengthCell=newSheet6.rows[0].cells.length;
			var arrCol=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD","BE","BF","BG"];
			var valueCurrCol;
			var range = sheet6.range("A2:"+arrCol[lengthCell-1]+""+dataSource6.total()+1);
			var range2 = sheet6.range("C2:"+arrCol[lengthCell-1]+""+dataSource6.total()+1);
			
			sheet6.range("A1:B1").background("#9C27B0");
			
			for(var i = 2;i<lengthCell;i++){
				valueCurrCol=sheet6.range(arrCol[i]+"1").value();
				
				if(valueCurrCol.lastIndexOf("CONFIRM")!= -1)
					sheet6.range(arrCol[i]+"1").background("#7986CB");
				else if(valueCurrCol.lastIndexOf("PLAN")!= -1)
					sheet6.range(arrCol[i]+"1").background("#9575CD");
				else
					sheet6.range(arrCol[i]+"1").background("#BA68C8");
			}
			
			sheet6.range("A1:"+arrCol[lengthCell-1]+"1").color("white");
			sheet6.range("A1:"+arrCol[lengthCell-1]+"1").verticalAlign("center");
			sheet6.range("A1:"+arrCol[lengthCell-1]+"1").textAlign("center");
			sheet6.range("A1:"+arrCol[lengthCell-1]+"1").bold(true);
			sheet6.range("A1:"+arrCol[lengthCell-1]+"1").enable(false);
			
			range2.textAlign("right");
			range2.format("#,#.###");
			range.enable(false);
			range.bold(true);
			range.color("black");
			if (range.isSortable()) {
	            range.sort();
	        }
			
			for(var j=0;j<dataSource6.total()+1;j++){
				for(var k=2;k<lengthCell;k++){
					if(sheet6.range(arrCol[k]+""+j).value() < 0){
						sheet6.range(arrCol[k]+""+j).background("#ef9a9a");
					}
				}
			}
		
			$("#btnNext").removeClass("hide");
		});
	}
</script>
