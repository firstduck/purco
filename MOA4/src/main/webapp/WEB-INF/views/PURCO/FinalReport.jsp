<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
body{
	overflow:hidden;
}
.hide{
	display:none;
}
#chooseWeek{
	color:black;
}
.header{
	font-size: 16px;
}
#processAllFdis{
	width:800px;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
</style>
<br><br><br><br>
<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<div class="display-for-form">No. Document  : <label id="docno" class="text">${docNo}</label><input type='hidden' id="docNo" name="docNo" value="${docNo}"></div><br>
<div style="margin-left:5px;">
		<table>
			<tr>
				<td><div class="display-for-form">Year</div></td>
				<td>:
						<kendo:comboBox id="year" name="year" style="margin-left:5px;">
							<kendo:dataSource data="${year}"></kendo:dataSource>
						</kendo:comboBox>
				</td>
			</tr>
		
	
			<tr>
				<td><div class="display-for-form">Week</div></td>
				<td>:
						<kendo:comboBox id="week" name="week" style="margin-left:5px;">
							<kendo:dataSource data="${week}"></kendo:dataSource>
						</kendo:comboBox>
				</td>
			</tr>
		</table>
</div>
<br><br>
<input type="button" class="k-button" id="sbmtBtn" value="Submit" onclick="ajaxGrid()"/>
<br><br>

<div class="hide" id="processAllFdis">
	<div class="display-for-form"><label id="remainingAllMaterial">Producible Product</label><br><br><label class="header">Week :</label><label class="header" id="chooseWeek"></label></div><br>
	<kendo:grid name="gridAllFdis" id="gridAllFdis" pageable="true" groupable="false" selectable="true" filterable="false" editable="false">
		    <kendo:grid-columns>
				<kendo:grid-column title="FINISH GOOD CODE" field="finishGoodCode"/>
	            <kendo:grid-column title="FINISH GOOD NAME" field="finishGoodName"/>
	            <kendo:grid-column title="FINISH GOOD QUANTITY" field="finishGoodQty" template= '<div style="text-align:right;">#= kendo.toString(finishGoodQty, "n0") #</div>'/>
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="10"></kendo:dataSource>
	</kendo:grid>
</div>
<br><br>
<script>
function ajaxGrid(){
	$("#processAllFdis").removeClass("hide");
	$("#chooseWeek").html(" "+$("#week").val());
	$.ajax({
		type:'GET',
		url:'/MOA4/PUR-CO/AjaxReportFinal/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
		success:function(data){
			$("#gridAllFdis").data("kendoGrid").dataSource.data(data);
		}
	});
}
</script>