<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<style>
body{
	overflow:hidden;
}
.hide{
	display:none;
}
#fdis , #chooseWeek{
	color:black;
}
#remainingAllMaterial{
	font-size: 20px;
}
.header{
	font-size: 16px;
}
#fdisFromRemainingMaterial{
	width:800px;
}
#fdisMaterial{
	width:800px;
}
.edit{
	display:none;
}
.hideCheck{
	display:none;
}
#ceklist{
	color:#33691E;
	font-size: 25px;
}
.hideWrong{
	display:none;
}
.hide{
	display:none;
}
#wrong{
	color:#d50000;
	font-size: 25px;
}
.numbers{
	text-align: right;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<form action="/MOA4/PUR-CO/SaveDatabase" method="POST" id="myForm"  name="myForm" target="hiddenFrame">
		<iframe name="hiddenFrame" style="display: none;"></iframe>
		<table>
			<tr style="margin-bottom: 50px"> 
			   	<td style="width: 200px" class="display-for-form">Document Number</td>
			   	<td class="display-for-form" style="width: 8px">:</td>
			   	<td style="height:20px">
			   		<label id="docno" class="text">${docNo}</label><input type='hidden' id="docNo" name="docNo" value="${docNo}">
			   	</td>
			</tr>
			<tr>
				<td><div class="display-for-form">Year</div></td>
				<td class="display-for-form" style="width: 8px">:</td>
				<td>
					<kendo:dropDownList id="year" name="year" style="margin-left:5px;">
						<kendo:dataSource data="${year}"></kendo:dataSource>
					</kendo:dropDownList>
				</td>
			</tr>
			<tr>
				<td><div class="display-for-form">Week</div></td>
				<td class="display-for-form" style="width: 8px">:</td>
				<td>
					<kendo:dropDownList id="week" name="week" style="margin-left:5px;">
						<kendo:dataSource data="${week}"></kendo:dataSource>
					</kendo:dropDownList>
				</td>
			</tr>
		</table>
	</form>
<br>
<input type="button" class="k-button" id="sbmtBtn" value="Submit" onclick="ajaxGrid()"/>
</fieldset>
<br><br>

<div class="hide" id="processAllMaterial">
	<div class="display-for-form"><label id="remainingAllMaterial">All Remaining Material</label><br><br><label class="header">Week :</label><label class="header" id="chooseWeek"></label></div><br>
	<kendo:grid name="gridAllMaterial" id="gridAllMaterial" pageable="true" groupable="false" selectable="true" filterable="false" editable="false">
		    <kendo:grid-columns>
				<kendo:grid-column title="MATERIAL CODE" field="MATERIALCODE"/>
	            <kendo:grid-column title="MATERIAL NAME" field="MATERIALNAME"/>
	            <kendo:grid-column title="REMAINING MATERIAL QUANTITY" field="REMAININGQUANTITY" template= '<div style="text-align:right;">#= kendo.toString(REMAININGQUANTITY, "n0") #</div>'/>
	            <kendo:grid-column title="BOM UOM" field="BOMUOM"/>
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="10"></kendo:dataSource>
	</kendo:grid>
</div>
<br><br>
<div class="hide" id="fdisFromRemainingMaterial">
	<div class="display-for-form">Producible Product From Remaining Material</div>
	
		<kendo:grid name="gridAllFdis" id="gridAllFdis" pageable="true" groupable="false" selectable="true" editable="true" filterable="false" onclick="onClick()">
			    <kendo:grid-editable mode="inline"/>
			    <kendo:grid-columns>
					<kendo:grid-column title="FINISH GOOD CODE" field="FINISHGOODCODE"/>
		            <kendo:grid-column title="FINISH GOOD NAME" field="FINISHGOODNAME"/>
		            <kendo:grid-column title="FINISH GOOD QUANTITY" field="FINISHGOODQTY" template= '<div style="text-align:right;">#= kendo.toString(FINISHGOODQTY, "n0") #</div>'/>
		            <kendo:grid-column title="&nbsp;"  width="100px">
	          			<kendo:grid-column-command>
		            		<kendo:grid-column-commandItem className="edit" name="edit"/>
		       			</kendo:grid-column-command>
		       		</kendo:grid-column>
		        </kendo:grid-columns>
		        
		         <kendo:grid-edit>
					<script>
						function grid_edit(e) {
							console.log(e.container);
							if(e.container.find("input[name=FINISHGOODQTY]").val().trim()==""||e.container.find("input[name=FINISHGOODQTY]").val().trim()==null){
								
							}
							else{
								var currQty = e.container.find("input[name=FINISHGOODQTY]").val();
								console.log(currQty);
								var dataRemainingMaterial = $("#gridAllMaterial").data("kendoGrid").dataSource.data();
							 	var dataMaterial = $("#gridMaterial").data("kendoGrid").dataSource.data();
							 	var kendo1 = $("#gridAllMaterial").data("kendoGrid");
							 	
							 	var lenMaterial = $("#gridMaterial").data("kendoGrid").dataSource.view().length;
							 	var lenRemainingMaterial = $("#gridAllMaterial").data("kendoGrid").dataSource.view().length;
							 	
								for(var i = 0;i<lenMaterial;i++){
									for(var j = 0;j<lenRemainingMaterial;j++){
										if(dataMaterial[i].MATERIALCODE == dataRemainingMaterial[j].MATERIALCODE){
											var kali = parseInt(currQty) * parseInt(dataMaterial[i].BOMQUANTITY);
											console.log(j);
											kendo1.select("tr:eq("+j+")");
											var grid1 = kendo1.dataItem(kendo1.select());
											dataRemainingMaterial[j].REMAININGQUANTITY = parseInt(dataRemainingMaterial[j].REMAININGQUANTITY) + kali;
											grid1.set("REMAININGQUANTITY",dataRemainingMaterial[j].REMAININGQUANTITY);
											kendo1.refresh();
											
										}
									}
								}
							}
							e.container.find(".k-button.k-button-icontext.k-grid-cancel").hide();
							e.container.find(".k-button.k-button-icontext.k-primary.k-grid-update").click(function() {
							 	var dataRemainingMaterial = $("#gridAllMaterial").data("kendoGrid").dataSource.data();
							 	var dataMaterial = $("#gridMaterial").data("kendoGrid").dataSource.data();
							 	var kendo = $("#gridAllFdis").data("kendoGrid");
							 	var kendo1 = $("#gridAllMaterial").data("kendoGrid");
							 	
								var grid = kendo.dataItem(kendo.select());
								var row = kendo.select().index();
							 	var lenMaterial = $("#gridMaterial").data("kendoGrid").dataSource.view().length;
							 	var lenRemainingMaterial = $("#gridAllMaterial").data("kendoGrid").dataSource.view().length;
								var isUpdate = true;
								e.container.find("input[name=success]").addClass("hide");
								if(e.container.find("input[name=FINISHGOODQTY]").val().trim()==""||e.container.find("input[name=FINISHGOODQTY]").val().trim()==null){
									
									isUpdate = false;
								}							
								if(isUpdate==false)
								{
									alert("quantity harus diisi");
									e.container.find("input[name=FINISHGOODQTY]").val("");
								}
								else{
									
									var flag = 1;
									for(var i = 0;i<lenMaterial;i++){
										for(var j = 0;j<lenRemainingMaterial;j++){
											if(dataMaterial[i].MATERIALCODE == dataRemainingMaterial[j].MATERIALCODE){
												var kali = parseInt(grid.FINISHGOODQTY) * parseInt(dataMaterial[i].BOMQUANTITY);
												console.log(kali);
												if(kali > parseInt(dataRemainingMaterial[i].REMAININGQUANTITY)){
													flag = 0;
													break;
												}
											}
										}
									}
									
									if(flag == 1){
										alert('Material '+grid.FINISHGOODNAME+' dapat dibuat');
										for(var i = 0;i<lenMaterial;i++){
											for(var j = 0;j<lenRemainingMaterial;j++){
												if(dataMaterial[i].MATERIALCODE == dataRemainingMaterial[j].MATERIALCODE){
													console.log(dataMaterial[i].MATERIALCODE + " = " + dataRemainingMaterial[j].MATERIALCODE);
													var kali = parseInt(grid.FINISHGOODQTY) * parseInt(dataMaterial[i].BOMQUANTITY);
													console.log(kali);
													console.log(j);
													kendo1.select("tr:eq("+j+")");
													var grid1 = kendo1.dataItem(kendo1.select());
													dataRemainingMaterial[j].REMAININGQUANTITY = parseInt(dataRemainingMaterial[j].REMAININGQUANTITY) - kali;
													grid1.set("REMAININGQUANTITY",dataRemainingMaterial[j].REMAININGQUANTITY);
													kendo1.refresh();
												}
											}
										}
									}
									else{
										alert('Material tidak cukup untuk '+grid.FINISHGOODNAME);
										e.container.find("input[name=FINISHGOODQTY]").val("");
									}
								}
							});
						}
					</script>
				</kendo:grid-edit>
		        <kendo:dataSource pageSize="10">
		        	<kendo:dataSource-schema> 
		              	<kendo:dataSource-schema-model>
			              	<kendo:dataSource-schema-model-fields>
			              		<kendo:dataSource-schema-model-field name="FINISHGOODCODE" editable="false">
			              		</kendo:dataSource-schema-model-field>
			              		<kendo:dataSource-schema-model-field name="FINISHGOODNAME" editable="false">
			              		</kendo:dataSource-schema-model-field>
			              		<kendo:dataSource-schema-model-field name="FINISHGOODQTY" type="number">
			              			<kendo:dataSource-schema-model-field-validation required="true" min="0" />
			              		</kendo:dataSource-schema-model-field>
			            	</kendo:dataSource-schema-model-fields>
		            	</kendo:dataSource-schema-model>           	          	
	         		</kendo:dataSource-schema>
		        </kendo:dataSource>
		</kendo:grid>
		<br>
		<div><input type="button" class="k-button" id="std" value="Save To Database"></div>
		<textarea class="hide" id='jsonProcess' name='jsonProcess'></textarea>
	
</div>

<br><br>
<div class="hide" id="fdisMaterial">
<div class="display-for-form">BOM</div>
<kendo:grid name="gridMaterial" id="gridMaterial" groupable="false" selectable="false" filterable="false">
	 <kendo:grid-columns>
		   <kendo:grid-column title="MATERIAL CODE" field="MATERIALCODE"/>
           <kendo:grid-column title="MATERIAL NAME" field="MATERIALNAME"/>
           <kendo:grid-column title="QUANTITY" field="BOMQUANTITY" template= '<div style="text-align:right;">#= kendo.toString(BOMQUANTITY, "n0") #</div>'/>
     </kendo:grid-columns>
</kendo:grid>     
</div>
<div class="hide" id="btnNext"><input type="button" class="k-button" id="next" value="Next"></div>
<script>
	$("#next").click(function(){
		window.open('/MOA4/PUR-CO/ReportFinal/?docNo='+$("#docNo").val());
	});
	function ajaxGrid(){
		$("#fdisFromRemainingMaterial").removeClass("hide");
		$("#btnNext").removeClass("hide");
		$("#processAllMaterial").removeClass("hide");
		$("#chooseWeek").html(" "+$("#week").val());
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxAllRemainingMaterial2/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
			success:function(data){
				$("#gridAllMaterial").data("kendoGrid").dataSource.data(data);
			}
		});
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxGetFdisFromRemainingMaterial/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
			success:function(data){
				$("#gridAllFdis").data("kendoGrid").dataSource.data(data);
			}
		});
	}
	function onClick(){
		$(".edit").show();
		$("#fdisMaterial").removeClass("hide");
		$("#btnNext").removeClass("hide");
		var kendo = $("#gridAllFdis").data("kendoGrid");
		var grid = kendo.dataItem(kendo.select());
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxGetMaterialBom/?finishGoodCode='+grid.FINISHGOODCODE+'&week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
			success:function(data){
				$("#gridMaterial").data("kendoGrid").dataSource.data(data);
			}
		});
	}
	$("#std").click(function(){
		var kendo = $("#gridAllFdis").data("kendoGrid");
		var processDataSource = kendo.dataSource.data();
		var json = JSON.stringify(processDataSource);
		$("#jsonProcess").val(json);
		console.log($("#jsonProcess").val());
		$("#myForm").submit();
		alert("masuk");
	});
	
</script>