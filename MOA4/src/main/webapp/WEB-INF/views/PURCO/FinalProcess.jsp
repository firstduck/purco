<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
body{
	overflow:hidden;
}
.hide{
	display:none;
}
#fdis , #chooseWeek{
	color:black;
}
.header{
	font-size: 16px;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
tr{
	height: 30px;
}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<table>
		<tr style="margin-bottom: 50px"> 
		   	<td style="width: 200px" class="display-for-form">Document Number</td>
		   	<td class="display-for-form" style="width: 8px">:</td>
		   	<td style="height:20px">
		   		<label id="docno" class="text">${docNo}</label><input type='hidden' id="docNo" name="docNo" value="${docNo}">
		   	</td>
		</tr>
		<tr>
			<td><div class="display-for-form">Year</div></td>
			<td class="display-for-form" style="width: 8px">:</td>
			<td>
				<kendo:dropDownList id="year" name="year" style="margin-left:5px;">
					<kendo:dataSource data="${year}"></kendo:dataSource>
				</kendo:dropDownList>
			</td>
		</tr>
		<tr>
			<td><div class="display-for-form">Week</div></td>
			<td class="display-for-form" style="width: 8px">:</td>
			<td>
				<kendo:dropDownList id="week" name="week" style="margin-left:5px;">
					<kendo:dataSource data="${week}"></kendo:dataSource>
				</kendo:dropDownList>
			</td>
		</tr>
	</table>
	
	<br>
	<input type="button" class="k-button" id="sbmtBtn" value="Submit" onclick="ajaxGrid()"/>
</fieldset>

<br>
<div class="hide" id="processAllMaterial">
	<fieldset>
		<legend>Remaining Materials for Week: <label id="chooseWeek"></label></legend>
			<kendo:grid name="gridAllMaterial" id="gridAllMaterial" pageable="true" groupable="false" selectable="true" editable="false" filterable="false">
				<kendo:grid-columns>
					<kendo:grid-column title="MATERIAL CODE" field="MATERIALCODE"/>
		            <kendo:grid-column title="MATERIAL NAME" field="MATERIALNAME"/>
		            <kendo:grid-column title="REMAINING MATERIAL QUANTITY" field="REMAININGQUANTITY" template= '<div style="text-align:right;">#= kendo.toString(REMAININGQUANTITY, "n0") #</div>'/>
		            <kendo:grid-column title="BOM UOM" field="BOMUOM"/>
		        </kendo:grid-columns>
		        <kendo:dataSource pageSize="10"></kendo:dataSource>
			</kendo:grid>
		
			<br>
			<div class="display-for-form">List of finish goods (Click for detail remaining materials)</div>
			<kendo:grid name="gridProcess" id="gridProcess" pageable="true" groupable="false" selectable="true" editable="false" filterable="false" onclick="onClick()">
			    <kendo:grid-columns>
					<kendo:grid-column title="FINISH GOOD CODE" field="finishGoodCode"/>  
		            <kendo:grid-column title="FINISH GOOD NAME" field="finishGoodName"/>   
		          	<kendo:grid-column title="PLANNED FINISH GOOD QUANTITY" field="plannedFinishGood" template= '<div style="text-align:right;">#= kendo.toString(plannedFinishGood, "n0") #</div>'/>
		          	<kendo:grid-column title="MAXIMUM FINISH GOOD QUANTITY" field="maxFinishGood" template= '<div style="text-align:right;">#= kendo.toString(maxFinishGood, "n0") #</div>'/>
		          	<kendo:grid-column title="FINAL FINISH GOOD QUANTITY" field="finalFinishGood" template= '<div style="text-align:right;">#= kendo.toString(finalFinishGood, "n0") #</div>'/>
		        </kendo:grid-columns>
		        <kendo:dataSource pageSize="10"></kendo:dataSource>
			</kendo:grid>
	</fieldset>
</div>

<br>
<div class="hide" id="processMaterial">
	<fieldset>
		<legend>Remaining Materials for Finish Good: <label id="fdis"></label></legend>
		<kendo:grid name="gridMaterial" id="gridMaterial" pageable="true" groupable="false" editable="false" filterable="false">
			<kendo:grid-columns>
				<kendo:grid-column title="MATERIAL CODE" field="materialCode"/>
	            <kendo:grid-column title="MATERIAL NAME" field="materialName"/>
	            <kendo:grid-column title="BOM QUANTITY" field="bomQty"/>
	            <kendo:grid-column title="BOM UOM" field="bomUom"/>
	            <kendo:grid-column title="REMAINING FINISH GOOD QUANTITY" field="remainingFinishGoodQty" template= '<div style="text-align:right;">#= kendo.toString(remainingFinishGoodQty, "n0") #</div>'/>
	            <kendo:grid-column title="REMAINING BOM QUANTITY" field="remainingBomQty" template= '<div style="text-align:right;">#= kendo.toString(remainingBomQty, "n0") #</div>'/>
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="10"></kendo:dataSource>
		</kendo:grid>
	</fieldset>
</div>

<div class="hide" id="btnNext">
	<input type="button" class="k-button" id="nextProcess" value="Next"/>
</div>

<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"/></div>

<script>
	$(document).ajaxStart(function () {
		dimStart();
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});
	
	$(document).ajaxStop(function () {
		dimStop();
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	var popupIsOpen = false;
	function dimStart(){
		$('#dimScreen').prop('hidden', false);
	}

	function dimStop(){
		if(!popupIsOpen){
			$('#dimScreen').prop('hidden', true);
		}
	}
	function ajaxGrid(){
		$("#btnNext").removeClass("hide");
		$("#processAllMaterial").removeClass("hide");
		$("#processMaterial").addClass("hide");
		$("#chooseWeek").html(" "+$("#week").val());
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxAllRemainingMaterial/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
			success:function(data){
				$("#gridAllMaterial").data("kendoGrid").dataSource.data(data);
				$.ajax({
					type:'GET',
					url:'/MOA4/PUR-CO/AjaxForGridFinal/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val(),
					success:function(data){
						$("#gridProcess").data("kendoGrid").dataSource.data(data);
						
					}
				});
			}
		});
	}
	function onClick(){
		var kendo = $("#gridProcess").data("kendoGrid");
		var grid = kendo.dataItem(kendo.select());
		$("#processMaterial").removeClass("hide");
		$("#fdis").html(" "+grid.finishGoodName);
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxForGridMaterial/?week='+$("#week").val()+'&year='+$("#year").val()+'&docNo='+$("#docNo").val()+
				'&finishGoodCode='+grid.finishGoodCode+'&min='+grid.finalFinishGood,
			success:function(data){
				$("#gridMaterial").data("kendoGrid").dataSource.data(data);	
			}
		});
	}
	$("#nextProcess").click(function(){
		window.open("/MOA4/PUR-CO/FinalForRemainingMaterial/?docNo="+$('#docNo').val());
	});
</script>