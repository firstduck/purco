<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	legend{
		color:#d50000;
		font-size:20px;
		font-weight: bold;
	}
	.display-for-form{
		color:#d50000;
		font-size:12px;
		font-weight:bold;
		font-family: Verdana, Helvetica, Sans-Serif;
		
	}
	.text{
		color:black;
		font-size:12px;
		font-family: Verdana, Helvetica, Sans-Serif;
		
	}
	form input[type="button"] {
		background: rgb(213,0,0);
		-webkit-border-radius: 30px;
		-moz-border-radius: 30px;
		-ms-border-radius: 30px;
		-o-border-radius: 30px;
		border-radius: 30px;
		-webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-moz-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-ms-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-o-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		border: 1px solid #D69E31;
		color: #fff;
		cursor: pointer;
		float: left;
		font: bold 15px Helvetica, Arial, sans-serif;
		height: 35px;
		text-shadow: 0 1px 0 rgba(255,255,255,0.5);
		width: 120px;
	}
	form input[type="button"]:hover {
		background: rgba(255,82,82 ,1);
	}
	.hide{
		display:none;
	}
	tr{
		height: 30px;
	}
	p.b {
	    font: 16px verdana;
	    margin-top: 20px;
	    color: gray;
	}
	#mayoraHeader{
		width: 124px;
		height:37px;
	}
	#logout{
		margin-left: 10px;
		width: 20px;
		height:20px;
	}

</style>
<script src="/MOA4/resources/MOA2/js/jquery.min.js"></script>
<br><br><br><br><br><br><br><br>

<table>
	<tr>
		<td>
			<img id="mayoraHeader" src='/MOA4/resources/images/mayora_header.png'/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<legend>Welcome, ${nameLogin} <img id="logout" src='/MOA4/resources/images/logout-symbol.png' onclick="logout()"/></legend>
	<form action="/MOA4/PUR-CO/SavetoDetailVendor" id="detailForm" method="POST" target="hiddenFrame">
		<iframe name="hiddenFrame" style="display: none;"></iframe>
		<input type='hidden' id="vendorCode" name="vendorCode" value="-TEST-">
		<input type='hidden' id="nameLogin" name="nameLogin" value="${nameLogin}">
		<input type='hidden' id="docNo" name="docNo" value="${docNo}">
		<input type='hidden' id="materialCode" name="materialCode" value="${materialCode}">
		<input type='hidden' id="week" name="week" value="${week}">
		<input type='hidden' id="year" name="year" value="${year}">
		<input type='hidden' id="materialName" name="materialName" value="${materialName}">
		<input type='hidden' id="planOrderQty" name="planOrderQty" value="${planOrderQty}">
		
		<table>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Material Name</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px" class="text">
			   		${materialName}
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Year</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px" class="text">
			   		${year}
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Week</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px" class="text">
			   		${week}
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Plan Order Quantity</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px" class="text">
			   		${planOrderQty}
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Quantity</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px" class="text">
			   		<input type="number" id="quantity" name="quantity">
			   	</td>
			</tr>
			<tr> 
			   	<td colspan="3">
			   		<div class="display-for-form hide" id="fail">Quantity must be inputted or Quantity must greater than zero!</div>
					<div class="display-for-form hide" id="success">Success!&nbsp;Thank you for your participation!</div>
			   	</td>
			</tr>
		</table>
		
		<br>
		<div><input id="btnSubmit" type="button" value="Submit" /></div>
	</form>
</fieldset>

<script>
	$("#btnSubmit").click(function(){
		if($("#quantity").val() <= 0 || $("#quantity").val() == "" ){
			$("#fail").removeClass("hide");
		}
		else{
			$("#btnSubmit").addClass("hide");
			$("#success").removeClass("hide");
			$("#fail").addClass("hide");
			$("#detailForm").submit();
		}
	});
	function logout(){
		window.location.href = '/MOA4/PUR-CO/LoginFormForConfirm';
	}
</script>