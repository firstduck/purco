<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	legend{
		color:#d50000;
		font-size:20px;
		font-weight: bold;
	}
	.display-for-form{
		color:#d50000;
		font-size:12px;
		font-weight:bold;
		font-family: Verdana, Helvetica, Sans-Serif;
	}
	.text{
		color:black;
		font-family: Verdana, Helvetica, Sans-Serif;
	}
	input[type="button"] {
		background: rgb(213,0,0);
		-webkit-border-radius: 30px;
		-moz-border-radius: 30px;
		-ms-border-radius: 30px;
		-o-border-radius: 30px;
		border-radius: 30px;
		-webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-moz-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-ms-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-o-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		border: 1px solid #D69E31;
		color: #fff;
		cursor: pointer;
		float: left;
		font: bold 15px Helvetica, Arial, sans-serif;
		height: 35px;
		text-shadow: 0 1px 0 rgba(255,255,255,0.5);
		width: 120px;
	}
	input[type="button"]:hover {
		background: rgba(255,82,82 ,1);
	}
	.hide{
		display:none;
	}
	tr{
		height: 30px;
	}
	p.b {
	    font: 16px verdana;
	    margin-top: 20px;
	    color: gray;
	}
	#mayoraHeader{
		width: 124px;
		height:37px;
	}
	#logout{
		margin-left: 10px;
		width: 20px;
		height:20px;
	}
</style>

<table>
	<tr>
		<td>
			<img id="mayoraHeader" src='/MOA4/resources/images/mayora_header.png'/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
<legend>Welcome, ${lsVendor.vendorName} <img id="logout" src='/MOA4/resources/images/logout-symbol.png' onclick="logout()"/></legend>
	<table>
		<tr style="margin-bottom: 50px"> 
		   	<td style="width: 200px" class="display-for-form">Vendor Code</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.vendorCode}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Vendor Name</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.vendorName}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Email</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.vendorEmail}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Address</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.vendorAddress}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Country</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.country}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Owner</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.owner}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Headquarter</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.headquarter}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Total Employee</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.totalEmployee}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Status</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${lsVendor.status}
		   	</td>
		</tr>
	</table>
	
	<input type='hidden' id="vendorCode" name="vendorCode" value="${lsVendor.vendorCode}"/>
	<input type='hidden' id="vendorName" name="vendorName" value="${lsVendor.vendorName}"/>
	<input type='hidden' id="email" name="email" value="${lsVendor.vendorEmail}">
	<input type='hidden' id="address" name="address" value="${lsVendor.vendorAddress}">
	<input type='hidden' id="country" name="country" value="${lsVendor.country}">
	<input type='hidden' id="owner" name="owner" value="${lsVendor.owner}">
	<input type='hidden' id="hq" name="hq" value="${lsVendor.headquarter}">
	<input type='hidden' id="totalEmployee" name="totalEmployee" value="${lsVendor.totalEmployee}">
	<input type='hidden' id="status" name="status" value="${lsVendor.status}">

	<br>
	<div><input id="edit" type="button" value="Edit" onclick="editVendor()"/></div>
</fieldset>
<script>
function logout(){
	window.location.href = '/MOA4/PUR-CO/LoginFormForConfirm';
}

function editVendor(){
	window.location.href = '/MOA4/PUR-CO/EditFormVendor/?vendorCode='+document.getElementById("vendorCode").value;
}
/* $("#edit").click(function(){
	$("#editForm").submit();
}); */
</script>