<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	p.b {
	    font: 16px verdana;
	    margin-bottom: auto;
	    color: gray;
	}
	body{
		overflow:hidden;
	}
	#wrong{
		color:#d50000;
		font-size: 25px;
	}
	.hide{
		display:none;
	}
	tr{
		height: 30px;
	}
</style>

<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<form id="processFinal" target="_blank" method="POST" action="/MOA4/PUR-CO/RemainingMaterials">
		<table>
			<tr style="margin-bottom: 50px"> 
			   	<td style="width: 200px" class="display-for-form">Document Number</td>
			   	<td class="display-for-form" style="width: 8px">:</td>
			   	<td style="height:20px">
			   		<label id="docno" class="text">${docNo}</label><input type='hidden' id="docNo" name="docNo" value="${docNo}">
			   	</td>
			</tr>
		</table>
	</form>
	<kendo:grid name="gridProcess" id="gridProcess" pageable="true" groupable="false" selectable="true" editable="false" filterable="false" onclick="onClick()">
		   <kendo:grid-columns>
				<kendo:grid-column title="MATERIAL CODE" field="materialCode"/>  
	            <kendo:grid-column title="MATERIAL NAME" field="materialName"/>   
	          	<kendo:grid-column title="WEEK" field="week"/>
	          	<kendo:grid-column title="YEAR" field="year"/>
	          	<kendo:grid-column title="PLAN ORDER" field="materialOutstanding" template= '<div style="text-align:right;">#= kendo.toString(materialOutstanding, "n0") #</div>'/>
	          	<kendo:grid-column title="CONFIRM" field="materialConfirm" template= '<div style="text-align:right;">#= kendo.toString(materialConfirm, "n0") #</div>'/>
	          	<kendo:grid-column title="DONE" field="done" template="<label id='wrong'>✘</label>"/>
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="10" data="${d_proc}"></kendo:dataSource>
	</kendo:grid>
	<br>
</fieldset>


<div id="divFdis" class="hide">
	<fieldset>
		<table>
			<tr style="margin-bottom: 50px"> 
			   	<td style="width: 200px" class="display-for-form">Material Code</td>
			   	<td class="display-for-form" style="width: 8px">:</td>
			   	<td style="height:20px">
			   		<label id="kodeMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Material Name</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="namaMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Week</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="weekMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr> 
			   	<td style="width: 200px" class="display-for-form">Year</td>
			   	<td class="display-for-form">:</td>
			   	<td style="height:20px">
			   		<label id="yearMaterial" class="text"></label>
			   	</td>
			</tr>
			<tr>
				<td colspan="3">
					<kendo:grid name="gridFdis" id="gridFdis" pageable="true" groupable="false" editable="false" filterable="false">
						<kendo:grid-columns>
								<kendo:grid-column title="DOCNO" field="DOCNO"/>
								<kendo:grid-column title="FINISH GOOD CODE" field="FINISHGOODCODE"/>  
								<kendo:grid-column title="FINISH GOOD NAME" field="FINISHGOODNAME"/>  
					            <kendo:grid-column title="BOM QUANTITY" field="BOMQUANTITY" template= '<div style="text-align:right;">#= kendo.toString(BOMQUANTITY, "n0") #</div>'/>
					            <kendo:grid-column title="BOM UOM" field="BOMUOM"/>
					            <kendo:grid-column title="PLANNED FINISH GOOD QUANTITY" field="PLANNEDFINISHGOODQTY" template= '<div style="text-align:right;">#= kendo.toString(PLANNEDFINISHGOODQTY, "n0") #</div>'/>
					            <kendo:grid-column title="CONFIRMED FINISH GOOD QUANTITY" field="CONFIRMEDFINISHGOODQTY" template= '<div style="text-align:right;">#= kendo.toString(CONFIRMEDFINISHGOODQTY, "n0") #</div>'/>
					    </kendo:grid-columns>
					    <kendo:dataSource pageSize="10"></kendo:dataSource>
					</kendo:grid>
				</td>
			</tr>
		</table>	
	</fieldset>
</div>

<br>
<div>
	<input type="button" class="k-button" id="nextProcess" value="Next"/>
 </div>
<script>
function onClick(){
	var kendo = $("#gridProcess").data("kendoGrid");
	var grid = kendo.dataItem(kendo.select());
	$("#kodeMaterial").html(grid.materialCode);
	$("#namaMaterial").html(grid.materialName);
	$("#weekMaterial").html(grid.week);
	$("#yearMaterial").html(grid.year);
	$.ajax({
		type:'GET', 
		url:'/MOA4/PUR-CO/AjaxFinalProcess/?materialCode='+grid.materialCode+'&week='+grid.week+'&year='+grid.year+'&docNo='+$("#docNo").val(),
		success:function(data){
			console.log(data);
			$("#gridFdis").data("kendoGrid").dataSource.data(data);
			$("#gridFdis").data("kendoGrid").refresh();
			$("#divFdis").removeClass("hide");
		}
	});
}

$("#nextProcess").click(function(){
	$("#processFinal").submit();
});
</script>