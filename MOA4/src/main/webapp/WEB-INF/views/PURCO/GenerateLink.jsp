<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
body{
	overflow:hidden;
}
.hide{
	display:none;
}
p.b {
    font: 16px verdana;
    margin-bottom: auto;
    color: gray;
}
tr{
	height: 30px;
}
</style>
<br><br><br><br><br>
<table>
	<tr>
		<td>
			<img src='/MOA4/resources/images/mayora_header.png' style="width: 124px;height:37px"/>
		</td>
		<td>
			<p class="b"> | Purchasing Confirmation</p>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<label style="color: gray;margin-top: auto;">_____________________________________________</label>
		</td>
	</tr>
</table>

<fieldset>
	<table>
		<tr style="margin-bottom: 50px"> 
		   	<td style="width: 200px" class="display-for-form">No. Document</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${docNo}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Week</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${week}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Year</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${year}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Material Code</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${materialCode}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Material Name</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${materialName}
		   	</td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">Plan Order Quantity</td>
		   	<td class="display-for-form">:</td>
		   	<td style="height:20px">
		   		${quantity}
		   	</td>
		</tr>
		<tr> 
		   	<td colspan="3"><button type="button" id="createLink" class="k-button">Create Link</button></td>
		</tr>
		<tr> 
		   	<td style="width: 200px" class="display-for-form">
		   		<div id="linkDiv" class="display-for-form hide">Shareable Link</div>
		   	</td>
		   	<td class="display-for-form">
		   		<div id="linkDiv-sep" class="display-for-form hide">:</div>
		   	</td>
		   	<td style="height:20px">
		   		<b><label id="link" class="text"></label></b>
		   	</td>
		</tr>
	</table>
</fieldset>

<input type='hidden' id="docNo" name="docNo" value="${docNo}">
<input type='hidden' id="week" name="week" value="${week}">
<input type='hidden' id="year" name="year" value="${year}">
<input type='hidden' id="materialCode" name="materialCode" value="${materialCode}">
<input type='hidden' id="materialName" name="materialName" value="${materialName}">
<input type='hidden' id="quantity" name="quantity" value="${quantity}">

<script>
	$("#createLink").click(function(){
		$("#createLink").addClass("hide");
		$.ajax({
			type:'GET',
			url:'/MOA4/PUR-CO/AjaxGenerateLink/?docNo='+$("#docNo").val()+'&week='+$("#week").val()+'&materialCode='+$("#materialCode").val()+'&year='+$("#year").val()+'&materialName='+$("#materialName").val()+'&quantity='+$("#quantity").val(),
			success:function(data){
				$("#linkDiv").removeClass("hide");
				$("#linkDiv-sep").removeClass("hide");
				$("#link").html(data);		
			}
		});
	});
</script>