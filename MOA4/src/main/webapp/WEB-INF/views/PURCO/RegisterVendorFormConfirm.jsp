<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
	h1{ font-size:28px;}
	h1{ color:#563D64;}
	a{ text-decoration: none; }
	.clearfix:after,
	form:after {
		content: ".";
		display: block;
		height: 0;
		clear: both;
		visibility: hidden;
	}
	.container { margin: 25px auto; position: relative; width: 900px; }
	#content {
		background: #f9f9f9;
		-webkit-box-shadow: 0 1px 0 #fff inset;
		-moz-box-shadow: 0 1px 0 #fff inset;
		-ms-box-shadow: 0 1px 0 #fff inset;
		-o-box-shadow: 0 1px 0 #fff inset;
		box-shadow: 0 1px 0 #fff inset;
		border: 1px solid #c4c6ca;
		margin: 0 auto;
		padding: 25px 0 0;
		position: relative;
		text-align: center;
		text-shadow: 0 1px 0 #fff;
		width: 400px;
	}
	#content h1 {
		color: #d50000;
		font: bold 25px Helvetica, Arial, sans-serif;
		letter-spacing: -0.05em;
		line-height: 20px;
		margin: 10px 0 30px;
	}
	#content h1:before,
	#content h1:after {
		content: "";
		height: 1px;
		position: absolute;
		top: 10px;
		width: 24%;
	}
	
	#content:after,
	#content:before {
		background: #f9f9f9;
		border: 1px solid #c4c6ca;
		content: "";
		display: block;
		height: 100%;
		left: -1px;
		position: absolute;
		width: 100%;
	}
	#content:after {
		-webkit-transform: rotate(2deg);
		-moz-transform: rotate(2deg);
		-ms-transform: rotate(2deg);
		-o-transform: rotate(2deg);
		transform: rotate(2deg);
		top: 0;
		z-index: -1;
	}
	#content:before {
		-webkit-transform: rotate(-3deg);
		-moz-transform: rotate(-3deg);
		-ms-transform: rotate(-3deg);
		-o-transform: rotate(-3deg);
		transform: rotate(-3deg);
		top: 0;
		z-index: -2;
	}
	#content form { margin: 0 20px; position: relative }
	#content form input[type="text"],#content form input[type="number"],#content form input[type="email"],
	#content form input[type="password"],select {
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		-ms-border-radius: 3px;
		-o-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 1px 0 #fff, 0 -2px 5px rgba(0,0,0,0.08) inset;
		-moz-box-shadow: 0 1px 0 #fff, 0 -2px 5px rgba(0,0,0,0.08) inset;
		-ms-box-shadow: 0 1px 0 #fff, 0 -2px 5px rgba(0,0,0,0.08) inset;
		-o-box-shadow: 0 1px 0 #fff, 0 -2px 5px rgba(0,0,0,0.08) inset;
		box-shadow: 0 1px 0 #fff, 0 -2px 5px rgba(0,0,0,0.08) inset;
		-webkit-transition: all 0.5s ease;
		-moz-transition: all 0.5s ease;
		-ms-transition: all 0.5s ease;
		-o-transition: all 0.5s ease;
		transition: all 0.5s ease;
		background: #eae7e7;
		border: 1px solid #c8c8c8;
		color: #777;
		font: 13px Helvetica, Arial, sans-serif;
		margin: 0 0 10px;
		padding: 15px 10px 15px 40px;
		width: 80%;
	}
	#content form input[type="text"]:focus,#content form input[type="number"]:focus, #content form input[type="email"]:focus,
	#content form input[type="password"]:focus,select:focus {
		-webkit-box-shadow: 0 0 2px #ed1c24 inset;
		-moz-box-shadow: 0 0 2px #ed1c24 inset;
		-ms-box-shadow: 0 0 2px #ed1c24 inset;
		-o-box-shadow: 0 0 2px #ed1c24 inset;
		box-shadow: 0 0 2px #ed1c24 inset;
		background-color: #fff;
		border: 1px solid #ed1c24;
		outline: none;
	}
	#content form input[type="submit"] {
		background: rgb(213,0,0);
		-webkit-border-radius: 30px;
		-moz-border-radius: 30px;
		-ms-border-radius: 30px;
		-o-border-radius: 30px;
		border-radius: 30px;
		-webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-moz-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-ms-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		-o-box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		box-shadow: 0 1px 0 rgba(255,255,255,0.8) inset;
		border: 1px solid #D69E31;
		color: #fff;
		cursor: pointer;
		float: left;
		font: bold 15px Helvetica, Arial, sans-serif;
		height: 35px;
		margin: 20px 0 35px 110px;
		position: relative;
		text-shadow: 0 1px 0 rgba(255,255,255,0.5);
		width: 120px;
	}
	#content form input[type="submit"]:hover {
		background: rgba(255,82,82 ,1);
	}
	#content form div a {
		color: #004a80;
	    float: right;
	    font-size: 12px;
	    margin: 30px 15px 0 0;
	    text-decoration: underline;
	}
	.button {
		background: rgb(247,249,250);
		-webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.1) inset;
		-moz-box-shadow: 0 1px 2px rgba(0,0,0,0.1) inset;
		-ms-box-shadow: 0 1px 2px rgba(0,0,0,0.1) inset;
		-o-box-shadow: 0 1px 2px rgba(0,0,0,0.1) inset;
		box-shadow: 0 1px 2px rgba(0,0,0,0.1) inset;
		-webkit-border-radius: 0 0 5px 5px;
		-moz-border-radius: 0 0 5px 5px;
		-o-border-radius: 0 0 5px 5px;
		-ms-border-radius: 0 0 5px 5px;
		border-radius: 0 0 5px 5px;
		border-top: 1px solid #CFD5D9;
		padding: 15px 0;
	}
	.button a {
		color: #7E7E7E;
		font-size: 17px;
		padding: 2px 0 2px 40px;
		text-decoration: none;
		-webkit-transition: all 0.3s ease;
		-moz-transition: all 0.3s ease;
		-ms-transition: all 0.3s ease;
		-o-transition: all 0.3s ease;
		transition: all 0.3s ease;
	}
	.button a:hover {
		background-position: 0 -135px;
		color: #00aeef;
	}
</style>

<br><br><br><br><br>

<div class="container">
	<section id="content">
		<form action="/MOA4/PUR-CO/Register/Action" method="POST">
		<input type="hidden" name="dn" value="${dn}">
			<h1>Mayora Vendor Register Form</h1>
			<div>
				<input type="text" placeholder="Vendor Code" value="${vendorCode}" id="vendorCode" name="vendorCode" readonly/>
			</div>
			<div>
				<input type="password" pattern=".{6,}" placeholder="Password (Min. length is 6)" value="" id="pass" name="pass" min="5" required/>
			</div>
			<div>
				<input type="text" placeholder="Vendor Name" value="" id="vendorName" name="vendorName" required/>
			</div>
			<div>
				<select id="country" name="country">
					<option>Indonesia</option>
					<option>China</option>
					<option>Malaysia</option>
					<option>Singapore</option>
				</select>
			</div>
			<div>
				<input type="email" placeholder="Email" value="" id="email" name="email" required/>
			</div>
			<div>
				<input type="text" placeholder="Address" value="" id="address" name="address" required/>
			</div>
			<div>
				<input type="text" placeholder="Owner" value="" id="owner" name="owner" required/>
			</div>
			<div>
				<input type="number" placeholder="Total Employee" value="" id="employee" name="employee" />
			</div>
			<div>
				<input type="text" placeholder="Headquarter" value="" id="hq" name="hq" required/>
			</div>
			<div>
				<input type="submit" value="Register"/>
			</div>
		</form>
	</section>
</div>
