<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<br/><br/>
<style>
 .panelButton { 
 	padding:-4px; 
 	margin-left:-4px; 
/*  	opacity : 0.5; */
 }
/*  img:hover { */
/*  	opacity : 1; */
/*  } */
  
 .pop { 
 	-webkit-animation-name: bounceIn;
	-webkit-animation-duration: 1s;
	-webkit-animation-timing-function: ease-out;
	-moz-animation-name: bounceIn;
	-moz-animation-duration: 1s;
	-moz-animation-timing-function: ease-out;
	animation-name: bounceIn;
	animation-duration: 1s;
	animation-timing-function: ease-out;
 }
</style>

<c:if test="${not empty groupAccess}"> 
<!-- 	<table> -->
			<c:if test="${groupAccess.edit == 1}"><a href="${groupAccess.webName}/Edit/?${parameter}=${paramVal}"><img title="Edit" class="pop" src="/MOA2/resources/css/images/PanelButton/edit.png"/></a></c:if>
			<c:if test="${groupAccess.delete_ == 1}"><a href="${groupAccess.webName}/Delete/?${parameter}=${paramVal}"><img title="Delete" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/delete.png"/></a></c:if>
			
			
			<c:if test="${groupAccess.review == 1}">
				<a href="${groupAccess.webName}/Review/?${parameter}=${paramVal}"><img title="Review" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/review.png"/></a>
			</c:if>
			
			<c:if test="${not empty transfer}"><a href="${groupAccess.webName}/Transfer/?${parameter}=${paramVal}"><img title="Transfer Group" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/transfer.png"/></a></c:if>
			
			<c:if test ="${not empty undo}">
				<a href="${groupAccess.webName}/Undo/?${parameter}=${paramVal}" onclick="javascript: return confirm('Are you sure to undo this document?');"><img title="Undo WF" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/undo-wf.png"/></a>
			</c:if>
			
			<c:if test="${groupAccess.approve == 1}">
				<a href="${groupAccess.webName}/Approve/?${parameter}=${paramVal}"><img title="Approve" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/approve.png"/></a>
				<a href="${groupAccess.webName}/Reject/?${parameter}=${paramVal}"><img title="Reject" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/reject.png"/></a>
			</c:if>
			
			<c:if test="${(groupAccess.approve == 1 || groupAccess.review == 1) && not empty returnWF}">
				<a href="${groupAccess.webName}/Return/?${parameter}=${paramVal}"><img title="Return" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/return.png"/></a>
			</c:if>
			
			<c:if test="${not empty undoClose}">
				<a href="${groupAccess.webName}/UndoClose/?${parameter}=${paramVal}"><img title="Undo Close" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/undo-close.png"/></a>
		 	</c:if>
		 	
		 	<c:if test="${not empty reload}">
				<a href="${groupAccess.webName}/Reload/?${parameter}=${paramVal}"><img title="Reload" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/reload.png"/></a>
		 	</c:if>
		 	
		 	<c:if test="${not empty saveExcel}"> 
				<a href="${groupAccess.webName}/SaveExcel/?${parameter}=${paramVal}"><img title="Save to Excel" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/save-to-excel.png"/></a>
		 	</c:if>  
		 	
		 	<c:if test="${not empty forceSend}"><a href="${groupAccess.webName}/ForceSend/?${parameter}=${paramVal}"><img title="Force Send" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/force-send.png"/></a></c:if>
			<c:if test="${not empty send}"><a href="${groupAccess.webName}/Send/?${parameter}=${paramVal}"><img title="Send" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/send.png"/></a></c:if>
			<c:if test="${groupAccess.print == 1}"><a href="${groupAccess.webName}/Print/?${parameter}=${paramVal}" target="_blank"><img title="Print" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/print.png"/></a></c:if>
<%-- 			<c:if test="${not empty showStatus}"><a href="${groupAccess.webName}/ShowStatus/?${parameter}=${paramVal}"><img title="Show Status" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/show-status.png"/></a></c:if> --%>
			<c:if test="${not empty showStatus}"><a href="/MOA2/ShowStatus/?${parameter}=${paramVal}"><img title="Show Status" class="panelButton pop" src="/MOA2/resources/css/images/PanelButton/show-status.png"/></a></c:if>
						
<!-- 	</table> -->
</c:if>
