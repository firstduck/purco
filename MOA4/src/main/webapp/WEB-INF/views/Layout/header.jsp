<%@page import="com.lowagie.text.Header"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



			<div id="header">
				<div id="logo"><img src="/MOA2/resources/images/moa-logo.png" style="margin-left:10px;" height="40px"></img></div>
				<div id="login" style=''>   
			<script> 
			$(document).ready(function() {
				$('#delegationKendo').change(function() {
					$('#welcomeUser').submit();
				});
				
				$('#test').keypress(function(e){
					if(e.which == 13 || event.keyCode == 13) {
						if($('#test').val().indexOf('\'') == -1)
			 	 			window.location = '/MOA/Home/Shortcut/?key='+$('#test').val();
						else $('#label_error').text("Invalid character : '");
			 		}
				});
			});
			</script>
			<table style="float:right; padding-top:-10px; margin-top:-10px;"  border=0>  
			<tr>
			<td>

				</td><td><form id='welcomeUser' action="/MOA/Account/ChangeUserDelegation/Action" method="POST">		
					<c:if test = "${empty UserLogin}">
						[ <a href="/MOA/Account/Login">Log On</a> ]  
					</c:if>
					<c:if test = "${not empty UserLogin}">
						Welcome 
						<c:choose>
							<c:when test="${EyeAmSystem == '1' }">
									<b>#SYSTEM#</b> ! as ${UserLogin.user.name}&nbsp;[
							</c:when>
								
							<c:otherwise>
								<b>${UserLogin.user.name}</b> ! 
								<c:if test = "${not empty delegateList}">
								as 
							
								<input type='hidden' id='name' name="name" value="${UserLogin.user.name}"/>
								 
								<kendo:dropDownList style="width:200px; text-align:left;" cssStyle="height:100px;" name="delegationKendo" index="0" value="${UserLogin.user.username}" filter="contains" placeholder="Select Delegate..." suggest="true" dataTextField="text" dataValueField="value" >
						    		<kendo:dataSource data="${delegateList}"></kendo:dataSource>
						    	</kendo:dropDownList>
						     
								</c:if>	
								
								[     
								<c:choose>
		    					<c:when test="${empty delegateList || delegateToUser.username == UserLogin.user.username}">
		        					<a href="/MOA/Account/Delegation">Set Delegation</a> | 
								 	<a href="/MOA/Account/ChangePassword">Change Pass</a> | 
		    					</c:when>
			    					<c:otherwise>
			    					</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
						
						<a href="/MOA/Account/Logout">Log Off</a> ] 
					</c:if>
					</form></td></tr></table>
				</div> 
			 	<br/><br/>   
			 	<c:if test = "${not empty UserLogin}">     
							<input type="text" id='test' placeholder="tcode" style="text-align:right;float:right;margin-top:3px;margin-right:3px;font-weight:bold"/>  <label style="font-weight:bolder; margin-top:7px;margin-right:5px;float:right;color:white" id="label_error">${errorHeader}</label>
							<% 
								request.getSession().removeAttribute("errorHeader"); 
							%> 
					</c:if>   
				<div id="navbar">  
					<ul id="menu">
						<li>
							Main  
							
							<ul>
								<li><a href="/MOA/Home">Home</a></li>
								<c:if test="${not empty UserLogin}">
									<li><a href="/MOA2/Home/JobList">Old Job List</a></li>
									<li><a href="/MOA2/Home/NewJobList">New Job List</a></li>
								</c:if>
								<li><label id="AddToFav">Add To Favorite</label></li>
								<li><a href="/MOA/Home/UserGuide">User Guide</a></li>
								<li><a href="/MOA/Home/About">About</a></li>
								<li><a href="/MOA2/downloadMobile">Download Mayora Mobile</a></li>
							</ul>
						</li>
						<li>
							Favorites
							<ul>
								<c:forEach var="mo" items="${favorites}">
									<li>${mo.moduleName}
										<ul>
											<c:forEach var="menu" items="${mo.list}">
												<li><a href="${menu.webname}/${menu.action}">${menu.menu}</a></li>
											</c:forEach>
										</ul>
									</li>
								</c:forEach>
							</ul>
						</li>
						<li>
							Menu
							<c:set var="m" value="${fn:split('A,B',',')}"/>
							<ul>
								<c:forEach var="application" items="${AccessibleApplication}">
									<li>
										${application.applicationName}
										<ul>
											<c:forEach var="module" items="${AccessibleModule}">
												<c:if test="${module.applicationCode == application.applicationCode && module.moduleCode != 'KPI-PR'}">
													<li><a href="${module.webName}">${module.moduleName}</a></li>
												</c:if>
											</c:forEach>
										</ul>
									</li>
								</c:forEach>
							</ul>
						</li>
						<c:forEach var="menuAccessMvc" items="${menuAccessMvc}" varStatus="status">
							<c:if test="${status.first}">
								<li> 
									${menuAccessMvc.moduleName}<input type="hidden" id="txtModName" name="txtModName" value="${menuAccessMvc.moduleCode}"/>
									<ul>
							</c:if>
										<li><a href="${menuAccessMvc.webName}/${menuAccessMvc.action}">${menuAccessMvc.menu}</a></li>
							<c:if test="${status.last}">
									</ul>
								 </li>
							</c:if>
						</c:forEach> 
						
						<table><tr><td style="padding-top:5px;padding-left:4px;"><i><b><label id="titlePage"></label></b></i></td></tr></table>

		            </ul>
				</div>
			</div>
	
			<script>
			
				$("#AddToFav").click(function(){
					var x = $("#txtModName").val();
					
		 			$.ajax({
	 				type: "GET",
	 				url:"/MOA2/Favorite/Add/?module="+x,

	 				success: function(result) { 
						alert(result);
						location.reload();
	 				}
	 				});
				});
				
				var y = window.location.href;
				var yArr = y.split('?');
				var currentPage = yArr[0].split('/');
				
				if(currentPage[currentPage.length-1] != '')
					$("#titlePage").html(currentPage[currentPage.length-1]);
				else $("#titlePage").html(currentPage[currentPage.length-2]);
			</script>