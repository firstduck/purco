<%@page import="com.lowagie.text.Header"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
function setFavorite(moduleCode, e)
{
	var heart = document.getElementById("heart2_" + e.id.split("_")[1]);
	$.ajax
	({
		type: "GET",
		url:"/MOA2/Favorite/Add/?module="+moduleCode,
		success: function(result) 
		{ 
			heart.style.display = "block";
			e.style.display = "none";
			$('#favorite1').load(document.URL +  ' #favorite1');
		}
	});
}
function deleteFavorite(moduleCode, e)
{
	var heart = document.getElementById("heart1_" + e.id.split("_")[1]);
	$.ajax
	({
		type: "GET",
		url:"/MOA2/Favorite/Delete/?module="+moduleCode,
		success: function(result) 
		{ 
			heart.style.display = "block";
			e.style.display = "none";
			$('#favorite1').load(document.URL +  ' #favorite1');
		}
	});
}

function mouseOverFavorite(e)
{
		e.innerHTML = "&#9733;";
}

function mouseOutFavorite(e)
{
// 	var heart = document.getElementById("heart1_" + e.id.split("_")[1]);
// 	if(e.style.display == "block")
		e.innerHTML = "&#9734;";
}

function mouseOverUnfavorite(e)
{
	e.innerHTML = "&#9734;";
}

function mouseOutUnfavorite(e)
{
// 	var heart = document.getElementById("heart1_" + e.id.split("_")[1]);
// 	if(e.style.display == "block")
		e.innerHTML = "&#9733;";
}

function loadMenu(e)
{
	var ul = document.getElementById("ul_" + e.id);
	if(ul.getElementsByTagName('li').length == 0)
	{
		$.ajax({
			url:"/MOA/GetMenuByModuleCode/?moduleCode="+e.id,
			method:"GET",
			success:function(data){
				if(ul.getElementsByTagName('li').length == 0)
				{
					for(var i=0;i<data.length;i++)
					{
						var li = document.createElement("li");
						var a = document.createElement("a");
						a.href = data[i].webName + "/" + data[i].action;
						a.innerHTML = data[i].menu;			
						a.className = e.id;
						li.appendChild(a);
						ul.appendChild(li);
					}
				}
				$("#divdetail_" + e.id).slideToggle();
			}
		});
	}
	else $("#divdetail_" + e.id).slideToggle();
}

function loadMenu2(e, searchMenu)
{
	var ul = document.getElementById("ul_" + e.id);
	if(ul.getElementsByTagName('li').length == 0)
	{
		$.ajax({
			url:"/MOA/GetMenuByModuleCode/?moduleCode="+e.id,
			method:"GET",
			success:function(data){
				if(ul.getElementsByTagName('li').length == 0)
				{
					var flag = false;
					for(var i=0;i<data.length;i++)
					{
						var li = document.createElement("li");
						var a = document.createElement("a");
						a.href = data[i].webName + "/" + data[i].action;
						a.innerHTML = data[i].menu;
						a.className = e.id;
						if(searchMenu != "") 
						{
							if(data[i].menu.indexOf(e.id + searchMenu) >= 0) 
							{
								a.style.color = "#ECC23A";	
								flag = true;
							}
						}

						
						li.appendChild(a);
						ul.appendChild(li);
					}
					if(!flag) e.style.color = "#ECC23A";
				}
				$("#divdetail_" + e.id).slideToggle();
			}
		});
	}
	else 
	{
		$("#divdetail_" + e.id).slideToggle();
	}
}
</script>

<c:forEach var="application" items="${AccessibleApplication}">
 	<label style="cursor:pointer;" onClick="clickHeader('${application.applicationCode}')" class="menuHeader" id="${application.applicationCode}">${application.applicationName}</label>
 	<div style="" id="div_${application.applicationCode}" class="divHeader">
 	<table>
 		<c:forEach var="module" items="${AccessibleModule}">
 			<c:if test="${module.applicationCode == application.applicationCode && module.moduleCode != 'KPI-PR' && module.moduleCode != 'PO' && module.moduleCode != 'KPI-MO' && module.moduleCode != 'MAT-RM' && module.moduleCode != 'MAT-PM' && module.moduleCode != 'MAT-AT' && module.moduleCode != 'MAT-SP' && module.moduleCode != 'MAT-SC' && module.moduleCode != 'MAT-PR'}">
          			<tr>
          				<td width="10px">
          				<c:set var="i" value="0"/>
          				<c:forEach var="fav" items="${favorites}">
          					<c:if test = "${fav.moduleCode == module.moduleCode}">
          						<c:set var="i" value="1"/>
          					</c:if>
          				</c:forEach>
          				<c:choose>
          					<c:when test="${i == 0}">
          						<label style="cursor:pointer;" onmouseover="mouseOverFavorite(this)" onmouseout="mouseOutFavorite(this)" onClick="setFavorite('${module.moduleCode}', this)" id="heart1_${module.moduleCode}">&#9734;</label>
          						<label style="cursor:pointer; display:none;" onmouseover="mouseOverUnfavorite(this)" onmouseout="mouseOutUnfavorite(this)" onClick="deleteFavorite('${module.moduleCode}', this)" id="heart2_${module.moduleCode}">&#9733;</label>
          					</c:when>
          					<c:otherwise>
          						<label style="cursor:pointer; display:none;" onmouseover="mouseOverFavorite(this)" onmouseout="mouseOutFavorite(this)" onClick="setFavorite('${module.moduleCode}', this)" id="heart1_${module.moduleCode}">&#9734;&nbsp;</label>
          						<label style="cursor:pointer;" onmouseover="mouseOverUnfavorite(this)" onmouseout="mouseOutUnfavorite(this)" onClick="deleteFavorite('${module.moduleCode}', this)" id="heart2_${module.moduleCode}">&#9733;</label>
          					</c:otherwise>
          				</c:choose>
          				</td>
          				<td><a class="${application.applicationCode}" id="${module.moduleCode}" href="#" onClick="loadMenu(this); return false;">${module.moduleName}</a></td></tr>
          			<tr><td colspan="2">
          				<div style="display:none; margin-top:-10px" id="divdetail_${module.moduleCode}">
          					<ul id="ul_${module.moduleCode}"></ul>
						</div>
					</td></tr>
       		</c:if>
 		</c:forEach>
 	</table>
   	</div> 
   	<div style="font-size:5px;">&nbsp;</div>
</c:forEach>
<script>
$(document).ready(function(){
	$(".divHeader").slideToggle();
});
function clickHeader(e)
{
	$("#div_"+e).slideToggle();
}

function searchMenu(e)
{
	var keyword = document.getElementById("searchMenu").value.trim().toUpperCase();
	var search = keyword;
	var searchMenu = "";
	if(search == '') 
	{
		search = "";
		searchMenu = "";
	}
	else
	{
		for(var i=0;i<keyword.length;i++)
		{
			if(!isNaN(parseInt(keyword.charAt(i))))
			{
				search = keyword.substring(0,i);
				searchMenu = keyword.substring(i); 
				break;
			}
		}
	}
	
	
	if (e.keyCode == 13)
	{
		var elementFocus = null;
		var menuHeader = document.getElementsByClassName("menuHeader");
		for(var i=0; i<menuHeader.length; i++)
		{
			var show = false;
			var menuDetail = document.getElementsByClassName(menuHeader[i].id);
			for(var ii=0; ii<menuDetail.length; ii++)
			{
				if(menuDetail[ii].innerHTML.toUpperCase().indexOf(search) != -1 || menuDetail[ii].id.toUpperCase().indexOf(search) != -1)
				{
					if(elementFocus == null) elementFocus = menuDetail[ii];
					menuDetail[ii].style.color = "#ECC23A";
					show = true;
					if(document.getElementById("divdetail_" + menuDetail[ii].id).style.display == "none")
					{
						if(searchMenu != "") menuDetail[ii].style.color = "";
						loadMenu2(menuDetail[ii], searchMenu);
					}
					else
					{
						if(searchMenu != "") 
						{
							var menuColor = document.getElementsByClassName(menuDetail[ii].id);
							var flag = false;
							for(var iii=0; iii < menuColor.length;iii++)
							{
								if(menuColor[iii].innerHTML.indexOf(menuDetail[ii].id + searchMenu) >= 0)
								{
									menuColor[iii].style.color = "#ECC23A";
									flag = true;
								}
								else menuColor[iii].style.color = "";
								
								if(menuColor[iii].innerHTML.indexOf(search + searchMenu) >= 0)
								{
									window.location = menuColor[iii].href;
								}
							}
							if(!flag) menuDetail[ii].style.color = "#ECC23A";
							else menuDetail[ii].style.color = "";
							
						}
						else
						{
							var menuColor = document.getElementsByClassName(menuDetail[ii].id);
							for(var iii=0; iii < menuColor.length;iii++)
							{
								menuColor[iii].style.color = "";
							}
						}
					}
				}
				else 
				{
// 					menuDetail[ii].style.fontWeight = "normal";
					menuDetail[ii].style.color = "";
					if(document.getElementById("divdetail_" + menuDetail[ii].id).style.display != "none")
					{
						$("#divdetail_" + menuDetail[ii].id).slideToggle();
						var menuColor = document.getElementsByClassName(menuDetail[ii].id);
						for(var iii=0; iii < menuColor.length;iii++)
							menuColor[iii].style.color = "";
					}
				}
			}
			if(show && document.getElementById("div_" + menuHeader[i].id).style.display == "none") 
			{	$("#div_" + menuHeader[i].id).slideToggle();
			}
			else if(!show && document.getElementById("div_" + menuHeader[i].id).style.display != "none") 
			{	$("#div_" + menuHeader[i].id).slideToggle();
			}
		}
		if(elementFocus!= null)
		try { setTimeout(function() { $("html, body").animate( { scrollTop:$("#"+elementFocus.id).offset().top - ( $(window).height() - $("#"+elementFocus.id).outerHeight(true) ) / 2 } ,500); } ,300); } catch(e) { }
		else { $("html, body").animate( { scrollTop:$("#searchMenu").offset().top - ( $(window).height() - $("#searchMenu").outerHeight(true) ) / 2 } ,500); }
		
// 		for(var i=0; i<menuHeader.length;i++)
// 		{
// 			var menuDetail = document.getElementsByClassName(menuHeader[i].id);
// 			for(var ii=0; ii<menuDetail.length; ii++)
// 			{
// 				if(menuDetailClick.indexOf(menuDetail[ii].id) >= 0) 
// 				{
// 					if(document.getElementById("divdetail_" + menuDetail[ii].id).style.display == "none")
// 					{
// 						loadMenu2(menuDetail[ii], searchMenu);
// 					}
// 				}
// 				else
// 				{
// 					if(document.getElementById("divdetail_" + menuDetail[ii].id).style.display != "none")
// 					{
// 						$("#divdetail_" + menuDetail[ii].id).slideToggle();
// 					}
// 				}
// 			}
// 		}
	}
}
</script>

