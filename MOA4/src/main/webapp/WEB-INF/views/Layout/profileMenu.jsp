<%@page import="com.lowagie.text.Header"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
#settingProfileMenu input
{
	font-family: Verdana, Helvetica, Sans-Serif;
}
</style>

<script>
function setAlias()
{
	$.ajax({
		url: "/MOA/Account/GetAlias/?username=${UserLogin.user.username}",
		type: "GET",
		success: function(data)
		{
			var alias = window.prompt("Your Alias Name : ", data);
			if(alias != null && alias != data && alias != "")
			{	
				$.ajax({
					url: "/MOA/Account/SetAlias/?username=${UserLogin.user.username}&alias=" + alias,
					type: "GET",
					success: function(data)
					{
						if(data) alert("Set Alias Success");
						else alert("Duplicate Alias Name");
					}	
				});
			}
			else if(alias != null && alias != "") alert("Set Alias Success");	
		}	
	});
	
}

function changeDelegation(e)
{
	window.location = "/MOA/Account/ChangeUserDelegation/Action/?name="+encodeURIComponent($("#name").val())+"&delegationKendo="+e.value;
}
</script>

<c:choose>
	<c:when test = "${not empty UserLogin}">
		<table width="100%">
			<tr>
				<td align="center"><img src="/MOA/resources/images/logo-myr-1.png" style="height: 100px;"><hr style="text-shadow: black;"/></td>
			</tr>
			<tr>
				<td>Welcome, </td>
			</tr>
			<tr>
				<td>
					<c:choose>
						<c:when test="${EyeAmSystem == '1' }">
								<b>#SYSTEM#</b> as ${UserLogin.user.name}&nbsp;
						</c:when>
														
						<c:otherwise>
							<label style='cursor:pointer' onClick="setAlias()" title="Set Alias">&#9734;</label><b>${UserLogin.user.name}</b> 
							<c:if test = "${not empty delegateList}"> 
								as 
								<input type='hidden' id='name' name="name" value="${UserLogin.user.name}"/>
								<select onchange="changeDelegation(this)">
								<c:forEach var="delegate" items="${delegateList}">
									<c:if test="${UserLogin.user.username == delegate.value}">
										<option value="${delegate.value}" selected="selected">${delegate.text}</option>
									</c:if>
									<c:if test="${UserLogin.user.username != delegate.value}">
										<option value="${delegate.value}">${delegate.text}</option>
									</c:if>
								</c:forEach>
								</select>
							</c:if>	
						</c:otherwise>
					</c:choose>	
				</td>
			</tr>
			<tr>
				<td align="right">
					<c:if test="${EyeAmSystem != '1'}">
					<div id="settingProfileMenu"></div>
							<script>
								function settingProfileMenu()
								{
									$("#settingProfileMenu").kendoWindow({
										title:"Profile Setting",
										width: "700px",
								        height: "475px",
								        actions: ["Close"],
								        modal:true,
								        close : function(){
								        	$("#npd").val($("#selectedValue1").html());
								        	$("#npdName").html($("#selectedValue2").html());
								        	
								        	this.destroy();
								    		var div = document.createElement("div");
								        	div.id = "settingProfileMenu";
								        	if(document.body != null){ document.body.appendChild(div); }
								        }
								    });
						
									$('#settingProfileMenu').load("/MOA/SettingProfileMenu");
									$("#settingProfileMenu").data("kendoWindow").center().open();
						
								}
							</script>
						<label class="link" style="cursor:pointer" onClick="javascript:window.location='/MOA2/Home/NewJobList/?m=ALL'"><a>Joblist</a>&nbsp;|</label>
						<label class="link" style="cursor:pointer" onClick="javascript:window.location='/MOA/Account/ChangePassword'"><a>Password</a>&nbsp;|</label>
						<label class="link" style="cursor:pointer" onClick="javascript:window.location='/MOA/Account/Delegation'"><a>Delegation</a>&nbsp;|</label>
					</c:if>
					<label class="link" style="cursor:pointer" onClick="javascript:window.location='/MOA/Account/Logout'"><a>Logout</a></label>
				</td>
			</tr>
		</table>	
	</c:when>	
	<c:otherwise>
		<script>
           	var loginProcess = false;
           	function login()
           	{
           		if(!loginProcess)
           		{
           			$("#waitingLink").css("display","block");
           			$("#buttonLogin").css("display","none");
           			
           			loginProcess = true;
            		$.ajax({
            			url:"/MOA/Account/LoginNew2/Action/?username="+$("#username").val() + "&password="+$("#password").val(),
            			method:"GET",
            			success:function(data){
            				if(data.login) window.location = data.redirect;
            				else 
            				{	
            					$("#waitingLink").css("display","none");
                       			$("#buttonLogin").css("display","block");
            					alert(data.message);
            					loginProcess = false;
            				}
            			}
            		});
           		}
           	}
           	
           	$(document).keypress(function(e) {
       			if (e.which == 13 || event.keyCode == 13) {
       				login();	
       			}
       		});
        </script>
		<table>
			<tr><td align="center"><img src="/MOA/resources/images/logo-myr-1.png" style="height: 100px;"><hr style="text-shadow: black;"/></td></tr>
			<tr><td align="center"><input type="text" id="username" placeholder="Username..." style="text-align: center;"/></td></tr>
			<tr><td style="font-size:2px">&nbsp;</td></tr>
			<tr><td align="center"><input type="password" id="password"  placeholder="Password..." style="text-align: center;"/></td></tr>
			<tr><td style="font-size:2px">&nbsp;</td></tr>
			<tr><td align="center">
				<input type="button" onClick="login()" value="Login" style="background-color:white; cursor:pointer;" id="buttonLogin"/>
				<img id="waitingLink" src="/MOA/resources/css/images/loading.gif" height="30px" title="Loading" style="border:1px; display:none;"/>
			</td></tr>
		</table>
	</c:otherwise>
</c:choose>
