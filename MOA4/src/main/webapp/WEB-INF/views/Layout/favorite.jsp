<%@page import="com.lowagie.text.Header"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if test="${not empty UserLogin}">
<script>
function deleteFavoriteFromFavorite(moduleCode, e)
{
	$.ajax
	({
		type: "GET",
		url:"/MOA2/Favorite/Delete/?module="+moduleCode,
		success: function(result) 
		{ 
			$('#favorite1').load(document.URL +  ' #favorite1');
			$("#heart1_"+moduleCode).css("display","block");
			$("#heart2_"+moduleCode).css("display","none");
		}
	});
}

function loadMenuFavorite(e)
{
	var ul = document.getElementById("ulFavorite_" + e.id);
	if(ul.getElementsByTagName('li').length == 0)
	{
		$.ajax({
			url:"/MOA/GetMenuByModuleCode/?moduleCode="+e.id,
			method:"GET",
			success:function(data){
				for(var i=0;i<data.length;i++)
				{
					var li = document.createElement("li");
					var a = document.createElement("a");
					a.href = data[i].webName + "/" + data[i].action;
					a.innerHTML = data[i].menu;
					li.appendChild(a);
					ul.appendChild(li);
				}
				$("#divdetailFavorite_" + e.id).slideToggle();
			}
		});
	}
	else $("#divdetailFavorite_" + e.id).slideToggle();
}
</script>

<c:if test="${not empty favorites}">&nbsp;&nbsp;<label class="favoritesTitle" onClick="clickHeader('favorites')"><b>Favorites</b></label>
<div id="div_favorites"><table>
<c:forEach var="fav" items="${favorites}">
	<tr><td style="padding-left:2px"></td><td><label class="hearts" style="cursor:pointer" onmouseover="mouseOverUnfavorite(this)" onmouseout="mouseOutUnfavorite(this)" onClick="deleteFavoriteFromFavorite('${fav.moduleCode}', this)">&#9733;</label></td><td><a onClick="loadMenuFavorite(this)" id="${fav.moduleCode}">${fav.moduleName}</a></td></tr>
	<tr><td colspan="3">
    		<div style="display:none; margin-top:-10px" id="divdetailFavorite_${fav.moduleCode}">
    			<ul id="ulFavorite_${fav.moduleCode}"></ul>
			</div>
		</td></tr>
</c:forEach>
</table></div>
</c:if>
<c:if test="${empty favorites}">
<hr/>
</c:if>
</c:if>