<%@page import="com.lowagie.text.Header"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
.portalMenu
{
	cursor:pointer;
}
.portalMenuTable
{
	border-spacing:5px;
}
.portalMenuTable td
{
	border:2px solid #ECC23A;
	color:black;
}

.portalMenuTable td:hover
{
 	background-color:#ECC23A;
}
</style>
<script>
function gotoRedirect(e)
{
	window.location = "/MOA/Account/ChangePortalMenu/?menuPortal=" + e;
}
</script>

<table class="portalMenuTable" width="100%" border=0>
	
		<c:forEach var="portalMenu" items="${AccessiblePortalMenu}">
		<tr>
			<c:choose>
			<c:when test="${portalMenu.menuCode == MenuPortal}">
				<td align="left" bgcolor="#ECC23A" onclick="gotoRedirect('${portalMenu.menuCode}')" class="portalMenu">
					<b style="color:white" title="${portalMenu.menuName}">${portalMenu.menuName}</b>
				</td>
			</c:when>	
			<c:otherwise>
				<td align="left" bgcolor="${portalMenu.color}" onclick="gotoRedirect('${portalMenu.menuCode}')" class="portalMenu">
					<b style="color:white" title="${portalMenu.menuName}">${portalMenu.menuName}</b>
				</td>
			</c:otherwise>
			</c:choose>
			</tr>
		</c:forEach>
	
</table>
