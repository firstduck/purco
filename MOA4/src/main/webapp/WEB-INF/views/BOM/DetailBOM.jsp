<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
	#gridPlant .k-alt {
 	   background-color: #e6e6e6;
	}
	.k-numerictextbox .k-select { display: none; }
 	.k-numerictextbox .k-numeric-wrap { padding-right: 2px; }
</style>

<form method="POST" id="bomForm" name="bomForm" onsubmit="return false" action="${action}">
	<fieldset>
		<div id="window">
		</div>
		
		<table>
			<tr>
				<td>
					<div class="display-for-form">Document Number </div>			
				</td>
				<td>:
					<label id="docNo" > ${docNo} </label>
				</td>
			</tr>
			<tr>	
				<td>
					<div class="display-for-form">Product Name</div>
				</td>
				<td>:
					<label id="productName"> ${productName} </label>
				</td>
			</tr>
		</table>
		<br>
		<div id="gridPlant" style="width:1000px;"></div>
	</fieldset>
	<input type="button" id="updateBtn" value="Update" />
	<input type="button" id="cancelBtn" value="Cancel" />

	<input type="hidden" id="productCode" name="productCode" value="${productCode}"/>
	<input type="hidden" id="gridData" name="gridData" />
	<input type="hidden" id="hidDocNo" name="hidDocNo" value="${docNo}"/>
</form>

<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"  /></div>

<script type="text/javascript">
	

	$(document).ajaxStart(function () {
		dimStart();
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});
	
	$(document).ajaxStop(function () {
		dimStop();
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	
	var popupIsOpen = false;
	function dimStart(){
		$('#dimScreen').prop('hidden', false);
	}

	function dimStop(){
		if(!popupIsOpen){
			$('#dimScreen').prop('hidden', true);
		}
	}
	
	$("#gridPlant").kendoGrid({
        dataSource: {
        	 transport: {
        	        read: {
        	            url: "/MOA4/BOM/GetDBOMAlter/?docNo=${docNo}",
        	            dataType: "json"
        	        },
        	 },
            schema: {
                model: {
                  id: "alternativeId",
                  fields: {
                    plant: { validation: { required: true }},
                    alternativeId: { type: "number", validation: { required: true, min:1}, defaultValue:"" },
                    rowNumber : {editable:false}
                  }
                }
              }
            },
        toolbar: ["create"],
        editable:{
			createAt: "bottom"
		},
        columns: [
		  { field: "rowNumber",title: "No.", width:50, template: "<span class='row-number'></span>"},
          { title :"Plant" ,field: "plant", width:200,editor: numericEditor},
          { field: "alternativeId", title: "Alternative Id", width: 200 ,editor: numericEditor},
          {command: { text: "View Details", click: showDetails }, title: " ", width: "200px" },
          {command: ["destroy"],title:""}
        ],
        dataBound: function () {
            var rows = this.items();
            var i = 0 ;
            $(rows).each(function () {
                var index = $(this).index() + 1;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
                var dataItem = $("#gridPlant").data("kendoGrid").dataSource.data()[i];
                dataItem["rowNumber"] = index;
                i++;
            });
        }
	});
	
	function numericEditor(container, options) {
		  $('<input data-bind="value:' + options.field + '"/>')
		    .appendTo(container)
		    .kendoNumericTextBox({
		      format: "{0:0}",
		      decimals: 0
		    });               
		}
		
	function showDetails(e) {
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		var docNo = $("#hidDocNo").val();

		if(dataItem.alternativeId == "" || dataItem.plant == ""){
			alert("There is an empty column!");
		}
		else{
			window.open("/MOA4/BOM/Detail/Create/?docNo="+docNo+"&alternativeId="+dataItem.alternativeId+"&product=${productName}"
				+"&plant="+dataItem.plant, '_blank');
		}
    }
	
	function newGridRow(){
		var grid =$("#gridPlant").data("kendoGrid");
		grid.dataSource.sync();
		grid.dataSource.add({});
	}
	
	$("#gridPlant tbody").on("dblclick", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		var productCode = $("#productCode").val();
		
		//buat popup plant
		if(colIdx==1){
			var grid = $("#gridPlant").data("kendoGrid");
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = grid.dataSource.data();
		    
		    $("#window").kendoWindow({
				width: "600px",
				height: "450px",
				actions: ["Close"],
				title: "Plant Browse", 
				modal:true,
				close:function(){ 
					if($("#selectedValue1").html()!=""){
						entityGrid[rowIdx]["plant"] = $("#selectedValue1").html();
						grid.refresh();
					}
					this.destroy();
					var div = document.createElement("div");
					div.id = "window";
					if(document.body!=null){
						document.body.appendChild(div);
					}
				}
			 });
		    
			 $("#window").load("/MOA4/BOM/KendoWindowSelectPlantBrowse/?window=window&column=2&materialCode="+productCode);
			 $("#window").data("kendoWindow").center().open();
		}
	});

	$("#gridPlant tbody").on("change", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		var productCode = $("#productCode").val();
		if(colIdx==1){
			var grid = $("#gridPlant").data("kendoGrid");
			var datasource = grid.dataSource;
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = datasource.data();
		    var plant = entityGrid[rowIdx]["plant"]; 
		    if(plant.toString().length > 0){
		    	$.ajax({
	 				type:"GET",
	 				url: "/MOA4/BOM/AjaxValidatePlant/?plant="+plant+"&productCode="+productCode,
	 				success: function(result)
	 				{
	 					if(!result){
	 						entityGrid[rowIdx]["plant"] = "";
	 						entityGrid[rowIdx]["alternativeId"] = "";
	 						alert("Plant tidak ditemukan");
	 					}
	 					grid.refresh();
	 				},
	 				error:function(e){
	 					entityGrid[rowIdx]["plant"] = "";
	 					entityGrid[rowIdx]["alternativeId"] = "";
 						grid.refresh();
 						alert(e.statusText);
				 		dimStop();
	 				}
		    	});
		    }
		}
// 		buat pas ganti alternative id nya, validasi gaboleh ada yang sama
		else if(colIdx==2){
			var grid = $("#gridPlant").data("kendoGrid");
			var datasource = grid.dataSource;
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = datasource.data();
		    grid.refresh();
		    var altId = entityGrid[rowIdx]["alternativeId"];
		    var totalRow = datasource.total();
		    for(var i=0;i<totalRow;i++){
		    	if(altId == entityGrid[i]["alternativeId"] && rowIdx != i){
			     	entityGrid[rowIdx]["alternativeId"] = "";
			     	alert("Alternative Id must be unique !");
					grid.refresh();
					break;
			    }
		    }
		}
	});
	
	$("#updateBtn").click(function(){
		
		var grid = $("#gridPlant").data("kendoGrid");
		var datasource = grid.dataSource;
		var data = datasource.data();
		var isNull = false;
		for(var i=0;i<data.length;i++){
			if(data[i]["plant"] == ""){
				isNull = true;
				break;
			}
			if(data[i]["alternativeId"] == ""){
				isNull = true;
				break;
			}
		}
		if(data.length<1){
			alert("Table cannot be empty!");
		}
		else if(isNull){
			alert("There is an empty column!");
		}
		else{
			var json = JSON.stringify(data);
			$("#hidDocNo").val("${docNo}");
			$("#gridData").val(json);
			document.bomForm.submit();
			document.getElementById("updateBtn").disabled = true;
			document.getElementById("cancelBtn").disabled = true;
		}
	});
	
	$("#cancelBtn").click(function(){
		document.getElementById("updateBtn").disabled = true;
		document.getElementById("cancelBtn").disabled = true;
		window.location.href="${cancelUrl}";
	});
</script>