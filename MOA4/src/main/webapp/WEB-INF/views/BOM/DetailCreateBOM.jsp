
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
	#gridDetail .k-alt {
 	   background-color: #e6e6e6;
	}
	.k-numerictextbox .k-select { display: none; }
 	.k-numerictextbox .k-numeric-wrap { padding-right: 2px; }
</style>

<form method="POST" id="bomForm" name="bomForm" onsubmit="return false" action="${action}">
	<fieldset>
	<div id="window"></div>
	<table>
		<tr>
			<td style="width: 150px;">
				<div class="display-for-form">Document Number </div>
			</td>
			<td>:
				<label id="docNo">${docNo}</label>
			</td>
		</tr>
		<tr>
			<td style="width: 150px;">
				<div class="display-for-form">Product Name</div>
			</td>
			<td>:
				<label id="productName">${productName}</label>
			</td>
		</tr>
		<tr>
			<td>
				<div class="display-for-form">Plant</div>
			</td>
			<td>:
				<label id="plant">${plant}</label>
			</td>
		</tr>
		<tr>
			<td>
				<div class="display-for-form">Alternative Id</div>			
			</td>
			<td>:
				<label id="alternativeId">${alternativeId}</label>
			</td>
		</tr>	
	</table>
	<div id="gridDetail" style="width:950px;"></div>
	
	
	</fieldset>
	<input type="button" id="saveBtn" value="Save" />
	<input type="button" id="cancelBtn" value="Cancel" />
	
	<input type="hidden" id="gridData" name="gridData" />
	<input type="hidden" id="altId" name="altId" value="${alternativeId}"/>
	<input type="hidden" id="hidDocNo" name="hidDocNo" value="${docNo}"/>
	<input type="hidden" id="materialCode" name="materialCode"/>
	<input type="hidden" id="currRow" name="currRow"/>
</form>
<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"  /></div>
<script>
	$(document).ajaxStart(function () {
		dimStart();
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});
	
	$(document).ajaxStop(function () {
		dimStop();
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	var popupIsOpen = false;
	function dimStart(){
		$('#dimScreen').prop('hidden', false);
	}
	
	function dimStop(){
		if(!popupIsOpen){
			$('#dimScreen').prop('hidden', true);
		}
	}
	
	$("#gridDetail").kendoGrid({
	    dataSource: {
	    	transport: {
    	        read: {
    	            url: "/MOA4/BOM/GetDDBom/?docNo=${docNo}&altId=${alternativeId}",
    	            dataType: "json"
    	        },
    	 	},
	        schema: {
	            model: {
	              id: "BOM_ID",
	              fields: {
	            	BOM_ID: { editable:false },
	                MATERIALCODE: { validation: { required: true} },
	                MATERIALNAME: { editable:false },
	                QTY : {type: "number", defaultValue:""},
	                UOM : {editable:false},
	              }
	            }
	          }
	        },
	    toolbar: ["create"],
	    editable:{
			createAt: "bottom"
		},
	    columns: [
		  { field: "BOM_ID",title: "BOM Id", width:100, template: "<span class='row-number'></span>"},
	      { field: "MATERIALCODE",title :"Material Code" , width:200 },
	      { field: "MATERIALNAME", title: "Material Name", width: 300 },
	      { field: "QTY", title: "Quantity", width: 100 },
	      { field: "UOM" , title: "UOM", width: 100 },
	      {command: ["destroy"],title:""}
	    ],
	    dataBound: function () {
	        var rows = this.items();
	        var i = 0 ;
	        $(rows).each(function () {
	            var index = $(this).index() + 1;
	            var rowLabel = $(this).find(".row-number");
	            $(rowLabel).html(index);
	            var dataItem = $("#gridDetail").data("kendoGrid").dataSource.data()[i];
	            dataItem["BOM_ID"] = index;
	            i++;
	        });
	    }
	  });
	
	$("#gridDetail tbody").on("dblclick", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		//buat popup plant
		if(colIdx==1){
			var grid = $("#gridDetail").data("kendoGrid");
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = grid.dataSource.data();
		    
		    $("#window").kendoWindow({
				width: "800px",
				height: "450px",
				actions: ["Close"],
				title: "Material Browse", 
				modal:true,
				close:function(){ 
					if($("#selectedValue1").html()!=""){
						entityGrid[rowIdx]["MATERIALCODE"] = $("#selectedValue1").html();
						entityGrid[rowIdx]["MATERIALNAME"] = $("#selectedValue2").html();
						entityGrid[rowIdx]["UOM"] = $("#selectedValue3").html();
						grid.refresh();
						$("#materialCode").val($("#selectedValue1").html());
						$("#currRow").val(rowIdx);
					}
					this.destroy();
					var div = document.createElement("div");
					div.id = "window";
					if(document.body!=null){
						document.body.appendChild(div);
					}
					$("#materialCode").val($("#materialCode").val()).change();
				}
		    
			 });
		     
			 $("#window").load("/MOA4/BOM/KendoWindowSelectMaterialBrowse/?window=window&column=3&type=BOM&plant=${plant}");
			 $("#window").data("kendoWindow").center().open();
		}
	});
	
	$("#gridDetail tbody").on("change", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		
		if(colIdx==1){
			var grid = $("#gridDetail").data("kendoGrid");
		    var rowIdx = $("tr", grid.tbody).index(row);
		    validateMaterial(rowIdx);
		}
	});
	
	$("#materialCode").on('change', function() {
	    var rowIdx = $("#currRow").val();
	    validateMaterial(rowIdx);
	});
	
	function validateMaterial(row){
		var grid = $("#gridDetail").data("kendoGrid");
		var datasource = grid.dataSource;
	    var rowIdx = row;
	    var entityGrid = grid.dataSource.data();
	    var material = entityGrid[rowIdx]["MATERIALCODE"];
	    var totalRow = datasource.total();
	    var isExist = false;
	    for(var i=0;i<totalRow;i++){
	    	if(material == entityGrid[i]["MATERIALCODE"] && rowIdx != i){
		     	isExist = true;
				break;
		    }
	    }
	    
	    if(isExist){
	    	entityGrid[rowIdx]["MATERIALCODE"] = "";
	     	entityGrid[rowIdx]["MATERIALNAME"] = "";
	     	entityGrid[rowIdx]["QTY"] = "";
	     	entityGrid[rowIdx]["UOM"] = "";
	     	alert("Material already exists !");
			grid.refresh();
	    }
	    else if(material.length > 0){
	    	$.ajax({
 				type:"GET",
 				url: "/MOA4/BOM/AjaxValidateMaterial/?material="+material+"&type=BOM&plant=${plant}",
 				success: function(result)
 				{
 					var res = result.split("#");
 					if(res[0]=="false"){
 						alert("Material tidak ditemukan");
 						entityGrid[rowIdx]["MATERIALCODE"] = "";
 						entityGrid[rowIdx]["MATERIALNAME"] = "";
 						entityGrid[rowIdx]["QTY"] = "";
 						entityGrid[rowIdx]["UOM"] = "";
 					}else{
 						entityGrid[rowIdx]["MATERIALNAME"] = res[1];
 						entityGrid[rowIdx]["UOM"] = res[2];
 					}
 					grid.refresh();
 				},
 				error:function(e){
 					entityGrid[rowIdx]["MATERIALCODE"] = "";
						grid.refresh();
						alert(e.statusText);
			 		dimStop();
 				}
	    	});
	    }
	}
	
	$("#saveBtn").click(function(){
		var grid = $("#gridDetail").data("kendoGrid");
		var datasource = grid.dataSource;
		var data = datasource.data();
		var isNull = false;
		var isZero = false;
		for(var i=0;i<data.length;i++){
			if(data[i]["MATERIALCODE"] == ""){
				isNull = true;
				break;
			}
			if(data[i]["QTY"] == ""){
				isNull = true;
				break;
			}
			if(data[i]["QTY"] == 0){
				isZero = true;
				break;
			}
		}
		if(data.length<1){
			alert("Table cannot be empty !");
		}
		else if(isNull){
			alert("There is empty column !");
		}
		else if(isZero){
			alert("Quantity must be more than 0 !");
		}
		else{
			var json = JSON.stringify(data);
			$("#gridData").val(json);
			document.bomForm.submit();
			document.getElementById("saveBtn").disabled = true;
			document.getElementById("cancelBtn").disabled = true;
		}
	});
	
	$("#cancelBtn").click(function(){
		document.getElementById("saveBtn").disabled = true;
		document.getElementById("cancelBtn").disabled = true;
		window.location.href="${cancelUrl}";
	});
</script>