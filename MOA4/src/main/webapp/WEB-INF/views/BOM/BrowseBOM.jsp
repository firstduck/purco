<%@taglib  uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- module: Bill Of Material (BOM)-->
 
         
  <h2>BOM - Browse</h2>
		<fieldset>
			<legend>Look Up</legend>
			<table>
			<tr>
				<td align="right"> 
					<div class="search display-for-form">Search</div>
				</td>
				<td>
					<INPUT TYPE="TEXT" id="search" name="search"  style="text-align:left">
				</td>
				<td>
					<input class="button" size="10" type="button"  id="btnSearch" value="Search" name="btnSearch" onclick="search()"/>
				</td>
			</tr> 
			</table>
		</fieldset>
	
		<br/>
		<kendo:grid name="gridBrowse" pageable="true" groupable="false" sortable="true">
			<kendo:grid-columns>  
	 	        <kendo:grid-column title="Document No." field="DOCNO"  template="<a href='/MOA4/BOM/Detail/?docNo=#= DOCNO #' target='_blank'>#= DOCNO #</a>" width="70px"/> 
	 	        <kendo:grid-column title="Type" field="TYPE" width="50px" />
	 	        <kendo:grid-column title="Product Code" field="MATERIALCODE" width="100px" />
	 	        <kendo:grid-column title="Product Name" field="MAKTG" width="200px" />
		        <kendo:grid-column title="Created By" field="CREATEDBY" width="30px"/>
		        <kendo:grid-column title="Created Date" field="CREATED_DATE" width="40px"/>
		        <kendo:grid-column title="Status" field="STATUS" width="30px" />
	        </kendo:grid-columns>
	        <kendo:dataSource pageSize="10" data="${data}">
	       </kendo:dataSource>
		</kendo:grid>
	
	<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"  /></div>
	<div id="popUp"></div>
	
	
	<script type="text/javascript">
	$(document).ajaxStart(function () {
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});

	$(document).ajaxStop(function () {
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	
	///ENTER action
	$("#search").keyup(function(event) {
	    if (event.keyCode === 13) {
	        $("#btnSearch").click();
	    }
	});

	////
	
	function search() { 
		var search = $("#search").val();
	  	$("#gridBrowse").data('kendoGrid').dataSource.data([]); //buat apus isi nya
		 	
	    var grid = $("#gridBrowse").data("kendoGrid");
	    $.ajax({
			type:"GET",
			url: "/MOA4/BOM/Browse/Data/?search="+search,
			success: function(result)
			{
				$("#gridBrowse").data('kendoGrid').dataSource.data(result);
				grid.refresh();
			},
			error:function(e){
				grid.refresh();
				alert(e.statusText);
	 			dimStop();
			}
   		});
	};
			
    </script>