<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
	#gridPlant .k-alt {
 	   background-color: #e6e6e6;
	}
	.k-numerictextbox .k-select { display: none; }
 	.k-numerictextbox .k-numeric-wrap { padding-right: 2px; }
</style>

<form method="POST" id="bomForm" name="bomForm" action="${action}">
	<fieldset>
		<div id="window">
		</div>
		
		<table>
			<tr>
				<td style="width: 200px;">
					<div class="display-for-form">Product Code</div>
				</td>
				<td style="width: 200px;">
					<div class="display-for-form">Product Name</div>
				</td>
			</tr>	
			<tr>
				<td>
					<div>
						<input id="productCode" name="productCode" ondblclick="windowProduct()" onchange="validateProduct(this)" style="width:200px;"/>
					</div>
				</td>
				<td>
					<div>
						<input id="productName" name="productName" style="width:400px;" disabled/>
					</div>
				</td>
			</tr>
		</table>
		<br>
		<div id="gridPlant" style="width:1000px;"></div>
	</fieldset>
	<input type="button" id="saveBtn" value="Save" />
	
	<input type="hidden" id="gridData" name="gridData" />
	<input type="hidden" id="hidDocNo" name="hidDocNo" />
	<input type="hidden" id="productExist" name="productExist" />
</form>

<div id="wait" style="display:none;width:70px;height:70px;position:fixed;top:50%;left:50%;padding:2px;"><img src='/MOA4/resources/img/loading-image.gif' class="waitGIF"  /></div>

<script>
	$(document).ready(function(){
		$("#gridPlant").hide();
		$("#saveBtn").hide();
	});
	
	$(document).ajaxStart(function () {
		dimStart();
	    document.body.style.cursor = 'wait';
	    $("#wait").css("display","block");
	});
	
	$(document).ajaxStop(function () {
		dimStop();
	    document.body.style.cursor = 'auto';
	    $("#wait").css("display","none");
	});
	var popupIsOpen = false;
	function dimStart(){
		$('#dimScreen').prop('hidden', false);
	}

	function dimStop(){
		if(!popupIsOpen){
			$('#dimScreen').prop('hidden', true);
		}
	}
	
	$("#gridPlant").kendoGrid({
        dataSource: {
            schema: {
                model: {
                  id: "ALTERNATIVE_ID",
                  fields: {
                    PLANT: { type: "number", validation: { required: true, min:1}, defaultValue:"" },
                    ALTERNATIVE_ID: { type: "number", validation: { required: true, min:1}, defaultValue:"" },
                    ROWNUM : {editable:false}
                  }
                }
              }
            },
        toolbar: ["create"],
        editable:{
			createAt: "bottom"
		},
        columns: [
		  { field: "ROWNUM",title: "No.", width:50, template: "<span class='row-number'></span>"},
          { title :"Plant" ,field: "PLANT", width:200, editor: numericEditor},
          { field: "ALTERNATIVE_ID", title: "Alternative Id", width: 200, editor: numericEditor },
          {command: ["destroy"],title:""}
        ],
        dataBound: function () {
            var rows = this.items();
            var i = 0 ;
            $(rows).each(function () {
                var index = $(this).index() + 1;
                var rowLabel = $(this).find(".row-number");
                $(rowLabel).html(index);
                var dataItem = $("#gridPlant").data("kendoGrid").dataSource.data()[i];
                dataItem["ROWNUM"] = index;
                i++;
            });
        }
	});
	
	function numericEditor(container, options) {
		  $('<input data-bind="value:' + options.field + '"/>')
		    .appendTo(container)
		    .kendoNumericTextBox({
		      format: "{0:0}",
		      decimals: 0
		    });               
		}
	
	function newGridRow(){
		var grid =$("#gridPlant").data("kendoGrid");
		grid.dataSource.sync();
		grid.dataSource.add({});
	}
	
	$("#productName").on('change', function() {
		var grid =$("#gridPlant").data("kendoGrid");
		$.ajax({
			type:"GET",
			url: "/MOA4/BOM/AjaxValidateProduct/?productCode="+$("#productCode").val(),
			success: function(result)
			{
				grid.dataSource.data(result);
				if(grid.dataSource.data().length > 0){
					$("#productExist").val("1");
				}else{
					$("#productExist").val("0");
				}
				$("#gridPlant").show();
				$("#saveBtn").show();
				
			},
			error:function(e){
				alert(e.statusText);
	 		dimStop();
			}
   		});
		
	});
	
	function validateProduct(e){
		$.ajax({
			type:"GET",
			url: "/MOA4/BOM/ValidateProductHeader/?productCode="+e.value,
			success: function(result)
			{
				var res = result.split("#");
				if(res[0]=="false"){
					$("#productCode").val("");
					$("#productName").val("");
					$("#gridPlant").data("kendoGrid").dataSource.data([]);
					
					$("#gridPlant").hide();
					$("#saveBtn").hide();
					alert("Material Code tidak ditemukan !");
				}else{
					$("#productCode").val(res[1]);
					$("#productName").val(res[2]).change();
				}
			},
			error:function(e){
				alert(e.statusText);
	 		dimStop();
			}
   		});
	}
	
	function windowProduct(){
		$("#gridPlant").hide();
		$("#saveBtn").hide();
		//kosongin datasource grid nya
		$("#gridPlant").data("kendoGrid").dataSource.data([]);
		
		$("#window").kendoWindow({
			width: "800px",
			height: "450px",
			actions: ["Close"],
			title: "Product Name Browse", 
			modal: false,
			close:function(){ 
				if($("#selectedValue1").html()!=""){
					$("#productCode").val($("#selectedValue1").html());
					$("#productName").val($("#selectedValue2").html()).change();
				}
				this.destroy();
				var div = document.createElement("div");
				div.id = "window";
				if(document.body != null) {document.body.appendChild(div);};
			}
		 });
		 $("#window").load("/MOA4/BOM/KendoWindowSelectProductBrowse/?window=window&column=2");
		 $("#window").data("kendoWindow").center();
	}
	
	$("#gridPlant tbody").on("dblclick", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		var productCode = $("#productCode").val();
		
		//buat popup plant
		if(colIdx==1){
			var grid = $("#gridPlant").data("kendoGrid");
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = grid.dataSource.data();
		    
		    $("#window").kendoWindow({
				width: "600px",
				height: "450px",
				actions: ["Close"],
				title: "Plant Browse", 
				modal:true,
				close:function(){ 
					if($("#selectedValue1").html()!=""){
						entityGrid[rowIdx]["PLANT"] = $("#selectedValue1").html();
						grid.refresh();
					}
					this.destroy();
					var div = document.createElement("div");
					div.id = "window";
					if(document.body!=null){
						document.body.appendChild(div);
					}
				}
			 });
		    
			 $("#window").load("/MOA4/BOM/KendoWindowSelectPlantBrowse/?window=window&column=2&materialCode="+productCode);
			 $("#window").data("kendoWindow").center().open();
		}
	});
	
	$("#gridPlant tbody").on("change", "td", function(e){
		var row = $(this).closest("tr");
		var colIdx = $("td", row).index(this);
		var productCode = $("#productCode").val();
		
		if(colIdx==1){
			var grid = $("#gridPlant").data("kendoGrid");
			var datasource = grid.dataSource;
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = datasource.data();
		    var plant = entityGrid[rowIdx]["PLANT"];
		    if(plant.toString().length > 0){
		    	$.ajax({
	 				type:"GET",
	 				url: "/MOA4/BOM/AjaxValidatePlant/?plant="+plant+"&productCode="+productCode,
	 				success: function(result)
	 				{
	 					if(!result){
	 						alert("Plant tidak ditemukan");
	 						entityGrid[rowIdx]["PLANT"] = "";
	 						entityGrid[rowIdx]["ALTERNATIVE_ID"] = "";
	 						grid.refresh();
	 					}	
	 				},
	 				error:function(e){
	 					entityGrid[rowIdx]["PLANT"] = "";
	 					entityGrid[rowIdx]["ALTERNATIVE_ID"] = "";
 						grid.refresh();
 						alert(e.statusText);
				 		dimStop();
	 				}
		    	});
		    }
		}
// 		buat pas ganti alternative id nya, validasi gaboleh ada yang sama
		else if(colIdx==2){
			var grid = $("#gridPlant").data("kendoGrid");
			var datasource = grid.dataSource;
		    var rowIdx = $("tr", grid.tbody).index(row);
		    var entityGrid = datasource.data();
		    grid.refresh();
		    var altId = entityGrid[rowIdx]["ALTERNATIVE_ID"];
		    var totalRow = datasource.total();
		    for(var i=0;i<totalRow;i++){
		    	if(altId == entityGrid[i]["ALTERNATIVE_ID"] && rowIdx != i){
			     	entityGrid[rowIdx]["ALTERNATIVE_ID"] = "";
			     	alert("Alternative Id must be unique !");
					grid.refresh();
					break;
			    }
		    }
		}
	});
	
	$("#saveBtn").click(function(){
		var grid = $("#gridPlant").data("kendoGrid");
		var datasource = grid.dataSource;
		var data = datasource.data();
		var isNull = false;
		for(var i=0;i<data.length;i++){
			if(data[i]["PLANT"] == ""){
				isNull = true;
				break;
			}
			if(data[i]["ALTERNATIVE_ID"] == ""){
				isNull = true;
				break;
			}
		}
		if(data.length<1){
			alert("Table cannot be empty!");
		}
		else if(isNull){
			alert("There is an empty colum!");
		}
		else{
			var json = JSON.stringify(data);
			$("#gridData").val(json);
			document.bomForm.submit();
			document.getElementById("saveBtn").disabled = true;
		}
	});
	
</script>