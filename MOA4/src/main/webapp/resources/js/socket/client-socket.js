// to keep the session id
var sessionId = '';

// name of the client
var name = '';
var username = '';
var height = 0;
var logSize = 0;
var MAX_LOG_SIZE = 100;

// socket connection url and port
var socket_url = '10.2.28.240';
var port = '8080';


$(document).ready(function() {
	openSocket();
	$(window).bind("beforeunload", function() { 
		if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
			 closeSocket();
		}
    });
});

var webSocket;

/**
 * Will open the socket connection with following condition :
 * 1. Have #base_username hidden value
 * 2. Have UserLogin in #base_username
 */
function openSocket() {
	if($("#base_username").val() === undefined){
		console.log("Socket Require #base_username!!");
		return;
	}
	if($("#base_username").val() == ''){
		console.log("Socket Require UserLogin!!");
		return;
	}
	
	if($("#base_ip_notification").val() === undefined){
		console.log("Socket Require #base_ip_notification!!");
		return;
	}
	if($("#base_ip_notification").val() == ''){
		console.log("Socket Require Address!!");
		return;
	}
	console.log("Socket: " + $("#base_ip_notification").val());
	console.log("User: " + $("#base_username").val());
	// Ensures only one connection is open at a time
	if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
		return;
	}
	webSocket = new WebSocket("ws://" + $("#base_ip_notification").val() + ":" + port
			+ "/mware/notification?name=" + $("#base_username").val());
	
	webSocket.onopen = function(event) {
		if (event.data === undefined)
			return;

	};

	webSocket.onmessage = function(event) {
		parseMessage(event.data);
	};

	webSocket.onclose = function(event) {
		
	};
}

/**
 * Closing the socket connection
 */
function closeSocket() {
	webSocket.close();
}

function parseMessage(message) {
	var jObj = $.parseJSON(message);
	
	if (jObj.flag == 'notification') {
		$.notify({
			title: 'Notification',
			message: jObj.message,
			url: jObj.link,
		},{
			type: 'minimalist',
			allow_dismiss: true,
			newest_on_top: false,
			placement: {
				from: "bottom",
				align: "right"
			},
			delay: 10000,
			allow_duplicates: true,
			animate: {
				enter: 'animated fadeInRight',
				exit: 'animated fadeOutRight'
			},
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
	    		'<img data-notify="icon" class="img-circle pull-left">' +
	    		'<span data-notify="title">{1}</span>' +
	    		'<span data-notify="message">{2}</span></br></br>' +
	    		'<span data-notify="url-link"><a href={3} target="_blank">(Show Status)</></span>' +
	    	'</div>'
		});
	}
}