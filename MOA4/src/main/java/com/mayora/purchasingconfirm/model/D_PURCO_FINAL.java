package com.mayora.purchasingconfirm.model;

public class D_PURCO_FINAL {
	private String finishGoodCode;
	private String finishGoodName;
	private String plannedFinishGood;
	private String maxFinishGood;
	private String finalFinishGood;
	public String getFinishGoodCode() {
		return finishGoodCode;
	}
	public void setFinishGoodCode(String finishGoodCode) {
		this.finishGoodCode = finishGoodCode;
	}
	public String getFinishGoodName() {
		return finishGoodName;
	}
	public void setFinishGoodName(String finishGoodName) {
		this.finishGoodName = finishGoodName;
	}
	public String getPlannedFinishGood() {
		return plannedFinishGood;
	}
	public void setPlannedFinishGood(String plannedFinishGood) {
		this.plannedFinishGood = plannedFinishGood;
	}
	public String getMaxFinishGood() {
		return maxFinishGood;
	}
	public void setMaxFinishGood(String maxFinishGood) {
		this.maxFinishGood = maxFinishGood;
	}
	public String getFinalFinishGood() {
		return finalFinishGood;
	}
	public void setFinalFinishGood(String finalFinishGood) {
		this.finalFinishGood = finalFinishGood;
	}
	
	
}
