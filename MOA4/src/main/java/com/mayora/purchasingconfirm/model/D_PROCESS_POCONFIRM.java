package com.mayora.purchasingconfirm.model;

public class D_PROCESS_POCONFIRM {
	private String materialCode;
	private String materialName;
	private String materialOutstanding;
	private String materialConfirm;
	private String week;
	private String year;
	
	
	public String getMaterialConfirm() {
		return materialConfirm;
	}
	public void setMaterialConfirm(String materialConfirm) {
		this.materialConfirm = materialConfirm;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getMaterialOutstanding() {
		return materialOutstanding;
	}
	public void setMaterialOutstanding(String materialOutstanding) {
		this.materialOutstanding = materialOutstanding;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	
	
}	
