package com.mayora.purchasingconfirm.model;

public class D_REPORT_FINAL {
	private String finishGoodCode;
	private String finishGoodName;
	private int finishGoodQty;
	public String getFinishGoodCode() {
		return finishGoodCode;
	}
	public void setFinishGoodCode(String finishGoodCode) {
		this.finishGoodCode = finishGoodCode;
	}
	public String getFinishGoodName() {
		return finishGoodName;
	}
	public void setFinishGoodName(String finishGoodName) {
		this.finishGoodName = finishGoodName;
	}
	public int getFinishGoodQty() {
		return finishGoodQty;
	}
	public void setFinishGoodQty(int finishGoodQty) {
		this.finishGoodQty = finishGoodQty;
	}
	
}
