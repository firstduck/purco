package com.mayora.purchasingconfirm.model;

public class D_FDISFROMREMAININGMATERIAL {
	private String FINISHGOODCODE;
	private String FINISHGOODNAME;
	private int FINISHGOODQTY;
	public String getFINISHGOODCODE() {
		return FINISHGOODCODE;
	}
	public void setFINISHGOODCODE(String fINISHGOODCODE) {
		FINISHGOODCODE = fINISHGOODCODE;
	}
	public String getFINISHGOODNAME() {
		return FINISHGOODNAME;
	}
	public void setFINISHGOODNAME(String fINISHGOODNAME) {
		FINISHGOODNAME = fINISHGOODNAME;
	}
	public int getFINISHGOODQTY() {
		return FINISHGOODQTY;
	}
	public void setFINISHGOODQTY(int fINISHGOODQTY) {
		FINISHGOODQTY = fINISHGOODQTY;
	}
	
	
	
	
}
