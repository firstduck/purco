package com.mayora.purchasingconfirm.model;

public class D_GRIDMATERIAL {
	private String materialName;
	private String materialCode;
	private String bomUom;
	private String bomQty;
	private String remainingFinishGoodQty;
	private String remainingBomQty;
	
	
	public String getBomQty() {
		return bomQty;
	}
	public void setBomQty(String bomQty) {
		this.bomQty = bomQty;
	}
	public String getBomUom() {
		return bomUom;
	}
	public void setBomUom(String bomUom) {
		this.bomUom = bomUom;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getRemainingFinishGoodQty() {
		return remainingFinishGoodQty;
	}
	public void setRemainingFinishGoodQty(String remainingFinishGoodQty) {
		this.remainingFinishGoodQty = remainingFinishGoodQty;
	}
	public String getRemainingBomQty() {
		return remainingBomQty;
	}
	public void setRemainingBomQty(String remainingBomQty) {
		this.remainingBomQty = remainingBomQty;
	}
	
	
}
