package com.mayora.purchasingconfirm.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.DD_PURCHASINGCONFIRMATION")
public class DD_PURCHASINGCONFIRMATION {
	@EmbeddedId
	private DD_PURCHASINGCONFIRMATION_PK pk;
	
	@Column(name="SUPPLIERNAME")
	private String vendorName;
	@Column(name="QUANTITY")
	private String quantity;
	
	public DD_PURCHASINGCONFIRMATION_PK getPk() {
		return pk;
	}
	public void setPk(DD_PURCHASINGCONFIRMATION_PK pk) {
		this.pk = pk;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
