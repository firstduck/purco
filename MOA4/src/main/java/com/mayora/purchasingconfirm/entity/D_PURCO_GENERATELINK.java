package com.mayora.purchasingconfirm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.D_PURCO_GENERATELINK")
public class D_PURCO_GENERATELINK {
	@Column(name="DOCNO")
	private String docNo;
	@Column(name="MATERIALCODE")
	private String materialCode;
	@Column(name="MATERIALNAME")
	private String materialName;
	@Id
	@Column(name="REF_DOCNO")
	private String refDocno;
	@Column(name="WEEK")
	private String week;
	@Column(name="YEAR")
	private String year;
	@Column(name="PLANORDER_QTY")
	private String qty;
	
	
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getRefDocno() {
		return refDocno;
	}
	public void setRefDocno(String refDocno) {
		this.refDocno = refDocno;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	
}
