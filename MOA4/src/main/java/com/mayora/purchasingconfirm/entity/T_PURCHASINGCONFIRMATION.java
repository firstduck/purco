package com.mayora.purchasingconfirm.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.mayora.purchasingconfirm.controller.PurchasingConfirmController;

@Entity
@Table(name="MYROA.T_PURCHASINGCONFIRMATION")
public class T_PURCHASINGCONFIRMATION extends PurchasingConfirmController{
	
	@EmbeddedId
	private T_PURCHASINGCONFIRMATION_PK pk;
	@Column(name="USERNAME")
	private String username;
	@Column(name="STATUS")
	private String status;
	@Column(name="LAST_RUN")
	private String lastRun;
	@Column(name="CREATED_BY")
	private String createdBy;
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	
	public String getLastRun() {
		return lastRun;
	}
	public void setLastRun(String lastRun) {
		this.lastRun = lastRun;
	}
	public T_PURCHASINGCONFIRMATION_PK getPk() {
		return pk;
	}
	public void setPk(T_PURCHASINGCONFIRMATION_PK pk) {
		this.pk = pk;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public void add(String a, String b, String c, String d, String e, String f, Date g, String h){
		this.pk.setDocNo(a);
		this.pk.setMonth(b);
		this.pk.setYear(c);
		
		this.setUsername(d);
		this.setStatus(e);
		this.setCreatedBy(f);
		this.setCreatedDate(g);
		this.setLastRun(h);
		pcAdapter.addObjectOA(this);
	}
	
	
}
