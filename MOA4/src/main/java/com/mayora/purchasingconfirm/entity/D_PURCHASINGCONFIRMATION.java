package com.mayora.purchasingconfirm.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.D_PURCHASINGCONFIRMATION")
public class D_PURCHASINGCONFIRMATION {

	@EmbeddedId
	private D_PURCHASINGCONFIRMATION_PK pk;
	@Column(name="MATERIALNAME")
	private String materialName;
	@Column(name="CONFIRM")
	private String confirm;
	@Column(name="OUTSTANDING")
	private String outstanding;
	@Column(name="DONE")
	private int done;
	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	public String getOutstanding() {
		return outstanding;
	}
	public void setOutstanding(String outstanding) {
		this.outstanding = outstanding;
	}
	public int getDone() {
		return done;
	}
	public void setDone(int done) {
		this.done = done;
	}
	public void setPk(D_PURCHASINGCONFIRMATION_PK pk) {
		this.pk = pk;
	}
	
	
	
		
	
}
