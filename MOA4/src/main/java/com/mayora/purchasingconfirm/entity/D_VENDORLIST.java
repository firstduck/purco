package com.mayora.purchasingconfirm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.D_VENDORLIST")
public class D_VENDORLIST {
	@Id
	@Column(name="VENDORCODE")
	private String vendorCode;
	@Column(name="VENDORNAME")
	private String vendorName;
	@Column(name="VENDOREMAIL")
	private String vendorEmail;
	@Column(name="VENDORADDRESS")
	private String vendorAddress;
	@Column(name="PASSWORD")
	private String password;
	@Column(name="COUNTRY")
	private String country;
	@Column(name="OWNER")
	private String owner;
	@Column(name="TOTAL_EMPLOYEE")
	private String totalEmployee;
	@Column(name="HEADQUARTER")
	private String headquarter;
	@Column(name="STATUS")
	private String status;
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getTotalEmployee() {
		return totalEmployee;
	}
	public void setTotalEmployee(String totalEmployee) {
		this.totalEmployee = totalEmployee;
	}
	public String getHeadquarter() {
		return headquarter;
	}
	public void setHeadquarter(String headquarter) {
		this.headquarter = headquarter;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	public String getVendorAddress() {
		return vendorAddress;
	}
	public void setVendorAddress(String vendorAddress) {
		this.vendorAddress = vendorAddress;
	}
	
	
}
