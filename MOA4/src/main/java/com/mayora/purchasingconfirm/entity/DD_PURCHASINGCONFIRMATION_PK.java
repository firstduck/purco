package com.mayora.purchasingconfirm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DD_PURCHASINGCONFIRMATION_PK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name="DOCNO")
	private String docNo;
	@Column(name="MATERIALCODE")
	private String materialCode;
	@Column(name="YEAR")
	private String year;
	@Column(name="WEEK")
	private String week;
	@Column(name="SUPPLIERCODE")
	private String vendorCode;
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
}
