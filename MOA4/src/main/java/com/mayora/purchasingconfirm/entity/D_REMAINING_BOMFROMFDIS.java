package com.mayora.purchasingconfirm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="D_REMAINING_BOMFROMFDIS")
public class D_REMAINING_BOMFROMFDIS {
	@Id
	@Column(name="DOCNO")
	private String docNo;
	@Column(name="WEEK")
	private String week;
	@Column(name="YEAR")
	private String year;
	@Column(name="MATERIALCODE")
	private String materialCode;
	@Column(name="MATERIALNAME")
	private String materialName;
	@Column(name="REMAININGQUANTITY")
	private String remainingQty;
	@Column(name="BOMUOM")
	private String bomUom;
	@Column(name="FINISHGOODCODE")
	private String finishGoodCode;
	@Column(name="FINISHGOODNAME")
	private String finishGoodName;
	
	
	public String getFinishGoodCode() {
		return finishGoodCode;
	}
	public void setFinishGoodCode(String finishGoodCode) {
		this.finishGoodCode = finishGoodCode;
	}
	public String getFinishGoodName() {
		return finishGoodName;
	}
	public void setFinishGoodName(String finishGoodName) {
		this.finishGoodName = finishGoodName;
	}
	public String getBomUom() {
		return bomUom;
	}
	public void setBomUom(String bomUom) {
		this.bomUom = bomUom;
	}
	
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getRemainingQty() {
		return remainingQty;
	}
	public void setRemainingQty(String remainingQty) {
		this.remainingQty = remainingQty;
	}
	
	
}
