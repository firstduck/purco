package com.mayora.purchasingconfirm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.D_PURCHASINGCONFIRMATION_FINAL")
public class D_PURCHASINGCONFIRMATION_FINAL {
	@Column(name="BOMQUANTITY")
	private String bomQty;
	@Column(name="BOMUOM")
	private String bomUom;
	@Column(name="CONFIRMEDFINISHGOODQTY")
	private String confirmedFinishGoodQty;
	@Id
	@Column(name="DOCNO")
	private String docNo;
	@Column(name="FINISHGOODCODE")
	private String finishGoodCode;
	@Column(name="FINISHGOODNAME")
	private String finishGoodName;
	@Column(name="PLANNEDFINISHGOODQTY")
	private String plannedFinishGoodQty;
	@Column(name="PRIORITY")
	private int priority;
	@Column(name="PLANNEDTOTALBOMQTY")
	private String plannedTotalBomQty;
	@Column(name="CONFIRMEDTOTALBOMQTY")
	private String confirmedTotalBomQty;
	@Column(name="MATERIALCODE")
	private String materialCode;
	@Column(name="MATERIALNAME")
	private String materialName;
	@Column(name="YEAR")
	private String year;
	@Column(name="WEEK")
	private String week;
	@Column(name="SIMULATION_DOCNO")
	private String simDocno;
	
	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getSimDocno() {
		return simDocno;
	}
	public void setSimDocno(String simDocno) {
		this.simDocno = simDocno;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getBomQty() {
		return bomQty;
	}
	public void setBomQty(String bomQty) {
		this.bomQty = bomQty;
	}
	public String getBomUom() {
		return bomUom;
	}
	public void setBomUom(String bomUom) {
		this.bomUom = bomUom;
	}
	public String getConfirmedFinishGoodQty() {
		return confirmedFinishGoodQty;
	}
	public void setConfirmedFinishGoodQty(String confirmedFinishGoodQty) {
		this.confirmedFinishGoodQty = confirmedFinishGoodQty;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getFinishGoodCode() {
		return finishGoodCode;
	}
	public void setFinishGoodCode(String finishGoodCode) {
		this.finishGoodCode = finishGoodCode;
	}
	public String getFinishGoodName() {
		return finishGoodName;
	}
	public void setFinishGoodName(String finishGoodName) {
		this.finishGoodName = finishGoodName;
	}
	public String getPlannedFinishGoodQty() {
		return plannedFinishGoodQty;
	}
	public void setPlannedFinishGoodQty(String plannedFinishGoodQty) {
		this.plannedFinishGoodQty = plannedFinishGoodQty;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getPlannedTotalBomQty() {
		return plannedTotalBomQty;
	}
	public void setPlannedTotalBomQty(String plannedTotalBomQty) {
		this.plannedTotalBomQty = plannedTotalBomQty;
	}
	public String getConfirmedTotalBomQty() {
		return confirmedTotalBomQty;
	}
	public void setConfirmedTotalBomQty(String confirmedTotalBomQty) {
		this.confirmedTotalBomQty = confirmedTotalBomQty;
	}
	
	
	
	
}

