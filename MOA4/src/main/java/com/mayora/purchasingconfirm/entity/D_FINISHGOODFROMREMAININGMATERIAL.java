package com.mayora.purchasingconfirm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.D_FDISFROMREMAININGMATERIAL")
public class D_FINISHGOODFROMREMAININGMATERIAL {
	@Id
	@Column(name = "DOCNO")
	private String docNo;
	@Column(name = "FINISHGOODCODE")
	private String finishGoodCode;
	@Column(name = "FINISHGOODNAME")
	private String finishGoodName;
	@Column(name = "FINISHGOODQTY")
	private int finishGoodQty;
	@Column(name = "WEEK")
	private String week;
	@Column(name = "YEAR")
	private String year;
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getFinishGoodCode() {
		return finishGoodCode;
	}
	public void setFinishGoodCode(String finishGoodCode) {
		this.finishGoodCode = finishGoodCode;
	}
	public String getFinishGoodName() {
		return finishGoodName;
	}
	public void setFinishGoodName(String finishGoodName) {
		this.finishGoodName = finishGoodName;
	}
	
	public int getFinishGoodQty() {
		return finishGoodQty;
	}
	public void setFinishGoodQty(int finishGoodQty) {
		this.finishGoodQty = finishGoodQty;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	
}
