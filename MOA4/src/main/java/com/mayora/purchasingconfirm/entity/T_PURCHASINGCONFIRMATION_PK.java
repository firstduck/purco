package com.mayora.purchasingconfirm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class T_PURCHASINGCONFIRMATION_PK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name="DOCNO")
	private String docNo;
	@Column(name="YEAR")
	private String year;
	@Column(name="MONTH")
	private String month;
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
}
