package com.mayora.purchasingconfirm.controller;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mayora.masterdata.model.KendoWindowBrowse2;
import com.mayora.moa.BaseController;
import com.mayora.moa.session.UserAccess;
import com.mayora.purchasingconfirm.adapter.PurchasingConfirmAdapter;
import com.mayora.purchasingconfirm.entity.DD_PURCHASINGCONFIRMATION;
import com.mayora.purchasingconfirm.entity.DD_PURCHASINGCONFIRMATION_PK;
import com.mayora.purchasingconfirm.entity.D_FINISHGOODFROMREMAININGMATERIAL;
import com.mayora.purchasingconfirm.entity.D_PURCHASINGCONFIRMATION;
import com.mayora.purchasingconfirm.entity.D_PURCHASINGCONFIRMATION_FINAL;
import com.mayora.purchasingconfirm.entity.D_PURCHASINGCONFIRMATION_PK;
import com.mayora.purchasingconfirm.entity.D_PURCO_GENERATELINK;
import com.mayora.purchasingconfirm.entity.D_REMAINING_BOMFROMFDIS;
import com.mayora.purchasingconfirm.entity.D_VENDORLIST;
import com.mayora.purchasingconfirm.entity.T_PURCHASINGCONFIRMATION;
import com.mayora.purchasingconfirm.entity.T_PURCHASINGCONFIRMATION_PK;
import com.mayora.purchasingconfirm.model.D_FDISFROMREMAININGMATERIAL;
import com.mayora.purchasingconfirm.model.D_GRIDMATERIAL;
import com.mayora.purchasingconfirm.model.D_PROCESS_POCONFIRM;
import com.mayora.purchasingconfirm.model.D_PURCO_FINAL;
import com.mayora.purchasingconfirm.model.D_REPORT_FINAL;
import com.mayora.purchasingconfirm.model.D_VENDORCONFIRM;


@Controller
public class PurchasingConfirmController extends BaseController{
	@Autowired
	protected PurchasingConfirmAdapter pcAdapter;
	
	//Controller untuk ke page spreadsheet
	@RequestMapping(value = { "/PUR-CO/Create" }, method = RequestMethod.GET)
	public String create(Model model, HttpServletRequest req){
		List<Integer> listYear = new ArrayList<>();
		List<Integer> listMonth = new ArrayList<>();
		List<String> companyName = new ArrayList<>();
				
		Calendar c = Calendar.getInstance();
		int currYear = c.get(Calendar.YEAR);
		
		listYear.add(currYear);
		listYear.add(currYear-1);
		listYear.add(currYear-2);
		
		for(int i=1;i<=12;i++){
			listMonth.add(i);
		}
		
		//get company name yang melakukan plan order
		companyName = pcAdapter.getDataCompanyName();
		
		model.addAttribute("year",listYear);
		model.addAttribute("month",listMonth);
		model.addAttribute("companyName",companyName);
		return "Spreadsheet";
	}
	
	//Ajax get Detail Plan Order untuk di tampilkan di spreadsheet
	@RequestMapping(value = { "/PUR-CO/AjaxSpreadsheet" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> ajaxSpreadsheet(Model model, HttpServletRequest req){
		String year = req.getParameter("year");
		String month = req.getParameter("month");
		String companyName = req.getParameter("companyName");
		String type = req.getParameter("type");
		
		int mon = Integer.parseInt(month);
		int[] months = {mon,mon+1,mon+2,mon+3};
		
		List<Map<String,Object>> listDataAll = new ArrayList<>();
		List<Map<String,Object>> listData = new ArrayList<>();
		
		for(int i = 0;i<months.length;i++){
			if(months[i] > 12){
				months[i]-=12;
				year+=1;
			}
			switch(type){
				case "PODetail": 
					listData = pcAdapter.getListPODetail(year, months[i], companyName);
					break;
				case "PO": 
					listData = pcAdapter.getListPO(year, months[i], companyName);
					break;
				case "PurchaseConfirm": 
					listData = pcAdapter.getListPurchaseConfirm(year, months[i],companyName);
					break;
			}
			listDataAll.addAll(listData);
		}
		return listDataAll;
	}
	
	@RequestMapping(value="/PUR-CO/AjaxDataSpreadsheet", method = {RequestMethod.GET})
	public @ResponseBody String ajaxDataSpreadsheet(HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes ra){
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		String companyName = request.getParameter("companyName");
		String type = request.getParameter("type");
		
		int mon = Integer.parseInt(month);
		int[] months = {mon,mon+1,mon+2,mon+3};
		
		List<String> listDataAll = new ArrayList<>();
		List<String> listData = new ArrayList<>();
		
		for(int i = 0;i<months.length;i++){
			if(months[i] > 12){
				months[i]-=12;
				year+=1;
			}
			
			listData = pcAdapter.getWeek(months[i], year);
			
			for(int j=0;j<listData.size();j++){
				listDataAll.add(listData.get(j));
			}
		}
		
		//jika data sama tidak diambil lagi
		Object[] st = listDataAll.toArray();
	    for (Object s : st) {
	      if (listDataAll.indexOf(s) != listDataAll.lastIndexOf(s)) {
	    	  listDataAll.remove(listDataAll.lastIndexOf(s));
	      }
	    }
	    
	    List<LinkedHashMap<String, String>> listDataPivot = new ArrayList<>();
	    switch(type){
		    case "DataConfirm": 
		    	pcAdapter.getWeekPivot(listDataAll,companyName,year,month);
		 		listDataPivot = pcAdapter.getPurchaseConfirmPivot(listDataAll);
		    	break;
		    case "DataPO": 
		    	pcAdapter.getWeekPivot2(listDataAll,companyName,year,month);
				listDataPivot = pcAdapter.getPlanOrderPivot(listDataAll);
		    	break;
		    case "DataPOAndConfirm": 
		    	pcAdapter.getViewWeekJoinConfirmAndPO(listDataAll);
				listDataPivot = pcAdapter.getWeekJoinConfirmAndPO(listDataAll);
		    	break;
	    }
	   
		return new Gson().toJson(listDataPivot);
	}
	
	//PROSES PERTAMA
	//get data GAP yang masih minus / bewarna merah lalu memasukan data ke tabel t_purchasingconfirmation dan d_purchasing confirmation
	@RequestMapping(value = { "/PUR-CO/LackingMaterials" }, method = RequestMethod.GET)
	public String lackingMaterials(Model model, HttpServletRequest req){
			String year = req.getParameter("year");
			String month = req.getParameter("month");
			
			DecimalFormat formatter = new DecimalFormat("#");
			UserAccess ua = this.getSession(req);
			String docNo= pcAdapter.getDocNo(pcAdapter.setModule(), ua.getUser().getGroupCode());
			pcAdapter.insertTrack(docNo, ua.getUser().getUsername(), pcAdapter.setModule(), "Insert", "#SELF#", "Insert PUR-CO", 0);
			T_PURCHASINGCONFIRMATION_PK t_pk= new T_PURCHASINGCONFIRMATION_PK();
			T_PURCHASINGCONFIRMATION t = new T_PURCHASINGCONFIRMATION();
			
			t_pk.setDocNo(docNo);
			t_pk.setMonth(month);
			t_pk.setYear(year);
			t.setPk(t_pk);
			t.setUsername(ua.getUser().getUsername());
			t.setStatus("OPEN");
			t.setCreatedBy(ua.getUser().getName());
			t.setCreatedDate(new Date());
			t.setLastRun("1");
			pcAdapter.addObjectOA(t);
			
			
			int mon = Integer.parseInt(month);
			int[] months = {mon,mon+1,mon+2,mon+3};
			List<D_PROCESS_POCONFIRM> d_proc = new ArrayList<>();
			for(int i = 0;i<months.length;i++){
				if(months[i] > 12){
					months[i]-=12;
					year+=1;
			}
			
			List<String> listData2 = pcAdapter.getWeek(months[i], year);
			
			for(int j=0;j<listData2.size();j++){
				String week = listData2.get(j)+"_GAP";
				List<Map<String,Object>> processList = pcAdapter.getWeekForProcess(week);
				
				for(int k=0;k<processList.size();k++){
					double materialOS = Double.parseDouble(processList.get(k).get(week).toString())*-1;
					
					D_PURCHASINGCONFIRMATION_PK d_pk = new D_PURCHASINGCONFIRMATION_PK();
					D_PURCHASINGCONFIRMATION d = new D_PURCHASINGCONFIRMATION();
					d_pk.setDocNo(docNo);
					d_pk.setYear(year);
					d_pk.setWeek(listData2.get(j).substring(8,10));
					d_pk.setMaterialCode(processList.get(k).get("MATERIALCODE").toString());
					d.setConfirm(0+"");
					d.setMaterialName(processList.get(k).get("MATERIALNAME").toString());
					d.setOutstanding(formatter.format(materialOS)+"");
					d.setDone(0);
					d.setPk(d_pk);
					pcAdapter.addObjectOA(d);
					
					D_PROCESS_POCONFIRM d_pro = new D_PROCESS_POCONFIRM();
					d_pro.setMaterialCode(processList.get(k).get("MATERIALCODE").toString());
					d_pro.setMaterialName(processList.get(k).get("MATERIALNAME").toString());
					d_pro.setMaterialOutstanding(formatter.format(materialOS)+"");
					d_pro.setMaterialConfirm(0+"");
					d_pro.setWeek(listData2.get(j).substring(8, 10));
					d_pro.setYear(listData2.get(j).substring(4, 8));
					d_proc.add(d_pro);
				}
			}
		}
		model.addAttribute("docNo",docNo);
		model.addAttribute("d_proc",d_proc);
		return "PurchasingConfirmProcess";
	}
	
	
	//PROSES KEDUA
	//get data yang masih memiliki GAP setelah proses pertama, dan insert into dd_purchasingconfirmation untuk vendor yang sudah confirm pada proses pertama
	@RequestMapping(value = { "/PUR-CO/AffectedProducts" }, method = {RequestMethod.GET,RequestMethod.POST})
	public String affectedProducts(Model model, HttpServletRequest req) throws JSONException{
		try{
			String json = req.getParameter("jsonProcess");
			System.out.println("jsonnn"+ json);
			String docNo = req.getParameter("docNo");
			T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
			tp.setLastRun("2");
			pcAdapter.updateObjectOA(tp);
			String jsonSessionConfirm = req.getParameter("sessionGridConfirm");
			String[] currSessionConfirm = jsonSessionConfirm.split("#");
			
			Type typeList = new TypeToken<List<D_PROCESS_POCONFIRM>>(){}.getType();
			Type listConfirmVendor = new TypeToken<List<D_VENDORCONFIRM>>(){}.getType();
			List<D_PROCESS_POCONFIRM> listDataGrid = new Gson().fromJson(json, typeList);
			pcAdapter.deletePurCo(docNo);
			pcAdapter.deleteDDPurCo(docNo);
			int currSessionIndex = -1;
			for(int i = 0;i<listDataGrid.size();i++){
				currSessionIndex++;
				String dataGrid="";
				
				if(!currSessionConfirm[currSessionIndex].equals("null")){
					dataGrid = currSessionConfirm[currSessionIndex];
				}
	
				if(!dataGrid.equals("")){
					List<D_VENDORCONFIRM> listDataVendor = new Gson().fromJson(dataGrid, listConfirmVendor);
					for(int j=0;j<listDataVendor.size();j++){
						DD_PURCHASINGCONFIRMATION_PK dd_pk = new DD_PURCHASINGCONFIRMATION_PK();
						DD_PURCHASINGCONFIRMATION dd = new DD_PURCHASINGCONFIRMATION();
						dd_pk.setDocNo(docNo);
						dd_pk.setMaterialCode(listDataGrid.get(i).getMaterialCode());
						dd_pk.setWeek(listDataGrid.get(i).getWeek());
						dd_pk.setYear(listDataGrid.get(i).getYear());
						dd_pk.setVendorCode(listDataVendor.get(j).getVendorCode());
						dd.setPk(dd_pk);
						dd.setQuantity(listDataVendor.get(j).getQuantity());
						dd.setVendorName(listDataVendor.get(j).getVendorName());
						pcAdapter.addObjectOA(dd);
					}
				}
				
				D_PURCHASINGCONFIRMATION_PK d_pk = new D_PURCHASINGCONFIRMATION_PK();
				D_PURCHASINGCONFIRMATION d = new D_PURCHASINGCONFIRMATION();
				d_pk.setDocNo(docNo);
				d_pk.setMaterialCode(listDataGrid.get(i).getMaterialCode());
				d_pk.setWeek(listDataGrid.get(i).getWeek());
				d_pk.setYear(listDataGrid.get(i).getYear());
				d.setOutstanding(listDataGrid.get(i).getMaterialOutstanding());
				d.setMaterialName(listDataGrid.get(i).getMaterialName());
				d.setConfirm(listDataGrid.get(i).getMaterialConfirm());
				d.setPk(d_pk);
				
				if(Integer.parseInt(listDataGrid.get(i).getMaterialConfirm()) >= Integer.parseInt(listDataGrid.get(i).getMaterialOutstanding())){
					d.setDone(1);
					listDataGrid.remove(i);
					i--;
				}
				else {
					d.setDone(0);
				}
				pcAdapter.addObjectOA(d);
			}
			model.addAttribute("json",json);
			model.addAttribute("docNo", docNo);
			model.addAttribute("d_proc",listDataGrid);
			return "NextConfirmProcess";
		}catch(Exception e){
			return "redirect:/PUR-CO/Create";
		}
	}
	
	//PROSES KETIGA
	//pada proses 2 dilihat material BOM yang masih kurang pengaruh ke finishgood mana, lalu pada function ini akan di proposional finishgoodnya sesuai dengan material BOM yang mampu di confirm oleh vendor
	//yang finishgood nya sudah di proposional di insert ke d_purchasingconfirmation_final
	@RequestMapping(value = { "/PUR-CO/AffectedProducts/Confirm" }, method = {RequestMethod.GET,RequestMethod.POST})
	public String affectedProductsConfirm(Model model, HttpServletRequest req){
		try{
			String json = req.getParameter("jsonProcess");
			String docNo = req.getParameter("docNo");
			T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
			tp.setLastRun("3");
			pcAdapter.updateObjectOA(tp);
			Type typeList = new TypeToken<List<D_PROCESS_POCONFIRM>>(){}.getType();
			@SuppressWarnings("unchecked")
			List<D_PROCESS_POCONFIRM> listDataGrid = (List<D_PROCESS_POCONFIRM>) new Gson().fromJson(json, typeList);
			for(int i=0;i<listDataGrid.size();i++){
				if(Integer.parseInt(listDataGrid.get(i).getMaterialConfirm()) >= Integer.parseInt(listDataGrid.get(i).getMaterialOutstanding())){
					listDataGrid.remove(i);
					i--;
				}
			}
			List<Map<String,Object>> dataList = pcAdapter.getDataForFinal(docNo);
			pcAdapter.deletePurchasingFinal(docNo);
			for(int i=0;i<dataList.size();i++){
				if(Integer.parseInt(dataList.get(i).get("OUTSTANDING").toString()) > Integer.parseInt(dataList.get(i).get("CONFIRM").toString())){
					int selisihConfirmAndOS = Integer.parseInt(dataList.get(i).get("OUTSTANDING").toString()) - Integer.parseInt(dataList.get(i).get("CONFIRM").toString());
					double bagi = ((double)selisihConfirmAndOS / Double.parseDouble(dataList.get(i).get("OUTSTANDING").toString()));
					double selisih = 100.0 - (bagi*100.0);
					double temp = Math.pow(10, 2);
				    double proposionalPercen = (double) Math.round(selisih*temp)/temp;
					String week = dataList.get(i).get("WEEK").toString();
					String year = dataList.get(i).get("YEAR").toString();
					String materialCode = dataList.get(i).get("MATERIALCODE").toString();
					String materialName = dataList.get(i).get("MATERIALNAME").toString();
					List<Map<String,Object>> fdisList = pcAdapter.getFdisData(week, year, materialCode);
					double totalBomPriority1=0;
					double totalBomPriority99=0;
					boolean priority1Exist = false;
					
					for(int k=0;k<fdisList.size();k++){
						int totalFDIS = Integer.parseInt(fdisList.get(k).get("FINISHGOODQUANTITY").toString());
						double confirmedTotalBomQty= (double) (totalFDIS * proposionalPercen / 100);
						//jika ada priority 1
						if(fdisList.get(k).get("PRIORITY").toString().equals("1")){
							double selisih1 = Double.parseDouble(fdisList.get(k).get("FINISHGOODQUANTITY").toString())-confirmedTotalBomQty;
							double hasil1 = selisih1 * Double.parseDouble(fdisList.get(k).get("MATERIALQTY").toString());
							totalBomPriority1+=hasil1;
							priority1Exist=true;
						}
						else if(fdisList.get(k).get("PRIORITY").toString().equals("99")){
							double hasil99 = Double.parseDouble(fdisList.get(k).get("FINISHGOODQUANTITY").toString())*Double.parseDouble(fdisList.get(k).get("MATERIALQTY").toString());
							totalBomPriority99+=hasil99;
						}
					}
					
					//jika ada priority 1
					if(priority1Exist==true){
						double proposionalGap = totalBomPriority1 / totalBomPriority99 * 100;
						proposionalPercen = 100-proposionalPercen;
						proposionalPercen+=proposionalGap;
						proposionalPercen = 100-proposionalPercen;
						
					}
					
					for(int j=0;j<fdisList.size();j++){
						int totalFDIS = Integer.parseInt(fdisList.get(j).get("FINISHGOODQUANTITY").toString());
						int confirmedTotalQty= (int) ((int)totalFDIS * proposionalPercen / 100);
						D_PURCHASINGCONFIRMATION_FINAL dpf = new D_PURCHASINGCONFIRMATION_FINAL();
						dpf.setDocNo(fdisList.get(j).get("DOCNO").toString());
						dpf.setPriority(Integer.parseInt(fdisList.get(j).get("PRIORITY").toString()));
						dpf.setFinishGoodCode(fdisList.get(j).get("FINISHGOODCODE").toString());
						dpf.setFinishGoodName(fdisList.get(j).get("FINISHGOODNAME").toString());
						dpf.setBomQty(fdisList.get(j).get("MATERIALQTY").toString());
						dpf.setBomUom(fdisList.get(j).get("UOM").toString());
						dpf.setPlannedFinishGoodQty(fdisList.get(j).get("FINISHGOODQUANTITY").toString());
						dpf.setPlannedTotalBomQty(fdisList.get(j).get("TOTALBOMQTY").toString());
						dpf.setYear(year);
						dpf.setWeek(week);
						dpf.setMaterialCode(materialCode);
						dpf.setMaterialName(materialName);
						dpf.setSimDocno(docNo);
						if(Integer.parseInt(fdisList.get(j).get("PRIORITY").toString()) == 1){
							dpf.setConfirmedFinishGoodQty(fdisList.get(j).get("FINISHGOODQUANTITY").toString());
							dpf.setConfirmedTotalBomQty((totalFDIS*Integer.parseInt(fdisList.get(j).get("MATERIALQTY").toString()))+"");
						}
						else{
							if(confirmedTotalQty < 0){
								dpf.setConfirmedFinishGoodQty(0+"");
								dpf.setConfirmedTotalBomQty(0+"");
							}
							else{
								dpf.setConfirmedFinishGoodQty(confirmedTotalQty+"");
								dpf.setConfirmedTotalBomQty((confirmedTotalQty*Integer.parseInt(fdisList.get(j).get("MATERIALQTY").toString()))+"");
							}
						}
						pcAdapter.addObjectOA(dpf);
					}
				}
			}
			model.addAttribute("docNo",docNo);
			model.addAttribute("d_proc",listDataGrid);
			return "FinalConfirmProcess";
		}catch(Exception e){
			return "redirect:/PUR-CO/Create";
		}
	}
	
	@RequestMapping(value = { "/PUR-CO/FinalForRemainingMaterial" }, method = {RequestMethod.GET,RequestMethod.POST})
	public String FinalForRemainingMaterial(Model model, HttpServletRequest req){
		List<Integer> listYear = new ArrayList<>();
		List<Integer> listWeek = new ArrayList<>();
		Calendar c = Calendar.getInstance();
		int currYear = c.get(Calendar.YEAR);
		listYear.add(currYear-2);listYear.add(currYear-1);listYear.add(currYear);
		for(int i=1;i<53;i++){
			listWeek.add(i);
		}
		T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(req.getParameter("docNo"));
		tp.setLastRun("5");
		pcAdapter.updateObjectOA(tp);
		model.addAttribute("year",listYear);
		model.addAttribute("week",listWeek);
		model.addAttribute("docNo", req.getParameter("docNo"));
		return "FinalForRemainingMaterial";
	}
	
	@RequestMapping(value = { "/PUR-CO/RemainingMaterials" }, method = {RequestMethod.GET,RequestMethod.POST})
	public String RemainingMaterials(Model model, HttpServletRequest req){
		try{
			List<Integer> listYear = new ArrayList<>();
			List<Integer> listWeek = new ArrayList<>();
			Calendar c = Calendar.getInstance();
			
			int currYear = c.get(Calendar.YEAR);
			listYear.add(currYear);
			listYear.add(currYear-1);
			listYear.add(currYear-2);
			
			for(int i=1;i<53;i++){
				listWeek.add(i);
			}
			
			String docNo = req.getParameter("docNo");
			T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
			tp.setLastRun("4");
			pcAdapter.updateObjectOA(tp);
			
			model.addAttribute("docNo",docNo);
			model.addAttribute("year",listYear);
			model.addAttribute("week",listWeek);
			return "FinalProcess";
		}catch(Exception e){
			return "redirect:/PUR-CO/Create";
		}
	}
	@RequestMapping(value = { "/PUR-CO/AjaxForGridFinal" }, method = RequestMethod.GET)
	public @ResponseBody List<D_PURCO_FINAL> AjaxForGridFinal(Model model, HttpServletRequest req){
		String docNo = req.getParameter("docNo");
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		
		List<Map<String,Object>> fdisList = pcAdapter.getDistinctFdis(week, year, docNo);
		List<D_PURCO_FINAL> finalList = new ArrayList<>();
		for(int i=0;i<fdisList.size();i++){
			D_PURCO_FINAL dpf = new D_PURCO_FINAL();
			dpf.setFinishGoodCode(fdisList.get(i).get("FINISHGOODCODE").toString());
			dpf.setFinishGoodName(fdisList.get(i).get("FINISHGOODNAME").toString());
			dpf.setPlannedFinishGood(fdisList.get(i).get("PLANNEDFINISHGOODQTY").toString());
			List<Map<String,Object>> maxMinFdis = pcAdapter.getMaxMinFdis(week,year,fdisList.get(i).get("FINISHGOODCODE").toString(),docNo);
			if(maxMinFdis.size() == 1){
				dpf.setFinalFinishGood(maxMinFdis.get(0).get("CONFIRMEDFINISHGOODQTY").toString());
				dpf.setMaxFinishGood(maxMinFdis.get(0).get("CONFIRMEDFINISHGOODQTY").toString());
			}
			else{
				int max=0, min=0;
				for(int j = 0;j<maxMinFdis.size();j++){
					int curr = Integer.parseInt(maxMinFdis.get(j).get("CONFIRMEDFINISHGOODQTY").toString());
					
					if(curr > max){
						min = max;
						max = curr;
					}
					else{
						if(curr < min || min==0){    
							min = curr;
						}
					}
				}
				dpf.setFinalFinishGood(min+"");
				dpf.setMaxFinishGood(max+"");
			}
			finalList.add(dpf);
		}
		return finalList;
	}
	public List<D_PURCO_FINAL> AjaxForReportFinal(String week, String year, String docNo){
		List<Map<String,Object>> fdisList = pcAdapter.getDistinctFdis(week, year, docNo);
		List<D_PURCO_FINAL> finalList = new ArrayList<>();
		for(int i=0;i<fdisList.size();i++){
			D_PURCO_FINAL dpf = new D_PURCO_FINAL();
			dpf.setFinishGoodCode(fdisList.get(i).get("FINISHGOODCODE").toString());
			dpf.setFinishGoodName(fdisList.get(i).get("FINISHGOODNAME").toString());
			dpf.setPlannedFinishGood(fdisList.get(i).get("PLANNEDFINISHGOODQTY").toString());
			List<Map<String,Object>> maxMinFdis = pcAdapter.getMaxMinFdis(week,year,fdisList.get(i).get("FINISHGOODCODE").toString(),docNo);
			if(maxMinFdis.size() == 1){
				dpf.setFinalFinishGood(maxMinFdis.get(0).get("CONFIRMEDFINISHGOODQTY").toString());
				dpf.setMaxFinishGood(maxMinFdis.get(0).get("CONFIRMEDFINISHGOODQTY").toString());
			}
			else{
				int max=0, min=0;
				for(int j = 0;j<maxMinFdis.size();j++){
					int curr = Integer.parseInt(maxMinFdis.get(j).get("CONFIRMEDFINISHGOODQTY").toString());
					
					if(curr > max){
						min = max;
						max = curr;
					}
					else{
						if(curr < min || min==0){    
							min = curr;
						}
					}
				}
				dpf.setFinalFinishGood(min+"");
				dpf.setMaxFinishGood(max+"");
			}
			finalList.add(dpf);
		}
		return finalList;
	}
	//get data remaining material from finishgood
	@RequestMapping(value = { "/PUR-CO/AjaxForGridMaterial" }, method = RequestMethod.GET)
	public @ResponseBody List<D_GRIDMATERIAL> AjaxForGridMaterial(Model model, HttpServletRequest req){
		String finishGoodCode = req.getParameter("finishGoodCode");
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		String min = req.getParameter("min");
		List<D_GRIDMATERIAL> dgmList = new ArrayList<>();
		List<Map<String,Object>> fdisList = pcAdapter.getFdisDataFinal(week, year, finishGoodCode,docNo);
		for(int i=0;i<fdisList.size();i++){
			int remainingFdis = Integer.parseInt(fdisList.get(i).get("CONFIRMEDFINISHGOODQTY").toString())-Integer.parseInt(min); 
			int remainingBomFdis = (Integer.parseInt(fdisList.get(i).get("CONFIRMEDFINISHGOODQTY").toString())-Integer.parseInt(min))*Integer.parseInt(fdisList.get(i).get("BOMQUANTITY").toString());
			D_GRIDMATERIAL dgm = new D_GRIDMATERIAL();
			dgm.setMaterialCode(fdisList.get(i).get("MATERIALCODE").toString());
			dgm.setMaterialName(fdisList.get(i).get("MATERIALNAME").toString());
			dgm.setRemainingFinishGoodQty(remainingFdis+"");
			dgm.setRemainingBomQty(remainingBomFdis+"");
			dgm.setBomUom(fdisList.get(i).get("BOMUOM").toString());
			dgm.setBomQty(fdisList.get(i).get("BOMQUANTITY").toString());
			dgmList.add(dgm);
		}
		return dgmList;
	}
	//get data fdis
	@RequestMapping(value = { "/PUR-CO/NextProcessFdis" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> NextProcessFdis(Model model, HttpServletRequest req){
		String materialCode = req.getParameter("materialCode");
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		List<Map<String,Object>> fdisList = pcAdapter.getFdisData(week, year, materialCode);
		return fdisList;
	}
	@RequestMapping(value = { "/PUR-CO/ReportFinal" }, method = RequestMethod.GET)
	public String ReportFinal(Model model, HttpServletRequest req){
		String docNo = req.getParameter("docNo");
		List<Integer> listYear = new ArrayList<>();
		List<Integer> listWeek = new ArrayList<>();
		Calendar c = Calendar.getInstance();
		int currYear = c.get(Calendar.YEAR);
		listYear.add(currYear-2);listYear.add(currYear-1);listYear.add(currYear);
		for(int i=1;i<53;i++){
			listWeek.add(i);
		}

		T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
		tp.setLastRun("6");
		pcAdapter.updateObjectOA(tp);
		model.addAttribute("year",listYear);
		model.addAttribute("week",listWeek);
		model.addAttribute("docNo",docNo);
		return "FinalReport";
	}
	@RequestMapping(value = { "/PUR-CO/AjaxReportFinal" }, method = RequestMethod.GET)
	public @ResponseBody List<D_REPORT_FINAL> FinalReport(Model model, HttpServletRequest req){
		String docNo = req.getParameter("docNo");
		String week = req.getParameter("week"); 
		String year = req.getParameter("year");
		List<D_PURCO_FINAL> fdisList = AjaxForReportFinal(week,year,docNo);
		List<D_REPORT_FINAL> dReportList = new ArrayList<>();
		List<Map<String,Object>> listFdis = pcAdapter.getDataFdisFromRemainingMaterial(docNo,week,year); 
		for(int i = 0;i<listFdis.size();i++){
			for(int j = 0;j<fdisList.size();j++){
				if(listFdis.get(i).get("FINISHGOODCODE").toString().equals(fdisList.get(j).getFinishGoodCode())){
					D_REPORT_FINAL dReport = new D_REPORT_FINAL();
					dReport.setFinishGoodCode(fdisList.get(j).getFinishGoodCode());
					dReport.setFinishGoodName(fdisList.get(j).getFinishGoodName());
					dReport.setFinishGoodQty(Integer.parseInt(listFdis.get(i).get("FINISHGOODQTY").toString())+Integer.parseInt(fdisList.get(j).getFinalFinishGood()));
					dReportList.add(dReport);
				}
			}
		}
		for(int k = 0;k<dReportList.size();k++){
			for(int j = 0;j<fdisList.size();j++){
				if(dReportList.get(k).getFinishGoodCode().equals(fdisList.get(j).getFinishGoodCode())){
					fdisList.remove(j);
				}
			}
		}
		for(int i = 0;i<fdisList.size();i++){
			D_REPORT_FINAL drf = new D_REPORT_FINAL();
			drf.setFinishGoodCode(fdisList.get(i).getFinishGoodCode());
			drf.setFinishGoodName(fdisList.get(i).getFinishGoodName());
			drf.setFinishGoodQty(Integer.parseInt(fdisList.get(i).getFinalFinishGood()));
			dReportList.add(drf);
		}
		return dReportList;
	}
	@RequestMapping(value = { "/PUR-CO/SaveDatabase" }, method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody void SaveDatabase(Model model, HttpServletRequest req){
		String docNo = req.getParameter("docNo");
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String json = req.getParameter("jsonProcess");
		Type typeList = new TypeToken<List<D_FDISFROMREMAININGMATERIAL>>(){}.getType();
		List<D_FDISFROMREMAININGMATERIAL> listDataGrid = new Gson().fromJson(json, typeList);
		pcAdapter.deleteFdisFromRemainingMaterial(docNo,year,week);
		System.out.println(docNo+"a"+year+"b"+"c"+week+"d"+json+"e"+new Gson().toJson(listDataGrid));
		for(int i = 0;i<listDataGrid.size();i++){
			D_FINISHGOODFROMREMAININGMATERIAL dfrm = new D_FINISHGOODFROMREMAININGMATERIAL();
			dfrm.setDocNo(docNo);
			dfrm.setWeek(week);
			dfrm.setYear(year);
			dfrm.setFinishGoodCode(listDataGrid.get(i).getFINISHGOODCODE());
			dfrm.setFinishGoodName(listDataGrid.get(i).getFINISHGOODNAME());
			dfrm.setFinishGoodQty(listDataGrid.get(i).getFINISHGOODQTY());
			System.out.println("before");
			pcAdapter.addObjectOA(dfrm);
			System.out.println("after");
		}
		
		//List<D_PURCO_FINAL> fdisList = AjaxForReportFinal(week,year,docNo);		
	}
	//get data fdis yang sudah di proposional
	@RequestMapping(value = { "/PUR-CO/AjaxFinalProcess" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxFinalProcess(Model model, HttpServletRequest req){
		String materialCode = req.getParameter("materialCode");
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		List<Map<String,Object>> fdisList = pcAdapter.getFdisFinal(week, year, materialCode,docNo);
		return fdisList;
	}
	
	//get each total material from all remaining finishgood
	@RequestMapping(value = { "/PUR-CO/AjaxAllRemainingMaterial" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxAllRemainingMaterial(Model model, HttpServletRequest req){
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		List<Map<String,Object>> fdisList = pcAdapter.getDistinctFdis(week, year, docNo);
		D_REMAINING_BOMFROMFDIS drb = new D_REMAINING_BOMFROMFDIS();
		drb.setDocNo(docNo);
		drb.setWeek(week);
		drb.setYear(year);	
		pcAdapter.deleteRemainingBomFromFdis(docNo);
		for(int i=0;i<fdisList.size();i++){
			drb.setFinishGoodCode(fdisList.get(i).get("FINISHGOODCODE").toString());
			drb.setFinishGoodName(fdisList.get(i).get("FINISHGOODNAME").toString());
			List<Map<String,Object>> maxMinFdis = pcAdapter.getMaxMinFdis(week,year,fdisList.get(i).get("FINISHGOODCODE").toString(),docNo);
			int max=0, min=0;
			if(maxMinFdis.size() == 1){
				min = Integer.parseInt(maxMinFdis.get(0).get("CONFIRMEDFINISHGOODQTY").toString());
			} 
			else if(maxMinFdis.size() > 1){
				
				for(int j = 0;j<maxMinFdis.size();j++){
					int curr = Integer.parseInt(maxMinFdis.get(j).get("CONFIRMEDFINISHGOODQTY").toString());
					if(curr > max){
						min = max;
						max = curr;
					}
					else{
						if(curr < min || min==0){    
							min = curr;
						}
					}
				}
			}
			
			//insert into d_remaining_bomfromfdis
			for(int k=0;k<maxMinFdis.size();k++){
				drb.setBomUom(maxMinFdis.get(k).get("BOMUOM").toString());
				drb.setMaterialCode(maxMinFdis.get(k).get("MATERIALCODE").toString());
				drb.setMaterialName(maxMinFdis.get(k).get("MATERIALNAME").toString());
				System.out.println("aaaaaa"+(Integer.parseInt(maxMinFdis.get(k).get("CONFIRMEDFINISHGOODQTY").toString()) - min)*Integer.parseInt(maxMinFdis.get(k).get("BOMQUANTITY").toString()));
				drb.setRemainingQty(((Integer.parseInt(maxMinFdis.get(k).get("CONFIRMEDFINISHGOODQTY").toString()) - min)*Integer.parseInt(maxMinFdis.get(k).get("BOMQUANTITY").toString()))+"");
				pcAdapter.addObjectOA(drb);
			}
		}
		List<Map<String,Object>> drbList = pcAdapter.getDataRemaining(docNo, week, year);
		return drbList;
	}
	@RequestMapping(value = { "/PUR-CO/AjaxAllRemainingMaterial2" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxAllRemainingMaterial2(Model model, HttpServletRequest req){
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		List<Map<String,Object>> drbList = pcAdapter.getDataRemaining(docNo, week, year);
		System.out.println("aaa");
		return drbList;
	}
	@RequestMapping(value = { "/PUR-CO/AjaxGetMaterialBom" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxGetMaterialBom(Model model, HttpServletRequest req){
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		String finishGoodCode = req.getParameter("finishGoodCode");
		List<Map<String,Object>> fdisList = pcAdapter.getFdisDataFinal(week, year, finishGoodCode,docNo);
		return fdisList;
	}
	@RequestMapping(value = { "/PUR-CO/AjaxGetFdisFromRemainingMaterial" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxGetFdisFromRemainingMaterial(Model model, HttpServletRequest req){
		String week = req.getParameter("week");
		String year = req.getParameter("year");
		String docNo = req.getParameter("docNo");
		String materialCode ="";
		List<Map<String,Object>> drbList = pcAdapter.getDataRemaining(docNo, week, year); 
		for(int i=0;i<drbList.size();i++){
			materialCode += "'"+drbList.get(i).get("MATERIALCODE").toString()+"'";
			if(i!=drbList.size()-1)
				materialCode+=",";
		}
		
		List<Map<String,Object>> fdisList = new ArrayList<>();
		if(!materialCode.isEmpty())
			fdisList = pcAdapter.getDataFinalFdisFromRemainingMaterial(materialCode,docNo, week, year);
		
		return fdisList;
	}
	
	//ajax get data vendor untuk confirm material yang kurang
	@RequestMapping(value="/PUR-CO/KendoWindowChooseVendor", method=RequestMethod.GET)
	public String kendoWindowChooseVendor(Model model , HttpServletRequest req){
		int column = Integer.valueOf(req.getParameter("column"));
		String search = req.getParameter("search");
				
		if(search != null){
			model.addAttribute("readUrl", "/PUR-CO/KendoWindowChooseVendorData/?search="+search);
			model.addAttribute("url", "/MOA4/PUR-CO/KendoWindowChooseVendor/?window=" +req.getParameter("window")+"&column="+column+"&search=");
			model.addAttribute("search", search);
		}
		else{
			model.addAttribute("readUrl", "/PUR-CO/KendoWindowChooseVendorData/");
			model.addAttribute("url", "/MOA4/PUR-CO/KendoWindowChooseVendor/?window=" +req.getParameter("window") +"&column="+column+"&search=");
		}
		String[] array1 = new String[column];
		array1[0] = "VENDOR CODE";
		array1[1] = "VENDOR NAME";
		model.addAttribute("titleArr", array1);
		
		String[] widthArr = new String[column];
		widthArr[0]="100px";
		widthArr[1]="100px";
		
		model.addAttribute("widthArr", widthArr);
		model.addAttribute("windowID", req.getParameter("window"));
		return "KendoWindowBrowseCopyFPSA";
	}
	
	@RequestMapping(value="/PUR-CO/KendoWindowChooseVendorData", method=RequestMethod.GET)
	public @ResponseBody String kendoWindowChooseVendorData(HttpServletRequest req){
		String search;
		if(req.getParameter("search")==null){
			search = "";
		}
		else{
			search = req.getParameter("search");
		}
		List<Map<String,Object>> lsVendor = pcAdapter.getVendorSAP(search);
		List<KendoWindowBrowse2> dataVendor = new ArrayList<>();
		for(Map<String,Object> com : lsVendor){
			dataVendor.add(new KendoWindowBrowse2(com.get("LIFNR").toString(),com.get("NAME1").toString()));
		}
		Gson gson = new Gson();
		String json = gson.toJson(dataVendor);
		return json;
	}
	
	@RequestMapping(value="/PUR-CO/Browse", method=RequestMethod.GET)
	public String Browse(HttpServletRequest req,Model model){
		List<Map<String,Object>> browseList = pcAdapter.getDataHeader();
		model.addAttribute("browseList",browseList);
		return "BrowsePurchasingConfirmation";
	}
	
	@RequestMapping(value="/PUR-CO/DetailBrowse", method=RequestMethod.GET)
	public String DetailBrowse(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		Map<String,Object> browseList = pcAdapter.getDataHeader2(docNo).get(0);
		model.addAttribute("browseList",browseList);
		return "DetailBrowse";
	}
	@RequestMapping(value="/PUR-CO/AjaxGetDataVendor", method=RequestMethod.GET)
	public @ResponseBody List<D_VENDORCONFIRM> AjaxGetDataVendor(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		String week=req.getParameter("week");
		String year=req.getParameter("year");
		String materialCode=req.getParameter("materialCode");
		List<D_VENDORCONFIRM> listDvc = new ArrayList<>();
		List<Map<String,Object>> browseList = pcAdapter.getAjaxGetVendor(docNo,week,year,materialCode);
		for(int i=0;i<browseList.size();i++){
			D_VENDORCONFIRM dvc = new D_VENDORCONFIRM();
			dvc.setVendorCode(browseList.get(i).get("SUPPLIERCODE").toString());
			dvc.setVendorName(browseList.get(i).get("SUPPLIERNAME").toString());
			dvc.setQuantity(browseList.get(i).get("QUANTITY").toString());
			listDvc.add(dvc);
		}
		return listDvc;
	}
	@RequestMapping(value="/PUR-CO/DetailForBox1", method=RequestMethod.GET)
	public String DetailForBox1(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
		tp.setLastRun("1");
		pcAdapter.updateObjectOA(tp);
		List<D_PROCESS_POCONFIRM> d_proc = new ArrayList<>();
		List<Map<String,Object>> processList = pcAdapter.getDataForFinal(docNo);
		for(int i = 0;i<processList.size();i++){
			D_PROCESS_POCONFIRM d_pro = new D_PROCESS_POCONFIRM();
			d_pro.setMaterialCode(processList.get(i).get("MATERIALCODE").toString());
			d_pro.setMaterialName(processList.get(i).get("MATERIALNAME").toString());
			d_pro.setMaterialOutstanding(processList.get(i).get("OUTSTANDING").toString());
			d_pro.setMaterialConfirm(processList.get(i).get("CONFIRM").toString());
			d_pro.setWeek(processList.get(i).get("WEEK").toString());
			d_pro.setYear(processList.get(i).get("YEAR").toString());
			d_proc.add(d_pro);
		}
		
		model.addAttribute("docNo",docNo);
		model.addAttribute("d_proc",d_proc);
		return "PurchasingConfirmProcess";
	}
	@RequestMapping(value="/PUR-CO/DetailForBox2", method=RequestMethod.GET)
	public String DetailForBox2(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
		tp.setLastRun("2");
		pcAdapter.updateObjectOA(tp);
		List<D_PROCESS_POCONFIRM> d_proc = new ArrayList<>();
		List<Map<String,Object>> processList = pcAdapter.getDataForFinal(docNo);
		for(int i = 0;i<processList.size();i++){
			if(Integer.parseInt(processList.get(i).get("CONFIRM").toString()) < Integer.parseInt(processList.get(i).get("OUTSTANDING").toString())){
				D_PROCESS_POCONFIRM d_pro = new D_PROCESS_POCONFIRM();
				d_pro.setMaterialCode(processList.get(i).get("MATERIALCODE").toString());
				d_pro.setMaterialName(processList.get(i).get("MATERIALNAME").toString());
				d_pro.setMaterialOutstanding(processList.get(i).get("OUTSTANDING").toString());
				d_pro.setMaterialConfirm(processList.get(i).get("CONFIRM").toString());
				d_pro.setWeek(processList.get(i).get("WEEK").toString());
				d_pro.setYear(processList.get(i).get("YEAR").toString());
				d_proc.add(d_pro);
			}
		}
		model.addAttribute("json",new Gson().toJson(d_proc));
		model.addAttribute("docNo",docNo);
		model.addAttribute("d_proc",d_proc);
		return "NextConfirmProcess";
	}
	@RequestMapping(value="/PUR-CO/DetailForBox3", method=RequestMethod.GET)
	public String DetailForBox3(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		T_PURCHASINGCONFIRMATION tp = pcAdapter.getTPC(docNo);
		tp.setLastRun("3");
		pcAdapter.updateObjectOA(tp);
		List<D_PROCESS_POCONFIRM> d_proc = new ArrayList<>();
		List<Map<String,Object>> processList = pcAdapter.getDataForFinal(docNo);
		for(int i = 0;i<processList.size();i++){
			if(Integer.parseInt(processList.get(i).get("CONFIRM").toString()) < Integer.parseInt(processList.get(i).get("OUTSTANDING").toString())){
				D_PROCESS_POCONFIRM d_pro = new D_PROCESS_POCONFIRM();
				d_pro.setMaterialCode(processList.get(i).get("MATERIALCODE").toString());
				d_pro.setMaterialName(processList.get(i).get("MATERIALNAME").toString());
				d_pro.setMaterialOutstanding(processList.get(i).get("OUTSTANDING").toString());
				d_pro.setMaterialConfirm(processList.get(i).get("CONFIRM").toString());
				d_pro.setWeek(processList.get(i).get("WEEK").toString());
				d_pro.setYear(processList.get(i).get("YEAR").toString());
				d_proc.add(d_pro);
			}
		}
		model.addAttribute("docNo",docNo);
		model.addAttribute("d_proc",d_proc);
		return "FinalConfirmProcess";
	}
	@RequestMapping(value="/PUR-CO/GenerateLink", method=RequestMethod.GET)
	public String GenerateLink(HttpServletRequest req,Model model){
		String docNo=req.getParameter("docNo");
		String week=req.getParameter("week");
		String year=req.getParameter("year");
		String materialCode = req.getParameter("materialCode");
		String materialName = req.getParameter("materialName");
		String quantity = req.getParameter("quantity");
		
		model.addAttribute("docNo",docNo);
		model.addAttribute("year",year);
		model.addAttribute("week",week);
		model.addAttribute("materialCode",materialCode);
		model.addAttribute("materialName",materialName);
		model.addAttribute("quantity",quantity);
		return "GenerateLink";
	}
	
	@RequestMapping(value="/PUR-CO/AjaxGenerateLink", method=RequestMethod.GET)
	public @ResponseBody String AjaxGenerateLink(HttpServletRequest req,Model model)throws Exception{
		BasicTextEncryptor encrypt = new BasicTextEncryptor();
		encrypt.setPassword("mayora");
		UserAccess ua = this.getSession(req);
		String RefDocno= pcAdapter.getDocNo("PC-REF", ua.getUser().getGroupCode());
		pcAdapter.insertTrack(RefDocno, ua.getUser().getUsername(), "PC-REF", "Insert", "#SELF#", "Insert PC-REF", 0);
		D_PURCO_GENERATELINK dpg = new D_PURCO_GENERATELINK();
		String docNo = req.getParameter("docNo");
		String materialCode = req.getParameter("materialCode");
		String materialName = req.getParameter("materialName");
		String week =req.getParameter("week");
		String year =req.getParameter("year");
		String qty =req.getParameter("quantity");
		dpg.setDocNo(docNo);
		dpg.setMaterialCode(materialCode);
		dpg.setMaterialName(materialName);
		dpg.setWeek(week);
		dpg.setYear(year);
		dpg.setRefDocno(RefDocno);
		dpg.setQty(qty);
		pcAdapter.addObjectOA(dpg);
		
		//decrypt
		String docNoEncrypt = encrypt.encrypt(RefDocno).replaceAll("/", "_").replaceAll("\\+", "-");
		String docNoDecrypt = docNoEncrypt.replaceAll("_", "/").replaceAll("-", "\\+");
		docNoDecrypt = encrypt.decrypt(docNoDecrypt);
		
		//encrypt
		String link = "localhost:8080/MOA4/PUR-CO/LoginVendor/?dn="+encrypt.encrypt(RefDocno).replaceAll("/", "_").replaceAll("\\+", "-");
		return link;
	}
	
	@RequestMapping(value="/PUR-CO/RegisterFormForConfirm", method=RequestMethod.GET)
	public String RegisterFormForConfirm(HttpServletRequest req,Model model){
		String dn = req.getParameter("dn");
		
		int maxVendorCode = pcAdapter.getMaxVendorCode()+1;
		String vendorCode = "VND"+maxVendorCode;
		
		model.addAttribute("dn", dn);
		model.addAttribute("vendorCode", vendorCode);
		
		return "RegisterFormForConfirm";
	}

	@RequestMapping(value="/PUR-CO/Register/Action", method={RequestMethod.POST})
	public String Register2Action(HttpServletRequest req,Model model){
		D_VENDORLIST dvl = new D_VENDORLIST();
		
		String vendorCode = req.getParameter("vendorCode");
		String vendorName = req.getParameter("vendorName");
		String email = req.getParameter("email");
		String dn = req.getParameter("dn");
		String address = req.getParameter("address");
		String password = req.getParameter("pass");
		String country = req.getParameter("country");
		String owner = req.getParameter("owner");
		String employee = req.getParameter("employee");
		String hq = req.getParameter("hq");
		
		dvl.setVendorCode(vendorCode);
		dvl.setVendorName(vendorName);
		dvl.setVendorEmail(email);
		dvl.setVendorAddress(address);
		dvl.setPassword(password);
		dvl.setCountry(country);
		dvl.setOwner(owner);
		dvl.setHeadquarter(hq);
		dvl.setTotalEmployee(employee);
		dvl.setStatus("Not Verified");
		pcAdapter.addObjectOA(dvl);
		
		if(dn=="")
			return "redirect:/PUR-CO/LoginVendor";
		else return "redirect:/PUR-CO/LoginVendor/?dn="+dn;
	}
	
	@RequestMapping(value="/PUR-CO/LoginVendor", method=RequestMethod.GET)
	public String LoginFormForConfirm(HttpServletRequest req,Model model){
		String docNo = req.getParameter("dn");
		model.addAttribute("dn", docNo);
		
		return "LoginFormForConfirm";
	}
	
	@RequestMapping(value="/PUR-CO/LoginVendor/Action", method={RequestMethod.POST})
	public String LoginAction(HttpServletRequest req,Model model){
		List<Map<String,Object>> loginList = pcAdapter.getAllVendor();
		
		String vendorCode = req.getParameter("vendorCode");
		String pass = req.getParameter("pass");
		String docNo = req.getParameter("dn");
		
		int currNum = 0;
		int login = 0;
		for(int i=0;i<loginList.size();i++){
			if(loginList.get(i).get("VENDORCODE").toString().equals(vendorCode) && loginList.get(i).get("PASSWORD").toString().equals(pass)){
				login = 1;
				currNum = i;
				break;
			}
			else{
				login= 0;
			}
		}
		
		if(docNo==""){
			if(login == 1){
				BasicTextEncryptor encrypt = new BasicTextEncryptor();
				encrypt.setPassword("mayora");
				vendorCode = encrypt.encrypt(vendorCode);
				return "redirect:/PUR-CO/DetailVendor/?key="+vendorCode;
			}
			else {
				model.addAttribute("error", "Vendor code or password is invalid");
				return "LoginFormForConfirm";
			}
		}
		else{
			if(login == 1)
				return "redirect:/PUR-CO/DetailConfirm/?dn="+docNo+"&vendorCode="+loginList.get(currNum).get("VENDORCODE").toString();
			else {
				model.addAttribute("dn", docNo);
				model.addAttribute("error", "Vendor code or password is invalid");
				return "LoginFormForConfirm";
			}
		}
	}
	@RequestMapping(value="/PUR-CO/DetailConfirm", method={RequestMethod.GET})
	public String DetailConfirm(HttpServletRequest req,Model model){
		String docNo = req.getParameter("dn");
		String vendorCode = req.getParameter("vendorCode");
		
		BasicTextEncryptor encrypt = new BasicTextEncryptor();
		encrypt.setPassword("mayora");
		String docNoDecrypt = docNo.replaceAll("_", "/").replaceAll("-", "\\+");
		docNoDecrypt = encrypt.decrypt(docNoDecrypt);
		Map<String,Object> list = pcAdapter.getDataGenerateLink(docNoDecrypt).get(0);
		
		List<D_VENDORLIST> lsVendor = pcAdapter.getVendorByCode(vendorCode);
		
		model.addAttribute("nameLogin",lsVendor.get(0).getVendorName());
		model.addAttribute("docNo", list.get("DOCNO"));
		model.addAttribute("week", list.get("WEEK"));
		model.addAttribute("year", list.get("YEAR"));
		model.addAttribute("materialName", list.get("MATERIALNAME"));
		model.addAttribute("materialCode", list.get("MATERIALCODE"));
		model.addAttribute("planOrderQty", list.get("PLANORDER_QTY"));
		
		return "DetailConfirm";
	}
	
	@RequestMapping(value="/PUR-CO/DetailVendor", method={RequestMethod.GET})
	public String DetailVendor(HttpServletRequest req,Model model){
		String key = req.getParameter("key");
		BasicTextEncryptor encrypt = new BasicTextEncryptor();
		encrypt.setPassword("mayora");
		String vendorCode = encrypt.decrypt(key);
		
		List<D_VENDORLIST> lsVendor = pcAdapter.getVendorByCode(vendorCode);
		model.addAttribute("lsVendor", lsVendor.get(0));
		return "DetailVendorPage";
	}
	
	@RequestMapping(value="/PUR-CO/AdminDetailVendor", method={RequestMethod.GET})
	public String AdminDetailVendor(HttpServletRequest req,Model model){
		UserAccess ua = this.getSession(req);
		model.addAttribute("nameLogin", ua.getUser().getName());
		
		List<Map<String,Object>> lsVendor = pcAdapter.getAllVendor();
		model.addAttribute("lsVendor", lsVendor);
		
		return "AdminPage";
	}
	
	@RequestMapping(value="/PUR-CO/SavetoDetailVendor", method={RequestMethod.POST})
	public void SavetoDetailVendor(HttpServletRequest req,Model model){
		pcAdapter.deleteDDPurCo2(req.getParameter("docNo"),req.getParameter("week"),req.getParameter("materialCode"),req.getParameter("nameLogin"));
		DD_PURCHASINGCONFIRMATION_PK dd_pk = new DD_PURCHASINGCONFIRMATION_PK();
		DD_PURCHASINGCONFIRMATION dd = new DD_PURCHASINGCONFIRMATION();
		
		dd_pk.setDocNo(req.getParameter("docNo"));
		dd_pk.setMaterialCode(req.getParameter("materialCode"));
		dd_pk.setWeek(req.getParameter("week"));
		dd_pk.setYear(req.getParameter("year"));
		dd_pk.setVendorCode(req.getParameter("vendorCode"));
		dd.setPk(dd_pk);
		dd.setQuantity(req.getParameter("quantity"));
		dd.setVendorName(req.getParameter("nameLogin"));
		pcAdapter.addObjectOA(dd);
	}

	@RequestMapping(value="/PUR-CO/UpdateVendor", method=RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> UpdateVendor(HttpServletRequest req,Model model){
		String vendorCode = req.getParameter("vendorCode");
		String status = req.getParameter("status");
		
		D_VENDORLIST dvl = pcAdapter.getVendorByCode(vendorCode).get(0);
		dvl.setStatus(status);
		pcAdapter.updateObjectOA(dvl);
		
		List<Map<String,Object>> lsVendor = pcAdapter.getAllVendor();
		return lsVendor;
	}
	
	@RequestMapping(value="/PUR-CO/EditFormVendor", method=RequestMethod.GET)
	public String EditFormVendor(HttpServletRequest req,Model model){
		String vendorCode = req.getParameter("vendorCode");
		
		List<D_VENDORLIST> lsVendor = pcAdapter.getVendorByCode(vendorCode);
		model.addAttribute("lsVendor", lsVendor.get(0));
		return "EditFormVendor";
	}
	
	@RequestMapping(value="/PUR-CO/EditFormVendor/Action", method=RequestMethod.POST)
	public String EditFormVendorAction(HttpServletRequest req,Model model){
		String vendorCode = req.getParameter("vendorCode");
		String vendorName = req.getParameter("vendorName");
		String email = req.getParameter("email");
		String address = req.getParameter("address");
		String country = req.getParameter("country");
		String owner = req.getParameter("owner");
		String hq = req.getParameter("hq");
		String totalEmployee = req.getParameter("employee");
		
		D_VENDORLIST dvl = pcAdapter.getVendorByCode(vendorCode).get(0);
		dvl.setVendorName(vendorName);
		dvl.setVendorEmail(email);
		dvl.setVendorAddress(address);
		dvl.setCountry(country);
		dvl.setOwner(owner);
		dvl.setHeadquarter(hq);
		dvl.setTotalEmployee(totalEmployee);
		pcAdapter.updateObjectOA(dvl);
		
		BasicTextEncryptor encrypt = new BasicTextEncryptor();
		encrypt.setPassword("mayora");
		vendorCode = encrypt.encrypt(vendorCode);
		return "redirect:/PUR-CO/DetailVendor/?key="+vendorCode;
	}
}
