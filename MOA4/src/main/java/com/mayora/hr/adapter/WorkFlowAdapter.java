package com.mayora.hr.adapter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.google.gson.Gson;
//import com.mayora.hr.entity.M_JobTitle;
import com.mayora.masterdata.entity.M_GroupAccess;
import com.mayora.masterdata.entity.M_JOBTITLEACCESSPK;
import com.mayora.masterdata.entity.M_JobTitleAccess;
import com.mayora.masterdata.entity.Module;
import com.mayora.moa.adapter.MoaBaseAdapter;
import com.mayora.moa.model.JobDetail;
import com.mayora.moa.session.UserAccess;
import com.mayora.moa.workflow.entity.LogWorkflow;
import com.mayora.moa.workflow.entity.V_AOLSTATUS;
import com.mayora.mware.client.WF;
import com.mayora.mware.tools.Print;
import com.mayora.wf.Adapter;
import com.mayora.wf.NuWorkflow;
import com.mayora.wf.WorkFlow;
import com.mayora.wf.WorkFlowConnection;
import com.mayora.wf.client.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
public class WorkFlowAdapter extends MoaBaseAdapter{

	String TAG = "WorkFlowAdapter";
	
	// TODO: Adding new by PMN
    public String getDocNo(String moduleCode, String groupCode) {
		Session session = sfWF.openSession();
        try {
        	Query query = session.createSQLQuery(
    			"SELECT MYRWF.GETDOCNO('"+ moduleCode +"', '"+ groupCode +"') FROM DUAL"  
    		);  
    		Object docNoList = query.uniqueResult();
    		return docNoList.toString();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback();
        	return null;
        } finally{
        	session.close();
        }
	}
	
    // TODO: sendOTP
    public Boolean sendOTP(final String docNo) {
    	Session session = sfWF.openSession();
    	
    	Boolean result = false;
    	try{
	    	String res = session.doReturningWork(new ReturningWork<String>() {
                @Override
                public String execute(Connection connection) throws SQLException {
                	String result = "false";
                	
                    Statement wfcV = connection.createStatement();
                    Statement wfcR = connection.createStatement();
                    Statement wfcO = connection.createStatement();
                    
                    List<String> lsRemote = new ArrayList<String>();
                    
                    try {
                    	String username = "";
            	    	String pin = "";
                    	
                    	ResultSet rsV = wfcV.executeQuery("SELECT * FROM V_DETAILCURRENTTRACKWORKFLOWLI WHERE DOCNO = '" + docNo + "' ");
                        if (rsV.next()) {
                            if(rsV.getString("ASSIGNGROUP") != null){
                            	username = rsV.getString("ASSIGNGROUP");
                            }
                        }
                        
                        ResultSet rsO = wfcO.executeQuery("SELECT * FROM T_OTPAUTH WHERE DOCNO = '" + docNo + "'");
                        if (rsO.next()) {
                            if(rsO.getString("OTP") != null){
                            	pin = rsO.getString("OTP");
                            }
                            
                        }
                        
                        ResultSet rsR = wfcR.executeQuery("SELECT * FROM MYRMD.M_USER_MOBILE WHERE USERNAME = '" + username + "'");
                        while (rsR.next()) {
                            if(rsR.getString("MOBILENO") != null){
                            	try{
                            		new WorkFlow().sendSMS(rsR.getString("MOBILENO"), docNo, pin);
                            		Print._msg(TAG, "Send OTP to: " + rsR.getString("MOBILENO"));
                            	}catch(Exception e){
                            		Print._msg(TAG, "Error get OTP remote number");
                            	}
                            }
                            
                        }
                        result = "true";
                    }catch(Exception e){
                        connection.rollback();
                    }finally {
                    	wfcV.close();
                    	wfcR.close();
                    	wfcO.close();
                        connection.close();
                    }
                    return result;
                }
            });
	    	if(res.equals("true")){
	    		result = true;
	    	}
    	}finally{
    		session.close();
    	}
    	
    	return result;
    }    
	public String checkFlowStatus(String docNo)
    {
        Session session = sfWF.openSession();
        String result = "";
        
        try{
        	result = new WorkFlow().checkFlowStatus(session, docNo);
        }finally{
        	session.close();
        }
        
        return result;
    }
	
	public void forceAssignOTP(String docNo, String docNoD, String userOTP){
		Session session = sfWF.openSession();
		try{
			new WorkFlow().forceAssignOTP(session, docNoD, userOTP);
		}catch(Exception e){
			System.out.println("setWorkflowD Error : " + e.getMessage());
		}finally{
			session.close();
		}
	}
	
	public void setDocumentWorkflow(String docNo, String docNoD, Module module, UserAccess ua){
		Session session = sfWF.openSession();
		try{
			new WorkFlow().setDocumentWorkflow(session, docNoD, ua.getUser().getUsername(), module.getModuleCode(), "", docNo);
		}catch(Exception e){
			System.out.println("setWorkflowD Error : " + e.getMessage());
		}finally{
			session.close();
		}
	}
	
	public void finishNotif(String docNo, UserAccess ua, Module module){
		Session session = sfWF.openSession();
		try{
			new WorkFlow().finishedNotification(session, docNo, ua, module, "IND");
		}catch(Exception e){
			System.out.println("Finish notif ex : " +e);
		}finally{
			session.close();
		}
	}
	
	public String reject(String docNo, Module module, UserAccess ua, String reason) {
		Session session = sfWF.openSession();
		try{
			return new WorkFlow().saveReject(session, docNo, module, ua, reason);
		}finally{
			session.close();
		}	
	}
	
	public String sendDoc(String docNo, UserAccess ua) {
		Session session = sfWF.openSession();
		try{
			return new WorkFlow().sendDoc(session, docNo, ua);
		}finally{
			session.close();
		}	
	}
	
	public String receiveDoc(String docNo, UserAccess ua) {
		Session session = sfWF.openSession();
		try{
			return new WorkFlow().receiveDoc(session, docNo, ua);
		}finally{
			session.close();
		}	
	}
	
	@SuppressWarnings("unchecked")
	public Integer getCommentSize(String docNo)
	{
		Session session = sfOA.openSession();
		List<String> list = new ArrayList<String>();
		try{
			list = session.createSQLQuery("select to_char(max(id)+1) as temp from myroa.D_Comment where docNo ='"+docNo+"'")
					.list();
			return Integer.valueOf(list.get(0));
		}catch(Exception e){
			return 0;
		} finally{
			session.close();
		}
	}
	
	public String setInitialAssignment(String docNo, String module, UserAccess ua) {
		Session session = sfWF.openSession();
		try{
			return new WorkFlow().setInitialAssignment(session, docNo, module, ua);
		}finally{
			session.close();
		}	
	}

	
	
	// TODO: getJobDetail
	@SuppressWarnings("unchecked")
	public List<JobDetail> getJobDetail(final String docNo, final String moduleCode, final String wfCode, final Integer path, final Integer line) throws SQLException {
		Session session = sfWF.openSession();
		try {
			List<Module> listMod = session.createQuery("from Module where moduleCode='"+moduleCode+"'")
								   .list();

			if(listMod.size() > 0){
				final String mView = listMod.get(0).getDatabaseName()+"."+listMod.get(0).getViewName();
				
				List<JobDetail> listJD = session.doReturningWork(new ReturningWork<List<JobDetail>>() {
					@Override
					public List<JobDetail> execute(Connection connection) throws SQLException {
						Statement stmt = connection.createStatement();
			              
						try {
							String query = "Select * from " + mView + " where docNo='" + docNo + "'";
			  				ResultSet rs = stmt.executeQuery(query);
			  				
		  					if(rs.next()) {
		  						List<JobDetail> listJD = new ArrayList<JobDetail>();
			  					query = "SELECT * from MYRWF.M_REMINDERMESSAGE where workflowcode='"+wfCode+"' AND workflowpathid="+path+" AND workflowlinesid="+line + " order by id";
				  				stmt = connection.createStatement();
				  				ResultSet rsReminder = stmt.executeQuery(query);
			  					
				  				if(rsReminder.next()) {
				  					do {
										String value = rsReminder.getString("FIELDTOSHOW"); //dynamic column name
										JobDetail JD = new JobDetail();
										JD.setFieldAlias(rsReminder.getString("FIELDALIAS"));
										JD.setFieldToShow(rs.getString(value));
										listJD.add(JD);
									}while(rsReminder.next());
			  					} else{
			  						query = "SELECT * from MYRWF.M_REMINDERMESSAGE where workflowcode='" + wfCode + "' AND workflowpathid = 0 AND workflowlinesid = 1 order by id";
					  				stmt = connection.createStatement();
					  				ResultSet rsReminderDef = stmt.executeQuery(query);
					  				while(rsReminderDef.next()) {
										String value = rsReminderDef.getString("FIELDTOSHOW"); //dynamic column name
										JobDetail JD = new JobDetail();
										JD.setFieldAlias(rsReminderDef.getString("FIELDALIAS"));
										JD.setFieldToShow(rs.getString(value));
										listJD.add(JD);
									}
			  					}
			  					
			  					if(listJD.size() == 0){
			  						JobDetail JD = new JobDetail();
									JD.setFieldAlias("Document:");
									JD.setFieldToShow("Unset Reminder");
									
									listJD.add(JD);
			  					}
			  					return listJD;
	  						}
						} catch(Exception e) { 
							e.printStackTrace(); 
						} finally {
		                	stmt.close();
						}
						return null;      
						
					}
				});
				return listJD;
			}
		}catch (Exception e) {
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String getJobDetail2(final String docNo, String moduleCode) throws SQLException {
		Session session = sfWF.openSession();
		try {
			
			List<Module> listMod = session.createQuery("from Module where moduleCode='"+moduleCode+"'").list();

			if(listMod.size() > 0){
				
				final String moduleView = listMod.get(0).getDatabaseName()+"."+listMod.get(0).getTemplateDetailName();
				final String primaryKey = listMod.get(0).getPrimaryKey();
				
				Query query = session.createSQLQuery("Select * from " + moduleView + " where " + primaryKey + " ='"+docNo+"'"); 
			
				System.out.println(query.getQueryString());
				query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				
				List<Map<String,Object>> aliasToValueMapList = new ArrayList<Map<String,Object>>();
				aliasToValueMapList = query.list();
				return new Gson().toJson(aliasToValueMapList);
			}
			
		}finally{
			session.close();
		}

		return "";
	}
	
	public String setWFMiddleWare(String docno, String moduleCode, String requestBy){
        String result = "true";
        Session session = sfWF.openSession();

        try {
            if(!WF.setWorkflow(session, docno, moduleCode, requestBy, false, true, false)){
                result = "false";
            }
        } catch (Exception e) {
            result = "false";
        }

        return result;
    }
	
	public String setWFMiddleWare(String docno, String moduleCode, String requestBy, Boolean afterSuccess, Boolean afterFailed){
        String result = "true";
        Session session = sfWF.openSession();

        try {
            if(!WF.setWorkflow(session, docno, moduleCode, requestBy, false, afterSuccess, afterFailed)){
                result = "false";
            }
        } catch (Exception e) {
            result = "false";
        }

        return result;
    }
	
	@SuppressWarnings("deprecation")
	public String notificationMiddleWare(String docno, String moduleCode, String requestBy,  String lang){
		String result = "true";
		Session session = sfWF.openSession();	
		
		try {
			if(!WF.notification(session, docno, moduleCode, requestBy, lang)){
				result = "false";
			}
		} catch (Exception e) {
			result = "false";
		}
		
		return result;
	}
	
	
	
	
	public Map<String, Object> getDetailCurrentWF(String docNo, Module m, UserAccess ua) {
		Session session = sfWF.openSession();
		Map<String, Object> rs = null;
		try{		
			WorkFlow wf = new WorkFlow();
			rs =  wf.getDetailCurrentWorkflowLines(session,docNo);	
		}finally{
			session.close();
		}		
		return rs ;
	}
	
	public String approve(String docNo,UserAccess ua) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status =  wf.approve(session,docNo, ua);		
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String finishedNotification(String docNo,UserAccess ua,Module m,String reason) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			
			status = wf.finishedNotification(session,docNo,ua, m, reason);
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String review(String docNo, UserAccess ua) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			 status =  wf.review(session,docNo, ua);		
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String saveReview(String docNo, UserAccess ua) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			 status =  wf.saveReview(session,docNo, ua);		
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String assign(String docNo, UserAccess ua,Module m) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			 status =  wf.assign(session,docNo,ua,m);	
		}finally{
			session.close();
		}		
		return status;
	}
	
	
	public String return_(String docNo, UserAccess ua,Module m,String WfCode, Integer ID ,Integer Line, Integer ParId, String var) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status = wf.return_(session,docNo, ua, m, WfCode, ID, Line, ParId, var);
		}finally{
			session.close();
		}		
		return status;
	}
	
//	public String saveReject(String docNo, Module m,UserAccess ua,String reason) {
//		Session session = sfWF.openSession();
//		String status="";
//		try{		
//			WorkFlow wf = new WorkFlow();
//			status = wf.saveReject(session,docNo,m,ua, reason);
//		}finally{
//			session.close();
//		}		
//		return status;
//	}	
	public String notification(String docNo, UserAccess ua,Module m,String var) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status =  wf.notification(session,docNo, ua, m, var);
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String undo(String docNo, UserAccess ua,Module m) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status = wf.undo(session,docNo, ua, m);
		}finally{
			session.close();
		}		
		return status;
	}
	
	public Boolean haveWorkflow(String docNo) {
		Session session = sfWF.openSession();
		Boolean status=null;
		try{		
			WorkFlow wf = new WorkFlow();
			status = wf.haveWorkflow(session,docNo);
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String removeLine(String docNo, String workflowcode,Integer line) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			wf.removeLine(session,docNo,workflowcode,line);
		}finally{
			session.close();
		}		
		return status;
	}

	@SuppressWarnings("deprecation")
	public String removeLineMid(String docNo, String moduleCode, String requestBy, String workflowcode,Integer line) {
		String result = "true";
		Session session = sfWF.openSession();	
		
		try {
			if(!WF.removeLine(session, docNo, moduleCode, requestBy, workflowcode, line)){
				result = "false";
			}
		} catch (Exception e) {
			result = "false";
		} finally{
			session.close();
		}
		
		return result;
	}
	
	public String returnUndo(String docNo, String name,Module module, String reason) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status = wf.returnUndo(session,docNo, name, module, reason);
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String saveReject(String docNo, Module module,UserAccess userAccess, String reason) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status = wf.saveReject(session,docNo, module, userAccess, reason);	
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String isClosedWorkflow(String docNo) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.isClosedWorkflow(session,docNo)) ? "true" : "false";
			}
			catch(Exception e) { status = "false"; }
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String sendNotif(String docNo, String moduleCode) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.sendNotif(session,docNo, moduleCode)) ? "true" : "false";
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String setReminder(String docNo, String lang, String username) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.setReminder(session,docNo, lang, username);
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String clearWorkflow(String docNo) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.clearWorkflow(session,docNo)) ? "true" : "false";
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String undoClose(String docNo, UserAccess userAccess, Module module) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.undoClose(session, docNo, userAccess, module)) ? "true" : "false";
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String removeSingleLine(String docNo, String workflowCode, Integer workflowLines ) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.removeSingleLine(session, docNo, workflowCode, workflowLines)) ? "true" : "false";
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String removeSingleLine(String docNo, String workflowCode, Integer workflowLines, Integer paralleline) {
        Session session = sfWF.openSession();
        String status="";
        try{
            WorkFlow wf = new WorkFlow();
            try {
                status = (wf.removeSingleLine(session, docNo, workflowCode, workflowLines, paralleline)) ? "true" : "false";
            }
            catch(Exception e) { status = "false"; }
        }finally{
            session.close();
        }
        return status;
    }

	 
	
	public String forceAssign(String docNo, String module, String groupWorkflowCode, String workflowCode, Integer workflowLines, String user,UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.forceAssign(session, docNo, module, groupWorkflowCode, workflowCode, workflowLines, user, userAccess);
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
		
	public void clearReminder(final String docNo){
		
		final Session session = sfWF.openSession();
		try{
			session.doWork(new Work() {
				@Override
				public void execute(Connection connection) throws SQLException {
					Statement stmt = connection.createStatement();
					try{
						WorkFlow wf = new WorkFlow();
						wf.clearReminder(stmt, docNo);
					}finally{
						stmt.close();
						connection.commit();
						connection.close();
					}
				}});
		}finally{
			session.close();
		}
	}

	public String saveReview(String docNo, String currentWFcode, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.saveReview(session, docNo, currentWFcode, userAccess);
			}
			catch(Exception e) { status = "false"; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public List<String> getCurrentWorkflowCode(String docNo) {
		Session session = sfWF.openSession();
		List<String> status = new ArrayList<String>();
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.getCurrentWorkflowCode(session, docNo);
			}
			catch(Exception e) { status = new ArrayList<String>(); } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String notifCreator(String docNo, Module module, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.notifCreator(session, docNo, module, userAccess)) ? "true" : "false";
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String send(String docNo, Module module, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.send(session, docNo, module, userAccess)) ? "true" : "false";
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String forceCloseLine(String docNo, UserAccess userAccess, String workflowCode, Integer workflowID, Integer workflowLines, Integer pararelID) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.forceCloseLine(session, docNo, userAccess, workflowCode, workflowID, workflowLines, pararelID)) ? "true" : "false";
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String print_(String docNo, String moduleCode, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = (wf.print_(session,docNo, moduleCode, userAccess)) ? "true" : "false";
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String forceNotif(String docNo, Module module, String footer) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.forceNotif(session, docNo, module, footer);
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String forceAssign(String docNo, String moduleCode, String groupWorkflowCode, String username, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.forceAssign(session, docNo, moduleCode, groupWorkflowCode, username, userAccess);
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String forceAssign(String docNo, String moduleCode, String groupWorkflowCode, String workflowCode, Integer workflowID, Integer workflowLines, String username, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.forceAssign(session, docNo, moduleCode, groupWorkflowCode, workflowCode, workflowID, workflowLines, username, userAccess);
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String forceAssignParallelID(String docNo, String moduleCode, String groupWorkflowCode, String workflowCode, Integer workflowLines, Integer paralelineID, String username, UserAccess userAccess) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.forceAssign(session, docNo, moduleCode, groupWorkflowCode, workflowCode, workflowLines, paralelineID, username, userAccess);
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String pending(String docNo, UserAccess userAccess, Integer month) {
		Session session = sfWF.openSession();
		String status = "";
		try{		
			WorkFlow wf = new WorkFlow();
			try {
				status = wf.pending(session, docNo, userAccess, month);
			}
			catch(Exception e) { status = ""; } 
		}finally{
			session.close();
		}		
		return status;
	}
	
	public String transfer(String docNo,UserAccess ua,Module m,String nextGroupWorkflowCode) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			status =  wf.transferGroup(session, docNo, ua, m, nextGroupWorkflowCode);
		}finally{
			session.close();
		}		
		return status;
	}
	
	@SuppressWarnings("unused")
	public void sendPIN(String docNo,String email,String access,String username){//HARUS DI UBAH
		
		Session session = sfWF.openSession();	
				try{
					WorkFlow wf = new WorkFlow();								
					WorkFlowConnection wfc = new WorkFlowConnection();
					
					String url = "192.168.0.98:8080";
					String msg = "";
					
					String url1="portal.mayora.co.id:8080";
						if(email != null){
							msg =" <html><head></head>" +
									 "<body>" +
									 "<h2>Mayora Pin Mobile</h2>" +

									 "<table>" +
									 "<tr>" +
									 "<td>Response For Ticket Help Desk</td><td>:</td><td>"+"<a href='http://"+url+"/MOA2/HelpDesk/Detail/?docNo="+docNo+"'>"+docNo+"</a>" +"</td>"+
									 "</tr>"+
									 "<tr>" +
									 "<td>Username</td><td>:</td><td>" + username +"</td>"+
									 "</tr>" +
									 "<tr>" +
									 "<td>Mobile PIN</td><td>:</td><td>" + access +"</td>"+
									 "</tr>" +
									 "</table>" +
									 
									 "</body>" +
									 "</html>";
							wfc.sendMail(session,wf.getMsgSender(), null, email, "Mayora PIN Mobile", null, msg);
					}
				 else{
						 msg =" <html><head></head>" +
								 "<body>" +
								 "<h2>Mayora Pin Mobile</h2>" +
								
								 "<table>" +
								 "<tr>" +
								 "<td>Response For Ticket Help Desk</td><td>:</td><td>"+"<a href='http://"+url+"/MOA2/HelpDesk/Detail/?docNo="+docNo+"'>"+docNo+"</a>" +"</td>"+
								 "</tr>"+
								 "<td>Username</td><td>:</td><td>" + username +"</td>"+
								 "</tr>" +
								 "<tr>" +
								 "<tr>" +
								 "<td>Mobile PIN</td><td>:</td><td>" + access +"</td>"+
								 "</tr>" +
								 "</table>" +
 
								 "</body>" +
								 "</html>";
						 wfc.sendMail(session,wf.getMsgSender(), null, "robin.revialus@mayora.co.id", "Mayora PIN Mobile", null, msg);
				 	} 
				}catch(Exception e){e.printStackTrace();}
		finally{session.close();}	
	}
	
	 public String transferPath(String docNo,UserAccess ua,Integer transferPathid) {
			Session session = sfWF.openSession();
			String status="";
			try{		
				WorkFlow wf = new WorkFlow();
				status =  wf.transferPath(session, docNo, ua, transferPathid);
			}finally{
				session.close();
			}		
			return status;
	}
	 
	 public String getSuperior(String username,String groupWF) {
			Session session = sfWF.openSession();
			String status="";
			try{		
				WorkFlow wf = new WorkFlow();
				status =  wf.getSuperior(session, username, groupWF);
			}finally{
				session.close();
			}		
			return status;
	}
	 
	 public void sendMail(String mail1,String mail2,String mail3,String mail4,String mail5, String mail6) {
			Session session = sfWF.openSession();
			try{
				new WorkFlowConnection().sendMail(session,mail1,mail2,mail3,mail4,mail5,mail6);
			}finally{
				session.close();
			}	
		}
	 public void autoAssignOrgNonClass(String docNo, String module, String emp){
			Session sessionWF = sfWF.openSession();
			try{
				new WorkFlow().autoAssignOrgNonClass(sessionWF, docNo, module, emp);
			
			}finally{
				sessionWF.close();
			}
		}
	 
	
	 
	 public String removeLineByTag(String docNo, String workflowcode,String tag) {
		Session session = sfWF.openSession();
		String status="";
		try{		
			WorkFlow wf = new WorkFlow();
			wf.removeSingleLineByTag(session, docNo, workflowcode, tag);
		}finally{
			session.close();
		}		
		return status;
	}
	 
	 public void autoAssign(String docNo, Module module, String emp){
		Session sessionWF = sfWF.openSession();
		try{
			new WorkFlow().autoAssign(sessionWF, docNo, module, emp);
		}finally{
			sessionWF.close();
		}
	}

	 public void autoAssignClass(String docNo, Module module, String emp){
		Session sessionWF = sfWF.openSession();
		try{
			new WorkFlow().autoAssign(sessionWF, docNo, module, emp);
		
		}finally{
			sessionWF.close();
		}
	}
	 
    public String autoAssign(String docNo, UserAccess ua,Module m) {
        Session session = sfWF.openSession();
        String status="";
        try{
            WorkFlow wf = new WorkFlow();
             status =  wf.autoAssign(session, docNo, m, ua.getUser().getEmpId());
        }finally{
            session.close();
        }
        return status;
    } 
 
    public String saveAssign(String docNo, UserAccess ua,Module m,String[]listGroup,String[]listPic,Boolean NeedClose) {
        Session session = sfWF.openSession();
        String status="";
        try{       
            WorkFlow wf = new WorkFlow();
           
            try {
                 status =  wf.saveAssign(session,docNo,ua, listGroup, listPic,true);
            } catch (Exception e) {
                if(listPic!=null){
                    status="#SUCCESS#";
                }
            }
            wf.removeLineNA(session, docNo);
        }finally{
            session.close();
        }       
        return status;
    }
	    
    @SuppressWarnings("unchecked")
	public List<V_AOLSTATUS> unsetWfAOL(){
		Session session = sfWF.openSession();
		List<V_AOLSTATUS> lsData = new ArrayList<V_AOLSTATUS>();
		
		try{
			Query query = session.createQuery("FROM V_AOLSTATUS WHERE workflow = 0");
			lsData = query.list();
		}finally{
			session.close();
		}
		return lsData;
	}
    
    @SuppressWarnings("unchecked")
	public List<V_AOLSTATUS> unsetMiddlewareAOL(){
		Session session = sfWF.openSession();
		List<V_AOLSTATUS> lsData = new ArrayList<V_AOLSTATUS>();
		
		try{
			Query query = session.createQuery("FROM V_AOLSTATUS WHERE workflow = 1 AND middleware = 0 AND CLOSEDATE IS NOT NULL");
			lsData = query.list();
		}finally{
			session.close();
		}

		return lsData;
	}
    
    public String autoAssignPerWFCode(String docNo, Module module, String empid) {
		Session session = sfWF.openSession();
		String ret = "";
		try{
			WorkFlow wf = new WorkFlow();
			ret = wf.autoAssignPerWFCode(session, docNo, module, empid);
		}finally{
			session.close();
		}
		return ret;
	} 
    
    public String setWorkflow(String docno){
		String result = "true";
		Session session = sfWF.openSession();	
		WorkFlow wf = new WorkFlow();
		Module module = Adapter.getModuleByDocno(session, docno);
		UserAccess user = Adapter.getUserByDocno(session, docno, module);
		try
		{
			if(result.equals("true"))
			{
		
				wf.clearWorkflow(session,docno);
				wf.setWorkflow(session, docno, module, user);
				wf.setInitialAssignment(session, docno, user.getUser().getUsername(), user);
		        wf.notification(session, docno, user, module, "IND");
			}
		}catch(Exception e){
			Print._err(TAG, e);
		} finally {
			session.close();
		}
		return result;
	}

    
    public String setWorkflowWithAutoAssign(String docno,String empId){
		String result = "true";
		Session session = sfWF.openSession();	
		WorkFlow wf = new WorkFlow();
		Module module = Adapter.getModuleByDocno(session, docno);
		UserAccess user = Adapter.getUserByDocno(session, docno, module);

		try{
			if(result.equals("true"))
			{
				wf.clearWorkflow(session,docno);
				wf.setWorkflow(session, docno, module, user);
				wf.setInitialAssignment(session, docno, user.getUser().getUsername(), user);
		        wf.notification(session, docno, user, module, "IND");
		        wf.autoAssign(session, docno, module,empId);
			}
		}finally{
			session.close();
		}
		return result;
	}
	
	@SuppressWarnings("unused")
	public String getEmpId(String docno){
		String empId = "";
		Session session = sfWF.openSession();	
		WorkFlow wf = new WorkFlow();
		Module module = Adapter.getModuleByDocno(session, docno);
		UserAccess user = Adapter.getUserByDocno(session, docno, module);

		try{
			empId = user.getUser().getEmpId();
		}finally{
			session.close();
		}
		return empId;
	}
 
	@SuppressWarnings("static-access")
	public LogWorkflow checkWorkflow(String docno)
	{
		NuWorkflow  wf = new NuWorkflow() {};
		Session session = sfWF.openSession();
		Log log = new Log();
		LogWorkflow logWf = new LogWorkflow();
		Module module = Adapter.getModuleByDocno(session, docno);
		UserAccess user = Adapter.getUserByDocno(session, docno, module);
		try
		{
			log = wf.checkWorkflow(session, docno, module, user);
			String status = log.getResult().toString();
			if(status.equals("SUCCESS"))
			{
				logWf.setStatus(status);
				logWf.setLog(log.getLog().toString());
			}
			else
			{	
				logWf.setStatus(status);
				logWf.setLog(log.getLog().toString());
			}

		}finally{
			session.close();
		}
		return logWf;
	}
	
	public void setWorkflowD(String docNo, String docNoD, Module module, UserAccess ua){
		Session session = sfWF.openSession();
		try{
			new WorkFlow().setWorkflowD(session, docNo, docNoD, module, ua);
		}catch(Exception e){
			System.out.println("setWorkflowD Error : " + e.getMessage());
		}finally{
			session.close();
		}
	}
	
//	public List<M_JobTitle> getJobTitleName(String jobTitleCode)
//    {
//        Session session = sfWF.openSession();
//        List<M_JobTitle> list = new ArrayList<M_JobTitle>();
//        try
//        {
//            list = session.createQuery("From MYRMD.M_JobTitle where jobTitleCode = '"+jobTitleCode+"'").list();
//        }
//        catch(Exception e)
//        { }
//        finally
//        {
//            session.close();
//        }
//        return list;
//    }

    @SuppressWarnings("unchecked")
    public List<M_JobTitleAccess> getAllJobTitleAccess(String search)
    {
        System.out.println(search);
        Session session = sfOA.openSession();
        List<M_JobTitleAccess> list = new ArrayList<M_JobTitleAccess>();
        try{
            if(search != null && !search.equals("")){
                list = session.createQuery("FROM M_JobTitleAccess " +
                        "WHERE (UPPER(groupName) LIKE :search " +
                        "OR UPPER(jobTitleName) LIKE :search" +
                        "OR UPPER(groupCode) LIKE :search" +
                        "OR UPPER(jobTitleCode)LIKE : search)ORDER BY materialName asc")
                        .setString("search","%" + search.toUpperCase() + "%").list();
            }else{
                list = session.createQuery("FROM M_JobTitleAccess ORDER BY jobTitleName asc").list();
            }
        } catch(Exception e)
        {

        }
        finally{
            session.close();
        }
        return list;
    }
        @SuppressWarnings("unchecked")
        public List<M_JobTitleAccess> getAjaxAllJobTitleAccess(String jobTitleCode)
        {
            Session session = sfOA.openSession();
            List<M_JobTitleAccess> list = new ArrayList<M_JobTitleAccess>();
            try{
                    list = session.createQuery("FROM M_JobTitleAccess where jobTitleCode = '"+jobTitleCode+"'ORDER BY jobTitleName asc").list();
            } catch(Exception e)
            {

            }
            finally{
                session.close();
            }
            return list;
    }
        @SuppressWarnings("unchecked")
        public List<M_JOBTITLEACCESSPK> getAjaxAllJobTitleAccess2(String jobTitleCode)
        {
            Session session = sfOA.openSession();
            List<M_JOBTITLEACCESSPK> list = new ArrayList<M_JOBTITLEACCESSPK>();
            try{
                    list = session.createQuery("FROM M_JOBTITLEACCESSPK where jobTitleCode = '"+jobTitleCode+"'ORDER BY jobTitleName asc").list();
            } catch(Exception e)
            {

            }
            finally{
                session.close();
            }
            return list;
    }

@SuppressWarnings("unchecked")
public List <M_GroupAccess> getAllGroupAccess(String search){

    Session session = sfWF.openSession();
    if(search == null || search.equals("")){
        List<M_GroupAccess> groupAccess = session.createQuery("FROM M_GroupAccess ORDER BY groupName ASC").setMaxResults(100).list();
        session.close();
        return groupAccess;
    }
    else{
        List<M_GroupAccess> groupAccess = session.createQuery("from M_GroupAccess where (upper(groupName) like :search OR upper(groupCode) like:search )ORDER BY groupName ASC")
                 .setString("search", "%" + search.toUpperCase() + "%").setMaxResults(100)
                 .list();
        session.close();
        return groupAccess;
    }

}

//    @SuppressWarnings("unchecked")
//    public List <M_JobTitle> getAllJobTitle(String search){
//
//        Session session = sfWF.openSession();
//        if(search == null || search.equals("")){
//            List<M_JobTitle> jobTitle = session.createQuery("FROM M_JobTitle ORDER BY jobTitleName ASC").setMaxResults(100).list();
//            session.close();
//            return jobTitle;
//        }
//        else{
//            List<M_JobTitle> jobTitle = session.createQuery("from M_JobTitle where (upper(jobTitleCode) like :search OR upper(jobTitleName) like:search )ORDER BY jobTitleName ASC")
//                     .setString("search", "%" + search.toUpperCase() + "%").setMaxResults(100)
//                     .list();
//            session.close();
//            return jobTitle;
//        }
//    }

//    @SuppressWarnings("unchecked")
//    public List<M_JobTitle> getAllJobtitle()
//    {
//        Session session = sfWF.openSession();
//        List<M_JobTitle> list = new ArrayList<M_JobTitle>();
//        try
//        {
//            list = session.createQuery("FROM M_JobTitle order by jobTitleName ASC ").list();
//        }
//        catch(Exception e)
//        {
//            session.close();
//        }
//
//        return list;
//    }

   
}
