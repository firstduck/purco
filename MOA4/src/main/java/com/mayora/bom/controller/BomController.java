package com.mayora.bom.controller;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mayora.bom.adapter.BomAdapter;
import com.mayora.bom.entity.T_BOM;
import com.mayora.bom.entity.D_BOM_ALTERNATIVE;
import com.mayora.masterdata.model.KendoWindowBrowse2;
import com.mayora.masterdata.model.KendoWindowBrowse3KPI;
import com.mayora.moa.BaseController;
import com.mayora.moa.session.UserAccess;

@Controller
public class BomController extends BaseController {
	@Autowired
	protected BomAdapter bomAdapter;
	
	@RequestMapping(value = { "/BOM/Create" }, method = RequestMethod.GET)
	public String formBOM(Model model, HttpServletRequest request){
		model.addAttribute("mode","create");
		model.addAttribute("action","/MOA4/BOM/Create/Action");
		
		return "formBOM";
	}
	
	@RequestMapping(value="/BOM/Create/Action", method = {RequestMethod.GET,RequestMethod.POST})
	public String createBOMAction (HttpServletRequest request, Model model,RedirectAttributes ra) {
		UserAccess ua = this.getSession(request);
		
		int productExist = Integer.parseInt(request.getParameter("productExist"));
		String productCode = request.getParameter("productCode");
		String type = "BOM";
		String gridData = request.getParameter("gridData");
		Gson gson = new Gson();
		Type typeList = new TypeToken<List<Map<String,String>>>(){}.getType();
		List<Map<String,String>> listDataGrid = gson.fromJson(gridData, typeList);
		
		if(productExist==0){
			String docNo = bomAdapter.getDocNo(bomAdapter.setModule(), ua.getUser().getGroupCode());
			bomAdapter.insertTrack(docNo, ua.getUser().getUsername(), bomAdapter.setModule(), "Insert", "#SELF#", "Insert BOM Create", 0);
			bomAdapter.insertCreate(docNo,productCode, type, listDataGrid, ua.getUser().getUsername(), ua.getUser().getName(),ua.getUser().getEmail());
		}else{
			bomAdapter.updateExistingProduct(productCode,listDataGrid);
		}
		
		return "redirect:/BOM/Browse";
	}
	
	@RequestMapping(value = { "/BOM/Detail" }, method = RequestMethod.GET)
	public String detailBOM(Model model, HttpServletRequest request){
		
		String docNo = request.getParameter("docNo");
		
		T_BOM bom = bomAdapter.getHeader(docNo);
		
		String productName = bomAdapter.getProductName(bom.getMaterialCode());
		
		model.addAttribute("docNo",docNo);
		model.addAttribute("type",bom.getType());
		model.addAttribute("productName",productName);
		model.addAttribute("productCode",bom.getMaterialCode());
		model.addAttribute("cancelUrl","/MOA4/BOM/Detail/?docNo="+docNo);
		model.addAttribute("action","/MOA4/BOM/Detail/Action");
		
		return "detailBOM";
	}
	
	@RequestMapping(value="/BOM/Detail/Action", method = {RequestMethod.GET,RequestMethod.POST})
	public String detailBOMAction (HttpServletRequest request, Model model,RedirectAttributes ra) {
		String docNo = request.getParameter("hidDocNo");
		String gridData = request.getParameter("gridData");
		Gson gson = new Gson();
		Type typeList = new TypeToken<List<Map<String,String>>>(){}.getType();
		List<Map<String,String>> listDataGrid =  gson.fromJson(gridData, typeList);
		
		bomAdapter.updateDetail(docNo,listDataGrid);
		return "redirect:/BOM/Detail/?docNo="+docNo;
	}
	
	@RequestMapping(value = { "/BOM/Detail/Create" }, method = RequestMethod.GET )
	public String detailCreateBOM(HttpServletRequest request, Model model,RedirectAttributes ra){
		int alternativeId = Integer.parseInt(request.getParameter("alternativeId"));
		String productName = request.getParameter("product");
		String type = request.getParameter("type");
		String plant = request.getParameter("plant");
		String docNo = request.getParameter("docNo");
		
		model.addAttribute("productName",productName);
		model.addAttribute("type",type);
		model.addAttribute("plant",plant);
		model.addAttribute("alternativeId",alternativeId);
		model.addAttribute("docNo",docNo);
		model.addAttribute("cancelUrl","/MOA4/BOM/Detail/?docNo="+docNo);
		model.addAttribute("action","/MOA4/BOM/Detail/Create/Action");
		
		return "detailCreateBOM";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/BOM/Detail/Create/Action", method = {RequestMethod.GET,RequestMethod.POST})
	public String detailCreateBOMAction (HttpServletRequest request, Model model,RedirectAttributes ra) {
		String altId = request.getParameter("altId");
		String gridData = request.getParameter("gridData");
		String docNo = request.getParameter("hidDocNo");
		Gson gson = new Gson();
		Type typeList = new TypeToken<List<Map<String,String>>>(){}.getType();
		List<Map<String,String>> listDataGrid = (List<Map<String,String>>) gson.fromJson(gridData, typeList);
		
		bomAdapter.insertDetailCreate(docNo,altId, listDataGrid);
		return "redirect:/BOM/Detail/?docNo="+docNo;
	}

	@RequestMapping(value = {"/BOM","/BOM/Browse"}, method = {RequestMethod.GET})
	public String browse(Model model,HttpSession session,HttpServletRequest request) throws Exception{
		
		model.addAttribute("readUrl", "/Browse/Data");
		model.addAttribute("menuAccessMvc", this.getMenuAccessMvc(request));
		List<Map<String,Object>> data = bomAdapter.getBrowseData("");
		model.addAttribute("data", data);
		
		return "browseBOM";
	}
	
	@RequestMapping(value="/BOM/Browse/Data", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody List<Map<String,Object>> getBrowseData (HttpServletRequest request, 
			Model model,RedirectAttributes ra) throws Exception{
		
		try{
			String search = request.getParameter("search");
			List<Map<String,Object>> detail = bomAdapter.getBrowseData(search);
			return detail;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	@RequestMapping(value = { "/BOM/GetDBOMAlter" }, method = RequestMethod.GET)
	public @ResponseBody List<D_BOM_ALTERNATIVE> GetDBOMAlter (Model model, HttpServletRequest request){
		
		return bomAdapter.getDBomAlter(request.getParameter("docNo"));
	}
	
	@RequestMapping(value = { "/BOM/GetDDBom" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> GetDDBom (Model model, HttpServletRequest request){
		
		return bomAdapter.getDDBom(request.getParameter("docNo"), Integer.parseInt(request.getParameter("altId")));
	}
	
	@RequestMapping(value = { "/BOM/AjaxValidatePlant" }, method = RequestMethod.GET)
	public @ResponseBody boolean AjaxValidatePlant(Model model, HttpServletRequest request){
		String plant = request.getParameter("plant");
		String productCode = request.getParameter("productCode");
		List<Map<String,Object>> lsPlant = bomAdapter.validatePlant(plant, productCode);
		
		if(lsPlant.size() == 0){
			return false;
		}else
			return true;
	}
	
	@RequestMapping(value = { "/BOM/AjaxValidateMaterial" }, method = RequestMethod.GET)
	public @ResponseBody String AjaxValidateMaterial(Model model, HttpServletRequest request){
		String plant = request.getParameter("plant");
		String type = request.getParameter("type");
		String material = request.getParameter("material");
		
		List<Map<String,Object>> lsMaterial = bomAdapter.validateMaterial(plant, type, material);
		if(lsMaterial.size() == 0){
			return "false#";
		}else
			return "true#"+lsMaterial.get(0).get("MAKTG").toString()+"#"+lsMaterial.get(0).get("MEINS").toString();
	}
	
	@RequestMapping(value = { "/BOM/AjaxValidateProduct" }, method = RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>> AjaxValidateProduct(Model model, HttpServletRequest request){
		String product = request.getParameter("productCode");
		List<Map<String,Object>> lsMaterial = bomAdapter.validateProduct(product);
		return lsMaterial;
	}
	
	@RequestMapping(value = { "/BOM/ValidateProductHeader" }, method = RequestMethod.GET)
	public @ResponseBody String ValidateProductHeader(Model model, HttpServletRequest request){
		String product = request.getParameter("productCode");
		
		List<Map<String,Object>> lsMaterial = bomAdapter.validateProductHeader(product);
		if(lsMaterial.size()<1){
			return "false#";
		}else{
			return "true#"+lsMaterial.get(0).get("MATNR").toString()+"#"+lsMaterial.get(0).get("MAKTG").toString();
		}
		
	}
	
	@RequestMapping(value="/BOM/KendoWindowSelectProductBrowse", method = RequestMethod.GET)
	public  String KendoWindowSelectProductBrowse(Model model, HttpServletRequest request,
			RedirectAttributes ra){
		model.addAttribute("menuAccessMvc", this.getMenuAccessMvc(request));
		int column = Integer.valueOf(request.getParameter("column"));
		
		if (request.getParameter("search") != null) {
			model.addAttribute("readUrl","/BOM/KendoWindowSelectProductDataBrowse/?search="+
					request.getParameter("search"));
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectProductBrowse/?window="+
					request.getParameter("window")+"&column="+column+"&search=");
			model.addAttribute("search", request.getParameter("search"));
		}
		else
		{
			model.addAttribute("readUrl","/BOM/KendoWindowSelectProductDataBrowse");
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectProductBrowse/?window="+
			request.getParameter("window")+"&column="+column+"&search=");
		}
		
		String[] stringArr = new String[column];
		stringArr[0] = "Product Code";
		stringArr[1] = "Product Name";
		
		model.addAttribute("titleArr",stringArr);
		
		String[] widthArr = new String[column];
		widthArr[0] = "200px";
		widthArr[1] = "400px";
		model.addAttribute("widthArr",widthArr);
		model.addAttribute("windowID",request.getParameter("window"));
		model.addAttribute("selectedCode",request.getParameter("selectedCode"));
		model.addAttribute("columnCount",column);
		
		return "KendoWindowBrowseCopyFPSA";
	}
	
	@RequestMapping(value = "/BOM/KendoWindowSelectProductDataBrowse", method = RequestMethod.GET)
	public @ResponseBody String KendoWindowSelectProductDataBrowse(HttpServletRequest request, RedirectAttributes ra) throws Exception
	{
		String search;
		if(request.getParameter("search") == null) search="";
		else search = request.getParameter("search");
		
		List<Map<String,Object>> listEmp =  bomAdapter.getProduct(search);
		Gson gson = new Gson();
		List<KendoWindowBrowse2> listKendo = new ArrayList<KendoWindowBrowse2>();
		for(Map<String,Object> emp : listEmp)
		{
			listKendo.add(new KendoWindowBrowse2(emp.get("MATNR").toString(),emp.get("MAKTG").toString()));
		}
		String json = gson.toJson(listKendo);
		return json;
	}
	
	@RequestMapping(value="/BOM/KendoWindowSelectPlantBrowse", method = RequestMethod.GET)
	public  String KendoWindowSelectPlantBrowse(Model model, HttpServletRequest request,
			RedirectAttributes ra){
		model.addAttribute("menuAccessMvc", this.getMenuAccessMvc(request));
		int column = Integer.valueOf(request.getParameter("column"));
		String materialCode = request.getParameter("materialCode");
				
		if (request.getParameter("search") != null) {
			model.addAttribute("readUrl","/BOM/KendoWindowSelectPlantDataBrowse/?search="+
					request.getParameter("search")+"&materialCode="+materialCode);
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectPlantBrowse/?window="+
					request.getParameter("window")+"&search="+"&materialCode="+materialCode);
			model.addAttribute("search", request.getParameter("search"));
		}
		else
		{
			model.addAttribute("readUrl","/BOM/KendoWindowSelectPlantDataBrowse/?materialCode="+materialCode);
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectPlantBrowse/?window="+
					request.getParameter("window")+"&search="+"&materialCode="+materialCode);
		}
		
		String[] stringArr = new String[column];
		stringArr[0] = "Plant Code";
		stringArr[1] = "Plant Name";
		
		model.addAttribute("titleArr",stringArr);
		
		String[] widthArr = new String[column];
		widthArr[0] = "100px";
		widthArr[1] = "300px";
		model.addAttribute("widthArr",widthArr);
		model.addAttribute("windowID",request.getParameter("window"));
		model.addAttribute("selectedCode",request.getParameter("selectedCode"));
		model.addAttribute("columnCount",column);
		
		return "KendoWindowBrowseCopyFPSA";
	}
	
	@RequestMapping(value = "/BOM/KendoWindowSelectPlantDataBrowse", method = RequestMethod.GET)
	public @ResponseBody String KendoWindowSelectPlantDataBrowse(HttpServletRequest request, RedirectAttributes ra) throws Exception
	{
		String search;
		Gson gson = new Gson();
		
		String materialCode = request.getParameter("materialCode");
		if(request.getParameter("search") == null) search="";
		else search = request.getParameter("search");
		
		List<Map<String,Object>> listPlant =  bomAdapter.getPlant(search, materialCode);
		List<KendoWindowBrowse2> listKendo = new ArrayList<KendoWindowBrowse2>();
		for(Map<String,Object> emp : listPlant)
		{
			if(emp.get("PLANTNAME") == null)
				listKendo.add(new KendoWindowBrowse2(emp.get("WERKS").toString(),""));
			else
				listKendo.add(new KendoWindowBrowse2(emp.get("WERKS").toString(),emp.get("PLANTNAME").toString()));
		}
		
		String json = gson.toJson(listKendo);
		return json;
	}
	
	@RequestMapping(value="/BOM/KendoWindowSelectMaterialBrowse", method = RequestMethod.GET)
	public  String KendoWindowSelectMaterialBrowse(Model model, HttpServletRequest request,
			RedirectAttributes ra){
		model.addAttribute("menuAccessMvc", this.getMenuAccessMvc(request));
		int column = Integer.valueOf(request.getParameter("column"));
		String type = request.getParameter("type");
		String plant = request.getParameter("plant");
				
		if (request.getParameter("search") != null) {
			model.addAttribute("readUrl","/BOM/KendoWindowSelectMaterialDataBrowse/?search="+
					request.getParameter("search")+"&type="+type+"&plant="+plant);
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectMaterialBrowse/?window="+
					request.getParameter("window")+"&search="+"&type="+type+"&plant="+plant);
			model.addAttribute("search", request.getParameter("search"));
		}
		else
		{
			model.addAttribute("readUrl","/BOM/KendoWindowSelectMaterialDataBrowse/?type="+type+"&plant="+plant);
			model.addAttribute("url","/MOA4/BOM/KendoWindowSelectMaterialBrowse/?window="+
					request.getParameter("window")+"&search="+"&type="+type+"&plant="+plant);
		}
		
		String[] stringArr = new String[column];
		stringArr[0] = "Material Code";
		stringArr[1] = "Material Name";
		stringArr[2] = "UOM";
		
		model.addAttribute("titleArr",stringArr);
		
		String[] widthArr = new String[column];
		widthArr[0] = "200px";
		widthArr[1] = "300px";
		widthArr[2] = "100px";
		model.addAttribute("widthArr",widthArr);
		model.addAttribute("windowID",request.getParameter("window"));
		model.addAttribute("selectedCode",request.getParameter("selectedCode"));
		model.addAttribute("columnCount",column);
		
		return "KendoWindowBrowseCopyFPSA";
	}
	
	@RequestMapping(value = "/BOM/KendoWindowSelectMaterialDataBrowse", method = RequestMethod.GET)
	public @ResponseBody String KendoWindowSelectMaterialDataBrowse(HttpServletRequest request, RedirectAttributes ra) throws Exception
	{
		String search;
		String type = request.getParameter("type");
		String plant = request.getParameter("plant");
		if(request.getParameter("search") == null) search="";
		else search = request.getParameter("search");
		
		List<Map<String,Object>> listMat =  bomAdapter.getMaterial(search, type, plant);
		Gson gson = new Gson();
		List<KendoWindowBrowse3KPI> listKendo = new ArrayList<KendoWindowBrowse3KPI>();
		for(Map<String,Object> emp : listMat)
		{
			listKendo.add(new KendoWindowBrowse3KPI(emp.get("MATNR").toString(),emp.get("MAKTG").toString(),emp.get("MEINS").toString()));
		}
		String json = gson.toJson(listKendo);
		return json;
	}
	
}
