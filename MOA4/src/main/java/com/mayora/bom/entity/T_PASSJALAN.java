package com.mayora.bom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRSAP.T_BOM")
public class T_PASSJALAN {
	@Id	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="CREATEDBY")
	private String createdBy;
	
	@Column(name="CREATEDDATE")
	private String createdDate;
	
	@Column(name="CREATOREMAIL")
	private String creatorEmail;
	
	@Column(name="CREATORNAME")
	private String creatorName;
	
	@Column(name="DOCREF")
	private String docRef;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="NOTES")
	private String notes;
	
	@Column(name="STATUS")
	private String status;
	
	public T_PASSJALAN() {
		super();
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatorEmail() {
		return creatorEmail;
	}

	public void setCreatorEmail(String creatorEmail) {
		this.creatorEmail = creatorEmail;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getDocRef() {
		return docRef;
	}

	public void setDocRef(String docRef) {
		this.docRef = docRef;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
