package com.mayora.bom.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class DD_BOM_PK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="ALTERNATIVE_ID")
	private String altId;
	
	@Column(name="BOM_ID")
	private String bomId;

	public DD_BOM_PK() {
		super();
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public String getBomId() {
		return bomId;
	}

	public void setBomId(String bomId) {
		this.bomId = bomId;
	}
	
}
