package com.mayora.bom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRSAP.T_BOM")
public class T_BOM {
	@Id	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MATERIALCODE")
	private String materialCode;

	@Column(name="CREATEDBY")
	private String createdBy;

	@Column(name="CREATORNAME")
	private String creatorName;
	
	@Column(name="CREATOREMAIL")
	private String creatorEmail;
	
	@Column(name="CREATEDDATE")
	private String createdDate;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="TYPE")
	private String type;
	
	public T_BOM() {

	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getCreatorEmail() {
		return creatorEmail;
	}

	public void setCreatorEmail(String creatorEmail) {
		this.creatorEmail = creatorEmail;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
