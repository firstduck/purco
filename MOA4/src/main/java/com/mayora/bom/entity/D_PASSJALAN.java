package com.mayora.bom.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRSAP.D_BOM_ALTERNATIVE")
public class D_PASSJALAN implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name="DOCNO")
	private String docNo;
	
	@Id	
	@Column(name="ID")
	private int id;
	
	@Column(name="ITEM")
	private String item;
	
	@Column(name="QTY")
	private int qty;
	
	public D_PASSJALAN() {
		super();
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	
}
