package com.mayora.bom.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRSAP.DD_BOM")
public class DD_BOM {
	@EmbeddedId	
	private DD_BOM_PK pk;
	
	@Column(name="MATERIALCODE")
	private String materialCode;
	
	@Column(name="UOM")
	private String uom;
	
	@Column(name="QTY")
	private int qty;

	public DD_BOM() {
		super();
	}

	public DD_BOM_PK getPk() {
		return pk;
	}

	public void setPk(DD_BOM_PK pk) {
		this.pk = pk;
	}

	public String getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	
}
