package com.mayora.bom.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRSAP.D_BOM_ALTERNATIVE")
public class D_BOM_ALTERNATIVE implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="DEFAULT_")
	private int default_;
	
	@Id
	@Column(name="ALTERNATIVE_ID")
	private int alternativeId;
	
	@Column(name="PLANT")
	private String plant;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public int getDefault_() {
		return default_;
	}

	public void setDefault_(int default_) {
		this.default_ = default_;
	}

	public int getAlternativeId() {
		return alternativeId;
	}

	public void setAlternativeId(int alternativeId) {
		this.alternativeId = alternativeId;
	}

	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}
	
	
	
}
