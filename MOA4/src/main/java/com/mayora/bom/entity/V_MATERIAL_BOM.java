package com.mayora.bom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYROA.V_MATERIAL_BOM")
public class V_MATERIAL_BOM {
	@Id	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="ALTERNATIVE_ID")
	private int alternativeId;
	
	@Column(name="BOM_ID")
	private int bomId;
	
	@Column(name="MATERIALCODE")
	private String materialCode;
	
	@Column(name="MATERIALNAME")
	private String materialName;
	
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	@Column(name="QTY")
	private int qty;
	
	@Column(name="UOM")
	private String uom;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public int getAlternativeId() {
		return alternativeId;
	}

	public void setAlternativeId(int alternativeId) {
		this.alternativeId = alternativeId;
	}

	public int getBomId() {
		return bomId;
	}

	public void setBomId(int bomId) {
		this.bomId = bomId;
	}

	public String getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}
	
}
