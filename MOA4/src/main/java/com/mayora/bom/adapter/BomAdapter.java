package com.mayora.bom.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.mayora.bom.entity.DD_BOM;
import com.mayora.bom.entity.T_BOM;
import com.mayora.bom.entity.V_MATERIAL_BOM;
import com.mayora.bom.entity.D_BOM_ALTERNATIVE;
import com.mayora.moa.adapter.MoaBaseAdapter;

public class BomAdapter extends MoaBaseAdapter {
	public String setModule(){
		return "BOM";
	}
	
	public void insertTrack(String docNo,String username,String moduleCode,String activity,String delegateBy,String reason,Integer canceled){
		Session session = sfOA.openSession();
		try{
			session.beginTransaction();
			Query query = session.createSQLQuery("INSERT INTO MYROA.T_TRACK(DOCNO,DATE_,USERNAME,MODULECODE,ACTIVITY,DELEGATEDBY,REASON,CANCELLED) " +
					"VALUES ('"+docNo+"',SYSDATE,'"+username+"','"+moduleCode+"','"+activity+"','"+delegateBy+"','"+reason+"',"+canceled+")");
			
			query.executeUpdate();
			System.out.println(query.getQueryString());
			session.getTransaction().commit();
			
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
	}
	
	public boolean insertCreate(String docNo,String productCode, String type, List<Map<String,String>> listDataGrid, String username, String name, String email){
		Session session = sfOA.openSession();
		Transaction tx = null;
		try{
			String createdDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date());
			String q = "INSERT ALL ";
			
			q = q.concat("INTO MYRSAP.T_BOM (DOCNO, MATERIALCODE, CREATEDBY, CREATORNAME, CREATOREMAIL, CREATEDDATE, STATUS, TYPE) " +
					"VALUES ('"+docNo+"', '"+productCode+"', '"+username+"', '"+name+"', '"+email+"', TO_DATE('"+createdDate+"','dd-MM-yyyy HH24:mi'), 'OPEN', '"+type+"') ");
			
			for(int i=0;i<listDataGrid.size();i++){
				int default_= 0;
				q = q.concat("INTO myrsap.d_bom_alternative (DOCNO, ALTERNATIVE_ID, DEFAULT_, PLANT) " +
						"VALUES ('"+docNo+"', '"+listDataGrid.get(i).get("ALTERNATIVE_ID").toString()+"', '"+default_+"', '"+listDataGrid.get(i).get("PLANT").toString()+"') ");
			}
			q = q.concat("SELECT * FROM DUAL ");
			System.out.println(q);
			tx = session.beginTransaction();
			session.createSQLQuery(q).executeUpdate();
			tx.commit();
			
			return true;
		}catch(HibernateException e){
			e.printStackTrace();
			tx.rollback();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateExistingProduct(String productCode, List<Map<String,String>> listDataGrid){
		Session session = sfOA.openSession();
		Transaction tx = null;
		try{
			String q = "SELECT DOCNO FROM MYRSAP.T_BOM A WHERE MATERIALCODE = :productCode ";
			
			Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("productCode", productCode);
			List<Map<String,Object>> ls = query.list(); 
			String docNo = ls.get(0).get("DOCNO").toString();
			
			//delete data d_bom_alternative by docno
			q = "DELETE FROM MYRSAP.D_BOM_ALTERNATIVE WHERE DOCNO = :docNo";
			tx = session.beginTransaction();
			query = session.createSQLQuery(q);
			query.setParameter("docNo", docNo);
			query.executeUpdate();
			tx.commit();
			
			//insert data baru
			q = "INSERT ALL ";
			
			for(int i=0;i<listDataGrid.size();i++){
				int default_;
//				if(listDataGrid.get(i).get("DEFAULT_").toString().equals("true") || listDataGrid.get(i).get("DEFAULT_").toString().equals("1"))
//					default_ = 1;
//				else default_ = 0;
				default_ = 0;
				q = q.concat("INTO myrsap.d_bom_alternative (DOCNO, ALTERNATIVE_ID, DEFAULT_, PLANT) " +
						"VALUES ('"+docNo+"', '"+listDataGrid.get(i).get("ALTERNATIVE_ID").toString()+"', '"+default_+"', '"+listDataGrid.get(i).get("PLANT").toString()+"') ");
			}
			q = q.concat("SELECT * FROM DUAL ");
			System.out.println(q);
			tx = session.beginTransaction();
			query = session.createSQLQuery(q);
			query.executeUpdate();
			tx.commit();
			
			return true;
		}catch(HibernateException e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	public boolean insertDetailCreate(String docNo,String altId, List<Map<String,String>> listDataGrid){
		Session session = sfOA.openSession();
		Transaction tx = null;
		try{
			//delete data dd_bom by docno
			String q = "DELETE FROM MYRSAP.DD_BOM WHERE DOCNO = :docNo and ALTERNATIVE_ID = :altId";
			tx = session.beginTransaction();
			Query query = session.createSQLQuery(q);
			query.setParameter("docNo", docNo);
			query.setParameter("altId", altId);
			query.executeUpdate();
			tx.commit();
			
			q = "INSERT ALL ";
			
			for(int i=0;i<listDataGrid.size();i++){
				q = q.concat("INTO myrsap.dd_bom (DOCNO, ALTERNATIVE_ID, BOM_ID, MATERIALCODE, UOM, QTY) " +
						"VALUES ('"+docNo+"', "+altId+", "+listDataGrid.get(i).get("BOM_ID").toString()+", '"+listDataGrid.get(i).get("MATERIALCODE").toString()+
						"' , '"+listDataGrid.get(i).get("UOM").toString()+"' , "+listDataGrid.get(i).get("QTY").toString()+" ) ");
			}
			q = q.concat("SELECT * FROM DUAL ");
			System.out.println(q);
			tx = session.beginTransaction();
			session.createSQLQuery(q).executeUpdate();
			tx.commit();
			
			return true;
		}catch(HibernateException e){
			e.printStackTrace();
			tx.rollback();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	public boolean updateDetail (String docNo, List<Map<String,String>> listDataGrid){
		Session session = sfOA.openSession();
		Transaction tx = null;
		try{
			//delete data d_bom_alternative by docno
			String q = "DELETE FROM MYRSAP.D_BOM_ALTERNATIVE WHERE DOCNO = :docNo";
			tx = session.beginTransaction();
			Query query = session.createSQLQuery(q);
			query.setParameter("docNo", docNo);
			query.executeUpdate();
			tx.commit();
			
			//insert data baru
			q = "INSERT ALL ";
			
			for(int i=0;i<listDataGrid.size();i++){
				int default_;
				if(listDataGrid.get(i).get("default_").toString().equals("true") || listDataGrid.get(i).get("default_").toString().equals("1"))
					default_ = 1;
				else default_ = 0;
				q = q.concat("INTO myrsap.d_bom_alternative (DOCNO, ALTERNATIVE_ID, DEFAULT_, PLANT) " +
						"VALUES ('"+docNo+"', '"+listDataGrid.get(i).get("alternativeId").toString()+"', '"+default_+"', '"+listDataGrid.get(i).get("plant").toString()+"') ");
			}
			q = q.concat("SELECT * FROM DUAL ");
			System.out.println(q);
			tx = session.beginTransaction();
			query = session.createSQLQuery(q);
			query.executeUpdate();
			tx.commit();
			
			return true;
		
		}catch(HibernateException e){
			e.printStackTrace();
			tx.rollback();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public T_BOM getHeader (String docNo){
		Session session = sfOA.openSession();
		try{
			Query query = session.createQuery("FROM T_BOM where docNo = :docNo");
			query.setParameter("docNo", docNo);
			List<T_BOM> lsBom = new ArrayList<T_BOM>();
			lsBom = query.list();
			
			return lsBom.get(0);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> validateProduct (String product){
		Session session = sfOA.openSession();
		try{
			Query query = session.createSQLQuery("SELECT ROWNUM, PLANT, ALTERNATIVE_ID, DEFAULT_ FROM MYRSAP.T_BOM A " +
					"JOIN MYRSAP.D_BOM_ALTERNATIVE B ON A.DOCNO = B.DOCNO where A.MATERIALCODE = :product ORDER BY ALTERNATIVE_ID");
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("product", product);
			List<Map<String,Object>> ls = new ArrayList<Map<String,Object>>();
			ls = query.list();
			
			return ls;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> validateProductHeader (String product){
		Session session = sfOA.openSession();
		try{
			Query query = session.createSQLQuery("SELECT DISTINCT MATNR, MAKTG FROM MYRSAPMD.VMD_MATERIAL " +
					"where MATNR = :product AND MTART = 'FERT' ");
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("product", product);
			List<Map<String,Object>> ls = new ArrayList<Map<String,Object>>();
			ls = query.list();
			
			return ls;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getProductName (String productCode){
		Session session = sfOA.openSession();
		try{
			Query query = session.createSQLQuery("SELECT MAKTG FROM MYRSAPMD.VMD_MATERIAL where MATNR = :productCode");
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("productCode", productCode);
			List<Map<String, Object>> ls = query.list();
			
			return ls.get(0).get("MAKTG").toString();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
			
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getBrowseData(String search) throws Exception{
		Session session = null;
		try{
			session = sfOA.openSession();
			String q = "SELECT distinct DOCNO, TYPE, MATERIALCODE, MAKTG, CREATEDBY, TO_CHAR(CREATEDDATE,'dd-MM-yyyy') AS CREATED_DATE ,CREATEDDATE, STATUS FROM MYRSAP.T_BOM A " +
					"join myrsapmd.vmd_material b on a.materialcode = b.matnr ";
			
			if(search!=null){
				if(!search.equals("")){
					search = search.toUpperCase();
					q = q + "where DOCNO LIKE '%"+search+"%' OR MAKTG LIKE '%"+search+"%' OR TYPE LIKE '%"+search+"%' OR MATERIALCODE LIKE '%"+search+"%' OR CREATEDBY LIKE '%"+search+"%' OR CREATEDDATE LIKE '%"+search+"%' " +
							"OR STATUS LIKE '%"+search+"%' ";
				}
			}
			q = q + "order BY CREATEDDATE desc "; 
			System.out.println(q);
			Query query = session.createSQLQuery(q);
				
		    return query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<D_BOM_ALTERNATIVE> getDBomAlter(String docNo){
		Session session = sfOA.openSession();
		try{
			Query query = session.createQuery("FROM D_BOM_ALTERNATIVE where docNo = :docNo order by plant, alternativeId");
			query.setParameter("docNo", docNo);
			List<D_BOM_ALTERNATIVE> lsBom = new ArrayList<D_BOM_ALTERNATIVE>();
			lsBom = query.list();
			
			return lsBom;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getDDBom(String docNo, int alternativeId){
		Session session = sfOA.openSession();
		try{
			Query query = session.createSQLQuery("SELECT DISTINCT DOCNO, ALTERNATIVE_ID, BOM_ID, MATERIALCODE, MAKTG AS MATERIALNAME, QTY, UOM " +
					"FROM MYRSAP.DD_BOM A join myrsapmd.vmd_material b on a.materialcode = b.matnr WHERE docNo = :docNo AND ALTERNATIVE_ID= :alternativeId ORDER BY BOM_ID ASC");
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("docNo", docNo);
			query.setParameter("alternativeId", alternativeId);
			
			return query.list();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getProduct(String search){
		Session session = null;
		try{
			session = sfOA.openSession();
            String q = "SELECT DISTINCT MATNR, MAKTG FROM MYRSAPMD.VMD_MATERIAL WHERE MTART ='FERT' AND (MAKTG LIKE '%"+search.toUpperCase()+"%' or MATNR LIKE '%"+search+"%') ";
            Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            
			return query.setMaxResults(100).list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getPlant(String search, String materialCode) {
		Session session = null;
		try{
			session = sfOA.openSession();
			String q = "SELECT a.WERKS, PLANTNAME FROM MYRSAPMD.VMD_MATERIAL A LEFT JOIN MYRMD.M_PLANT B ON A.WERKS = B.PLANTCODE " +
					"WHERE A.MATNR = :materialCode AND A.WERKS LIKE '1%' AND A.WERKS LIKE '%"+search+"%' ORDER BY A.WERKS";
			Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				
            query.setParameter("materialCode", materialCode);
            
			return query.setMaxResults(100).list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getMaterial(String search, String type, String plant) {
		Session session = null;
		try{
			session = sfOA.openSession();
			String q = "SELECT distinct MATNR, MAKTG, MEINS FROM MYRSAPMD.VMD_MATERIAL WHERE WERKS = :plant and "; 
			
			if(type.equals("BOM")){
				q += "MTART IN ('ROH','HALB','VERP') ";
			}else if(type.equals("PRP")){
				q += "MTART = 'ZPRP' ";
			}
			q += "AND (MATNR LIKE '%"+search+"%' OR MAKTG LIKE '%"+search.toUpperCase()+"%') ORDER BY MATNR";
			Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            query.setParameter("plant", plant);
            
			return query.setMaxResults(100).list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> validatePlant (String plant, String productCode){
		Session session = null;
		try{
			session = sfOA.openSession();
			String q = "SELECT WERKS FROM MYRSAPMD.VMD_MATERIAL WHERE MTART ='FERT' AND MATNR = :productCode and werks = :plant";
			Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("productCode", productCode);
			query.setParameter("plant", plant);
			
			return query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> validateMaterial (String plant, String type, String material){
		Session session = null;
		try{
			session = sfOA.openSession();
			String q = "SELECT DISTINCT MAKTG, MEINS FROM MYRSAPMD.VMD_MATERIAL WHERE MATNR = :material and WERKS = :plant and "; 
			
			if(type.equals("BOM")){
				q += "MTART IN ('ROH','HALB','VERP') ";
			}else if(type.equals("PRP")){
				q += "MTART = 'ZPRP' ";
			}
			Query query = session.createSQLQuery(q).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setParameter("material", material);
			query.setParameter("plant", plant);
			
			return query.list();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally{
			session.close();
		}
	}

}
