package com.mayora.moa.adapter;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mayora.wf.WorkFlow;
import com.mayora.wf.WorkFlowConnection;
import com.mayora.moa.model.JobList;
import com.mayora.moa.workflow.adapter.WorkflowBaseAdapter;
import com.mayora.moa.workflow.entity.V_DetailCurrentWorkflowLines;
import com.mayora.moa.workflow.entity.V_DocumentStatus;
import com.google.gson.Gson;

public class MoaBaseAdapter extends WorkflowBaseAdapter{

//	@Autowired  
//	@Qualifier("sfWF")  
//	protected SessionFactory sfWF;       
//	
	@Autowired  
	@Qualifier("sfOA")  
	protected SessionFactory sfOA; 
	
	@Autowired  
	@Qualifier("sfSAP")  
	protected SessionFactory sfSAP;
	
		
	public void setSessionFactoryOA(SessionFactory sessionFactory) {
        this.sfOA = sessionFactory;
    }
	
    public boolean addObjectOA(Object object) {
        Session session = sfOA.openSession();
        try {
	        session.beginTransaction();
	        session.save(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	System.out.println(e);
        	System.out.println(new Gson().toJson(object));
        	session.close();
        	return false;
        }
       
    }
    
    public boolean deleteObjectOA(Object object, String docNo) {
    	Session session = sfOA.openSession();
        try {
	        session.beginTransaction();
	        session.load(object, docNo);
	        session.delete(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	System.out.println(e);
        	session.close();
        	return false;
        } 

    }
    
    public boolean updateObjectOA(Object object) {
    	Session session = sfOA.openSession();
        try {
	        session.beginTransaction();
	        session.update(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	e.printStackTrace();
        	session.close();
        	return false;
        } 
        
    }
    
    public boolean addObjectSAP(Object object) {
        Session session = sfSAP.openSession();
        try {
	        session.beginTransaction();
	        session.save(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	System.out.println(e);
        	session.close();
        	return false;
        }
       
    }

    public boolean deleteAll(String table,String variable, String value) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery("Delete from "+table+" where "+variable+"='"+value+"'");
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  }
    }
    
    public boolean deleteFlagAll(String table,String variable, String value) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery("update "+table+" set isdeleted = 1 where "+variable+"='"+value+"'");
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  	}
    }
    
    public boolean deleteAllForHelpDesk(String table,String variable, String value,String value2,String value3) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery("Delete from "+table+" where "+variable+"='"+value+"' and pic='"+value2+"' and groupCode='"+value3+"'");
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  }
    }
    
    public boolean deleteAll(String table,String variable1, String value1, String variable2, String value2) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery("Delete from "+table+" where "+variable1+"='"+value1+"' and "+ variable2+" = '"+value2+"'");
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  }
    }
    
    public boolean executeSQL(String qry) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery(qry);
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  }
    }
	
//	public void setSessionFactory(SessionFactory sessionFactory) {
//        this.sfWF = sessionFactory;
//    }
//	
//    public boolean addObject(Object object) {
//        Session session = sfWF.openSession();
//        try {
//	        session.beginTransaction();
//	        session.save(object);
//	        session.getTransaction().commit();
//	        session.close();
//	        return true;
//        } catch(HibernateException e) { 
//        	session.getTransaction().rollback(); 
//        	session.close();
//        	return false;
//        } 
//        
//    }
//    
//    public boolean deleteObject(Object object, String docNo) {
//    	Session session = sfWF.openSession();
//        try {
//	        session.beginTransaction();
//	        session.load(object, docNo);
//	        session.delete(object);
//	        session.getTransaction().commit();
//	        session.close();
//	        return true;
//        } catch(HibernateException e) { 
//        	session.getTransaction().rollback(); 
//        	session.close();
//        	return false;
//        } 
//  
//    }
//    
//    public boolean updateObject(Object object) {
//    	Session session = sfWF.openSession();
//        try {
//	        session.beginTransaction();
//	        session.update(object);
//	        session.getTransaction().commit();
//	        session.close();
//	        return true;
//        } catch(HibernateException e) { 
//        	session.getTransaction().rollback(); 
//        	session.close();
//        	return false;
//        } 
//      
//    }
    

	public String getDocNo(String moduleCode, String groupCode) {
		Session session = sfOA.openSession();
        try {
        	Query query = session.createSQLQuery(
    			"SELECT MYROA.GETDOCNO('"+ moduleCode +"', '"+ groupCode +"') FROM DUAL"  
    		);  
    		Object docNoList = query.uniqueResult();
    		session.close();
    		return docNoList.toString();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback();
        	session.close();
        	return null;
        } 
	}
	
	public String getDocNoSAP(String moduleCode, String groupCode) {
		Session session = sfOA.openSession();
        try {
        	Query query = session.createSQLQuery(
    			"SELECT MYRSAP.GETDOCNO('"+ moduleCode +"', '"+ groupCode +"') FROM DUAL"  
    		);  
    		Object docNoList = query.uniqueResult();
    		session.close();
    		return docNoList.toString();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback();
        	session.close();
        	return null;
        } 
	}
	
	public String getApprovedBy(String docNo, String wfCode, Integer line) {
		Session session = sfOA.openSession();
        try {
        	Query query = session.createSQLQuery(
    			"SELECT MYROA.GETAPPROVEDBY('"+ docNo +"', '"+ wfCode +" , '"+line+"'') FROM DUAL"  
    		);  
    		Object docNoList = query.uniqueResult();
    		session.close();
    		return docNoList.toString();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback();
        	session.close();
        	return null;
        } 
	}
	
	public Boolean isRejectedDocument(String docNo)
	{
		Boolean reject = false;
		Session session = sfOA.openSession();
		
		List<String> list = session.createSQLQuery("select tag from myrwf.t_selectedworkflow where docno ='"+docNo+"' and tag='RETURNED'").list();
		
		if(list.size()>0)reject = true;
		else{
			list = session.createSQLQuery("select rejectby from myrwf.V_DocumentStatus where docno ='"+docNo+"' and rejectby is not null").list();
			
			if(list.size()>0)reject = true;
		}
//		List<V_DocumentStatus> listDocStatus = session.createQuery("from V_DocumentStatus where docNo ='"+docNo+"')")
//				.list();
//		for(V_DocumentStatus doc : listDocStatus)
//			if(doc.getRejectByUsername() != null) reject = true;
		session.close();
		return reject;
	}
	
public String getSetting(final String type){
		
		final Session session = sfWF.openSession();
		String res = session.doReturningWork(new ReturningWork<String>() {
            @Override
            public String execute(Connection connection) throws SQLException {
                Statement stmt = connection.createStatement();
                try{
                   WorkFlowConnection wfc = new WorkFlowConnection();
             	   String url = wfc.getSetting(stmt,"MOA_2");
             	   return url;
               }finally{
            	   stmt.close();
                   connection.commit();
               }
          }});
		session.close();
		return res;
	}
	
	public void sendEmailNotif(final String msg, final String rcpt, final String cc, final String subject){
		
		final Session session = sfWF.openSession();
		session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
               Statement stmt = connection.createStatement();
               try{
            	   WorkFlow wf = new WorkFlow();
            	   WorkFlowConnection wfc = new WorkFlowConnection();
            	   wfc.sendMail(session, wf.getMsgSender(), cc, rcpt, subject, null, msg);
               }finally{
                   stmt.close();
                   connection.commit();
                   connection.close();
               }
            }});
		session.close();
	}
	
	public boolean deleteAttachment(String table,String variable, String value,String variable1, String value1) {
		Session session = sfOA.openSession();
	  try {
	      session.beginTransaction();	
	      Query query = session.createSQLQuery("Delete from "+table+" where "+variable+"='"+value+"'  and  "+variable1+"='"+value1+"'");
	      System.out.println(query.getQueryString());
	      query.executeUpdate();
	      session.getTransaction().commit();
	      session.close();
		  return true;
	  } catch(HibernateException e) { 
	  	session.getTransaction().rollback(); 
	  	session.close();
	  	return false;
	  }
    }
	
	public List<?> getObjectSAPMD(String object, String where)
	{
		Session session = sfOA.openSession();
		List<?> list = session.createQuery("from " +object+ " " + where).list();
		session.close();
		return list;
	}
	
	public boolean isNotBlockedMaterial(String plantCode,String materialCode) {
		Session session = sfOA.openSession();
	  try {
		  String q = "SELECT MMSTA FROM MYRSAPMD.MARC where MATNR = '"+materialCode+"' and WERKS = '"+plantCode+"'";
	      List<String> query = session.createSQLQuery(q).list();
	      session.close();
	      
	      try 
	      {
	    	  if(query.get(0).equals("01") || query.get(0).equals("02") || query.get(0).equals("03") || query.get(0).equals("04")) return false;
	    	  else return true;
	      }
	      catch(Exception e) { }
		  return true;
	  } catch(HibernateException e) { 
	  	session.close();
	  	return false;
	  }
	}
	
	public List<Map<String,Object>> getMapQuery(String q)
	{
		Session session = sfOA.openSession();
		Query query = session.createSQLQuery(q);
		
		System.out.println(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		
		List<Map<String,Object>> aliasToValueMapList = new ArrayList<Map<String,Object>>();
		aliasToValueMapList = query.list();
		session.close();
		
		return aliasToValueMapList;
	}
	
	public String getProjectPathWindows(String[] pathProject, String projectPath) {
		for (int a = 1; a < pathProject.length - 6; a++) { 
    		projectPath = projectPath + pathProject[a] + "\\"; 
    	}
		projectPath = projectPath.replace("%20", " ");
        return projectPath;
	}
	
	//TAMBAH//
	public List<V_DocumentStatus> getAllDocumentStatus(String docNo)
	{
		Session session = sfOA.openSession();
		List<V_DocumentStatus> list = session.createQuery("from V_DocumentStatus where docNo = '"+docNo+"' order by workflowID,workflowLinesId,idPararelLine").list();
		session.close();
		return list;
	}
	
	public List<V_DetailCurrentWorkflowLines> getDetailCurrentWF(String docNo)
	{
		Session session = sfOA.openSession();
		List<V_DetailCurrentWorkflowLines> list = session.createQuery("from V_DetailCurrentWorkflowLines where docNo = '"+docNo+"'").list();

		session.close();
		return list;
	}
	
	
}
