package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="MYRWF.M_AUTOASSIGN")

public class M_AutoAssign {
//	PK
//	AUTOASSIGNPATHID
//	MODULECODE
//	WORKFLOWCODE
	@EmbeddedId
	private M_AutoAssign_PK pk; 
	 
	@Transient
	private String idWorkflowPath;
	 	
	@Transient
	private String dummyModule;
	
	@Transient
	private String dummyWorkflow;
	


	public String getIdWorkflowPath() {
		return idWorkflowPath;
	}

	public void setIdWorkflowPath(String idWorkflowPath) {
		this.idWorkflowPath = idWorkflowPath;
	}

	public String getDummyModule() {
		return dummyModule;
	}

	public void setDummyModule(String dummyModule) {
		this.dummyModule = dummyModule;
	}

	public String getDummyWorkflow() {
		return dummyWorkflow;
	}

	public void setDummyWorkflow(String dummyWorkflow) {
		this.dummyWorkflow = dummyWorkflow;
	}

	public M_AutoAssign_PK getPk() {  
		return pk;
	}

	public void setPk(M_AutoAssign_PK pk) {
		this.pk = pk;
	}

	@Column(name="FUNCTIONCODE")
	private String functionCode;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="WHERECLAUSE")
	private String whereClause;

	public String getFunctionCode() {
		return functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	
}
