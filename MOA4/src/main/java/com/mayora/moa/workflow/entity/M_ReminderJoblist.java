package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="MYRWF.M_REMINDERJOBLIST")
public class M_ReminderJoblist {

	@Id
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="DURATION")
	private Integer duration;
	
	@Column(name="STARTHOUR")
	private Integer startHour;
	
	@Column(name="ENDHOUR")
	private Integer endHour;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	public Integer getEndHour() {
		return endHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}
	
	
}
