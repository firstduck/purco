package com.mayora.moa.workflow.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_JOBLISTMOBILE")

public class V_JoblistMobile {
	@Id
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="WORKFLOWID")
	private Integer workflowID;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathID;
	
	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesID;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="ASSIGNEDPIC")
	private String assignedPIC;
	
	@Column(name="CREATEDBY")
	private String createdBy;
	
	@Column(name="CREATEDDATE")
	private Date createdDate;
	
	@Column(name="WEBNAME")
	private String webName;
	
	private String lastDate = "-";
	
	private String lastUser = "-";

	@Column(name="CREATORNAME")
	private String creatorName;

	private String lastApprove = "-";

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getWorkflowID() {
		return workflowID;
	}

	public void setWorkflowID(Integer workflowID) {
		this.workflowID = workflowID;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public Integer getWorkflowPathID() {
		return workflowPathID;
	}

	public void setWorkflowPathID(Integer workflowPathID) {
		this.workflowPathID = workflowPathID;
	}

	public Integer getWorkflowLinesID() {
		return workflowLinesID;
	}

	public void setWorkflowLinesID(Integer workflowLinesID) {
		this.workflowLinesID = workflowLinesID;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssignGroup() {
		return assignGroup;
	}

	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	public String getAssignedPIC() {
		return assignedPIC;
	}

	public void setAssignedPIC(String assignedPIC) {
		this.assignedPIC = assignedPIC;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getLastAprpove() {
		return lastApprove;
	}

	public void setLastApprove(String lastApprove) {
		this.lastApprove = lastApprove;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
}
