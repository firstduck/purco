package com.mayora.moa.workflow.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_GroupAccessWorkflow_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="USER_NAME")
	private String username;

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
