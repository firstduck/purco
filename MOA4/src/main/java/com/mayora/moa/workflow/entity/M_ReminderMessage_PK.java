package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class M_ReminderMessage_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public M_ReminderMessage_PK(){}
	public M_ReminderMessage_PK(String workflowCode,Integer workflowPathId,Integer workflowLinesId,Integer id) {
		this.workflowCode = workflowCode;
		this.workflowPathId = workflowPathId;
		this.workflowLinesId = workflowLinesId;
		this.id = id;
	}

	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathId;

	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesId;
	
	@Column(name="ID")
	private Integer id;

	/**
	 * @return the workflowCode
	 */
	public String getWorkflowCode() {
		return workflowCode;
	}

	/**
	 * @param workflowCode the workflowCode to set
	 */
	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	/**
	 * @return the workflowPathId
	 */
	public Integer getWorkflowPathId() {
		return workflowPathId;
	}

	/**
	 * @param workflowPathId the workflowPathId to set
	 */
	public void setWorkflowPathId(Integer workflowPathId) {
		this.workflowPathId = workflowPathId;
	}

	/**
	 * @return the workflowLinesId
	 */
	public Integer getWorkflowLinesId() {
		return workflowLinesId;
	}

	/**
	 * @param workflowLinesId the workflowLinesId to set
	 */
	public void setWorkflowLinesId(Integer workflowLinesId) {
		this.workflowLinesId = workflowLinesId;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	
	

}
