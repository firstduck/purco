package com.mayora.moa.workflow.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author User
 *
 */
@Embeddable
public class V_DocumentStatus_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID")
	private String documentStatusID;

	@Column(name="USERNAME")
	private String username;

	public String getDocumentStatusID() {
		return documentStatusID;
	}

	public void setDocumentStatusID(String documentStatusID) {
		this.documentStatusID = documentStatusID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
