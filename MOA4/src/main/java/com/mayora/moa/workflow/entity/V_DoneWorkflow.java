package com.mayora.moa.workflow.entity;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name="MYRWF.V_DONEWORKFLOW")
public class V_DoneWorkflow {

	@Id
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="WORKFLOWID")
	private Integer workflowID;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathID;
	
	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesID;
	
	@Column(name="IDPARARELLINE")
	private Integer idPararelLine;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="ASSIGNEDPIC")
	private String assignedPIC;
	
	@Column(name="DATE_")
	private Timestamp date_;
	
	@Column(name="INSERTBY")
	private String insertBy;
	
	@Column(name="NEEDREMINDER")
	private Boolean needReminder;
	
	@Column(name="REMINDERDURATION")
	private Integer reminderDuration;
	
	@Column(name="NEEDCCSUPERIOR")
	private Boolean needCcSuperior;
	
	@Column(name="CCLIST")
	private String ccList;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="WORKFLOWNAME")
	private String workflowName;
	
	@Column(name="NEEDASSIGNMENT")
	private Boolean needAssignment;
	
	@Column(name="INITIALASSIGNMENT")
	private Boolean initialAssignment;
	
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	
	@Column(name="CLOSEDATE")
	private Timestamp closeDate;
	
	@Column(name="USERNAME")
	private String username;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getWorkflowID() {
		return workflowID;
	}

	public void setWorkflowID(Integer workflowID) {
		this.workflowID = workflowID;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public Integer getWorkflowPathID() {
		return workflowPathID;
	}

	public void setWorkflowPathID(Integer workflowPathID) {
		this.workflowPathID = workflowPathID;
	}

	public Integer getWorkflowLinesID() {
		return workflowLinesID;
	}

	public void setWorkflowLinesID(Integer workflowLinesID) {
		this.workflowLinesID = workflowLinesID;
	}

	public Integer getIdPararelLine() {
		return idPararelLine;
	}

	public void setIdPararelLine(Integer idPararelLine) {
		this.idPararelLine = idPararelLine;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssignGroup() {
		return assignGroup;
	}

	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	public String getAssignedPIC() {
		return assignedPIC;
	}

	public void setAssignedPIC(String assignedPIC) {
		this.assignedPIC = assignedPIC;
	}

	public Timestamp getDate_() {
		return date_;
	}

	public void setDate_(Timestamp date_) {
		this.date_ = date_;
	}

	public String getInsertBy() {
		return insertBy;
	}

	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}

	public Boolean getNeedReminder() {
		return needReminder;
	}

	public void setNeedReminder(Boolean needReminder) {
		this.needReminder = needReminder;
	}

	public Integer getReminderDuration() {
		return reminderDuration;
	}

	public void setReminderDuration(Integer reminderDuration) {
		this.reminderDuration = reminderDuration;
	}

	public Boolean getNeedCcSuperior() {
		return needCcSuperior;
	}

	public void setNeedCcSuperior(Boolean needCcSuperior) {
		this.needCcSuperior = needCcSuperior;
	}

	public String getCcList() {
		return ccList;
	}

	public void setCcList(String ccList) {
		this.ccList = ccList;
	}

	public String getDescLines() {
		return descLines;
	}

	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Boolean getNeedAssignment() {
		return needAssignment;
	}

	public void setNeedAssignment(Boolean needAssignment) {
		this.needAssignment = needAssignment;
	}

	public Boolean getInitialAssignment() {
		return initialAssignment;
	}

	public void setInitialAssignment(Boolean initialAssignment) {
		this.initialAssignment = initialAssignment;
	}

	public String getDescAssignment() {
		return descAssignment;
	}

	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}

	public Timestamp getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Timestamp closeDate) {
		this.closeDate = closeDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
