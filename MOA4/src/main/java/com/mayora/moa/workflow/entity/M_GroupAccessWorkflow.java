package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_GROUPACCESSWORKFLOW")
public class M_GroupAccessWorkflow {
	
	@EmbeddedId
	private M_GroupAccesssWorkflow_PK pk;
	
	@Column(name="SUPERIOR")
	private String superior;
	
	@Column(name="ACTIVE")
	private Boolean active;
	
	@Column(name="DEFAULTVALUE")
	private Boolean defaultValue;
	
	@Column(name="DEFAULTSUPERIOR")
	private Boolean defaultSuperior;
	
	@Column(name="SUBGROUPWORKFLOW")
	private String subGroupWorkflow;
	
	@Transient
	private String dummyGroupCode;
	
	@Transient
	private String dummyUsername;
	
	@Transient
	private String name;
	
	@Transient
	private String superiorName;
	
	@Transient
	private Integer index;
	
//	@Transient
//	private Boolean isDeleted = false;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSuperiorName() {
		return superiorName;
	}

	public void setSuperiorName(String superiorName) {
		this.superiorName = superiorName;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

//	public Boolean getIsDeleted() {
//		return isDeleted;
//	}
//
//	public void setIsDeleted(Boolean isDeleted) {
//		this.isDeleted = isDeleted;
//	}

	public String getDummyGroupCode() {
		return this.pk.getGroupCode();
	}

	public String getDummyUsername() {
		return this.pk.getUsername();
	}

	public M_GroupAccesssWorkflow_PK getPk() {
		return pk;
	}

	public void setPk(M_GroupAccesssWorkflow_PK pk) {
		this.pk = pk;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getDefaultSuperior() {
		return defaultSuperior;
	}

	public void setDefaultSuperior(Boolean defaultSuperior) {
		this.defaultSuperior = defaultSuperior;
	}

	public String getSubGroupWorkflow() {
		return subGroupWorkflow;
	}

	public void setSubGroupWorkflow(String subGroupWorkflow) {
		this.subGroupWorkflow = subGroupWorkflow;
	}
	
}
