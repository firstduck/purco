package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="MYRWF.M_WORKFLOWLINES")
public class M_Workflowlines {
	
	@EmbeddedId
	private M_Workflowlines_PK id;
	
	
	@Column(name="ACTIVITY")
	private String activity;
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	@Column(name="NEEDASSIGNMENT")
	private String needAssignment;
	@Column(name="INITIALASSIGNMENT")
	private String initialAssignment;
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="NEEDREMINDER")
	private String needReminder;
	@Column(name="REMINDERDURATION")
	private String reminderDuration;
	@Column(name="NEEDCCSUPERIOR")
	private String needCcSuperior;
	@Column(name="CCLIST")
	private String ccList;
	@Column(name="TAG")
	private String tag;
	@Column(name="DIRECTNOTIF")
	private String directNotif;
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Transient
	private String codeWorkflow;
	@Transient
	private String idPathWorkflow;
	@Transient
	private String idLinesWorkflow;
	@Transient
	private String idLinePararel;
	
	/**
	 * @return the codeWorkflow
	 */
	public String getCodeWorkflow() {
		return id.getWorkflowCode();
	}
	/**
	 * @return the idPathWorkflow
	 */
	public String getIdPathWorkflow() {
		return id.getWorkflowPathId();
	}
	/**
	 * @return the idLinesWorkflow
	 */
	public String getIdLinesWorkflow() {
		return id.getWorkflowLinesId();
	}
	/**
	 * @return the idLinePararel
	 */
	public String getIdLinePararel() {
		return id.getIdPararelLine();
	}
	
	
	
	
	
	
	
	
	/**
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}
	/**
	 * @param activity the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}
	/**
	 * @return the assignGroup
	 */
	public String getAssignGroup() {
		return assignGroup;
	}
	/**
	 * @param assignGroup the assignGroup to set
	 */
	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}
	/**
	 * @return the needAssignment
	 */
	public String getNeedAssignment() {
		return needAssignment;
	}
	/**
	 * @param needAssignment the needAssignment to set
	 */
	public void setNeedAssignment(String needAssignment) {
		this.needAssignment = needAssignment;
	}
	/**
	 * @return the initialAssignment
	 */
	public String getInitialAssignment() {
		return initialAssignment;
	}
	/**
	 * @param initialAssignment the initialAssignment to set
	 */
	public void setInitialAssignment(String initialAssignment) {
		this.initialAssignment = initialAssignment;
	}
	/**
	 * @return the descAssignment
	 */
	public String getDescAssignment() {
		return descAssignment;
	}
	/**
	 * @param descAssignment the descAssignment to set
	 */
	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}
	/**
	 * @return the descLines
	 */
	public String getDescLines() {
		return descLines;
	}
	/**
	 * @param descLines the descLines to set
	 */
	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}
	/**
	 * @return the needReminder
	 */
	public String getNeedReminder() {
		return needReminder;
	}
	/**
	 * @param needReminder the needReminder to set
	 */
	public void setNeedReminder(String needReminder) {
		this.needReminder = needReminder;
	}
	/**
	 * @return the reminderDuration
	 */
	public String getReminderDuration() {
		return reminderDuration;
	}
	/**
	 * @param reminderDuration the reminderDuration to set
	 */
	public void setReminderDuration(String reminderDuration) {
		this.reminderDuration = reminderDuration;
	}
	/**
	 * @return the needCcSuperior
	 */
	public String getNeedCcSuperior() {
		return needCcSuperior;
	}
	/**
	 * @param needCcSuperior the needCcSuperior to set
	 */
	public void setNeedCcSuperior(String needCcSuperior) {
		this.needCcSuperior = needCcSuperior;
	}
	/**
	 * @return the ccList
	 */
	public String getCcList() {
		return ccList;
	}
	/**
	 * @param ccList the ccList to set
	 */
	public void setCcList(String ccList) {
		this.ccList = ccList;
	}
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * @return the directNotif
	 */
	public String getDirectNotif() {
		return directNotif;
	}
	/**
	 * @param directNotif the directNotif to set
	 */
	public void setDirectNotif(String directNotif) {
		this.directNotif = directNotif;
	}
	/**
	 * @return the id
	 */
	public M_Workflowlines_PK getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(M_Workflowlines_PK id) {
		this.id = id;
	}
	/**
	 * @return the groupWorkflowCode
	 */
	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}
	/**
	 * @param groupWorkflowCode the groupWorkflowCode to set
	 */
	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}
	
	
	
	
	
	
	
}
