package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_GroupAccesssWorkflow_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupCode;
	
	@Column(name="USERNAME")
	private String username;

	public M_GroupAccesssWorkflow_PK(){
		
	}
	
	public M_GroupAccesssWorkflow_PK(String groupCode, String username){
		this.groupCode = groupCode;
		this.username = username;
	}
	
	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
