package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_USERAUTHORIZATION")
public class V_UserAuthorization {

	@EmbeddedId
	private V_UserAuthorization_PK pk;
	
	@Column(name="VALUE")
	private String value;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="PRIMARYKEY")
	private String primaryKey;
	
	@Column(name="BROWSEPRIMARYKEY")
	private String browsePrimaryKey;

	public V_UserAuthorization_PK getPk() {
		return pk;
	}

	public void setPk(V_UserAuthorization_PK pk) {
		this.pk = pk;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getBrowsePrimaryKey() {
		return browsePrimaryKey;
	}

	public void setBrowsePrimaryKey(String browsePrimaryKey) {
		this.browsePrimaryKey = browsePrimaryKey;
	}
	
	
	
}
