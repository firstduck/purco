package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_WORKFLOWPATH")
public class M_Workflowpath {
	@Column(name="WORKFLOWPATHDESC")
	private String workflowPathDesc;
	@Column(name="FIELDID")
	private String fieldId;
	@Column(name="OPERATOR")
	private String operator;
	@Column(name="VALUE")
	private String value;
	@Column(name="CONNECTOR")
	private String connector;
	
	@Transient
	private String idWorkflowPath;
	@Transient
	private String codeWorkflow;
	@Transient
	private String idPath;
	@Transient
	private String idPathCreate;
	
	
	
	@EmbeddedId
	private M_Workflowpath_PK id;

	public String getIdWorkflowPathInsert() {
		return idWorkflowPath;
	}
	public String getCodeWorkflowInsert() {
		return codeWorkflow;
	}
	public String getIdPathInsert() {
		return idPath;
	}
	

	
	public String getIdWorkflowPath() {
		return id.getWorkflowPathId();
	}
	public String getCodeWorkflow() {
		return id.getWorkflowCode();
	}
	public String getIdPath() {
		return id.getId();
	}
	
	public String getIdWorkflowPathPK() {
		return id.getWorkflowPathId();
	}
	public String getCodeWorkflowPK() {
		return id.getWorkflowCode();
	}
	public String getIdPathPK() {
		return id.getId();
	}
	/**
	 * @return the workflowPathDesc
	 */
	public String getWorkflowPathDesc() {
		return workflowPathDesc;
	}

	/**
	 * @param workflowPathDesc the workflowPathDesc to set
	 */
	public void setWorkflowPathDesc(String workflowPathDesc) {
		this.workflowPathDesc = workflowPathDesc;
	}

	/**
	 * @return the fieldId
	 */
	public String getFieldId() {
		return fieldId;
	}

	/**
	 * @param fieldId the fieldId to set
	 */
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the connector
	 */
	public String getConnector() {
		return connector;
	}

	/**
	 * @param connector the connector to set
	 */
	public void setConnector(String connector) {
		this.connector = connector;
	}

	/**
	 * @return the id
	 */
	public M_Workflowpath_PK getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(M_Workflowpath_PK id) {
		this.id = id;
	}
	/**
	 * @param idWorkflowPath the idWorkflowPath to set
	 */
	public void setIdWorkflowPath(String idWorkflowPath) {
		this.idWorkflowPath = idWorkflowPath;
	}
	/**
	 * @param codeWorkflow the codeWorkflow to set
	 */
	public void setCodeWorkflow(String codeWorkflow) {
		this.codeWorkflow = codeWorkflow;
	}
	/**
	 * @param idPath the idPath to set
	 */
	public void setIdPath(String idPath) {
		this.idPath = idPath;
	}
	/**
	 * @return the idPathCreate
	 */
	public String getIdPathCreate() {
		return idPathCreate;
	}
	/**
	 * @param idPathCreate the idPathCreate to set
	 */
	public void setIdPathCreate(String idPathCreate) {
		this.idPathCreate = idPathCreate;
	}
	
}
