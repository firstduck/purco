package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_TRANSFERGROUPWORKFLOW")
public class V_TransferGroupWorkflow {

	@EmbeddedId
	private V_TransferGroupWorkflow_PK pk;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="NEXTGROUPWORKFLOWCODE")
	private String nextGroupWorkflowCode;
	
	@Column(name="NEXTGROUPWORKFLOWCODEDESC")
	private String nextGroupWorkflowCodeDesc;

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public V_TransferGroupWorkflow_PK getPk() {
		return pk;
	}

	public void setPk(V_TransferGroupWorkflow_PK pk) {
		this.pk = pk;
	}

	public String getNextGroupWorkflowCode() {
		return nextGroupWorkflowCode;
	}

	public void setNextGroupWorkflowCode(String nextGroupWorkflowCode) {
		this.nextGroupWorkflowCode = nextGroupWorkflowCode;
	}

	public String getNextGroupWorkflowCodeDesc() {
		return nextGroupWorkflowCodeDesc;
	}

	public void setNextGroupWorkflowCodeDesc(String nextGroupWorkflowCodeDesc) {
		this.nextGroupWorkflowCodeDesc = nextGroupWorkflowCodeDesc;
	}
	
	
}
