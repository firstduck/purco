package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_Workflowpath_PK implements Serializable{
	private static final long serialVersionUID = 1L;

	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private String workflowPathId;

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public String getWorkflowPathId() {
		return workflowPathId;
	}

	public void setWorkflowPathId(String workflowPathId) {
		this.workflowPathId = workflowPathId;
	}
	
	
}
