package com.mayora.moa.workflow.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_DETAILCURRENTTRACKWORKFLOWLI")
public class V_DetailCurrentTrackWorkflowLi {
	
	@Id
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="WORKFLOWID")
	private Integer workflowID;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathID;
	
	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesID;
	
	@Column(name="IDPARARELLINE")
	private Integer IdPararelLine;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	
	@Column(name="INSERTBY")
	private String insertBy;
	
	@Column(name="DATE_")
	private Date date_;
	
	@Column(name="WEBNAME")
	private String webName;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getWorkflowID() {
		return workflowID;
	}

	public void setWorkflowID(Integer workflowID) {
		this.workflowID = workflowID;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public Integer getWorkflowPathID() {
		return workflowPathID;
	}

	public void setWorkflowPathID(Integer workflowPathID) {
		this.workflowPathID = workflowPathID;
	}

	public Integer getWorkflowLinesID() {
		return workflowLinesID;
	}

	public void setWorkflowLinesID(Integer workflowLinesID) {
		this.workflowLinesID = workflowLinesID;
	}

	public Integer getIdPararelLine() {
		return IdPararelLine;
	}

	public void setIdPararelLine(Integer idPararelLine) {
		IdPararelLine = idPararelLine;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssignGroup() {
		return assignGroup;
	}

	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	public String getDescLines() {
		return descLines;
	}

	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	public String getDescAssignment() {
		return descAssignment;
	}

	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}

	public String getInsertBy() {
		return insertBy;
	}

	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}

	public Date getDate_() {
		return date_;
	}

	public void setDate_(Date date_) {
		this.date_ = date_;
	}

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}
}
