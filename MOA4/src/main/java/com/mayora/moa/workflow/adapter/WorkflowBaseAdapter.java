package com.mayora.moa.workflow.adapter;

import java.io.Serializable;

import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.google.gson.Gson;

public class WorkflowBaseAdapter {

	@Autowired  
	@Qualifier("sfWF")  
	protected SessionFactory sfWF;       
	
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sfWF = sessionFactory;
    }
	
	
	
    public boolean addObject(Object object) {
        Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.save(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
        
    }
    
    public boolean deleteObject(Object object, String docNo) {
    	Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.load(object, docNo);
	        session.delete(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
  
    }
    
    
    public boolean updateObject(Object object) {
    	Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.update(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }
    
    public boolean saveupdateObject(Object object) {
    	Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.saveOrUpdate(object);
	        session.getTransaction().commit();
	        session.close();
	        
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }
    
    public boolean saveupdateObject(Object object, Serializable id) {
    	Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.load(object, id);
	        session.saveOrUpdate(object);
	        session.getTransaction().commit();
	        session.close();
	        
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }
    
    public boolean insertOrUpdateObject(Object object) {
    	Session session = sfWF.openSession();
        try {
	        session.beginTransaction();
	        session.saveOrUpdate(object);
	        session.getTransaction().commit();
	        session.close();
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }
}
