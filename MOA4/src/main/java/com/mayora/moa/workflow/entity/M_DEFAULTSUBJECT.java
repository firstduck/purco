package com.mayora.moa.workflow.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_DEFAULTSUBJECT")

public class M_DEFAULTSUBJECT {
	
	@EmbeddedId
	private M_DEFAULTSUBJECT_PK id;
	
	@Column(name="SUBJECT")
	private String subject;

	@Transient
	private String activityId;
	
	public String getActivityId(){
		return id.getActivity();
	}
	
	/**
	 * @return the id
	 */
	public M_DEFAULTSUBJECT_PK getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(M_DEFAULTSUBJECT_PK id) {
		this.id = id;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	

}
