package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class V_TransferGroupWorkflow_PK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="ID")
	private Integer id;

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
