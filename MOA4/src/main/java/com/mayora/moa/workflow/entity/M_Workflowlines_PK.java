package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_Workflowlines_PK implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	@Column(name="WORKFLOWPATHID")
	private String workflowPathId;
	@Column(name="WORKFLOWLINESID")
	private String workflowLinesId;
	@Column(name="IDPARARELLINE")
	private String idPararelLine;
	
	/**
	 * @return the workflowCode
	 */
	public String getWorkflowCode() {
		return workflowCode;
	}
	/**
	 * @param workflowCode the workflowCode to set
	 */
	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}
	/**
	 * @return the workflowPathId
	 */
	public String getWorkflowPathId() {
		return workflowPathId;
	}
	/**
	 * @param workflowPathId the workflowPathId to set
	 */
	public void setWorkflowPathId(String workflowPathId) {
		this.workflowPathId = workflowPathId;
	}
	/**
	 * @return the workflowLinesId
	 */
	public String getWorkflowLinesId() {
		return workflowLinesId;
	}
	/**
	 * @param workflowLinesId the workflowLinesId to set
	 */
	public void setWorkflowLinesId(String workflowLinesId) {
		this.workflowLinesId = workflowLinesId;
	}
	/**
	 * @return the idPararelLine
	 */
	public String getIdPararelLine() {
		return idPararelLine;
	}
	/**
	 * @param idPararelLine the idPararelLine to set
	 */
	public void setIdPararelLine(String idPararelLine) {
		this.idPararelLine = idPararelLine;
	}
	
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
	
	
	
}
