package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.T_BATCHWORKFLOW")
public class T_BatchWorkflow {

	@Id
	@Column(name="BATCHNO")
	private String batchNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="GROUPAPPROVAL")
	private Boolean groupApproval = false;

	public Boolean getGroupApproval() {
		return groupApproval;
	}

	public void setGroupApproval(Boolean groupApproval) {
		this.groupApproval = groupApproval;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
}
