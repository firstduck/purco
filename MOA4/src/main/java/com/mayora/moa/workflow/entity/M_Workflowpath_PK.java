package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_Workflowpath_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="WORKFLOWPATHID")
	private String workflowPathId;
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	@Column(name="ID")
	private String id;
	/**
	 * @return the workflowPathId
	 */
	public String getWorkflowPathId() {
		return workflowPathId;
	}
	/**
	 * @param workflowPathId the workflowPathId to set
	 */
	public void setWorkflowPathId(String workflowPathId) {
		this.workflowPathId = workflowPathId;
	}
	/**
	 * @return the workflowCode
	 */
	public String getWorkflowCode() {
		return workflowCode;
	}
	/**
	 * @param workflowCode the workflowCode to set
	 */
	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
