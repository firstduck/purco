package com.mayora.moa.workflow.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_SELECTEDWORKFLOW")
public class V_SelectedWorkflow {
	
	@EmbeddedId
	private V_SelectedWorkflow_PK pk;

	public V_SelectedWorkflow_PK getPk() {
		return pk;
	}

	public void setPk(V_SelectedWorkflow_PK pk) {
		this.pk = pk;
	}

	@Column(name="INITIALASSIGNMENT")
	private Boolean initialAssignment;
	
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="ASSIGNEDPIC")
	private String assignedPIC;
	
	@Column(name="DATE_")
	private Timestamp date_;
	
	@Column(name="INSERTBY")
	private String insertBy;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="SUPERIOR")
	private String superior;
	
	@Column(name="NEEDREMINDER")
	private Boolean needReminder;
	
	@Column(name="REMINDERDURATION")
	private Integer reminderDuration;
	
	@Column(name="NEEDCCSUPERIOR")
	private Boolean needCCSuperior;
	
	@Column(name="CCLIST")
	private String ccList;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="ACTIVE")
	private Integer active;
	
	@Column(name="WORKFLOWNAME")
	private String workflowName;
	
	@Column(name="CLOSEDATE")
	private Timestamp closeDate;
	
	@Column(name="CLOSEBY")
	private String closeBy;
	
	@Column(name="REJECTDATE")
	private Timestamp rejectDate;
	
	@Column(name="REJECTBY")
	private String rejectBy;
	
	@Column(name="NEEDASSIGNMENT")
	private Boolean needAssignment;

	public Boolean getInitialAssignment() {
		return initialAssignment;
	}

	public void setInitialAssignment(Boolean initialAssignment) {
		this.initialAssignment = initialAssignment;
	}

	public String getDescAssignment() {
		return descAssignment;
	}

	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssignGroup() {
		return assignGroup;
	}

	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	public String getAssignedPIC() {
		return assignedPIC;
	}

	public void setAssignedPIC(String assignedPIC) {
		this.assignedPIC = assignedPIC;
	}

	public Timestamp getDate_() {
		return date_;
	}

	public void setDate_(Timestamp date_) {
		this.date_ = date_;
	}

	public String getInsertBy() {
		return insertBy;
	}

	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public Boolean getNeedReminder() {
		return needReminder;
	}

	public void setNeedReminder(Boolean needReminder) {
		this.needReminder = needReminder;
	}

	public Integer getReminderDuration() {
		return reminderDuration;
	}

	public void setReminderDuration(Integer reminderDuration) {
		this.reminderDuration = reminderDuration;
	}

	public Boolean getNeedCCSuperior() {
		return needCCSuperior;
	}

	public void setNeedCCSuperior(Boolean needCCSuperior) {
		this.needCCSuperior = needCCSuperior;
	}

	public String getCcList() {
		return ccList;
	}

	public void setCcList(String ccList) {
		this.ccList = ccList;
	}

	public String getDescLines() {
		return descLines;
	}

	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Timestamp getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Timestamp closeDate) {
		this.closeDate = closeDate;
	}

	public String getCloseBy() {
		return closeBy;
	}

	public void setCloseBy(String closeBy) {
		this.closeBy = closeBy;
	}

	public Timestamp getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Timestamp rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectBy() {
		return rejectBy;
	}

	public void setRejectBy(String rejectBy) {
		this.rejectBy = rejectBy;
	}

	public Boolean getNeedAssignment() {
		return needAssignment;
	}

	public void setNeedAssignment(Boolean needAssignment) {
		this.needAssignment = needAssignment;
	}

}
