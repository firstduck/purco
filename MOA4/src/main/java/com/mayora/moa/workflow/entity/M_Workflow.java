package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_WORKFLOW")
public class M_Workflow {
	
	@Column(name="WORKFLOWNAME")
	private String workflowName;
	
	@EmbeddedId
	private M_Workflow_PK id;

	@Transient
	private String idWorkflow;
	@Transient
	private String codeWorkflow;
	
	public String getDummyIdInsert() {
		return idWorkflow;
	}
	public String getDummyCodeInsert() {
		return codeWorkflow;
	}
	
	public String getIdWorkflow() {
		return id.getWorkflowId();
	}
	public String getCodeWorkflow() {
		return id.getWorkflowCode();
	}
	
	
	

	/**
	 * @return the id
	 */
	public M_Workflow_PK getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(M_Workflow_PK id) {
		this.id = id;
	}
	/**
	 * @return the workflowName
	 */
	public String getWorkflowName() {
		return workflowName;
	}
	/**
	 * @param workflowName the workflowName to set
	 */
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}
	/**
	 * @param idWorkflow the idWorkflow to set
	 */
	public void setIdWorkflow(String idWorkflow) {
		this.idWorkflow = idWorkflow;
	}
	/**
	 * @param codeWorkflow the codeWorkflow to set
	 */
	public void setCodeWorkflow(String codeWorkflow) {
		this.codeWorkflow = codeWorkflow;
	}
	
}
