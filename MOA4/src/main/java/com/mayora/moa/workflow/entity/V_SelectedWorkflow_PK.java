package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_SelectedWorkflow_PK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getWorkflowID() {
		return workflowID;
	}

	public void setWorkflowID(Integer workflowID) {
		this.workflowID = workflowID;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public Integer getWorkflowPathID() {
		return workflowPathID;
	}

	public void setWorkflowPathID(Integer workflowPathID) {
		this.workflowPathID = workflowPathID;
	}

	public Integer getWorkflowLinesID() {
		return workflowLinesID;
	}

	public void setWorkflowLinesID(Integer workflowLinesID) {
		this.workflowLinesID = workflowLinesID;
	}

	public Integer getIDPararelLine() {
		return IDPararelLine;
	}

	public void setIDPararelLine(Integer iDPararelLine) {
		IDPararelLine = iDPararelLine;
	}

	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="WORKFLOWID")
	private Integer workflowID;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathID;
	
	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesID;
	
	@Column(name="IDPARARELLINE")
	private Integer IDPararelLine;
}
