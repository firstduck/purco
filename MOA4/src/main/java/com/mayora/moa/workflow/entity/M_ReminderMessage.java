package com.mayora.moa.workflow.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_REMINDERMESSAGE")

public class M_ReminderMessage {
	
	@EmbeddedId
	private M_ReminderMessage_PK id;
	
	@Column(name="FIELDTOSHOW")
	private String fieldToShow;
	
	@Column(name="FIELDALIAS")
	private String fieldAlias;
	
	@Column(name="HEADER")
	private String header;
	
	@Column(name="FOOTER")
	private String footer;
	
	@Column(name="SHOWINEMAIL")
	private Boolean showInEmail;
	
	@Transient
	private Integer idDummy;
	
	public void setLinesid(Integer lines){
		id.setWorkflowLinesId(lines);
	}

	public Integer getIdDummyInsert() {
		return idDummy;
	}

	public Integer getIdDummy() {
		return id.getId();
	}
	
	
	/**
	 * @return the id
	 */
	public M_ReminderMessage_PK getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(M_ReminderMessage_PK id) {
		this.id = id;
	}

	/**
	 * @return the fieldToShow
	 */
	public String getFieldToShow() {
		return fieldToShow;
	}

	/**
	 * @param fieldToShow the fieldToShow to set
	 */
	public void setFieldToShow(String fieldToShow) {
		this.fieldToShow = fieldToShow;
	}

	/**
	 * @return the fieldAlias
	 */
	public String getFieldAlias() {
		return fieldAlias;
	}

	/**
	 * @param fieldAlias the fieldAlias to set
	 */
	public void setFieldAlias(String fieldAlias) {
		this.fieldAlias = fieldAlias;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * @return the footer
	 */
	public String getFooter() {
		return footer;
	}

	/**
	 * @param footer the footer to set
	 */
	public void setFooter(String footer) {
		this.footer = footer;
	}

	/**
	 * @return the showInEmail
	 */
	public Boolean getShowInEmail() {
		return showInEmail;
	}

	/**
	 * @param showInEmail the showInEmail to set
	 */
	public void setShowInEmail(Boolean showInEmail) {
		this.showInEmail = showInEmail;
	}

	

}
