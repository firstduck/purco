package com.mayora.moa.workflow.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_DETAILCURRENTWORKFLOWLINES")
public class V_DetailCurrentWorkflowLines {
	
	@EmbeddedId
	private V_DetailCurrentWorkflowLines_PK pk;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="ASSIGNEDPIC")
	private String assignedPIC;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="NEEDREMINDER")
	private Boolean needReminder;
	
	@Column(name="REMINDERDURATION")
	private Integer reminderDuration;
	
	@Column(name="NEEDCCSUPERIOR")
	private Boolean needCCsuperior;
	
	@Column(name="CCLIST")
	private String ccList;
	
	@Column(name="SUPERIOR")
	private String superior;
	
	@Column(name="ACTIVE")
	private Integer active;
	
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	
	@Column(name="INSERTBY")
	private String insertBy;
	
	@Column(name="DATE_")
	private Date date_;
	
	@Column(name="WEBNAME")
	private String webName;
	
	@Column(name="TAG")
	private String tag;
	
	
	

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public V_DetailCurrentWorkflowLines_PK getPk() {
		return pk;
	}

	public void setPk(V_DetailCurrentWorkflowLines_PK pk) {
		this.pk = pk;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssignGroup() {
		return assignGroup;
	}

	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	public String getDescLines() {
		return descLines;
	}

	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	public String getDescAssignment() {
		return descAssignment;
	}

	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}

	public String getAssignedPIC() {
		return assignedPIC;
	}

	public void setAssignedPIC(String assignedPIC) {
		this.assignedPIC = assignedPIC;
	}

	public Boolean getNeedReminder() {
		return needReminder;
	}

	public void setNeedReminder(Boolean needReminder) {
		this.needReminder = needReminder;
	}

	public Integer getReminderDuration() {
		return reminderDuration;
	}

	public void setReminderDuration(Integer reminderDuration) {
		this.reminderDuration = reminderDuration;
	}

	public Boolean getNeedCCsuperior() {
		return needCCsuperior;
	}

	public void setNeedCCsuperior(Boolean needCCsuperior) {
		this.needCCsuperior = needCCsuperior;
	}

	public String getCcList() {
		return ccList;
	}

	public void setCcList(String ccList) {
		this.ccList = ccList;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getInsertBy() {
		return insertBy;
	}

	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}

	public Date getDate_() {
		return date_;
	}

	public void setDate_(Date date_) {
		this.date_ = date_;
	}

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
