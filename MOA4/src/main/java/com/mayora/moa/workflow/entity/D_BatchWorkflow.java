package com.mayora.moa.workflow.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.D_BATCHWORKFLOW")
public class D_BatchWorkflow {

	@EmbeddedId
	private D_BatchWorkflow_PK pk;

	public D_BatchWorkflow_PK getPk() {
		return pk;
	}

	public void setPk(D_BatchWorkflow_PK pk) {
		this.pk = pk;
	}
}
