package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.M_AUTOASSIGNPATH")

public class M_AutoAssignPath {
	
	@EmbeddedId
	private M_AutoAssignPath_PK pk;
	
	@Transient
	private String dummyId;
	
	@Transient
	private String dummyPath;
	
	@Transient
	private String dummyModule;
	
	@Transient
	private String dummyWorkflow;
	
	
	public String getDummyId() {
		return dummyId;
	}
	public String getDummyPath() {
		return dummyPath;
	}
	public String getDummyModule() {
		return dummyModule;
	}
	public String getDummyWorkflow() {
		return dummyWorkflow;
	}

	
	public void setDummyId(String dummyId) {
		this.dummyId = dummyId;
	}
	public void setDummyPath(String dummyPath) {
		this.dummyPath = dummyPath;
	}
	public void setDummyModule(String dummyModule) {
		this.dummyModule = dummyModule;
	}
	public void setDummyWorkflow(String dummyWorkflow) {
		this.dummyWorkflow = dummyWorkflow;
	}


	@Column(name="FIELD")
	private String field;
	
	@Column(name="OPERATOR")
	private String operator;
	
	@Column(name="VALUE")
	private String value;
	
	@Column(name="CONNECTOR")
	private String connector;
	
	@Column(name="DESCRIPTION")
	private String description;

	public M_AutoAssignPath_PK getPk() {
		return pk;
	}

	public void setPk(M_AutoAssignPath_PK pk) {
		this.pk = pk;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	} 
	
}
