package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRWF.M_GROUPWORKFLOW")
public class M_GroupWorkflow {
	
	@Id
	@NotBlank(message="Please Input Code")
	@Column(name="GROUPWORKFLOWCODE")
	private String groupCode;
	
	@Column(name="GROUPWORKFLOWDESC")
	private String description;
	
	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
