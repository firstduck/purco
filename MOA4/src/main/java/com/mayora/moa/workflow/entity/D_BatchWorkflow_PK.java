package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class D_BatchWorkflow_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="BATCHNO")
	private String batchNo;
	
	@Column(name="DOCNO")
	private String docNo;
	
	public D_BatchWorkflow_PK(){
		
	}
	
	public D_BatchWorkflow_PK(String batchNo, String docNo){
		this.batchNo = batchNo;
		this.docNo = docNo;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
}
