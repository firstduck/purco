package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class T_SelectedWorkflow_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	
	@Column(name="DOCNO")
	private String docNo;

	@Column(name="MODULECODE")
	private String moduleCode;

	@Column(name="WORKFLOWID")
	private String workflowId;
	
	@Column(name="IDPARARELLINE")
	private String idPararelLine;

	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWPATHID")
	private String workflowPathId;

	@Column(name="WORKFLOWLINESID")
	private String workflowLinesId;

	/**
	 * @return the docNo
	 */
	public String getDocNo() {
		return docNo;
	}

	/**
	 * @param docNo the docNo to set
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	/**
	 * @return the moduleCode
	 */
	public String getModuleCode() {
		return moduleCode;
	}

	/**
	 * @param moduleCode the moduleCode to set
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**
	 * @return the workflowId
	 */
	public String getWorkflowId() {
		return workflowId;
	}

	/**
	 * @param workflowId the workflowId to set
	 */
	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	/**
	 * @return the idPararelLine
	 */
	public String getIdPararelLine() {
		return idPararelLine;
	}

	/**
	 * @param idPararelLine the idPararelLine to set
	 */
	public void setIdPararelLine(String idPararelLine) {
		this.idPararelLine = idPararelLine;
	}

	/**
	 * @return the workflowCode
	 */
	public String getWorkflowCode() {
		return workflowCode;
	}

	/**
	 * @param workflowCode the workflowCode to set
	 */
	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	/**
	 * @return the workflowPathId
	 */
	public String getWorkflowPathId() {
		return workflowPathId;
	}

	/**
	 * @param workflowPathId the workflowPathId to set
	 */
	public void setWorkflowPathId(String workflowPathId) {
		this.workflowPathId = workflowPathId;
	}

	/**
	 * @return the workflowLinesId
	 */
	public String getWorkflowLinesId() {
		return workflowLinesId;
	}

	/**
	 * @param workflowLinesId the workflowLinesId to set
	 */
	public void setWorkflowLinesId(String workflowLinesId) {
		this.workflowLinesId = workflowLinesId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	
	
	

}
