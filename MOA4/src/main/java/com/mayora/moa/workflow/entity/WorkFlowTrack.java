package com.mayora.moa.workflow.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.T_TRACK") 
public class WorkFlowTrack {
	
	@Id
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="DATE_")
	private Date date_; 

	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ACTIVITY")
	private String activity;
	  
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="BACKUPDOCNO")
	private String backupDocNo;
	
	@Column(name="DELEGATEDBY")
	private String delegatedBy;
	
	@Column(name="REASON")
	private String reason;
	
	@Column(name="CANCELLED")
	private Integer cancelled;

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public Date getDate_() {
		return date_;
	}

	public void setDate_(Date date_) {
		this.date_ = date_;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getBackupDocNo() {
		return backupDocNo;
	}

	public void setBackupDocNo(String backupDocNo) {
		this.backupDocNo = backupDocNo;
	}

	public String getDelegatedBy() {
		return delegatedBy;
	}

	public void setDelegatedBy(String delegatedBy) {
		this.delegatedBy = delegatedBy;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getCancelled() {
		return cancelled;
	}

	public void setCancelled(Integer cancelled) {
		this.cancelled = cancelled;
	}
}

