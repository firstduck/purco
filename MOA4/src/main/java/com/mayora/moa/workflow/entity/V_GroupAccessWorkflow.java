package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.V_GROUPACCESSWORKFLOW")
public class V_GroupAccessWorkflow {

	@EmbeddedId
	private V_GroupAccessWorkflow_PK pk;
	
	@Column(name="GROUPWORKFLOWNAME")
	private String groupWorkflowName;

	@Column(name="NAME")
	private String name;
	
	@Column(name="SUPERIOR_USER_NAME")
	private String superiorUsername;
	
	@Column(name="SUPERIOR_NAME")
	private String superiorName;
	
	@Column(name="ACTIVE")
	private Boolean isActive;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="EMAILSUPERIOR")
	private String emailSuperior;
	
	@Column(name="DEFAULTVALUE")
	private Boolean defaultValue;
	
	@Column(name="DEFAULTSUPERIOR")
	private Boolean defaultSuperior;
	
	@Column(name="SUBGROUPWORKFLOW")
	private String subGroupWorkflow;
	
	
	@Transient
	private String dummyUsername;
	@Transient
	private String dummyGroupWorkflowCode;
	
	public String getDummyUsername() {
		return pk.getUsername();
	}
	public String getDummyGroupWorkflowCode() {
		return pk.getGroupWorkflowCode();
	}

	public void setDummyUsername(String dummyUsername) {
		this.dummyUsername = dummyUsername;
	}

	public V_GroupAccessWorkflow_PK getPk() {
		return pk;
	}

	public void setPk(V_GroupAccessWorkflow_PK pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSuperiorUsername() {
		return superiorUsername;
	}

	public void setSuperiorUsername(String superiorUsername) {
		this.superiorUsername = superiorUsername;
	}

	public String getSuperiorName() {
		return superiorName;
	}

	public void setSuperiorName(String superiorName) {
		this.superiorName = superiorName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailSuperior() {
		return emailSuperior;
	}

	public void setEmailSuperior(String emailSuperior) {
		this.emailSuperior = emailSuperior;
	}

	public Boolean getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getDefaultSuperior() {
		return defaultSuperior;
	}

	public void setDefaultSuperior(Boolean defaultSuperior) {
		this.defaultSuperior = defaultSuperior;
	}

	/**
	 * @return the groupWorkflowName
	 */
	public String getGroupWorkflowName() {
		return groupWorkflowName;
	}

	/**
	 * @param groupWorkflowName the groupWorkflowName to set
	 */
	public void setGroupWorkflowName(String groupWorkflowName) {
		this.groupWorkflowName = groupWorkflowName;
	}
	public String getSubGroupWorkflow() {
		return subGroupWorkflow;
	}
	public void setSubGroupWorkflow(String subGroupWorkflow) {
		this.subGroupWorkflow = subGroupWorkflow;
	}
	
	
}
