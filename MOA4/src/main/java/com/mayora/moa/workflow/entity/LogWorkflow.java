package com.mayora.moa.workflow.entity;

public class LogWorkflow {

	private String log;
	private String status;
	
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
