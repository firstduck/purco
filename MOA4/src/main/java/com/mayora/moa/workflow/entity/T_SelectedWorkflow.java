package com.mayora.moa.workflow.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.T_SELECTEDWORKFLOW")

public class T_SelectedWorkflow {
	
	@EmbeddedId
	private T_SelectedWorkflow_PK id;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="ASSIGNGROUP")
	private String assignGroup;
	
	@Column(name="ASSIGNEDPIC")
	private String assignedPic;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="DATE_")
	private Date date;
	@Column(name="INSERTBY")
	private String insertBy;
	@Column(name="NEEDREMINDER")
	private String needReminder;
	@Column(name="REMINDERDURATION")
	private String reminderDuration;
	@Column(name="NEEDCCSUPERIOR")
	private String needCcSuperior;
	@Column(name="WORKFLOWNAME")
	private String workflowName;
	@Column(name="NEEDASSIGNMENT")
	private String needAssignment;
	@Column(name="INITIALASSIGNMENT")
	private String initialAssignment;
	@Column(name="DESCASSIGNMENT")
	private String descAssignment;
	@Column(name="CLOSEDATE")
	private Date closeDate;
	@Column(name="CLOSEBY")
	private String closeBy;
	@Column(name="REJECTDATE")
	private Date rejectDate;
	@Column(name="REJECTBY")
	private String rejectBy;
	@Column(name="TAG")
	private String tag;
	@Transient
	private String codeModule;
	@Transient
	private String idWorkflow;
	@Transient
	private String pararelLineId;
	@Transient
	private String codeWorkflow;
	@Transient
	private String pathWorkflowId;
	@Transient
	private String linesWorkflowId;
	
	@Transient
	private String dummyId;
	
	public String getDummyId() {
		return id.getWorkflowId() 
				+ " " + id.getWorkflowLinesId()
				+ " "+ id.getIdPararelLine()
				+ " "+ id.getWorkflowPathId();
	}
	
	public String getCodeModule() {
		return id.getModuleCode();
	}
	public String getIdWorkflow() {
		return id.getWorkflowId();
	}
	public String getPararelLineId() {
		return id.getIdPararelLine();
	}
	public String getCodeWorkflow() {
		return id.getWorkflowCode();
	}
	public String getPathWorkflowId() {
		return id.getWorkflowPathId();
	}
	public String getLinesWorkflowId() {
		return id.getWorkflowLinesId();
	}
	
	
	/**
	 * @return the id
	 */
	public T_SelectedWorkflow_PK getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(T_SelectedWorkflow_PK id) {
		this.id = id;
	}

	/**
	 * @return the groupWorkflowCode
	 */
	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	/**
	 * @param groupWorkflowCode the groupWorkflowCode to set
	 */
	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	/**
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * @param activity the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}

	/**
	 * @return the assignGroup
	 */
	public String getAssignGroup() {
		return assignGroup;
	}

	/**
	 * @param assignGroup the assignGroup to set
	 */
	public void setAssignGroup(String assignGroup) {
		this.assignGroup = assignGroup;
	}

	/**
	 * @return the assignedPic
	 */
	public String getAssignedPic() {
		return assignedPic;
	}

	/**
	 * @param assignedPic the assignedPic to set
	 */
	public void setAssignedPic(String assignedPic) {
		this.assignedPic = assignedPic;
	}

	/**
	 * @return the descLines
	 */
	public String getDescLines() {
		return descLines;
	}

	/**
	 * @param descLines the descLines to set
	 */
	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	/**
	 * @return the codeModule
	 */
	public String getCodeModuleInsert() {
		return codeModule;
	}

	/**
	 * @return the idWorkflow
	 */
	public String getIdWorkflowInsert() {
		return idWorkflow;
	}

	/**
	 * @return the pararelLineId
	 */
	public String getPararelLineIdInsert() {
		return pararelLineId;
	}

	/**
	 * @return the codeWorkflow
	 */
	public String getCodeWorkflowInsert() {
		return codeWorkflow;
	}

	/**
	 * @return the pathWorkflowId
	 */
	public String getPathWorkflowIdInsert() {
		return pathWorkflowId;
	}

	/**
	 * @return the linesWorkflowId
	 */
	public String getLinesWorkflowIdInsert() {
		return linesWorkflowId;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the insertBy
	 */
	public String getInsertBy() {
		return insertBy;
	}
	/**
	 * @param insertBy the insertBy to set
	 */
	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}
	/**
	 * @return the needReminder
	 */
	public String getNeedReminder() {
		return needReminder;
	}
	/**
	 * @param needReminder the needReminder to set
	 */
	public void setNeedReminder(String needReminder) {
		this.needReminder = needReminder;
	}
	/**
	 * @return the reminderDuration
	 */
	public String getReminderDuration() {
		return reminderDuration;
	}
	/**
	 * @param reminderDuration the reminderDuration to set
	 */
	public void setReminderDuration(String reminderDuration) {
		this.reminderDuration = reminderDuration;
	}
	/**
	 * @return the needCcSuperior
	 */
	public String getNeedCcSuperior() {
		return needCcSuperior;
	}
	/**
	 * @param needCcSuperior the needCcSuperior to set
	 */
	public void setNeedCcSuperior(String needCcSuperior) {
		this.needCcSuperior = needCcSuperior;
	}
	/**
	 * @return the workflowName
	 */
	public String getWorkflowName() {
		return workflowName;
	}
	/**
	 * @param workflowName the workflowName to set
	 */
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}
	/**
	 * @return the needAssignment
	 */
	public String getNeedAssignment() {
		return needAssignment;
	}
	/**
	 * @param needAssignment the needAssignment to set
	 */
	public void setNeedAssignment(String needAssignment) {
		this.needAssignment = needAssignment;
	}
	/**
	 * @return the initialAssignment
	 */
	public String getInitialAssignment() {
		return initialAssignment;
	}
	/**
	 * @param initialAssignment the initialAssignment to set
	 */
	public void setInitialAssignment(String initialAssignment) {
		this.initialAssignment = initialAssignment;
	}
	/**
	 * @return the descAssignment
	 */
	public String getDescAssignment() {
		return descAssignment;
	}
	/**
	 * @param descAssignment the descAssignment to set
	 */
	public void setDescAssignment(String descAssignment) {
		this.descAssignment = descAssignment;
	}
	/**
	 * @return the closeDate
	 */
	public Date getCloseDate() {
		return closeDate;
	}
	/**
	 * @param closeDate the closeDate to set
	 */
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}
	/**
	 * @return the closeBy
	 */
	public String getCloseBy() {
		return closeBy;
	}
	/**
	 * @param closeBy the closeBy to set
	 */
	public void setCloseBy(String closeBy) {
		this.closeBy = closeBy;
	}
	/**
	 * @return the rejectDate
	 */
	public Date getRejectDate() {
		return rejectDate;
	}
	/**
	 * @param rejectDate the rejectDate to set
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}
	/**
	 * @return the rejectBy
	 */
	public String getRejectBy() {
		return rejectBy;
	}
	/**
	 * @param rejectBy the rejectBy to set
	 */
	public void setRejectBy(String rejectBy) {
		this.rejectBy = rejectBy;
	}
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * @return the docNoSap
	 */
	
	/**
	 * @param docNoSap the docNoSap to set
	 */
	
	/**
	 * @param codeModule the codeModule to set
	 */
	public void setCodeModule(String codeModule) {
		this.codeModule = codeModule;
	}
	/**
	 * @param idWorkflow the idWorkflow to set
	 */
	public void setIdWorkflow(String idWorkflow) {
		this.idWorkflow = idWorkflow;
	}
	/**
	 * @param pararelLineId the pararelLineId to set
	 */
	public void setPararelLineId(String pararelLineId) {
		this.pararelLineId = pararelLineId;
	}
	/**
	 * @param codeWorkflow the codeWorkflow to set
	 */
	public void setCodeWorkflow(String codeWorkflow) {
		this.codeWorkflow = codeWorkflow;
	}
	/**
	 * @param pathWorkflowId the pathWorkflowId to set
	 */
	public void setPathWorkflowId(String pathWorkflowId) {
		this.pathWorkflowId = pathWorkflowId;
	}
	/**
	 * @param linesWorkflowId the linesWorkflowId to set
	 */
	public void setLinesWorkflowId(String linesWorkflowId) {
		this.linesWorkflowId = linesWorkflowId;
	}

	
	
	

}
