package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_GROUPACCESSWORKFLOWWITHOPTIO")

public class V_GroupAccessWorkFlowWithOptio {

	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkFlowCode;
	
	@Id
	@Column(name="USER_NAME")
	private String userName;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="SUPERIOR_USER_NAME")
	private String superiorUserName;
	
	@Column(name="SUPERIOR_NAME")
	private String superiorName;
	
	@Column(name="ACTIVE")
	private Integer active;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="EMAILSUPERIOR")
	private String emailSuperior;
	
	@Column(name="DEFAULTVALUE")
	private Integer defaultValue;
	
	@Column(name="DEFAULTSUPERIOR")
	private Integer defaultSuperior;
	
	public String getGroupWorkFlowCode() {
		return groupWorkFlowCode;
	}

	public void setGroupWorkFlowCode(String groupWorkFlowCode) {
		this.groupWorkFlowCode = groupWorkFlowCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSuperiorUserName() {
		return superiorUserName;
	}

	public void setSuperiorUserName(String superiorUserName) {
		this.superiorUserName = superiorUserName;
	}

	public String getSuperiorName() {
		return superiorName;
	}

	public void setSuperiorName(String superiorName) {
		this.superiorName = superiorName;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailSuperior() {
		return emailSuperior;
	}

	public void setEmailSuperior(String emailSuperior) {
		this.emailSuperior = emailSuperior;
	}

	public Integer getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Integer defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getDefaultSuperior() {
		return defaultSuperior;
	}

	public void setDefaultSuperior(Integer defaultSuperior) {
		this.defaultSuperior = defaultSuperior;
	}


}
