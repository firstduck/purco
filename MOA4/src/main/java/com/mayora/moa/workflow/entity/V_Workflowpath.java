package com.mayora.moa.workflow.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRWF.V_WORKFLOWPATH")
public class V_Workflowpath {
	
	@Column(name="WORKFLOWPATHDESC")
	private String workflowPathDesc;

	@EmbeddedId
	private V_Workflowpath_PK pk;

	public String getWorkflowPathDesc() {
		return workflowPathDesc;
	}

	public void setWorkflowPathDesc(String workflowPathDesc) {
		this.workflowPathDesc = workflowPathDesc;
	}

	public V_Workflowpath_PK getPk() {
		return pk;
	}

	public void setPk(V_Workflowpath_PK pk) {
		this.pk = pk;
	}
	
}
