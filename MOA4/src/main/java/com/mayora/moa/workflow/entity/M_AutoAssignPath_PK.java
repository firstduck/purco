package com.mayora.moa.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
 
@Embeddable
public class M_AutoAssignPath_PK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="AUTOASSIGNPATHID")
	private String autoAssignPathID;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="WORKFLOWCODE")
	private String workFlowCode;
	
	@Column(name="ID")
	private String id;

	public String getAutoAssignPathID() {
		return autoAssignPathID;
	}

	public void setAutoAssignPathID(String autoAssignPathID) {
		this.autoAssignPathID = autoAssignPathID;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getWorkFlowCode() {
		return workFlowCode;
	}

	public void setWorkFlowCode(String workFlowCode) {
		this.workFlowCode = workFlowCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
