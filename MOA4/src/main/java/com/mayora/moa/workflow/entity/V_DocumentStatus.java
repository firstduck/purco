package com.mayora.moa.workflow.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRWF.V_DOCUMENTSTATUS")
public class V_DocumentStatus {
	
	@EmbeddedId
	private V_DocumentStatus_PK pk;
	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="WORKFLOWID")
	private Integer workflowID;
	
	@Column(name="WORKFLOWCODE")
	private String workflowCode;
	
	@Column(name="WORKFLOWNAME")
	private String workflowName;
	
	@Column(name="WORKFLOWPATHID")
	private Integer workflowPathId;
	
	@Column(name="WORKFLOWLINESID")
	private Integer workflowLinesId;
	
	@Column(name="IDPARARELLINE")
	private Integer idPararelLine;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="GROUPWORKFLOWDESC")
	private String groupWorkflowDesc;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="CREATEDBY")
	private String createdBy;
	
	@Column(name="PIC")
	private String pic;
	
	@Column(name="DESCLINES")
	private String descLines;
	
	@Column(name="CLOSEDATE")
	private Timestamp closeDate;
	
	@Column(name="CLOSEBYUSERNAME")
	private String closeByUsername;
	
	@Column(name="CLOSEBY")
	private String closeBy;
	
	@Column(name="REJECTDATE")
	private Timestamp rejectDate;
	
	@Column(name="REJECTBYUSERNAME")
	private String rejectByUsername;
	
	@Column(name="REJECTBY")
	private String rejectBy;

	@Transient
	private String documentStatusID;

	@Transient
	private String username;
	
	public V_DocumentStatus_PK getPk() {
		return pk;
	}

	public void setPk(V_DocumentStatus_PK pk) {
		this.pk = pk;
	}

	public String getDocumentStatusID() {
		return this.pk.getDocumentStatusID();
	}

	public void setDocumentStatusID(String documentStatusID) {
		try 
		{
			this.pk.setDocumentStatusID(documentStatusID);
		}
		catch(Exception e)
		{
			this.setPk(new V_DocumentStatus_PK());
			this.pk.setDocumentStatusID(documentStatusID);
		}
	}

	public String getUsername() {
		return this.pk.getUsername();
	}

	public void setUsername(String username) {
		try 
		{
			this.pk.setUsername(username);
		}
		catch(Exception e)
		{
			this.setPk(new V_DocumentStatus_PK());
			this.pk.setUsername(username);
		}
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getWorkflowID() {
		return workflowID;
	}

	public void setWorkflowID(Integer workflowID) {
		this.workflowID = workflowID;
	}

	public String getWorkflowCode() {
		return workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Integer getWorkflowPathId() {
		return workflowPathId;
	}

	public void setWorkflowPathId(Integer workflowPathId) {
		this.workflowPathId = workflowPathId;
	}

	public Integer getWorkflowLinesId() {
		return workflowLinesId;
	}

	public void setWorkflowLinesId(Integer workflowLinesId) {
		this.workflowLinesId = workflowLinesId;
	}

	public Integer getIdPararelLine() {
		return idPararelLine;
	}

	public void setIdPararelLine(Integer idPararelLine) {
		this.idPararelLine = idPararelLine;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getDescLines() {
		return descLines;
	}

	public void setDescLines(String descLines) {
		this.descLines = descLines;
	}

	public Timestamp getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Timestamp closeDate) {
		this.closeDate = closeDate;
	}

	public String getCloseByUsername() {
		return closeByUsername;
	}

	public void setCloseByUsername(String closeByUsername) {
		this.closeByUsername = closeByUsername;
	}

	public String getCloseBy() {
		return closeBy;
	}

	public void setCloseBy(String closeBy) {
		this.closeBy = closeBy;
	}

	public Timestamp getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Timestamp rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectByUsername() {
		return rejectByUsername;
	}

	public void setRejectByUsername(String rejectByUsername) {
		this.rejectByUsername = rejectByUsername;
	}

	public String getRejectBy() {
		return rejectBy;
	}

	public void setRejectBy(String rejectBy) {
		this.rejectBy = rejectBy;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getGroupWorkflowDesc() {
		return groupWorkflowDesc;
	}

	public void setGroupWorkflowDesc(String groupWorkflowDesc) {
		this.groupWorkflowDesc = groupWorkflowDesc;
	}	
}