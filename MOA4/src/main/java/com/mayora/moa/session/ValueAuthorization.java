package com.mayora.moa.session;

public class ValueAuthorization {
	
	private String moduleCode; 
	
	private String code; 
	
	private String value;
	
	private Boolean selected;

	public String getValue() {
		return value;
	}

	public void setValue(String string) {
		this.value = string;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}