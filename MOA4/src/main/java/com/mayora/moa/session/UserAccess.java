package com.mayora.moa.session;

import java.util.List;

import com.mayora.masterdata.entity.M_TempUser;
import com.mayora.masterdata.entity.MenuAccessMvc;
import com.mayora.masterdata.entity.User;

public class UserAccess {
	private String DelegatedBy;
	private String userGroupAccess;
	private User user;
	private List<MenuAccessMvc> menuAccessMvcList;
	private M_TempUser userPA;
	
	public M_TempUser getUserPA() {
		return userPA;
	}
	public void setUserPA(M_TempUser userPA) {
		this.userPA = userPA;
	}
	public String getDelegatedBy() {
		return DelegatedBy;
	}
	public void setDelegatedBy(String delegatedBy) {
		DelegatedBy = delegatedBy;
	}
	public String getUserGroupAccess() {
		return userGroupAccess;
	}
	public void setUserGroupAccess(String userGroupAccess) {
		this.userGroupAccess = userGroupAccess;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<MenuAccessMvc> getMenuAccessMvcList() {
		return menuAccessMvcList;
	}
	public void setMenuAccessMvcList(List<MenuAccessMvc> menuAccessMvcList) {
		this.menuAccessMvcList = menuAccessMvcList;
	}
	
	
	
	
}
