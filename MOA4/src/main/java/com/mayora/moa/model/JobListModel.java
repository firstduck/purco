package com.mayora.moa.model;

public class JobListModel {
	private String DocNo;
	private String ModuleCode;
	private String ModuleName;
	private String WorkFlowID;
	private String WorkFlowCode;
	private String WorkFlowPathID;
	private String WorkFlowLinesID;
	private String IDPararelLine;
	private String GroupWorkFLowCode;
	private String AssignGroup;
	private String AssignedPIC;
	private String UserName;
	private String WebName;
	private String CreatedBy;
	private String CreatorName;
	private String CreatedDate;
	private String Activity;
	private String LastDate;
	private String LastUser;
	private String LastApprove;
	
	JobListModel()
	{}
	
	public String getDocNo() {
		return DocNo;
	}
	public void setDocNo(String docNo) {
		DocNo = docNo;
	}
	public String getModuleCode() {
		return ModuleCode;
	}
	public void setModuleCode(String moduleCode) {
		ModuleCode = moduleCode;
	}
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public String getWorkFlowID() {
		return WorkFlowID;
	}
	public void setWorkFlowID(String workFlowID) {
		WorkFlowID = workFlowID;
	}
	public String getWorkFlowCode() {
		return WorkFlowCode;
	}
	public void setWorkFlowCode(String workFlowCode) {
		WorkFlowCode = workFlowCode;
	}
	public String getWorkFlowPathID() {
		return WorkFlowPathID;
	}
	public void setWorkFlowPathID(String workFlowPathID) {
		WorkFlowPathID = workFlowPathID;
	}
	public String getWorkFlowLinesID() {
		return WorkFlowLinesID;
	}
	public void setWorkFlowLinesID(String workFlowLinesID) {
		WorkFlowLinesID = workFlowLinesID;
	}
	public String getIDPararelLine() {
		return IDPararelLine;
	}
	public void setIDPararelLine(String iDPararelLine) {
		IDPararelLine = iDPararelLine;
	}
	public String getGroupWorkFLowCode() {
		return GroupWorkFLowCode;
	}
	public void setGroupWorkFLowCode(String groupWorkFLowCode) {
		GroupWorkFLowCode = groupWorkFLowCode;
	}
	public String getAssignGroup() {
		return AssignGroup;
	}
	public void setAssignGroup(String assignGroup) {
		AssignGroup = assignGroup;
	}
	public String getAssignedPIC() {
		return AssignedPIC;
	}
	public void setAssignedPIC(String assignedPIC) {
		AssignedPIC = assignedPIC;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getWebName() {
		return WebName;
	}
	public void setWebName(String webName) {
		WebName = webName;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public String getCreatorName() {
		return CreatorName;
	}
	public void setCreatorName(String creatorName) {
		CreatorName = creatorName;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getActivity() {
		return Activity;
	}
	public void setActivity(String activity) {
		Activity = activity;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public String getLastUser() {
		return LastUser;
	}
	public void setLastUser(String lastUser) {
		LastUser = lastUser;
	}
	public String getLastApprove() {
		return LastApprove;
	}
	public void setLastApprove(String lastApprove) {
		LastApprove = lastApprove;
	}
	
	
}
