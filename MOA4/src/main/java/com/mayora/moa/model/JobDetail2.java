package com.mayora.moa.model;

public class JobDetail2{
	
	private String FieldToShow;
	
	private String FieldAlias;
	
	private String value;

	public String getFieldToShow() {
		return FieldToShow;
	}

	public void setFieldToShow(String fieldToShow) {
		FieldToShow = fieldToShow;
	}

	public String getFieldAlias() {
		return FieldAlias;
	}

	public void setFieldAlias(String fieldAlias) {
		FieldAlias = fieldAlias;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}