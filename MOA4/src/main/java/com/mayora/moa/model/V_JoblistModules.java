package com.mayora.moa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MYRWF.V_JOBLISTMODULES")
public class V_JoblistModules {
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="USERNAME")
	private String username;
	
	@Id
	@Column(name="MODULECODE")
	private String moduleCode;

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
}
