package com.mayora.moa.model;

public class ModuleJobListModel {
	
	private String modulecode;
	private String modulename;
	private String username;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ModuleJobListModel(String modulecode,String modulename, String username) {
		setModulecode(modulecode);
		setModulename(modulename);
		setUsername(username);
	}

	public String getModulecode() {
		return modulecode;
	}

	public void setModulecode(String modulecode) {
		this.modulecode = modulecode;
	}

	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}


	
}
