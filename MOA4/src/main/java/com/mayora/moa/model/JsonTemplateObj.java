package com.mayora.moa.model;

import java.util.ArrayList;
import java.util.List;

public class JsonTemplateObj {
	
	String Header;
	String Message;
	String Error;
	String Size;
	Object data;
	
	public JsonTemplateObj() {
		Header = "";
		Message = "";
		Error = "";
		Size = "0";
		data = new Object();
	}

	public String getHeader() {
		return Header;
	}

	public void setHeader(String header) {
		Header = header;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getError() {
		return Error;
	}

	public void setError(String error) {
		Error = error;
	}

	public String getSize() {
		return Size;
	}

	public void setSize(String size) {
		Size = size;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
