package com.mayora.moa.model;

public class JobInBatchSimple {
	private String docNo;
	private String username;
	private String action;
	private String reason;
	
	public JobInBatchSimple(String docNo,String username,String action,String reason)
	{
		this.docNo = docNo;
		this.username = username;
		this.action = action;
		this.reason = reason;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
