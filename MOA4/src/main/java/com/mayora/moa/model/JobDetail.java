package com.mayora.moa.model;

public class JobDetail{
	
	private String fieldToShow;
	
	private String fieldAlias;
	
	private String value;

	public String getFieldToShow() {
		return fieldToShow;
	}

	public void setFieldToShow(String fieldToShow) {
		this.fieldToShow = fieldToShow;
	}

	public String getFieldAlias() {
		return fieldAlias;
	}

	public void setFieldAlias(String fieldAlias) {
		this.fieldAlias = fieldAlias;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}