package com.mayora.moa.model;

public class JobListModel2 {

	private String docNo;
	private String moduleCode;
	private String activity;
	private String createdBy;
	private String createdDate;
	private String workflowCode;
	private String webName;
	private String pathID;
	private String linesID;
	private String lastUser;
	private String lastDate;
	private String lastApproveName;
	private String creatorName;
	private String moduleName;
	private String action;
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getWorkflowCode() {
		return workflowCode;
	}
	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}
	public String getWebName() {
		return webName;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	public String getPathID() {
		return pathID;
	}
	public void setPathID(String pathID) {
		this.pathID = pathID;
	}
	public String getLinesID() {
		return linesID;
	}
	public void setLinesID(String linesID) {
		this.linesID = linesID;
	}
	public String getLastUser() {
		return lastUser;
	}
	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}
	public String getLastDate() {
		return lastDate;
	}
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	public String getLastApproveName() {
		return lastApproveName;
	}
	public void setLastApproveName(String lastApproveName) {
		this.lastApproveName = lastApproveName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
}
