package com.mayora.moa;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mayora.moa.adapter.MoaBaseAdapter;
import com.mayora.moa.workflow.entity.V_DetailCurrentWorkflowLines;
import com.mayora.moa.workflow.entity.V_DocumentStatus;

@Controller
public class ShowStatusController extends BaseController{

	@RequestMapping(value = "/MOA/ShowStatus", method = RequestMethod.GET)
	public String showStatusContractPO(HttpServletRequest request, Model model,
			RedirectAttributes ra, HttpSession session) {
//		if (this.isLogin(request, contractPOAdapter.setModule(), "Access")) {
			model.addAttribute("menuAccessMvc", this.getMenuAccessMvc(request));
			List<V_DocumentStatus> listDocStatus = clientAdapter.getAllDocumentStatus(request.getParameter("docNo"));
			model.addAttribute("title", "PO Contract Document Status");
			model.addAttribute("link", "/MOA2/ContractPO/Detail/?docNo=" + request.getParameter("docNo"));
			model.addAttribute("docStatus", listDocStatus);
			
			List<String> listLines = new ArrayList<String>();
			List<V_DetailCurrentWorkflowLines> listCurrent = clientAdapter.getDetailCurrentWF(request.getParameter("docNo"));
			
			for(V_DetailCurrentWorkflowLines vCurrent : listCurrent)
			{
				listLines.add(vCurrent.getPk().getWorkflowCode().toString() + "." + vCurrent.getPk().getWorkflowID().toString() + "." + vCurrent.getPk().getWorkflowLinesID());
			}
			
			model.addAttribute("current", listLines);
			model.addAttribute("reject",clientAdapter.isRejectedDocument(request.getParameter("docNo")));
			return "ShowStatus";
//		}
		//return "redirect:/";
	}
	
}
