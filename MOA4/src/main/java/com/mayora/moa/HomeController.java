package com.mayora.moa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mayora.masterdata.entity.D_User;
import com.mayora.masterdata.entity.D_User_PK;
import com.mayora.masterdata.entity.M_User;

import javax.servlet.http.HttpServletResponse;
@Controller
public class HomeController extends BaseController { 
	
	String TAG = "HomeController";
	
	public String getTimeBetween(long start, long end){
        long millis = end - start;

        long msecond = (millis % 1000);
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        return String.format("%02d:%02d:%02d:%02d", hour, minute, second, msecond);
	}
	
	@RequestMapping(value = {"/", "/Home"}, method = {RequestMethod.GET})
	public String home(	HttpServletRequest request,HttpServletResponse res,
						RedirectAttributes ra,
						HttpSession session,
						Model model) throws IOException{
		this.setSessionCookie(request);
		return "home";
	}
	
	@RequestMapping(value = {"/model"}, method = {RequestMethod.GET})
	public String model(	HttpServletRequest request,HttpServletResponse res,
						RedirectAttributes ra,
						HttpSession session,
						Model model) throws IOException{
		
		List<D_User> lsD = new ArrayList<D_User>();
		List<M_User> lsM = new ArrayList<M_User>();
		
		for (int i = 0; i < 900000; i++) {
			
			System.out.println(i);
			
			M_User user = new M_User();
			user.setName("Polly Markurin");
			user.setEmail("markurin.polly@mayora.co.id");
			user.setEmpId("00025876");
			user.setUserName("MG25876");
			
			lsM.add(user);
		}
		
		for (int i = 0; i < 900000; i++) {
			
			System.out.println(i);
			
			D_User user = new D_User();
			
			D_User_PK pk = new D_User_PK();
			pk.setGroupCode("MYOR");
			pk.setUsername("MG25876");
			
			user.setUserPK(pk);
			
			lsD.add(user);
		}
		
//		session.setAttribute("lsDataSession", lsD);
		model.addAttribute("lsData", lsM);
		model.addAttribute("name", "PMN");
		
		return "model-user";
	}
}
