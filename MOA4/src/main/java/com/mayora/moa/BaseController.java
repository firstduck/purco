package com.mayora.moa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.mayora.base.RootBaseController;
import com.mayora.client.ClientAdapter;
import com.mayora.masterdata.adapter.UserAdapter;

@Controller  
public class BaseController extends RootBaseController { 

	@Autowired
	protected ServletContext context;
	
	@Autowired
    protected UserAdapter userAdapter;
	@Autowired
    protected ClientAdapter clientAdapter;
	
//	protected String getProjectPathWindows(String[] pathProject, String projectPath) {
//		for (int a = 1; a < pathProject.length - 6; a++) { 
//    		projectPath = projectPath + pathProject[a] + "\\"; 
//    	}
//		projectPath = projectPath.replace("%20", " ");
//        return projectPath;
//	}
//	
//	protected String getProjectPathLinux(String[] pathProject, String projectPath) {
//		for (int a = 1; a < pathProject.length - 6; a++) { 
//    		projectPath = projectPath + pathProject[a] + "/"; 
//    	}
//		projectPath = projectPath.replace("%20", " ");
//        return projectPath;
//	}
	
	protected String getProjectPathWindowsOld(String[] pathProject, String projectPath) {
		for (int a = 1; a < pathProject.length - 6; a++) { 
			projectPath = projectPath + pathProject[a] + "\\"; 
		}
		projectPath = projectPath.replace("%20", " ");
		return projectPath;
	}

	protected String getProjectPathWindows(String[] pathProject, String projectPath) {
		for (int a = 1; a < pathProject.length; a++) { 
			projectPath = projectPath + pathProject[a] + "\\"; 
			if(pathProject[a].equals("WEB-INF")){
				break;
			}
		}
		projectPath = projectPath.replace("%20", " ");
		return projectPath;
	}

	protected String getProjectPathLinuxOld(String[] pathProject, String projectPath) {
		for (int a = 1; a < pathProject.length - 6; a++) { 
			projectPath = projectPath + pathProject[a] + "/"; 
		}
		projectPath = projectPath.replace("%20", " ");
		return projectPath;
	}

	protected String getProjectPathLinux(String[] pathProject, String projectPath) {
		for (int a = 1; a < pathProject.length; a++) { 
			projectPath = projectPath + pathProject[a] + "/"; 
			if(pathProject[a].equals("WEB-INF")){
				break;
			}
		}
		projectPath = projectPath.replace("%20", " ");
		return projectPath;
	}
	
	
	public String callUrl(String urlString, String data) throws IOException{
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
	   
		OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
	    
	    writer2.write(data);
	    writer2.flush();

	    BufferedReader reader2 = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    List<String> listString2 = new ArrayList<String>();
	    String line = null;
	    while ((line = reader2.readLine()) != null) {
	    	listString2.add(line);
	    }
	    reader2.close();
	    
	    return listString2.get(0);
	}

	
	public String getErrorStatus(String status){
		switch (status.toUpperCase()) {
			case "#DONEBYOTHER#": {
				return "Sorry - but the document you requested has been closed";
			}
			case "#UNDONE#": {
				return "Sorry - you can't review the document yet";
			}
			case "#DONE#": {
				return "Sorry - you have already done";
			}
			case "#UNAUTHORIZED#": {
				return "Sorry - you are not authorized to review the document";
			}
			case "#FAILED#": {
				return "Sorry - something wrong is occurred";
			}
			case "#CLOSED#": {
				return "Sorry - the document is closed";
			}
			default: {
				System.out.println(status.toUpperCase());
				return "Sorry - error to access the document";
			}
		}
	}	
	
	public String callBapi(String urlString, String data) throws IOException{
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
	   
		OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
	    
	    writer2.write(data);
	    writer2.flush();

	    BufferedReader reader2 = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    List<String> listString2 = new ArrayList<String>();
	    String line = null;
	    while ((line = reader2.readLine()) != null) {
	    	listString2.add(line);
	    }
	    reader2.close();
	    
	    return listString2.get(0);
	}
	
	public String PemisahRibuanRupiah(String a)  {

		try 
		{ 
	        Double nilai = Double.parseDouble(a);
	
	        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
	        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
	
	        formatRp.setCurrencySymbol("");
	        formatRp.setMonetaryDecimalSeparator(',');
	        formatRp.setGroupingSeparator('.');
	
	        kursIndonesia.setDecimalFormatSymbols(formatRp);
	
	        return kursIndonesia.format(nilai);
		}
		catch(Exception e) { return ""; }
	}

	public String PemisahRibuanDollar(String a)  {
		try {
		    Double nilai = Double.parseDouble(a);
		
		    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		    DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
		
		    formatRp.setCurrencySymbol("");
		    formatRp.setMonetaryDecimalSeparator('.');
		    formatRp.setGroupingSeparator(',');
		
		    kursIndonesia.setDecimalFormatSymbols(formatRp);
		
		    return kursIndonesia.format(nilai);
		}
		catch(Exception e) { return ""; }
	}
	

	
}
