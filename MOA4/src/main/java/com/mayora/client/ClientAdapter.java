package com.mayora.client;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mayora.client.entity.M_GcmPushNotification;
import com.mayora.client.entity.ModuleGroup;
import com.mayora.client.entity.PairObject;
import com.mayora.client.entity.T_ClientSession;
import com.mayora.masterdata.entity.AccessibleModule;
import com.mayora.masterdata.entity.M_UserMobile;
import com.mayora.masterdata.entity.VM_COMMENT;
import com.mayora.masterdata.entity.VM_COMMENT_PK;
import com.mayora.masterdata.model.DropDownListItem;
import com.mayora.moa.adapter.MoaBaseAdapter;
import com.mayora.moa.model.JobList;
import com.mayora.moa.session.UserAccess;
import com.mayora.mware.tools.Print;
import com.mayora.tools.PTools;

public class ClientAdapter extends MoaBaseAdapter {
	
	String TAG = "ClientAdapter";
	
	public String getTimeBetween(long start, long end){
        long millis = end - start;

        long msecond = (millis % 1000);
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        return String.format("%02d(h) : %02d(m) : %02d(s) : %02d(ms)", hour, minute, second, msecond);
    }
	
	public String setModule() {
		return "UPDATE";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<LinkedHashMap<String,String>> getViewTable(String tableName, String docNo)
	{
		Session session = sfOA.openSession();
		
		List<LinkedHashMap<String,String>> lsResult = new ArrayList<LinkedHashMap<String,String>>();
		
		List<LinkedHashMap<String,String>> lsData = new ArrayList<LinkedHashMap <String,String>>();
		List<String> lsOrder = new ArrayList<String>();
		
		try{
			Query qData = session.createSQLQuery("SELECT * FROM " + tableName + " " +
					"WHERE DOCNO = '" + docNo + "' ");
			qData.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			
			lsData = qData.list();
			
			String[] tableTemp = tableName.split("\\.");
			if(tableTemp.length > 1){
				tableName = tableTemp[1];
			}
			
			Query qOrder = session.createSQLQuery("SELECT COLUMN_NAME FROM ALL_TAB_COLUMNS " +
					"WHERE TABLE_NAME = '" + tableName + "' ORDER BY COLUMN_ID ");
			
			lsOrder = qOrder.list();
			
			if(lsOrder.size() > 0){
				for (Map<String,String> entry : lsData) {
					LinkedHashMap<String,String> item = new LinkedHashMap();
					for (String entity : lsOrder) {
						if(!entity.equals("DOCNO")){
							Object o = entry.get(entity);
							String value = "";
							if(o != null){
								value = o.toString();
							}
							item.put(entity, value);
						}
					}
					lsResult.add(item);
				}
			}else{
				lsResult.addAll(lsData);
			}
		}catch(Exception e){
			Print._err(TAG, e);
		} finally{
			session.close();
		}
		
		return lsResult;
	}

	@SuppressWarnings("unchecked")
	public List<AccessibleModule> getAccessibleModule(String moduleCode, String groupCode)
	{
		Session session = sfOA.openSession();
		List<AccessibleModule> list = new ArrayList<AccessibleModule>();
		try{
			list = (List<AccessibleModule>) session.createQuery("from AccessibleModule where moduleCode ='"+moduleCode+"' and groupCode = '"+groupCode+"'").list();
		}finally{
			session.close();
		}
		return list;
	}
	
	private String setDate(Date date){
	    SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
	    String format =  "to_date('"+SDF.format(date)+"', 'DD-MM-YYYY HH24:MI:SS')";
	   
	    return format;
    } 
		
	public void insertD_Comment(final String moduleCode, final String docNo, final String notesType, final String notes, Date createdDate, final String username)
	{
		final String createDate = this.setDate(createdDate);
		
		Session session = sfWF.openSession();

		session.doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {
				  Statement pStmt = null;
	              try
	              {
	                 String sql = "SELECT databaseName FROM MYRMD.M_MODULE WHERE moduleCode='"+moduleCode+"'";
	                 pStmt = connection.createStatement();
	
	                 ResultSet rs =  pStmt.executeQuery(sql);
	                 
	                 if(rs.next()){
	                	 String dbName = rs.getString("DATABASENAME");
			
	                	 sql = "SELECT NVL(MAX(ID),0) as ID FROM " + dbName + ".D_COMMENT WHERE docNo='"+docNo+"'";

	                	 rs = pStmt.executeQuery(sql);

	                	 if(rs.next()){
	                		 Integer id = rs.getInt("ID")+1;
	                		 
		                	 sql = "INSERT INTO " + dbName + ".D_COMMENT VALUES('"+docNo+"',"+id+",'"+notesType+"','"+notes+"',"+createDate+",'"+username+"')";
		
		                	 pStmt.executeUpdate(sql); //seharusnya auto-commit
		                	 
	                	 }
	                 }
	              }
	              finally
	              {
	                 pStmt.close();
	              }       
			}
		});
		session.close();
	}
		
	@SuppressWarnings("unchecked")
	public List<DropDownListItem> getJobListModules(UserAccess ua) {
		Session session  = sfWF.openSession();
		List<DropDownListItem> list = new ArrayList<DropDownListItem>();
		list.add(new DropDownListItem("All", "ALL"));
		
		
		String username = ua.getUser().getUsername();
		String delegatedBy = ua.getDelegatedBy();
		String qString = "select modulecode, modulename from myrwf.V_JoblistModules where username= '"+username+"' or assignedPIC = '"+username+"'" ;
		
		if(!delegatedBy.equals("#SELF#")){
			qString = "select j.modulecode,j.modulename from myrwf.v_joblistmodules j"+
					" join myrmd.v_delegation d"+
					" on (j.username = d.delegateasusername or j.assignedPIC = d.delegateasusername) and d.modulecode = j.modulecode" +
					" where " +
					" (j.username = '"+username+"' or j.assignedPIC = '"+username+"')"+
					" and d.delegateasusername ='"+username+"'"+
					" and d.username='"+delegatedBy+"'";
		}
		
		Query query = session.createSQLQuery(qString);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		
		List<Map<String,Object>> listMod = new ArrayList<Map<String,Object>>();
		
		try{
			listMod = query.list();

			for(Map<String,Object> v : listMod){
				list.add(new DropDownListItem(v.get("MODULENAME").toString(), v.get("MODULECODE").toString()));
			}
		}finally{
			session.close();
		}
		return list;
	}
		
	
	@SuppressWarnings("unchecked")
	public String getModuleGroupByUsername(String username) {
		Session session  = sfOA.openSession();
		
		Query query  = session.createSQLQuery("select v.modulecode, m.modulename, m.webname, count(v.modulecode) jumlah from myrwf.v_detailcurrentworkflowlines v join myrmd.m_module m on m.modulecode=v.modulecode WHERE (username ='"+username+"' OR assignedPIC = '"+username+"')" +
				" AND (activity = 'Approve' OR activity = 'ReviewApprove') AND V.ISMOBILEREADY = 1 group by v.modulecode, m.modulename, m.webname");

		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);

		List<Map<String,Object>> alias = new ArrayList<Map<String,Object>>();
		try{
			alias = query.list(); 
		}finally{
			session.close();
		}

		return new Gson().toJson(alias);
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public List<JobList> getJobListByUsernameMobile(String type, String username, String delegatedBy, String module){
		Session session  = sfOA.openSession();
		
		String  query = "select j.* " +
				" from myrwf.V_JoblistMobile j WHERE (j.username ='"+username+"' OR j.assignedPIC = '"+username+"')" +
				" AND j.activity in ('Approve','ReviewApprove','Print','Assign','Review','BatchReview')";		
		
		if(module.equals("ALL")){
			
			if(!delegatedBy.equals("#SELF#")){
					query += " and j.modulecode in (select modulecode from myrmd.v_delegation d where d.delegateasusername = '"+username+"' and  d.username = '"+delegatedBy+"')";
			}
			
		}else{
			query += " AND j.moduleCode = '"+module+"'";	
		}
		
		if(type.equals("mobile")){
			query = query.replace(",'Print','Assign','Review','BatchReview'", "");
		}
		query += " order by createdDate asc";
		Query query1 = session.createSQLQuery(query);
		query1.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);

		List<Map<String,Object>> listVDC = new ArrayList<Map<String,Object>>();
		
		try{
			listVDC = query1.list();
		}finally{
			session.close();
		}
		
		List<JobList> listJL = new ArrayList<JobList>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		
		for(int i = 0; i < listVDC.size(); i++){

			JobList JL = new JobList();
			String act = listVDC.get(i).get("ACTIVITY").toString();
			String web = listVDC.get(i).get("WEBNAME").toString();
			String docNo = listVDC.get(i).get("DOCNO").toString();
			
			if(act.equals("Review") || act.equals("ReviewApprove") || act.equals("Assign") || act.equals("Print")) JL.setAction(act);
			else JL.setAction("Hold");
			JL.setReason(null);
			JL.setDocNo(docNo);
			JL.setModuleCode(listVDC.get(i).get("MODULECODE").toString());
			JL.setModuleName(listVDC.get(i).get("MODULENAME").toString());
			if(listVDC.get(i).get("ACTIVITY").toString().equals("ReviewApprove") && type.equals("mobile")){
				JL.setActivity("Approve");
			}else{
				JL.setActivity(listVDC.get(i).get("ACTIVITY").toString());
			}
			
			JL.setWorkflowCode(listVDC.get(i).get("WORKFLOWCODE").toString());
			JL.setWebName(listVDC.get(i).get("WEBNAME").toString());
			JL.setPathID(Integer.valueOf(listVDC.get(i).get("WORKFLOWPATHID").toString()));
			JL.setLinesID(Integer.valueOf(listVDC.get(i).get("WORKFLOWLINESID").toString()));
			JL.setLastUser("");
			JL.setCreatorName(listVDC.get(i).get("CREATORNAME").toString());
			JL.setLastApproveName("");
			JL.setCreatedBy(listVDC.get(i).get("CREATEDBY").toString());
			
			if(listVDC.get(i).get("CREATEDDATE") == null){
				JL.setCreatedDate(null);
			}else{
				try{
					JL.setCreatedDate(sdf.format(sdf1.parse(listVDC.get(i).get("CREATEDDATE").toString())));
				}catch(Exception e){}
				
			}
			JL.setLastDate("");
			
			listJL.add(JL);
		}
		
		
		return listJL;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public List<JobList> getJobListOfflineByUsername(String type, String username, String delegatedBy, String module){
		Session session  = sfOA.openSession();
		
		String  query = "select j.* " +
				" from myrwf.V_JoblistOffline j WHERE (j.username ='"+username+"' OR j.assignedPIC = '"+username+"')" +
				" AND j.activity in ('Approve','ReviewApprove','Print','Assign','Review','BatchReview')";		
		
		if(module.equals("ALL")){
			
			if(!delegatedBy.equals("#SELF#")){
					query += " and j.modulecode in (select modulecode from myrmd.v_delegation d where d.delegateasusername = '"+username+"' and  d.username = '"+delegatedBy+"')";
			}
			
		}else{
			query += " AND j.moduleCode = '"+module+"'";	
		}
		
		if(type.equals("mobile")){
			query = query.replace(",'Print','Assign','Review','BatchReview'", "");
		}
		query += " order by createdDate asc";
		System.out.println("query = "+query);
		Query query1 = session.createSQLQuery(query);
		query1.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);

		List<Map<String,Object>> listVDC = new ArrayList<Map<String,Object>>();
		
		try{
			listVDC = query1.list();
		}finally{
			session.close();
		}
		
		List<JobList> listJL = new ArrayList<JobList>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		
		for(int i = 0; i < listVDC.size(); i++){

			JobList JL = new JobList();
			String act = listVDC.get(i).get("ACTIVITY").toString();
			String web = listVDC.get(i).get("WEBNAME").toString();
			String docNo = listVDC.get(i).get("DOCNO").toString();
			
			if(act.equals("Review") || act.equals("ReviewApprove") || act.equals("Assign") || act.equals("Print")) JL.setAction(act);
			else JL.setAction("Hold");

			JL.setReason(null);
			JL.setDocNo(docNo);
			JL.setModuleCode(listVDC.get(i).get("MODULECODE").toString());
			JL.setModuleName(listVDC.get(i).get("MODULENAME").toString());
			if(listVDC.get(i).get("ACTIVITY").toString().equals("ReviewApprove") && type.equals("mobile")){
				JL.setActivity("Approve");
			}else{
				JL.setActivity(listVDC.get(i).get("ACTIVITY").toString());
			}
			
			JL.setWorkflowCode(listVDC.get(i).get("WORKFLOWCODE").toString());
			JL.setWorkflowID(listVDC.get(i).get("WORKFLOWID").toString());
			JL.setWebName(listVDC.get(i).get("WEBNAME").toString());
			JL.setPathID(Integer.valueOf(listVDC.get(i).get("WORKFLOWPATHID").toString()));
			JL.setLinesID(Integer.valueOf(listVDC.get(i).get("WORKFLOWLINESID").toString()));
			JL.setIdPararel(listVDC.get(i).get("IDPARARELLINE").toString());
			JL.setLastUser(listVDC.get(i).get("LASTAPPROVE").toString());
			JL.setCreatorName(listVDC.get(i).get("CREATORNAME").toString());
			JL.setLastApproveName(listVDC.get(i).get("LASTUSER").toString());
			JL.setCreatedBy(listVDC.get(i).get("CREATEDBY").toString());
			
			if(listVDC.get(i).get("CREATEDDATE") == null){
				JL.setCreatedDate(null);
			}else{
				try{
					JL.setCreatedDate(sdf.format(sdf1.parse(listVDC.get(i).get("CREATEDDATE").toString())));
				}catch(Exception e){}
				
			}
			JL.setLastDate(listVDC.get(i).get("LASTDATE").toString());
			
			listJL.add(JL);
		}
		
		
		return listJL;
	}
	
	 private Boolean checkCurrentSelected(JobList joblist, List<Map<String,Object>> lsCurrent){
		 Boolean isCurrent = false;
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		 
		 for (int i = 0; i < lsCurrent.size(); i++) {
			 
			 if(joblist.getDocNo().equals("00158/FPSA/07/2017/MYOR") &&
					 joblist.getWorkflowCode().equals("FPSA_Review")){
				 System.out.println("");
			 }
			 
			 if(joblist.getDocNo().equals(lsCurrent.get(i).get("DOCNO").toString()) 
					 && joblist.getWorkflowCode().equals(lsCurrent.get(i).get("WORKFLOWCODE"))){
				 
				 isCurrent = true;
				 if(!joblist.getWorkflowID().toString().equals(lsCurrent.get(i).get("WORKFLOWID").toString())){
					 isCurrent = false;
				 }
				 if(!joblist.getLinesID().toString().equals(lsCurrent.get(i).get("WORKFLOWLINESID").toString())){
					 isCurrent = false;
				 }
				 
				 if(isCurrent){
					joblist.setLastUser(lsCurrent.get(i).get("LASTAPPROVE").toString());
					joblist.setLastApproveName(lsCurrent.get(i).get("LASTUSER").toString());
					 
					if(lsCurrent.get(i).get("LASTDATE")==null){
						 joblist.setLastDate(null);
					}else{
						try{
							joblist.setLastDate(sdf.format(sdf1.parse(lsCurrent.get(i).get("LASTDATE").toString())));
						}catch(Exception e){}
					}
					break;
				 }
			 }else{
				 continue;
			 }
		}
		 return isCurrent;
	 }
	
	@SuppressWarnings({ "unchecked", "unused" })
	public List<JobList> getJobListByUsername(String type, String username, String delegatedBy, String module){
		Session session  = sfOA.openSession();
		
		String  query = "select j.* " +
				" from myrwf.V_Joblist j WHERE " +
				"(j.username ='"+username+"' OR j.assignedPIC = '"+username+"')" +
				" AND j.activity in ('Approve','ReviewApprove','Print','Assign','Review','BatchReview')";		
		
		if(module.equals("ALL")){
			
			if(!delegatedBy.equals("#SELF#")){
					query += " and j.modulecode in (select modulecode from myrmd.v_delegation d where d.delegateasusername = '"+username+"' and  d.username = '"+delegatedBy+"')";
			}
			
		}else{
			query += " AND j.moduleCode = '"+module+"'";	
		}
		
		if(type.equals("mobile")){
			query = query.replace(",'Print','Assign','Review','BatchReview'", "");
		}
		query += " order by createdDate asc";
		Query query1 = session.createSQLQuery(query);
		query1.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);

		List<Map<String,Object>> listVDC = new ArrayList<Map<String,Object>>();
		
		try{
			listVDC = query1.list();
		}finally{
			session.close();
		}
		
		List<JobList> listJL = new ArrayList<JobList>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		
		for(int i = 0; i < listVDC.size(); i++){

			JobList JL = new JobList();
			String act = listVDC.get(i).get("ACTIVITY").toString();
			String web = listVDC.get(i).get("WEBNAME").toString();
			String docNo = listVDC.get(i).get("DOCNO").toString();
			
			if(act.equals("Review") || act.equals("ReviewApprove") || act.equals("Assign") || act.equals("Print")) JL.setAction(act);
			else JL.setAction("Hold");

			JL.setReason(null);
			JL.setDocNo(docNo);
			JL.setModuleCode(listVDC.get(i).get("MODULECODE").toString());
			JL.setModuleName(listVDC.get(i).get("MODULENAME").toString());
			if(listVDC.get(i).get("ACTIVITY").toString().equals("ReviewApprove") && type.equals("mobile")){
				JL.setActivity("Approve");
			}else{
				JL.setActivity(listVDC.get(i).get("ACTIVITY").toString());
			}
			
			JL.setWorkflowCode(listVDC.get(i).get("WORKFLOWCODE").toString());
			JL.setWebName(listVDC.get(i).get("WEBNAME").toString());
			JL.setPathID(Integer.valueOf(listVDC.get(i).get("WORKFLOWPATHID").toString()));
			JL.setLinesID(Integer.valueOf(listVDC.get(i).get("WORKFLOWLINESID").toString()));
			JL.setLastUser(listVDC.get(i).get("LASTAPPROVE").toString());
			JL.setCreatorName(listVDC.get(i).get("CREATORNAME").toString());
			JL.setLastApproveName(listVDC.get(i).get("LASTUSER").toString());
			JL.setCreatedBy(listVDC.get(i).get("CREATEDBY").toString());
			
			if(listVDC.get(i).get("CREATEDDATE") == null){
				JL.setCreatedDate(null);
			}else{
				try{
					JL.setCreatedDate(sdf.format(sdf1.parse(listVDC.get(i).get("CREATEDDATE").toString())));
				}catch(Exception e){}
				
			}
			if(listVDC.get(i).get("LASTDATE")==null){
				JL.setLastDate(null);
			}else{
				try{
					JL.setLastDate(sdf.format(sdf1.parse(listVDC.get(i).get("LASTDATE").toString())));
				}catch(Exception e){}
			}
			
			listJL.add(JL);
		}
		
		
		return listJL;
	}
	
	@SuppressWarnings("unchecked")
	public M_GcmPushNotification hasGCM(String username) {
		Session session = sfWF.openSession();
		
		List<M_GcmPushNotification> list = new ArrayList<M_GcmPushNotification>();
		try{
			list = session.createQuery("from M_GcmPushNotification where nik ='"+username+"'").list();
			return list.get(0);
		}catch(Exception e){
			return null;
		}finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public Integer isMobileVerified(String username, String sn)
	{
		Session session = sfOA.openSession();

		List<M_UserMobile> list = new ArrayList<>();
		try{
			list = session.createQuery("from M_UserMobile where pk.username = '"+ username +"' and sn = '" + sn + "'").list();
		}finally{
			session.close();
		}

		if(list.size()>0)
		{
			M_UserMobile userMobile = list.get(0);
			if(userMobile.getIsVerified() && new Date().after(userMobile.getStartDate()) && new Date().before(userMobile.getEndDate()))
				return 1;
			else 
			{
				userMobile.setIsVerified(false);
				this.updateObjectOA(userMobile);
				return -1;			
			}
		}
		else return 0;
	}
	
	public void deleteVerifiedClient(String username)
	{
		Session session = sfOA.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE FROM M_UserMobile where username ='"+username+"'");
			query.executeUpdate();
			session.getTransaction().commit();
		} 
		catch(HibernateException e) { 
        	session.getTransaction().rollback();  
		}
		finally{ 
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public Boolean verifyPIN(String username, String serial, String pin)
	{
		Session session = sfOA.openSession();
		
		List<M_UserMobile> list = new ArrayList<M_UserMobile>();
		try{
			list = session.createQuery("from M_UserMobile where pk.username = '"+ username +"' and pin = '" + pin + "' and isVerified = 0").list();
		}catch(Exception e){
			session.close();
		}

		if(list.size()>0) 
		{
			M_UserMobile userMobile = list.get(0);
			userMobile.setSn(serial);
			userMobile.setIsVerified(true);
			userMobile.setStartDate(new Date());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, 6);
			cal.add(Calendar.DATE, 6);
			userMobile.setEndDate(cal.getTime());
			this.updateObjectOA(userMobile);
			
			return true; 
		}
		else return false;
		
	}
	
	@SuppressWarnings("unchecked")
	public Boolean checkSN(String username, String serial)
	{
		Session session = sfOA.openSession();
		List<M_UserMobile> list = new ArrayList<M_UserMobile>();
		try{
			list = session.createQuery("from M_UserMobile where pk.username = '"+ username +"' and sn = '"+ serial +"' and isVerified = 1").list();
		}finally{
			session.close();
		}

		if(list.size()>0) 
		{
			return true;
		}
		else return false;
		
	}

	@SuppressWarnings("unchecked")
	public String getJobListNumByUsername(String username) {
		Session session = sfOA.openSession();
		
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		try{
			list = session.createSQLQuery("select count(distinct docno) jumlah from myrwf.V_JoblistMobile WHERE (username ='"+username+"' OR assignedPIC = '"+username+"')" +
					"AND (activity = 'Approve' OR activity = 'ReviewApprove') AND ISMOBILEREADY = 1").list();
		}finally{
			session.close();
		}

		if(list.size()>0) return list.get(0).toString();
		
		return "0";
	}
	
	@SuppressWarnings("unchecked")
	public T_ClientSession getClientSession(String accessCode)
	{
		Session session = sfOA.openSession();
		List<T_ClientSession> list = new ArrayList<T_ClientSession>();
		try{
			list = session.createQuery("from T_ClientSession where accessCode='" + accessCode +"'").list();
		}finally{
			session.close();
		}

		if(list.size()>0) return list.get(0);
		else return null;
	}
	
	@SuppressWarnings("unchecked")
	public T_ClientSession getClientSessionByAppCodeIP(String appCode, String ipClient)
	{
		Session session = sfOA.openSession();
		List<T_ClientSession> list = new ArrayList<T_ClientSession>();
		try{
			list = session.createQuery("from T_ClientSession where appCode='" + appCode + "' and ipClient = '"+ ipClient +"'").list();
		}finally{
			session.close();
		}

		if(list.size()>0) return list.get(0);
		else return null;
	}
	
	public void insertClientSession(T_ClientSession clientSession, String mode)
	{
		if(mode.equals("update"))
			this.deleteObject(new T_ClientSession(), clientSession.getAccessCode());
		this.addObject(clientSession);
	}
	
	@SuppressWarnings("unchecked")
	public String getModuleCodeByDocNo(String docNo)
	{
		Session session = sfOA.openSession(); 
		Query query = session.createSQLQuery("select distinct(moduleCode) from MYRWF.T_SELECTEDWORKFLOW where docNo ='"+docNo+"'");
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		
		List<Map<String,Object>> aliasToValueMapList = new ArrayList<Map<String,Object>>();
		try{
			aliasToValueMapList = query.list();
		}finally{
			session.close();
		}
		return aliasToValueMapList.get(0).get("MODULECODE").toString();
	}
	
	
	public String getDataClientCommentNew(final String database, final String docNo){
        Session session = sfOA.openSession();
        String res = "";

        try{
            res = session.doReturningWork(new ReturningWork <String>() {
                @Override
                public String execute(Connection connection) throws SQLException {

                    String result = "";

                    List<VM_COMMENT> lsVmComment = new ArrayList<VM_COMMENT>();
                    Statement wfc = connection.createStatement();
                    try {

                        String query = "SELECT d.DOCNO, d.ID , d.NOTESTYPE, d.NOTES, d.CREATEDDATE, d.USERNAME ,u.NAME FROM "+database+".D_COMMENT d JOIN MYRMD.M_USER u ON d.USERNAME = u.USERNAME WHERE DOCNO = '"+docNo+"'";
                        ResultSet rs = wfc.executeQuery(query);

                        while (rs.next()) {
                            VM_COMMENT vMComment = new VM_COMMENT();
                            VM_COMMENT_PK vMCommentPK = new VM_COMMENT_PK();
							vMCommentPK.setDocNo(rs.getString("DOCNO"));
                            vMCommentPK.setIndex(rs.getInt("ID"));
                            vMComment.setPk(vMCommentPK);
							vMComment.setCreatedDate(rs.getDate("CREATEDDATE"));
							vMComment.setNotesType(rs.getString("NOTESTYPE"));
							vMComment.setNotes(rs.getString("NOTES"));
							vMComment.setUsername(rs.getString("USERNAME"));
							vMComment.setName(rs.getString("NAME"));

                            lsVmComment.add(vMComment);
                        }

                        Gson gson = new GsonBuilder().serializeNulls().create();
                        result = gson.toJson(lsVmComment);
                    }catch(Exception e){
                        Print._err(TAG, e);
                    } finally {
                        wfc.close();
                        connection.close();
                    }
                    return result;
                }
            });
        }finally{
            session.close();
        }
        return res;
    }


	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getOutstanding(String username) {
		Session session = sfOA.openSession();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try{
			String qString = "select count(modulecode) num, modulecode, modulename from myrwf.v_joblist where (username='"+username+"' or assignedpic='"+username+"') group by modulecode, modulename";
			Query query = session.createSQLQuery(qString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			list = query.list();
		}finally{
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getDone(String username) {
		Session session = sfOA.openSession();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try{
			String qString = "select count(DISTINCT v.DOCNO) num, v.modulecode, m.modulename, m.webname from myrwf.v_finishedworkflow v "+
            " join myrmd.m_module m on m.modulecode = v.modulecode "+
            " where createdby='"+username+"' and trunc(sysdate)-trunc(createddate) <= 60 "+
            " group by v.modulecode, m.modulename, m.webname"; 
			Query query = session.createSQLQuery(qString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			list = query.list();
		}finally{
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getDoneList(String username, String modulecode) {
		Session session = sfOA.openSession();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try{
			String qString = "select v.docno, h.lastapprove, h.lastuser, to_char(h.lastdate,'dd-mm-yyyy') lastdate"
						+" from myrwf.v_finishedworkflow v"
						+" join myrwf.v_lasthistory h on h.docno = v.docno"
						+" where createdby='"+username+"' and modulecode='"+modulecode+"' and trunc(sysdate)-trunc(LASTDATE) <= 60";
			Query query = session.createSQLQuery(qString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			list = query.list();
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getInProgress(String username) {
		Session session = sfOA.openSession();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try{
			String qString = "select count(DISTINCT DOCNO) num, modulecode, modulename, webname from myrwf.v_joblist where createdBy='"+username+"' group by modulecode, modulename, webname";
			Query query = session.createSQLQuery(qString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			list = query.list();
		}finally{
			session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> getInProgressList(String username, String moduleCode) {
		Session session = sfOA.openSession();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		try{
			String qString = "select DISTINCT docno, lastapprove, lastuser, to_char(lastdate,'dd-mm-yyyy') lastdate " +
					", modulecode, workflowcode, workflowpathid as pathid, workflowlinesid as lineid from myrwf.v_joblist where createdby='"+username+"' and modulecode='"+moduleCode+"'";
			Query query = session.createSQLQuery(qString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			list = query.list();
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public String getOTPByDocNo(String docNo){
		Session session = sfOA.openSession();
		
		String OTP = "-";
		try{
			String s = "SELECT * FROM MYRWF.T_OTPAUTH WHERE DOCNO = '" + docNo + "' ";
			Query query  = session.createSQLQuery(s);
			
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			
			List<Map<String,Object>> ls = query.list();
			
			if(ls.size() > 0){
				OTP = ls.get(0).get("OTP").toString();
			}
			
		}catch(Exception e){
			Print._err(TAG, e);
		} finally{
			session.close();
		}
		
		return OTP;
	}
	
	// TODO: getJobListModulesNew
	public List<DropDownListItem> getJobListModulesNew(UserAccess ua) {
		List<JobList> lsJoblist = getJobListByUsernameNew("", ua.getUser().getUsername(), ua.getDelegatedBy(), "ALL", false);

		List<DropDownListItem> list = new ArrayList<DropDownListItem>();
		list.add(new DropDownListItem("All", "ALL"));
		
		List<String> lsTemp = new ArrayList<String>();
		
		for(JobList joblist : lsJoblist){
			if(!lsTemp.contains(joblist.getModuleCode())){
				lsTemp.add(joblist.getModuleCode());
				list.add(new DropDownListItem(joblist.getModuleName(), joblist.getModuleCode()));
			}
		}
		
		return list;
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<JobList> getJobListByUsernameNew(String type, 
			String username, 
			String delegatedBy, 
			String module,
			Boolean checkOffline){
		Session session  = sfOA.openSession();
		List<JobList> lsData = new ArrayList<JobList>();
		try{
			String qSzSelect = "SELECT COUNT(DOCNO) as TOTAL " +
					"FROM MYRWF.VP_SELECTEDWORKFLOW2 " +
					"WHERE (ASSIGNEDPIC = '" + username + "' OR USERNAME = '" + username + "') " +
					"AND ACTIVITY IN('Approve','ReviewApprove','Print','Assign','Review','BatchReview') " +
					"AND CLOSEDATE IS NULL AND CLOSEBY IS NULL " +
					"AND REJECTDATE IS NULL AND REJECTBY IS NULL ";
			
			if(module.equals("ALL")){
				if(!delegatedBy.equals("#SELF#")){
					qSzSelect += " AND MODULECODE IN (SELECT MODULECODE FROM MYRMD.V_DELEGATION d " +
							"WHERE d.DELEGATEASUSERNAME = '" + username + "' AND d.USERNAME = '" + delegatedBy + "')";
				}
			}else{
				qSzSelect += " AND MODULECODE = '" + module + "'";	
			}
			
			if(type.equals("mobile")){
				qSzSelect += "AND ISMOBILEREADY = 1 ";
				qSzSelect = qSzSelect.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'Approve','ReviewApprove','ReceiveDoc','SendDoc'");
			} else if(type.equals("mobiledoc")){
				qSzSelect += "AND ISMOBILEREADY = 1 ";
				qSzSelect = qSzSelect.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'ReceiveDoc','SendDoc'");
			} else if(type.equals("desktop")){
				qSzSelect += "AND ISMOBILEREADY = 1 ";
			}
			
			if(checkOffline){
				qSzSelect += "AND DOCNO NOT IN (SELECT DOCNO FROM MYRWF.T_PENDINGWORKFLOW) ";
			}
			
			Query querySelected = session.createSQLQuery(qSzSelect);
			querySelected.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			
			List<Map<String,Object>> lsVDC = querySelected.list();
			
			Integer size = 0;
			if(lsVDC.size() > 0){
				size = Integer.valueOf(lsVDC.get(0).get("TOTAL").toString());
			}
			
			if(size < 1500){
				Print._msg(TAG, "qSzSelect: " + size + " | getJobListDataL" + "\n");
				lsData = getJobListDataL(type, username, delegatedBy, module, checkOffline);
			}else{
				Print._msg(TAG, "qSzSelect: " + size + " | getJobListDataV" + "\n");
				lsData = getJobListDataV(type, username, delegatedBy, module, checkOffline);
			}
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		
		return lsData;
		
	}
	
	// TODO: getJobListByUsernameNew
	@SuppressWarnings({ "unchecked", "unused" })
	public List<JobList> getJobListDataV(
			String type, 
			String username, 
			String delegatedBy, 
			String module,
			Boolean checkOffline){
		Session session  = sfOA.openSession();
		
		List<JobList> lsData = new ArrayList<JobList>();
		long init = System.currentTimeMillis();
		try{
			String qsJoblist = "SELECT j.* " +
					" FROM MYRWF.V_JOBLIST j WHERE (j.USERNAME ='" + username + "' OR j.ASSIGNEDPIC = '" + username + "')" +
					" AND j.ACTIVITY IN ('Approve','ReviewApprove','Print','Assign','Review','BatchReview')";		
			
			if(module.equals("ALL")){
				if(!delegatedBy.equals("#SELF#")){
					qsJoblist += " AND j.modulecode IN (" +
							"SELECT MODULECODE FROM MYRMD.V_DELEGATION d " +
							"WHERE d.DELEGATEASUSERNAME = '" + username + "' " +
							"AND  d.USERNAME = '" + delegatedBy + "')";
				}
			}else{
				qsJoblist += " AND j.MODULECODE = '"+module+"'";	
			}
			
			if(type.equals("mobile")){
				qsJoblist += "AND ISMOBILEREADY = 1 ";
				qsJoblist = qsJoblist.replace("MYRWF.V_JOBLIST", "MYRWF.V_JOBLISTMOBILE");
				qsJoblist = qsJoblist.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'Approve','ReviewApprove','ReceiveDoc','SendDoc'");
			} else if(type.equals("mobiledoc")){
				qsJoblist += "AND ISMOBILEREADY = 1 ";
				qsJoblist = qsJoblist.replace("MYRWF.V_JOBLIST", "MYRWF.V_JOBLISTOFFLINE");
				qsJoblist = qsJoblist.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'ReceiveDoc','SendDoc'");
			} else if(type.equals("desktop")){
				qsJoblist += "AND ISMOBILEREADY = 1 ";
				qsJoblist = qsJoblist.replace("MYRWF.V_JOBLIST", "MYRWF.V_JOBLISTOFFLINE");
			}
			
			if(checkOffline){
				qsJoblist += "AND DOCNO NOT IN (SELECT DOCNO FROM MYRWF.T_PENDINGWORKFLOW) ";
			}
			
			qsJoblist += " ORDER BY CREATEDDATE ASC";
			
			Query qJoblist = session.createSQLQuery(qsJoblist);
			qJoblist.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			
			List<Map<String,Object>> lsVDC = new ArrayList<Map<String,Object>>();
			lsVDC = qJoblist.list();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			
			for(int i = 0; i < lsVDC.size(); i++){
				
				JobList JL = new JobList();
				String act = lsVDC.get(i).get("ACTIVITY").toString();
				String web = lsVDC.get(i).get("WEBNAME").toString();
				String docNo = lsVDC.get(i).get("DOCNO").toString();
				
				if(act.equals("Review") || act.equals("ReviewApprove") || act.equals("Assign") || act.equals("Print")) JL.setAction(act);
				else JL.setAction("Hold");
				JL.setReason(null);
				JL.setDocNo(docNo);
				JL.setWorkflowID(lsVDC.get(i).get("WORKFLOWID").toString());
				JL.setModuleCode(lsVDC.get(i).get("MODULECODE").toString());
				JL.setModuleName(lsVDC.get(i).get("MODULENAME").toString());
				if(lsVDC.get(i).get("ACTIVITY").toString().equals("ReviewApprove") && type.equals("mobile")){
					JL.setActivity("Approve");
				}else{
					JL.setActivity(lsVDC.get(i).get("ACTIVITY").toString());
				}
				
				JL.setWorkflowCode(lsVDC.get(i).get("WORKFLOWCODE").toString());
				JL.setWebName(lsVDC.get(i).get("WEBNAME").toString());
				JL.setPathID(Integer.valueOf(lsVDC.get(i).get("WORKFLOWPATHID").toString()));
				JL.setLinesID(Integer.valueOf(lsVDC.get(i).get("WORKFLOWLINESID").toString()));
				JL.setIdPararel(lsVDC.get(i).get("IDPARARELLINE").toString());
				JL.setCreatorName(lsVDC.get(i).get("CREATORNAME").toString());
				JL.setCreatedBy(lsVDC.get(i).get("CREATEDBY").toString());
				
				if(lsVDC.get(i).get("CREATEDDATE") == null){
					JL.setCreatedDate(null);
				}else{
					try{
						JL.setCreatedDate(sdf.format(sdf1.parse(lsVDC.get(i).get("CREATEDDATE").toString())));
					}catch(Exception e){}
				}
				
				if(lsVDC.get(i).get("LASTAPPROVE").toString() == null){
					JL.setLastUser("-");
				}else{
					JL.setLastUser(lsVDC.get(i).get("LASTAPPROVE").toString());
				}

				if(lsVDC.get(i).get("LASTUSER").toString() == null){
					JL.setLastApproveName("-");
				}else{
					JL.setLastApproveName(lsVDC.get(i).get("LASTUSER").toString());
				}
				
				if(lsVDC.get(i).get("LASTDATE") == null){
					JL.setLastDate("-");
				}else{
					try{
						JL.setLastDate(sdf.format(sdf1.parse(lsVDC.get(i).get("LASTDATE").toString())));
					}catch(Exception e){}
				}
				
				if(act.equals("SendDoc") || act.equals("ReceiveDoc")){
					JL.setOTP(getOTPByDocNo(docNo));
				}else{
					JL.setOTP("-");
				}
				
				lsData.add(JL);
			}
			
			Print._msg(TAG,"getJobListDataV(" + username + ")" + "\n" + qsJoblist + "\n" + getTimeBetween(init, System.currentTimeMillis()).toString() + "\n");
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		
		return lsData;
	}
	
	// TODO: getJobListByUsernameNew
	@SuppressWarnings({ "unchecked" })
	public List<JobList> getJobListDataL(
			String type, 
			String username, 
			String delegatedBy, 
			String module,
			Boolean checkOffline){
		Session session  = sfOA.openSession();
		
		List<JobList> lsData = new ArrayList<JobList>();
		long init = System.currentTimeMillis();
		try{
			String qSelected = "SELECT * FROM MYRWF.VP_SELECTEDWORKFLOW2 " +
					"WHERE (ASSIGNEDPIC = '" + username + "' OR USERNAME = '" + username + "') " +
					"AND ACTIVITY IN('Approve','ReviewApprove','Print','Assign','Review','BatchReview') " +
					"AND CLOSEDATE IS NULL AND CLOSEBY IS NULL " +
					"AND REJECTDATE IS NULL AND REJECTBY IS NULL ";
			
			if(module.equals("ALL")){
				if(!delegatedBy.equals("#SELF#")){
					qSelected += " AND MODULECODE IN (SELECT MODULECODE FROM MYRMD.V_DELEGATION d " +
							"WHERE d.DELEGATEASUSERNAME = '" + username + "' AND d.USERNAME = '" + delegatedBy + "')";
				}
			}else{
				qSelected += " AND MODULECODE = '" + module + "'";	
			}
			
			if(type.equals("mobile")){
				qSelected += "AND ISMOBILEREADY = 1 ";
				qSelected = qSelected.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'Approve','ReviewApprove','ReceiveDoc','SendDoc'");
			} else if(type.equals("mobiledoc")){
				qSelected += "AND ISMOBILEREADY = 1 ";
				qSelected = qSelected.replace("'Approve','ReviewApprove','Print','Assign','Review','BatchReview'"
						, "'ReceiveDoc','SendDoc'");
			} else if(type.equals("desktop")){
				qSelected += "AND ISMOBILEREADY = 1 ";
			}
			
			if(checkOffline){
				qSelected += "AND DOCNO NOT IN (SELECT DOCNO FROM MYRWF.T_PENDINGWORKFLOW) ";
			}
			
			Query querySelected = session.createSQLQuery(qSelected);
			querySelected.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			
			List<Map<String,Object>> lsVDC = querySelected.list();
			
			List<Map<String,Object>> lsCurrent = new ArrayList<Map<String,Object>>();
			
			if(lsVDC.size() > 0){
				qSelected = qSelected.replace("*", "DOCNO"); 
						
				String qCurrent = "SELECT * FROM MYRWF.VP_CURRENTWORKFLOWLINES WHERE DOCNO IN(" + qSelected
						+ ")";
				Query queryCurrent = session.createSQLQuery(qCurrent);
				queryCurrent.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				
				lsCurrent = queryCurrent.list();
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			
			for(int i = 0; i < lsVDC.size(); i++){
				try{
					JobList JL = new JobList();
					String act = lsVDC.get(i).get("ACTIVITY").toString();
					String docNo = lsVDC.get(i).get("DOCNO").toString();
					
					if(act.equals("Review") || act.equals("ReviewApprove") || act.equals("Assign") || act.equals("Print")) 
						JL.setAction(act);
					else 
						JL.setAction("Hold");
					
					JL.setReason(null);
					
					JL.setDocNo(docNo);
					JL.setWorkflowID(lsVDC.get(i).get("WORKFLOWID").toString());
					JL.setModuleCode(lsVDC.get(i).get("MODULECODE").toString());
					JL.setModuleName(lsVDC.get(i).get("MODULENAME").toString());
					if(lsVDC.get(i).get("ACTIVITY").toString().equals("ReviewApprove") && type.equals("mobile")){
						JL.setActivity("Approve");
					}else{
						JL.setActivity(lsVDC.get(i).get("ACTIVITY").toString());
					}
					
					JL.setWorkflowCode(lsVDC.get(i).get("WORKFLOWCODE").toString());
					JL.setWebName(lsVDC.get(i).get("WEBNAME").toString());
					JL.setPathID(Integer.valueOf(lsVDC.get(i).get("WORKFLOWPATHID").toString()));
					JL.setLinesID(Integer.valueOf(lsVDC.get(i).get("WORKFLOWLINESID").toString()));
					JL.setIdPararel(lsVDC.get(i).get("IDPARARELLINE").toString());
					JL.setCreatorName(lsVDC.get(i).get("CREATORNAME").toString());
					JL.setCreatedBy(lsVDC.get(i).get("CREATEDBY").toString());
					
					if(lsVDC.get(i).get("CREATEDDATE") == null){
						JL.setCreatedDate(null);
					}else{
						try{
							JL.setCreatedDate(sdf.format(sdf1.parse(lsVDC.get(i).get("CREATEDDATE").toString())));
						}catch(Exception e){}
						
					}
					JL.setLastUser("");
					JL.setLastApproveName("");
					JL.setLastDate("");
					
					if(act.equals("SendDoc") || act.equals("ReceiveDoc")){
						JL.setOTP(getOTPByDocNo(docNo));
					}else{
						JL.setOTP("-");
					}
					
					if(checkCurrentSelected(JL, lsCurrent)){
						lsData.add(JL);
					}	
				}catch(Exception e){
					System.out.println("i: " + i);
					e.printStackTrace();
				}
			}
			
			Print._msg(TAG, "getJobListDataL(" + username + ")" + "\n" + getTimeBetween(init, System.currentTimeMillis()).toString() + "\n");
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		
		return lsData;
	}
	
	// TODO: getModuleGroupByUsernameNew
	public String getModuleGroupByUsernameNew(String type, String username){
		Session session  = sfOA.openSession();
		List<JobList> lsData = new ArrayList<JobList>();
		List<ModuleGroup> lsGroup = new ArrayList<ModuleGroup>();
		
		try{
			lsData = getJobListDataL(type, username, "#SELF#", "ALL", false);
			
			for (JobList data : lsData) {
				Boolean exist = false;
				
				for (ModuleGroup mCheck : lsGroup) {
					if(mCheck.getMODULECODE().equals(data.getModuleCode())){
						mCheck.setJUMLAH(mCheck.getJUMLAH()+1);
						
						exist = true;
						break;
					}
				}
				
				if(!exist){
					ModuleGroup n = new ModuleGroup();
					n.setMODULECODE(data.getModuleCode());
					n.setMODULENAME(data.getModuleName());
					n.setWEBNAME(data.getWebName());
					n.setJUMLAH(1);
					
					lsGroup.add(n);
				}
			}
		}catch(Exception e){
			Print._err(TAG, e);
		}finally{
			session.close();
		}
		
		return new Gson().toJson(lsGroup);
	}
	
	
	


	
}
