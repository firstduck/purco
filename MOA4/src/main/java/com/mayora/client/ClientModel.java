package com.mayora.client;

import java.util.Timer;

import com.mayora.masterdata.entity.User;

public class ClientModel {
	
	private User user;
	
	private String appCode;
	
	private String ipClient;
	
	private Timer timer;
	
	public ClientModel(User user, String appCode, String ipClient, Timer timer)
	{
		this.user = user;
		this.appCode = appCode;
		this.ipClient = ipClient;
		this.timer = timer;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getIpClient() {
		return ipClient;
	}

	public void setIpClient(String ipClient) {
		this.ipClient = ipClient;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
