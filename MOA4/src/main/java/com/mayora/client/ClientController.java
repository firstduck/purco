package com.mayora.client;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.io.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mayora.base.OSValidator;
import com.mayora.client.entity.M_GcmPushNotification;
import com.mayora.client.entity.T_ClientSession;
import com.mayora.masterdata.entity.D_GroupAuthorization;
import com.mayora.masterdata.entity.M_Application;
import com.mayora.masterdata.entity.MaxGroupAccess;
import com.mayora.masterdata.entity.Module;
import com.mayora.masterdata.entity.User;
import com.mayora.masterdata.entity.UserAuthorization;
import com.mayora.masterdata.model.UserChangePassword;
import com.mayora.moa.BaseController;
import com.mayora.moa.model.JobDetail;
import com.mayora.moa.session.UserAccess;
import com.mayora.mware.tools.Print;
import com.mayora.tools.PTools;
import com.mayora.wf.WorkFlow;

@Controller
public class ClientController extends BaseController {

	String TAG = "ClientController";
	
	public Map<String, ClientModel> mapSession = new HashMap<String, ClientModel>();
	
	public String getTimeBetween(long start, long end){
        long millis = end - start;

        long msecond = (millis % 1000);
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        return String.format("%02d(h) : %02d(m) : %02d(s) : %02d(ms)", hour, minute, second, msecond);
    }
	
	
	public User getUserCheckAccess(String access)
	{
		
		User user = null;
		try
		{
			user = mapSession.get(access).getUser();
		}
		catch(Exception e) { }
		return user;
	}
	
	private String generateAccessToken(String token, String username,String password)
	{
		Date date = new Date();
		String before = String.valueOf(date.getTime()) + token + username.charAt(0)+ password.charAt(password.length()-1);
		String after = "";
		
	    try
	    {
	        MessageDigest m = MessageDigest.getInstance("MD5");
	        byte[] data = before.getBytes();
	        m.update(data,0,data.length);
	        BigInteger i= new BigInteger(1,m.digest());
	        after = String.format("%1$032X", i);
	    }
	    catch (Exception e) { }
	        
		return after;
	}
	
	@RequestMapping(value = "/Login", method = {RequestMethod.GET,RequestMethod.POST}) 
	public @ResponseBody String login(HttpServletRequest request) {
		String token = request.getParameter("access");
		
		if(userAdapter.getTokenByAppCode(token))
		{
			
			Gson gson = new Gson();
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			Boolean bolehLogin = false;
			
			if(username.contains("@test") && password.equals("@cessmobil3")){
				username = username.toUpperCase();
				username = username.replace("@test", "");
				username = username.replace("@TEST", "");
				bolehLogin = true;
			}
			else if(checkPasswordServerLama(username, password))
			{
				bolehLogin = true;
			}
			else {
				try {
					if(userAdapter.checkPassword(username.toUpperCase(), password) == 3){
						bolehLogin = true;
					}
				} catch (Exception e) {	}
			}
			
			if(bolehLogin){
				try 
				{	
					User user = userAdapter.getUserByUsername(username).get(0);
					
					String json = gson.toJson(user);  
					String accessToken = generateAccessToken(token, username, password);
					
					mapSession.put(accessToken, new ClientModel(user,token,request.getRemoteAddr(),setTimerCookieLogin(accessToken)));
					
					UserAccess ua = new UserAccess();
					ua.setDelegatedBy("#SELF#");
					ua.setUser(user);
					
					clientAdapter.addObject(new T_ClientSession(accessToken, new Gson().toJson(ua), token, request.getRemoteAddr()));
						
					
					return accessToken+"\n"+json;  
				}
				catch(Exception e) 
				{
					return "Sorry, Invalid Username or Password!"; 
				}
			}
			return "Sorry, Invalid Username or Password!";
		}
		return "Your application has no access!";
	}

	
	
	

	
	@RequestMapping(value = "" + "/Logout", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody void logout(HttpServletRequest request) { 
		try{
			mapSession.get(request.getParameter("access")).getTimer().cancel();
			mapSession.get(request.getParameter("access")).getTimer().purge();
			
			mapSession.remove(request.getParameter("access"));
			
			clientAdapter.deleteObject(new T_ClientSession(), request.getParameter("access"));
		}
		catch(Exception e) { }
	}
	
	@RequestMapping(value = "/Client/pathToJasper", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String pathToJasper(HttpServletRequest request) { 
		String path = "";
		try{
			OSValidator osValidator = new OSValidator();
	        
	        String[] pathProject = this.getClass().getProtectionDomain().getCodeSource().getLocation().toString().split("/");
	        if (osValidator.cekOS() == 1) { //windows
	        	path = this.getProjectPathWindows(pathProject, "")+ "jasper\\BarangBekas_Review.jasper";
	 
	        } else if (osValidator.cekOS() == 3) { //linux
	        	path = this.getProjectPathLinux(pathProject, "/")+ "jasper/BarangBekas_Review.jasper";
	        }
		}
		catch(Exception e) { 
			path = PTools.getExeption(e);
		}
		return path;
	}
	
	
	
	@RequestMapping(value = "/FinishDownload", method = RequestMethod.POST) 
	public @ResponseBody void deletedownloadedFile(HttpServletRequest request) { 
	
		String pathFile = request.getSession().getServletContext().getRealPath("/resources/").replace("\\", "/") +"/"+ request.getParameter("access")+"_update.jar";	

		File file = new File(pathFile);
		file.delete();

	}
	
	@RequestMapping(value = "/FinishDownloadUpdate", method = { RequestMethod.POST, RequestMethod.GET }) 
	public @ResponseBody void deletedownloadedFileUpdate(HttpServletRequest request) { 
	
		String pathFile = request.getParameter("path");	

		File file = new File(pathFile);
		file.delete();

	}
	
	@RequestMapping(value = "/CheckCookie", method = RequestMethod.POST) 
	public @ResponseBody String checkCookie(HttpServletRequest request) { 

		
		for (Entry<String, ClientModel> entry : mapSession.entrySet()) {
        if (entry.getValue().getIpClient().equals(request.getRemoteAddr()) && entry.getValue().getAppCode().equals(request.getParameter("app"))) {
	        	return entry.getKey() + "\n" + new Gson().toJson(entry.getValue().getUser());
	        }
	    }
		
		T_ClientSession clientSession = clientAdapter.getClientSessionByAppCodeIP(request.getParameter("app"), request.getRemoteAddr()); 
		if(clientSession != null)
		{
			try {
				
				mapSession.put(clientSession.getAccessCode(), new ClientModel(new Gson().fromJson(clientSession.getUserAccess(), UserAccess.class).getUser(),clientSession.getAppCode(),clientSession.getIpClient(),setTimerCookieLogin(clientSession.getAccessCode())));
						
				return clientSession.getAccessCode() + "\n" + new Gson().toJson(new Gson().fromJson(clientSession.getUserAccess(), UserAccess.class).getUser());
			}
			catch(Exception e) { }
		}
		return "false";
	}
	
	private Boolean checkPasswordServerLama(String username,String password)
	{
		try
		{
			String urlString = "http://192.168.0.152/MOA/Account/JsonLogon?UserName="+username+"&Password="+password;
			URL url = new URL(urlString);
		    URLConnection conn = url.openConnection();
		    String line;
		    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		    Boolean _result = false;
		    while ((line = reader.readLine()) != null) {
		    	if(line.equals("true")) _result = true;
		    }
		   
		    reader.close();
		    return _result;
		}
		catch(Exception e){ }
			return false;
	}
	
	private Timer setTimerCookieLogin(String access)
	{
		final String acc = access; 
		Timer t = new Timer();
		t.schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
		                // your code here
		            	mapSession.remove(acc);
		            	clientAdapter.deleteObject(new T_ClientSession(), acc);
		            }
		        }, 
		        1000*3600*24
		);
		return t;
	}
	
	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST) 
	public @ResponseBody String changePassword(HttpServletRequest request) { 

		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			String currentPassword = request.getParameter("current");
			String newPassword = request.getParameter("new");
	
			try {
				UserChangePassword changePassword = new UserChangePassword();
				changePassword.setPassword(currentPassword);
				changePassword.setNewPassword(newPassword);
				changePassword.setConfirmPassword(newPassword);
				
				Integer feedBack = userAdapter.ChangePassword(changePassword, user);
				switch (feedBack) {
				case 1 : //password tidak ditemukan
					return "Invalid password!";
				case 3 :
				{
					//sukses
					mapSession.get(request.getParameter("access")).setUser(userAdapter.getUserByUsername(user.getUsername()).get(0));
					return "Change Password Success";
				}
				default :
					return "Invalid password!";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "Error";
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/Client/upload/save", method = RequestMethod.POST)
    public @ResponseBody String save(@RequestParam List<MultipartFile> files,
    									HttpServletRequest request,
    									RedirectAttributes ra,
    									HttpSession session,
    									Model model) throws IOException {
		List<MultipartFile> listUpload;
		if(session.getAttribute("listUpload") != null) listUpload = (List<MultipartFile>) session.getAttribute("listUpload");
		else listUpload = new ArrayList<MultipartFile>();
		
		for(MultipartFile mpf: files)
		{
			listUpload.add(mpf);
 		}
		session.setAttribute("listUpload",listUpload);
		return "";
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/Client/upload/remove", method = RequestMethod.POST)
	public @ResponseBody String remove(@RequestParam String[] fileNames,
										HttpServletRequest request,
										RedirectAttributes ra,
										HttpSession session,
										Model model) {
		List<MultipartFile> listUpload = (List<MultipartFile>) session.getAttribute("listUpload");
		for (String fileName : fileNames) {
		      for(MultipartFile att : listUpload)
		      {
		    	  if(att.getOriginalFilename().equals(fileName))
		    	  {
		    		  listUpload.remove(att);
		    		  break;
		    	  }
		      } 
		}
		
		session.setAttribute("listUpload",listUpload);
		
        return "";
    }
	
	@RequestMapping(value = "/GetJobListNum", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getJobListNum(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			return clientAdapter.getJobListByUsernameNew("mobile",user.getUsername(), "#SELF#", "ALL", false).size()+"";
		}
	}
	
	@RequestMapping(value = "/GetJobListNumDoc", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getJobListNumDoc(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			return clientAdapter.getJobListByUsernameNew("mobiledoc",user.getUsername(), "#SELF#", "ALL", false).size()+"";
		}
	}
	
	@RequestMapping(value = "/GetModuleGroup", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getModuleGroup(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			return clientAdapter.getModuleGroupByUsernameNew("mobile", user.getUsername());
		}
	}
	
	@RequestMapping(value = "/GetModuleGroupDoc", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getModuleGroupDocument(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			return clientAdapter.getModuleGroupByUsernameNew("mobiledoc", user.getUsername());
		}
	}
	
	// TODO: Get Joblist Portal
	@RequestMapping(value = "/GetJobList", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getJobList(HttpServletRequest request) {

		System.out.println(request.getParameter("access"));
		
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			String moduleCode = request.getParameter("moduleCode");
			
			Gson gson = new GsonBuilder().serializeNulls().create();
			if(request.getParameter("type") != null) {
				return gson.toJson(clientAdapter.getJobListByUsernameNew(request.getParameter("type"),user.getUsername(),"#SELF#",moduleCode, false));
			}else{
				return gson.toJson(clientAdapter.getJobListByUsernameNew("mobile", user.getUsername(), "#SELF#" ,moduleCode, false));
			}
		}
	}
	
	// TODO: Get Joblist Document, sudah tidak digunakan karna untuk mobile gabung dengan joblist biasa
	@RequestMapping(value = "/GetJobListDoc", method = {RequestMethod.POST,RequestMethod.GET}) 
   	public @ResponseBody String getJobListDocument(HttpServletRequest request) {

   		User user = getUserCheckAccess(request.getParameter("access"));
   		if(user == null) return "Expired Access!";
   		else
   		{
   			String moduleCode = request.getParameter("moduleCode");
   			
   			Gson gson = new GsonBuilder().serializeNulls().create();
   			return gson.toJson(clientAdapter.getJobListByUsernameNew("mobiledoc", user.getUsername(), "#SELF#", moduleCode, false));
   		}
   	}
	
	// TODO: Get Joblist Desktop
	@RequestMapping(value = "/GetJobListOffline", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getJobListOffline(HttpServletRequest request) {

		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			String moduleCode = request.getParameter("moduleCode");
			
			Gson gson = new GsonBuilder().serializeNulls().create();
			return gson.toJson(clientAdapter.getJobListByUsernameNew("desktop", user.getUsername(), "#SELF#", moduleCode, true));
		}
	}
	
	
	@RequestMapping(value = "/IsMobileVerified", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String IsMobileVerified(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{ 
			return clientAdapter.isMobileVerified(user.getUsername(), request.getParameter("sn")).toString();
		}
	}
	
	@RequestMapping(value = "/VerifyPIN", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String VerifyPIN(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{ 
			return clientAdapter.verifyPIN(user.getUsername(), request.getParameter("sn"), request.getParameter("pin")).toString();
		}
	}
	
	@RequestMapping(value = "/CheckSN", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String CheckSN(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{ 
			return clientAdapter.checkSN(user.getUsername(), request.getParameter("sn")).toString();
		}
	}
	

	
	@RequestMapping(value = "/doRegisGcmDeviceId", method = {RequestMethod.POST})
	public @ResponseBody String doRegisGcmDeviceId(HttpServletRequest request)  {

		String nik = request.getParameter("nik");
		String gcmId = request.getParameter("gcmId");
		String gcmRegisDate = request.getParameter("gcmRegisDate");
		
		try{
			M_GcmPushNotification gcm = new M_GcmPushNotification();
			gcm.setNik(nik);
			gcm.setGcmRegisDate(new SimpleDateFormat("dd-MM-yyyy").parse(gcmRegisDate));
			gcm.setGcmId(gcmId);
			if(userAdapter.addObjectMD(gcm)) return "Data Sudah Berhasil di-input";
			else return "Data Sudah Pernah Terdaftar";
		}catch(Exception e){
			return e.getMessage(); 
		}
	}
	
	 /* MOA MOBILE*/
	 @RequestMapping(value = "/GetOutstanding", method = {RequestMethod.POST,RequestMethod.GET}) 
	 public @ResponseBody String getOutstanding(HttpServletRequest request) {
		try{
			User user = getUserCheckAccess(request.getParameter("access"));
			if(user == null) return "Expired Access!";
			else
			{ 
				return new Gson().toJson(clientAdapter.getOutstanding(user.getUsername()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	} 
	 
	 @RequestMapping(value = "/GetDone", method = {RequestMethod.POST,RequestMethod.GET}) 
	 public @ResponseBody String getDone(HttpServletRequest request) {
		try{
			User user = getUserCheckAccess(request.getParameter("access"));
			if(user == null) return "Expired Access!";
			else
			{ 
				return new Gson().toJson(clientAdapter.getDone(user.getUsername()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	} 
	 
	 @RequestMapping(value = "/GetDoneList", method = {RequestMethod.POST,RequestMethod.GET}) 
	 public @ResponseBody String getDoneList(HttpServletRequest request) {
		try{
			User user = getUserCheckAccess(request.getParameter("access"));
			if(user == null) return "Expired Access!";
			else
			{ 
				return new Gson().toJson(clientAdapter.getDoneList(user.getUsername(), request.getParameter("moduleCode")));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	} 
	 
	 @RequestMapping(value = "/GetInProgress", method = {RequestMethod.POST,RequestMethod.GET}) 
	 public @ResponseBody String getInProgress(HttpServletRequest request) {
		try{
			User user = getUserCheckAccess(request.getParameter("access"));
			if(user == null) return "Expired Access!";
			else
			{ 
				return new Gson().toJson(clientAdapter.getInProgress(user.getUsername()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	} 
	 
	 @RequestMapping(value = "/GetInProgressList", method = {RequestMethod.POST,RequestMethod.GET}) 
	 public @ResponseBody String getInProgressList(HttpServletRequest request) {
		try{
			User user = getUserCheckAccess(request.getParameter("access"));
			if(user == null) return "Expired Access!";
			else
			{ 
				return new Gson().toJson(clientAdapter.getInProgressList(user.getUsername(),request.getParameter("moduleCode")));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	} 
	/*END OF MOBILE*/
	
	 
	
	private String afterJobList(String docNo, String webName, UserAccess ua, String action){
		try {
			String urlString = "";

			if(action.equals("ap") || action.equals("a"))
				urlString = "http://"+"192.168.0.98"+":8080" + webName + "/AfterApprove/?docNo=" + docNo + "&username=" + ua.getUser().getUsername() + "&delegate=" + ua.getDelegatedBy();
			else if (action.equals("re") || action.equals("rj"))
				urlString = "http://"+"192.168.0.98"+":8080" + webName + "/AfterReject/?docNo=" + docNo + "&username=" + ua.getUser().getUsername() + "&delegate=" + ua.getDelegatedBy();
			else if (action.equals("rt"))
				urlString = "http://"+"192.168.0.98"+":8080" + webName + "/AfterReturn/?docNo=" + docNo + "&username=" + ua.getUser().getUsername() + "&delegate=" + ua.getDelegatedBy();
			
			try{
				URL url = new URL(urlString);
			    URLConnection conn = url.openConnection();

			    String line1 = "";
			    String line = "";
			    BufferedReader reader = null;
		
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			    
			    while ((line = reader.readLine()) != null) {
			    	line1 = line;
			    }
			    reader.close();
			    
			    if(line1 != null) return line1;
			}
			catch(Exception e) { }
			return null;
		}catch(Exception e) { }
		return null;
	}
	
//	@RequestMapping(value = "/GetLeaveStock", method = {RequestMethod.POST,RequestMethod.GET}) 
//	public @ResponseBody String getLeaveStock(HttpServletRequest request) {
//		try{
//			User user = getUserCheckAccess(request.getParameter("access"));
//			if(user == null) return "Expired Access!";
//			else
//			{ 
////				String url = "http://10.1.21.19:8080/ESS/UPL/GetLeaveStock";
//				String url = "http://192.168.0.98:8080/ESS/UPL/GetLeaveStock";
//				String data = "empId="+request.getParameter("empId");
//				return callUrl(url,data);
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return null;
//	} 
	
//	@RequestMapping(value = "/SaveCuti", method = {RequestMethod.POST,RequestMethod.GET}) 
//	public @ResponseBody String saveCuti(HttpServletRequest request) {
//		try{
//			User user = getUserCheckAccess(request.getParameter("access"));
//			if(user == null) return "Expired Access!";
//			else
//			{ 
////				String url = "http://10.1.21.19:8080/ESS/UPL/MobileCreate/Action";
//				String url = "http://192.168.0.98:8080/ESS/UPL/MobileCreate/Action";
//				String data = "username="+user.getUsername()+"&groupCode="+user.getGroupCode()+"&type="+request.getParameter("type")+"&empId="+request.getParameter("empId")+"&startDate="+request.getParameter("startDate")+"&endDate="+request.getParameter("endDate")+"&reason="+request.getParameter("reason");
//				return callUrl(url,data);
//			}
//		}catch(Exception e){
//			System.out.println(e);
//		}
//		return null;
//	}
	
	@RequestMapping(value = "/GetAccessibleModule", method = {RequestMethod.POST,RequestMethod.GET}) 
	public @ResponseBody String getAccessibleModule(HttpServletRequest request) {
		User user = getUserCheckAccess(request.getParameter("access"));
		if(user == null) return "Expired Access!";
		else
		{
			return new Gson().toJson(accessibleModuleAdapter.accessibleModule(user.getUsername()));

		}
	}
	
	
	@RequestMapping(value = "/GetUserPanel", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody String getUserPanel(HttpServletRequest request,
								RedirectAttributes ra,
								HttpSession session,
								Model model) throws IOException {
		try {
			return new Gson().toJson(clientAdapter.getAccessibleModule(request.getParameter("moduleCode"), request.getParameter("groupCode")));
		}
		catch(Exception e) { }
		return "";
	}
	@RequestMapping(value = "/getInbox", method = {RequestMethod.GET , RequestMethod.POST})
    public @ResponseBody String getInbox (HttpServletRequest request , RedirectAttributes ra , Model model)
    {

        String link = "http://192.168.8.80:8080/MayWare/Client/getInbox";
        String data = "username="+request.getParameter("username");
        String json = "";
        try {
            json = callUrl(link,data);
        } catch (Exception e) {
            System.out.println(e);
        }

        return json;
    }

    @RequestMapping(value = "/getTotalInbox", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody String getTotalInbox (HttpServletRequest request , RedirectAttributes ra , Model model)
    {

        String link = "http://192.168.8.80:8080/MayWare/Client/getTotalInbox";
        String data = "username="+ request.getParameter("username");
        String json = "";
        try {
            json = callUrl(link,data);
        } catch (Exception e) {
            System.out.println(e);
        }

        return json;
    } 
    
    @RequestMapping(value = "/OTP", method = {RequestMethod.POST,RequestMethod.GET}) 
   	public @ResponseBody String otp(HttpServletRequest request) {
    	String result = "";
    	try{
    		new WorkFlow().sendSMS("081299360088", "00015/D-KAR/06/2017/MYOR", "599877");
    		result = "Send SMS Success";
    	}catch(Exception e){
    		result = "Send SMS Failed";
    	}
		
    	return result;
   	}
    
 	@RequestMapping(value = "/Client/resendOTP", method = {RequestMethod.POST,RequestMethod.GET}) 
 	public @ResponseBody String resendOTP(HttpServletRequest request) { 
 		
 		String result = "";
 		
 		User user = getUserCheckAccess(request.getParameter("access"));
 		if(user == null) return "Expired Access!";
 		else
 		{
 			String link = "http://" + context.getInitParameter("APPS") + ":8080/MOA2/Client/resendOTPServer";
			String data = "docNo="+request.getParameter("docNo");
			try {
				Print._msg(TAG, "/Client/resendOTP link: \n" + link + "?" + data);
				result = callUrl(link,data);
			} catch (Exception e) {
				Print._err(TAG, e);
				
				result = "false";
			}
 			return result;
 		}
 	}
    
}
