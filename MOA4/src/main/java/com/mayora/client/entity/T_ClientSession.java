package com.mayora.client.entity;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.T_CLIENTSESSION")
public class T_ClientSession {
	
	public T_ClientSession() { }
	
	public T_ClientSession(String accessCode, String userAccess, String appCode, String ipClient) {
		this.accessCode = accessCode;
		this.userAccess = userAccess;
		this.appCode = appCode;
		this.ipClient = ipClient;
	}
	
	@Id	
	@Column(name="ACCESSCODE")
	private String accessCode;
	
	@Column(name="USERACCESS")
	private String userAccess;
	
	@Column(name="APPCODE")
	private String appCode;
	
	@Column(name="IPCLIENT")
	private String ipClient;
	
	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getUserAccess() {
		return userAccess;
	}

	public void setUserAccess(String userAccess) {
		this.userAccess = userAccess;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getIpClient() {
		return ipClient;
	}

	public void setIpClient(String ipClient) {
		this.ipClient = ipClient;
	}

	
}