package com.mayora.client.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_GCMPUSHNOTIFICATION")
public class M_GcmPushNotification {
	
	@Id	
	@Column(name="GCMID")
	private String gcmId;
	
	@Column(name="NIK")
	private String nik;
	
	@Column(name="GCMREGISDATE")
	private Date gcmRegisDate;

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public Date getGcmRegisDate() {
		return gcmRegisDate;
	}

	public void setGcmRegisDate(Date gcmRegisDate) {
		this.gcmRegisDate = gcmRegisDate;
	}
}