package com.mayora.client.entity;

public class ModuleGroup {
	String WEBNAME;
	String MODULENAME;
	String MODULECODE;
	Integer JUMLAH;
	
	public String getWEBNAME() {
		return WEBNAME;
	}
	public void setWEBNAME(String wEBNAME) {
		WEBNAME = wEBNAME;
	}
	public String getMODULENAME() {
		return MODULENAME;
	}
	public void setMODULENAME(String mODULENAME) {
		MODULENAME = mODULENAME;
	}
	public String getMODULECODE() {
		return MODULECODE;
	}
	public void setMODULECODE(String mODULECODE) {
		MODULECODE = mODULECODE;
	}
	public Integer getJUMLAH() {
		return JUMLAH;
	}
	public void setJUMLAH(Integer jUMLAH) {
		JUMLAH = jUMLAH;
	}
}
