package com.mayora.masterdata.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_UserMobile_PK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public M_UserMobile_PK() { }
	public M_UserMobile_PK(String username, String mobileNO) { 
		this.username = username;
		this.mobileNO = mobileNO;
	}
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="MOBILENO")
	private String mobileNO;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMobileNO() {
		return mobileNO;
	}
	public void setMobileNO(String mobileNO) {
		this.mobileNO = mobileNO;
	}
}
