package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_MODULE")
public class Module {

	@Id
	@NotBlank(message="Please Input ModuleCode")
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="DEFAULTMODULECODE")
	private String defaultModuleCode;
	
	@Column(name="DEFAULTMODULENAME")
	private String defaultModuleName;
	
	@Column(name="DATABASENAME")
	private String databaseName;
	
	@Column(name="TABLENAME")
	private String tableName;
	
	@Column(name="DETAILNAME")
	private String detailName;
	
	@Column(name="VIEWNAME")
	private String viewName;
	
	@Column(name="VIEWDETAILNAME")
	private String viewDetailName;
	
	@Column(name="TEMPLATEDETAILNAME")
	private String templateDetailName;
	
	@Column(name="PRIMARYKEY")
	private String primaryKey;
	
	@Column(name="BROWSENAME")
	private String browseName;
	
	@Column(name="BROWSEPRIMARYKEY")
	private String browsePrimaryKey;
	
	@Column(name="DISPLAYMEMBER")
	private String displayMember;
	
	@Column(name="WEBNAME")
	private String webName;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="CODENAME")
	private String codeName;
	
	@Column(name="ISACTIVE")
	private Integer isActive;
	
	@Column(name="ID")
	private Integer id;
	
	@Column(name="ISTESTING")
	private Integer isTesting;
	
	@Column(name="TESTINGPIC")
	private String testPIC;
	
	@Column(name="ISMOBILEREADY")
	private Integer isMobileReady;
	
	public Integer getIsTesting() {
		return isTesting;
	}
	public void setIsTesting(Integer isTesting) {
		this.isTesting = isTesting;
	}
	public String getTestPIC() {
		return testPIC;
	}
	public void setTestPIC(String testPIC) {
		this.testPIC = testPIC;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getDefaultModuleCode() {
		return defaultModuleCode;
	}
	public void setDefaultModuleCode(String defaultModuleCode) {
		this.defaultModuleCode = defaultModuleCode;
	}
	public String getDefaultModuleName() {
		return defaultModuleName;
	}
	public void setDefaultModuleName(String defaultModuleName) {
		this.defaultModuleName = defaultModuleName;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getDetailName() {
		return detailName;
	}
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public String getViewDetailName() {
		return viewDetailName;
	}
	public void setViewDetailName(String viewDetailName) {
		this.viewDetailName = viewDetailName;
	}
	public String getTemplateDetailName() {
		return templateDetailName;
	}
	public void setTemplateDetailName(String templateDetailName) {
		this.templateDetailName = templateDetailName;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getBrowseName() {
		return browseName;
	}
	public void setBrowseName(String browseName) {
		this.browseName = browseName;
	}
	public String getBrowsePrimaryKey() {
		return browsePrimaryKey;
	}
	public void setBrowsePrimaryKey(String browsePrimaryKey) {
		this.browsePrimaryKey = browsePrimaryKey;
	}
	public String getDisplayMember() {
		return displayMember;
	}
	public void setDisplayMember(String displayMember) {
		this.displayMember = displayMember;
	}
	public String getWebName() {
		return webName;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIsMobileReady() {
		return isMobileReady;
	}
	public void setIsMobileReady(Integer isMobileReady) {
		this.isMobileReady = isMobileReady;
	}
	
	
}
