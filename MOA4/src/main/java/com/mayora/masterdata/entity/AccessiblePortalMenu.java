package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_ACCESSIBLEPORTALMENU")
public class AccessiblePortalMenu {

	@Id
	@Column(name="MENUCODE")
	private String menuCode;
	
	@Column(name="MENUNAME") 
	private String menuName;
	
	@Column(name="COLOR")
	private String color;

	@Column(name="LINK")
	private String link;
	
	@Column(name="ID")
	private Integer portalMenuID;

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getPortalMenuID() {
		return portalMenuID;
	}

	public void setPortalMenuID(Integer portalMenuID) {
		this.portalMenuID = portalMenuID;
	}
	
	
}
