package com.mayora.masterdata.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

public class T_SESSIONLOG_PK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="SERVER")
	String server;

	@Column(name="USERNAME")
	String username;
	
	@Column(name="LOG_DATE")
	Date log_Date;

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getLog_Date() {
		return log_Date;
	}

	public void setLog_Date(Date log_Date) {
		this.log_Date = log_Date;
	}
	
}
