package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class D_User_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public D_User_PK() {
    }



    public D_User_PK(String username, String groupCode) {
        this.username = username;
        this.groupCode = groupCode;
    }


	@Column(name="USERNAME")
	private String username;
	
	@Column(name="GROUPCODE")
	private String groupCode;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
}
