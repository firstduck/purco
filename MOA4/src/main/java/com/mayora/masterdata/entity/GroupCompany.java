package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_GROUPCOMPANY")
public class GroupCompany {

	@Id
	@Column(name="GROUPCODE")
	private String groupCompanyCode;
	
	@Column(name="GROUPNAME")
	private String groupName;

	@Column(name="SHORTID")
	private Integer shortId;

	public String getGroupCompanyCode() {
		return groupCompanyCode;
	}

	public void setGroupCompanyCode(String groupCode) {
		this.groupCompanyCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getShortId() {
		return shortId;
	}

	public void setShortId(Integer shortId) {
		this.shortId = shortId;
	}

}
