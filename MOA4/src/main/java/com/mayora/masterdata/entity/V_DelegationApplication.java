package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_DELEGATIONAPPLICATION")
public class V_DelegationApplication {

	@Id
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME") 
	private String applicationName;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="DELEGATEASUSERNAME")
	private String delegateAsUsername;
	
	@Column(name="MENUCODE")
	private String menuCode;
	
	@Column(name="LINK")
	private String link;
	
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDelegateAsUsername() {
		return delegateAsUsername;
	}
	public void setDelegateAsUsername(String delegateAsUsername) {
		this.delegateAsUsername = delegateAsUsername;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
