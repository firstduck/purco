package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_GROUPACCESS")
public class V_GroupAccess {
	
	@EmbeddedId
	private V_GroupAccess_PK vgaPK;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME")
	private String applicationName;
	
	@Column(name="ACCESS_")
	private Boolean access_;
	
	@Column(name="VIEWALL")
	private Boolean viewAll;
	
	@Column(name="INSERT_")
	private Boolean insert_;
	
	@Column(name="EDIT")
	private Boolean edit;
	
	@Column(name="DELETE_")
	private Boolean delete_;
	
	@Column(name="PRINT")
	private Boolean print;
	
	@Column(name="REVIEW")
	private Boolean review;
	
	@Column(name="APPROVE")
	private Boolean approve;
	
	@Column(name="ID")
	private Integer idGroupAccess;

	@Column(name="IDAPPLICATION")
	private Integer idApplication;

	@Column(name="WEBNAME")
	private String webName;

	@Column(name="CODENAME")
	private String codeName;
	
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Boolean getAccess_() {
		return access_;
	}

	public void setAccess_(Boolean access_) {
		this.access_ = access_;
	}

	public Boolean getViewAll() {
		return viewAll;
	}

	public void setViewAll(Boolean viewAll) {
		this.viewAll = viewAll;
	}

	public Boolean getInsert_() {
		return insert_;
	}

	public void setInsert_(Boolean insert_) {
		this.insert_ = insert_;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public Boolean getDelete_() {
		return delete_;
	}

	public void setDelete_(Boolean delete_) {
		this.delete_ = delete_;
	}

	public Boolean getPrint() {
		return print;
	}

	public void setPrint(Boolean print) {
		this.print = print;
	}

	public Boolean getReview() {
		return review;
	}

	public void setReview(Boolean review) {
		this.review = review;
	}

	public Boolean getApprove() {
		return approve;
	}

	public void setApprove(Boolean approve) {
		this.approve = approve;
	}
	
	public Integer getIdGroupAccess() {
		return idGroupAccess;
	}

	public void setIdGroupAccess(Integer idGroupAccess) {
		this.idGroupAccess = idGroupAccess;
	}

	public Integer getIdApplication() {
		return idApplication;
	}

	public void setIdApplication(Integer idApplication) {
		this.idApplication = idApplication;
	}

	public String getWebName() {
		return webName;
	}

	public void setWebName(String webName) {
		this.webName = webName;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public V_GroupAccess_PK getVgaPK() {
		return vgaPK;
	}

	public void setVgaPK(V_GroupAccess_PK vgaPK) {
		this.vgaPK = vgaPK;
	}
}
