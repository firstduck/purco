package com.mayora.masterdata.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_USER_MOBILE")
public class M_UserMobile {
	
	@EmbeddedId
	private M_UserMobile_PK pk;
	
	@Column(name="SN")
	private String sn;
	
	@Column(name="STARTDATE")
	private Date startDate;
	
	@Column(name="ENDDATE")
	private Date endDate;
	
	@Column(name="PIN")
	private String pin;
	
	@Column(name="ISVERIFIED")
	private Boolean isVerified;
	
	public M_UserMobile_PK getPk() {
		return pk;
	}

	public void setPk(M_UserMobile_PK pk) {
		this.pk = pk;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
