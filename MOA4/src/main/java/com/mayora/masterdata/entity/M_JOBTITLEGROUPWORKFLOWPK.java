package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_JOBTITLEGROUPWORKFLOW")
public class M_JOBTITLEGROUPWORKFLOWPK {
	
	@Id
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;
	
	@Column(name="JOBTITLECODE")
	private String jobTitleCode;
	
	@Column(name="JOBTITLENAME")
	private String jobTitleName;
	
	@Column(name="GROUPWORKFLOWNAME")
	private String groupWorkflowName;

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public String getJobTitleCode() {
		return jobTitleCode;
	}

	public void setJobTitleCode(String jobTitleCode) {
		this.jobTitleCode = jobTitleCode;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}

	public String getGroupWorkflowName() {
		return groupWorkflowName;
	}

	public void setGroupWorkflowName(String groupWorkflowName) {
		this.groupWorkflowName = groupWorkflowName;
	}

	
	

}
