package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.VD_GroupAuthorization")
public class VD_GroupAuthorization {
	
	@EmbeddedId
	private VD_GroupAuthorization_PK vd_gauthPK;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="VALUE")
	private String value;

	public VD_GroupAuthorization_PK getVd_gauthPK() {
		return vd_gauthPK;
	}

	public void setVd_gauthPK(VD_GroupAuthorization_PK vd_gauthPK) {
		this.vd_gauthPK = vd_gauthPK;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

