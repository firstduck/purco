package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="MYRMD.D_GroupAuthorization")
public class D_GroupAuthorization {
	
	@EmbeddedId
	private D_GroupAuthorization_PK dgauthPK;
	
	@Column(name="VALUE")
	private String value;

	public D_GroupAuthorization_PK getDgauthPK() {
		return dgauthPK;
	}

	public void setDgauthPK(D_GroupAuthorization_PK dgauthPK) {
		this.dgauthPK = dgauthPK;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
