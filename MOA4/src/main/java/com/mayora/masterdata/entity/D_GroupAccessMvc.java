package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRMD.D_GroupAccessMvc")
public class D_GroupAccessMvc {

	@EmbeddedId
	private D_GroupAccessMvc_PK pk;
	
	@Column(name="ID")
	private Integer id;
	
	@Column(name="ACCESS_")
	private Boolean access_;
	
	@Column(name="CONTROLLER")
	private String controller;

	public D_GroupAccessMvc_PK getPk() {
		return pk;
	}

	public void setPk(D_GroupAccessMvc_PK pk) {
		this.pk = pk;
	}

	public Boolean getAccess_() {
		return access_;
	}

	public void setAccess_(Boolean access_) {
		this.access_ = access_;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}