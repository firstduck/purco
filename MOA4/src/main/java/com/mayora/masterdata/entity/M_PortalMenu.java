package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_PORTALMENU")
public class M_PortalMenu {
	
	@Id
	@Column(name="ID")
	private Integer portalMenuID;
	
	@Column(name="MENUCODE") 
	private String menuCode;
	
	@Column(name="MENUNAME")
	private String menuName;
	
	@Column(name="LINK")
	private String link;
	
	@Column(name="COLOR")
	private String color;
	
	@Transient
	private String colorDark;
	
	public String getColorDark() {
		return colorDark;
	}

	public void setColorDark(String colorDark) {
		this.colorDark = colorDark;
	}

	public Integer getPortalMenuID() {
		return portalMenuID;
	}

	public void setPortalMenuID(Integer portalMenuID) {
		this.portalMenuID = portalMenuID;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
}
