package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="MYRMD.V_ACCESSIBLEMODULE")
public class AccessibleModule {
	
	public AccessibleModule() { }
	public AccessibleModule(String moduleName, String applicationCode, String applicationName, String moduleCode, String webName, String delegateAsUsername, String link, String menuCode)
	{
		this.pk = new AccessibleModule_PK(moduleCode, delegateAsUsername);
		this.moduleName = moduleName;
		this.applicationCode = applicationCode;
		this.applicationName = applicationName;
		this.moduleCode = moduleCode;
		this.webName = webName;
		this.username = delegateAsUsername;
		this.link = link;
		this.menuCode = menuCode;
	}
	
	@Transient
	private String moduleCode;
	
	@EmbeddedId
	private AccessibleModule_PK pk;
	
	@Column(name="GROUPCODEUSER")
	private String groupCodeUser;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME")
	private String applicationName;
	
	@Transient
	private String username;
	
	@Column(name="WEBNAME")
	private String webName;
	
	@Column(name="MENUCODE")
	private String menuCode;
	
	@Column(name="LINK") 
	private String link;
	
	
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getModuleCode() {
		return this.pk.getModuleCode();
	}
	public void setModuleCode(String moduleCode) {
		try 
		{
			this.pk.setModuleCode(moduleCode);
		}
		catch(Exception e)
		{
			this.setPk(new AccessibleModule_PK());
			this.pk.setModuleCode(moduleCode);
		}
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getUsername() {
		return this.pk.getUsername();
	}
	public void setUsername(String username) {
		try 
		{
			this.pk.setUsername(username);
		}
		catch(Exception e)
		{
			this.setPk(new AccessibleModule_PK());
			this.pk.setUsername(username);
		}
	}
	
	public String getWebName() {
		return webName;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	public AccessibleModule_PK getPk() {
		return pk;
	}
	public void setPk(AccessibleModule_PK pk) {
		this.pk = pk;
	}
	public String getGroupCodeUser() {
		return groupCodeUser;
	}
	public void setGroupCodeUser(String groupCodeUser) {
		this.groupCodeUser = groupCodeUser;
	}
	
}
