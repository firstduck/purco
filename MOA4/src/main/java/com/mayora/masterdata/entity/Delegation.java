package com.mayora.masterdata.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_DELEGATION")
public class Delegation {
	
	@EmbeddedId
    private Delegation_PK delegationPK;
	
	@Column(name="STARTDATE")
	private Timestamp startDate;
	
	@Column(name="ENDDATE")
	private Timestamp endDate;

	public Delegation_PK getDelegationPK() {
		return delegationPK;
	}

	public void setDelegationPK(Delegation_PK delegationPK) {
		this.delegationPK = delegationPK;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
}
