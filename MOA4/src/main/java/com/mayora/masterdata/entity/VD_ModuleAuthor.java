package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.VD_MODULEAUTHOR")
public class VD_ModuleAuthor {
	
	@EmbeddedId
	private VD_ModuleAuthor_PK vd_maPK;
	 
	@Column(name="MODULENAME")
	private String moduleName;

	public VD_ModuleAuthor_PK getVd_maPK() {
		return vd_maPK;
	}

	public void setVd_maPK(VD_ModuleAuthor_PK vd_maPK) {
		this.vd_maPK = vd_maPK;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
}
