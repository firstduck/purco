package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_JOBTITLEACCESS")
public class M_JOBTITLEACCESSPK {

	@Id
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="JOBTITLECODE")
	private String jobTitleCode;
	
	@Column(name="GROUPNAME")
	private String groupName;
	
	@Column(name="JOBTITLENAME")
	private String jobTitleName;
	
	@Column(name="PLANTCODE")
	private String plantCode;

	public String getGroupCode() {
		return groupCode;
	}

	public String getJobTitleCode() {
		return jobTitleCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}


	public String getPlantCode() {
		return plantCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public void setJobTitleCode(String jobTitleCode) {
		this.jobTitleCode = jobTitleCode;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}


	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}
	
	
}
