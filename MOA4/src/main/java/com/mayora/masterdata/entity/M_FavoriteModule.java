package com.mayora.masterdata.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_FAVORITEMODULE")
public class M_FavoriteModule {
	
	@EmbeddedId
	M_FavoriteModule_PK pk;

	public M_FavoriteModule_PK getPk() {
		return pk;
	}

	public void setPk(M_FavoriteModule_PK pk) {
		this.pk = pk;
	}
	
}
