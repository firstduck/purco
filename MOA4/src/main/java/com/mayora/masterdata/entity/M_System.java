package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_SYSTEM")
public class M_System {
	
	@Id
	@Column(name="SYSTEMCODE")
	@NotBlank(message="Please Input SystemCode")
	private String systemCode;
	
	@Column(name="SYSTEMNAME") 
	@NotBlank(message="Please Input SystemName")
	private String systemName;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="ENABLED")
	private Boolean enabled;
	
	@Column(name="ID")
	private Integer id;
	
	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Integer getID() {
		return id;
	}
	public void setID(Integer id) {
		this.id = id;
	}
}
