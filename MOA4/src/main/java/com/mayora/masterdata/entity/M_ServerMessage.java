package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRMD.M_SERVERMESSAGE")
public class M_ServerMessage{

	@EmbeddedId
	private M_ServerMessage_PK serverMessagePK;
	
	@Column(name="MESSAGE")
	private String message;
	
	@Column(name="ISDELETED")
	private Boolean isDeleted;
	
	@Transient
	private String dummySysCode;
	
	public String getDummySysCode() {
		return this.serverMessagePK.getSystemCode();
	}

	public M_ServerMessage_PK getServerMessagePK() {
		return serverMessagePK;
	}

	public void setServerMessagePK(M_ServerMessage_PK serverMessagePK) {
		this.serverMessagePK = serverMessagePK;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
