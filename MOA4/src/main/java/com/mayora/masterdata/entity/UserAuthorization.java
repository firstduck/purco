package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.V_USERAUTHORIZATION")
public class UserAuthorization {
	
	@Column(name="USERNAME") 
	private String username;
	
	@Id
	@Column(name="ID") 
	private String id;
	
	@Column(name="VALUE") 
	private String value;
	
	@Column(name="MODULECODE") 
	private String moduleCode;
	
	@Column(name="MODULENAME") 
	private String moduleName;
	
	@Column(name="PRIMARYKEY") 
	private String primaryKey;
	
	@Column(name="BROWSEPRIMARYKEY") 
	private String browsePK;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getBrowsePK() {
		return browsePK;
	}
	public void setBrowsePK(String browsePK) {
		this.browsePK = browsePK;
	}
	

}
