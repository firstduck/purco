package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="MYRMD.M_CURRENCY")
public class M_CURRENCY_OPTION {
	
	@Id
	@Column(name="CURRENCYCODE")
	private String currencyCode;
	
	@Column(name="CURRENCYNAME")
	private String currencyName;
	
	@Column(name="SYMBOL")
	private String symbol;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	
}
