package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.D_BRANCH")
public class D_Branch {

	@Id	
	@Column(name="SUBBRANCHCODE")
	private String subBranchCode;
	
	@Column(name="PLANTCODE")
	private String plantCode;
	
	@Column(name="BRANCHCODE")
	private String branchCode;
	
	@Column(name="SUBBRANCHNAME")
	private String subBranchName;

	public String getSubBranchCode() {
		return subBranchCode;
	}

	public void setSubBranchCode(String subBranchCode) {
		this.subBranchCode = subBranchCode;
	}

	public String getPlantCode() {
		return plantCode;
	}

	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getSubBranchName() {
		return subBranchName;
	}

	public void setSubBranchName(String subBranchName) {
		this.subBranchName = subBranchName;
	}
}
