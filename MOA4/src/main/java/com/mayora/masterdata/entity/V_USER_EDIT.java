package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name="MYRMD.V_USER_EDIT")
public class V_USER_EDIT {
	
	@Id
	@Column(name="NAME")
	private String name;
	
	@Column (name="USERNAME")
	private String username;
	
	@Column (name="GROUPCODE")
	private String groupCode;
	
	@Column(name="GROUPNAME")
	private String groupName;
	
	@Column(name="EMPID")
	private String empId;
	
	@Column(name="JOBTITLENAME")
	private String jobTitleName;
	
	@Column(name="JOBTITLENAME2")
	private String jobTitleName2;
	
	@Column(name="FLAG")
	private Boolean flag;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupCode() {
		return groupCode;
	}
	
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}

	public String getJobTitleName2() {
		return jobTitleName2;
	}

	public void setJobTitleName2(String jobTitleName2) {
		this.jobTitleName2 = jobTitleName2;
	}


	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	
	
	
	
	
	


}
