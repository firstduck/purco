package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_FavoriteModule_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="USERNAME")
	private String username;
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="ID_")
	private Integer id_;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Integer getId_() {
		return id_;
	}

	public void setId_(Integer id_) {
		this.id_ = id_;
	}
}
