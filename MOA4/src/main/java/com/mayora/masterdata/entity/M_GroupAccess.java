package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_GROUPACCESS")
public class M_GroupAccess {

	@Id
	@Column(name="GROUPCODE")
	@NotBlank(message="Please Input Group Code")
	private String groupCode;

	@Column(name="GROUPNAME")
	@NotBlank(message="Please Input Group Name")
	private String groupName;
	
	@Column(name="DESCRIPTION")
	private String description;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
