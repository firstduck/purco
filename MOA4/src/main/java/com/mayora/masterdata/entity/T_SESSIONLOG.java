package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.T_SESSIONLOG")
public class T_SESSIONLOG {
	
	@EmbeddedId
	private T_SESSIONLOG_PK logPK;
	
	@Column(name="ACTIVITY")
	String activity;
	
	@Column(name="MODULECODE")
	String moduleCode;

	public T_SESSIONLOG_PK getLogPK() {
		return logPK;
	}

	public void setLogPK(T_SESSIONLOG_PK logPK) {
		this.logPK = logPK;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

}
