package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_JOBTITLEGROUPWORKFLOW")
public class M_JOBTITLEGROUPWORKFLOW {
	
	public M_JOBTITLEGROUPWORKFLOW() {
	}

	public M_JOBTITLEGROUPWORKFLOW(M_JOBTITLEGROUPWORKFLOW_PK pk,
			String jobTitleName, String groupWorkflowName) {
		this.pk = pk;
		this.jobTitleName = jobTitleName;
		this.groupWorkflowName = groupWorkflowName;
	}

	@EmbeddedId
	private M_JOBTITLEGROUPWORKFLOW_PK pk;
	
	@Column(name="JOBTITLENAME")
	private String jobTitleName;
	
	@Column(name="GROUPWORKFLOWNAME")
	private String groupWorkflowName;

	public M_JOBTITLEGROUPWORKFLOW_PK getPk() {
		return pk;
	}

	public void setPk(M_JOBTITLEGROUPWORKFLOW_PK pk) {
		this.pk = pk;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}

	public String getGroupWorkflowName() {
		return groupWorkflowName;
	}

	public void setGroupWorkflowName(String groupWorkflowName) {
		this.groupWorkflowName = groupWorkflowName;
	}
	
	
	
}
