package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_OPERATIONCALENDAR")

public class V_OPERATION_CALENDER {
		
	@Id
	@Column(name = "ID_")
	private String id;
	
	@Column(name = "YEAR")
	private String year;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "MONTHTEXT")
	private String monthText;
	
	@Column(name = "WEEKOFMONTH")
	private String weekOfMonth;
	
	@Column(name = "WEEKSOFMONTHTEXT")
	private String weeksOfMonthText;
	
	@Column(name = "WEEK")
	private String week;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMonthText() {
		return monthText;
	}

	public void setMonthText(String monthText) {
		this.monthText = monthText;
	}

	public String getWeekOfMonth() {
		return weekOfMonth;
	}

	public void setWeekOfMonth(String weekOfMonth) {
		this.weekOfMonth = weekOfMonth;
	}

	public String getWeeksOfMonthText() {
		return weeksOfMonthText;
	}

	public void setWeeksOfMonthText(String weeksOfMonthText) {
		this.weeksOfMonthText = weeksOfMonthText;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}
	
	
}
