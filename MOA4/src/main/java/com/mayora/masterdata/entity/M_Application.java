package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_APPLICATION")
public class M_Application {
	
	@Id
	@Column(name="APPLICATIONCODE")
	@NotBlank(message="Please Input ApplicationCode")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME") 
	@NotBlank(message="Please Input ApplicationName")
	private String applicationName;

	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DEVELOPMENTLANGUAGE")
	private String developmentLanguage;
	
	@Column(name="TYPE")
	private Character type;
	
	@Column(name="ISACTIVE")
	private Boolean isActive;
	
	@Column(name="SYSTEMCODE")
	private String systemCode;
	
	@Column(name="ID")
	private Integer id;

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDevelopmentLanguage() {
		return developmentLanguage;
	}

	public void setDevelopmentLanguage(String developmentLanguage) {
		this.developmentLanguage = developmentLanguage;
	}

	public Character getType() {
		return type;
	}

	public void setType(Character type) {
		this.type = type;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
