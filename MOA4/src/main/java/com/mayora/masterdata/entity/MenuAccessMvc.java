package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_MENUACCESSMVC")
public class MenuAccessMvc {
	
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="WEBNAME")
	private String webName;
	 
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="MENU")
	private String menu;
	
	@Column(name="CONTROLLER")
	private String controller;
	
	@Column(name="ACTION")
	private String action;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ACCESS_")
	private Integer access_;
	
	@Column(name="DEFAULT_")
	private Integer default_;
	
	public String getWebName() {
		return webName;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public String getController() {
		return controller;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getAccess_() {
		return access_;
	}
	public void setAccess_(Integer access_) {
		this.access_ = access_;
	}
	public Integer getDefault_() {
		return default_;
	}
	public void setDefault_(Integer default_) {
		this.default_ = default_;
	}
}
