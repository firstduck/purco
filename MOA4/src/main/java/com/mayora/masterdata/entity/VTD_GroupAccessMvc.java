package com.mayora.masterdata.entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYRMD.VTD_GROUPACCESSMVC")
public class VTD_GroupAccessMvc {

	@EmbeddedId
	private VTD_GroupAccessMvc_PK pk;

	@Transient
	private String dummyAction;
	
	public String getDummyAction() {
		return pk.getActionMenu();
	}

	public void setDummyAction(String dummyAction) {
		this.dummyAction = dummyAction;
	}

	public VTD_GroupAccessMvc_PK getPk() {
		return pk;
	}

	public void setPk(VTD_GroupAccessMvc_PK pk) {
		this.pk = pk;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Boolean getAccess_() {
		return access_;
	}

	public void setAccess_(Boolean access_) {
		this.access_ = access_;
	}

	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME")
	private String applicationName;
	
	@Column(name="ACCESS_")
	private Boolean access_;
	
	@Column(name="CONTROLLER")
	private String controller;
	
	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	@Column(name="ID")
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}