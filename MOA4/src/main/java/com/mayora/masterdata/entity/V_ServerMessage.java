package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_SERVERMESSAGE")
public class V_ServerMessage{
	
	
	@Column(name="SYSTEMCODE")
	private String systemCode;
	
	@Column(name="ID")
	private Integer index;
	
	@Column(name="MESSAGE")
	private String message;
	
	@Column(name="ISDELETED")
	private Boolean isDeleted;

	@Id
	@Column(name="ROW_ID")
	private String id_Row;

	
	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getId_Row() {
		return id_Row;
	}

	public void setId_Row(String id_Row) {
		this.id_Row = id_Row;
	}
	
	
}
