package com.mayora.masterdata.entity;

public class VM_COMMENT_PK {


	private String docNo; 
	private Integer index;

	public String getDocNo() {
		return docNo;
	}

	public Integer getIndex() {
		return index;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
	
	
}
