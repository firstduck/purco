package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AccessibleModule_PK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public AccessibleModule_PK() { }
	public AccessibleModule_PK(String moduleCode, String username) {
		this.moduleCode = moduleCode;
		this.username = username;
	}
	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="USERNAME")
	private String username;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
