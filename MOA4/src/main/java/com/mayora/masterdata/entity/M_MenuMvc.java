package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
//import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="M_MENUMVC")
public class M_MenuMvc {

	@EmbeddedId
	private M_MenuMvc_PK pk;
	
//	@Id
//	@Column(name="MODULECODE")
//	private String moduleCode;
//	
	@Transient
	private String kendoId;
	
//
//	@Column(name="ID")
//	private Integer id;
//	
	@Column(name="MENU")
	private String menu;
	
	@Column(name="CONTROLLER")
	private String controller;
	
	@Column(name="ACTION")
	private String action;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="ISMENU")
	private Boolean isMenu;

	@Transient
	private Integer dummyId;
	
	@Transient
	private String dummyModuleCode;
	
	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsMenu() {
		return isMenu;
	}

	public void setIsMenu(Boolean isMenu) {
		this.isMenu = isMenu;
	}

	public M_MenuMvc_PK getPk() {
		return pk;
	}

	public void setPk(M_MenuMvc_PK pk) {
		this.pk = pk;
	}

	
	
	
	public Integer getDummyId() {
		return this.pk.getId();
	}

	public void setDummyId(Integer dummyId) {
		this.dummyId = dummyId;
	}

	public String getDummyModuleCode() {
		return this.pk.getModuleCode();
	}

	public void setDummyModuleCode(String dummyModuleCode) {
		this.dummyModuleCode = dummyModuleCode;
	}

//	public String getModuleCode() {
//		return moduleCode;
//	}
//
//	public void setModuleCode(String moduleCode) {
//		this.moduleCode = moduleCode;
//	}
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
	

	public String getKendoId() {
		return kendoId;
	}

	public void setKendoId(String kendoId) {
		this.kendoId = kendoId;
	}
}
