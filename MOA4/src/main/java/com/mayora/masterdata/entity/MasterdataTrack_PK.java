package com.mayora.masterdata.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MasterdataTrack_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="DOCNO")
	private String docNo;
	
	@Column(name="DATE_")
	private Date date_;

	public MasterdataTrack_PK(){
		
	}
	
	public MasterdataTrack_PK(String docNo, Date date){
		this.docNo = docNo;
		this.date_ = date;
	}
	
	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public Date getDate_() {
		return date_;
	}

	public void setDate_(Date date_) {
		this.date_ = date_;
	}
}
