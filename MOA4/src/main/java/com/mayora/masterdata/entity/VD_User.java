package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.VD_USER")
public class VD_User {
	
	@EmbeddedId
    private VD_User_PK vd_userPK;

	@Column(name= "NAME")
	private String name;

	@Column(name= "GROUPNAME")
	private String groupName;
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public VD_User_PK getVd_userPK() {
		return vd_userPK;
	}
	public void setVd_userPK(VD_User_PK vd_userPK) {
		this.vd_userPK = vd_userPK;
	}
}
