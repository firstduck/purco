package com.mayora.masterdata.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_DistinctDelegation_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="DELEGATEASUSERNAME")
	private String delegateAsUsername;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDelegateAsUsername() {
		return delegateAsUsername;
	}

	public void setDelegateAsUsername(String delegateAsUsername) {
		this.delegateAsUsername = delegateAsUsername;
	}
}
