package com.mayora.masterdata.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

 @Entity
 @Table(name = "MYRMD.D_USER")
 public class D_User {
	 
	 public D_User() {

	    }


	    public D_User(D_User_PK userPK) {
	        this.userPK = userPK;
	    } 

    @EmbeddedId
    private D_User_PK userPK;

	public D_User_PK getUserPK() {
		return userPK;
	}

	public void setUserPK(D_User_PK userPK) {
		this.userPK = userPK;
	}
 }
