package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_USER")
public class M_User {

	@Id
	@Column(name="USERNAME")
	private String userName;
	
	@Column(name="PASSWORD")
	private String passWord;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="EMPID")
	private String empId;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="ISDELETED")
	private String isDeleted;
	
	@Column(name="USINGLDAP")
	private String usingLDAP;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUsingLDAP() {
		return usingLDAP;
	}

	public void setUsingLDAP(String usingLDAP) {
		this.usingLDAP = usingLDAP;
	}
	
	
}
