package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class M_JOBTITLEGROUPWORKFLOW_PK implements Serializable {
	
	public static final long serialVersion =  1L;
	
	public M_JOBTITLEGROUPWORKFLOW_PK() 
	{
		
	}

	public M_JOBTITLEGROUPWORKFLOW_PK(String jobTitleCode,
			String groupWorkflowCode) 
	{
		
		this.jobTitleCode = jobTitleCode;
		this.groupWorkflowCode = groupWorkflowCode;
	}

	@Column(name="JOBTITLECODE")
	private String jobTitleCode;
	
	@Column(name="GROUPWORKFLOWCODE")
	private String groupWorkflowCode;

	public String getJobTitleCode() {
		return jobTitleCode;
	}

	public void setJobTitleCode(String jobTitleCode) {
		this.jobTitleCode = jobTitleCode;
	}

	public String getGroupWorkflowCode() {
		return groupWorkflowCode;
	}

	public void setGroupWorkflowCode(String groupWorkflowCode) {
		this.groupWorkflowCode = groupWorkflowCode;
	}

	public static long getSerialversion() {
		return serialVersion;
	}
	
	
	
}
