package com.mayora.masterdata.entity;

import java.util.Date;

public class VM_COMMENT {

	private VM_COMMENT_PK pk;
	
	private String notesType;

	private String notes;
	
	private Date createdDate;
	
	private String username;
	
	private String name;

	public String getNotesType() {
		return notesType;
	}

	public String getNotes() {
		return notes;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public void setNotesType(String notesType) {
		this.notesType = notesType;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public void setName(String name) {
		this.name = name;
	}

	public VM_COMMENT_PK getPk() {
		return pk;
	}

	public void setPk(VM_COMMENT_PK pk) {
		this.pk = pk;
	}
	
	
	
	
}
