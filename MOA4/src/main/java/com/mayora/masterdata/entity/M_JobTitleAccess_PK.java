package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_JobTitleAccess_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public M_JobTitleAccess_PK(String groupCode, String jobTitleCode) {
		super();
		this.groupCode = groupCode;
		this.jobTitleCode = jobTitleCode;
	}
	
	public M_JobTitleAccess_PK()
	{
		
	}

	@Column(name="GROUPCODE")
	public String groupCode;
	
	@Column(name="JOBTITLECODE")
	public String jobTitleCode;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getJobTitleCode() {
		return jobTitleCode;
	}

	public void setJobTitleCode(String jobTitleCode) {
		this.jobTitleCode = jobTitleCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
