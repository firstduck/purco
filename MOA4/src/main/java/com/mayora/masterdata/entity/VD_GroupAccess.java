package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VD_GROUPACCESS")
public class VD_GroupAccess {

	@Column(name="GROUPCODE")
	private String groupCode;

	@Id
	@Column(name="MODULECODE")
	private String moduleCode;
    
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME")
	private String applicationName;
	
	@Column(name="ACCESS_")
	private Boolean access_;
	
	@Column(name="VIEWALL")
	private Boolean viewAll;
	
	@Column(name="INSERT_")
	private Boolean insert_;
	
	@Column(name="EDIT")
	private Boolean edit;
	
	@Column(name="DELETE_")
	private Boolean delete_;
	
	@Column(name="PRINT")
	private Boolean print;
	
	@Column(name="REVIEW")
	private Boolean review;
	
	@Column(name="APPROVE")
	private Boolean approve;
	
	@Column(name="ADM")
	private Boolean adm;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Boolean getAdm() {
		return adm;
	}

	public void setAdm(Boolean adm) {
		this.adm = adm;
	}

//	public VD_GroupAccess_PK getVdgaPK() {
//		return vdgaPK;
//	}
//
//	public void setVdgaPK(VD_GroupAccess_PK vdgaPK) {
//		this.vdgaPK = vdgaPK;
//	}

	public Boolean getADM() {
		return adm;
	}

	public void setADM(Boolean adm) {
		this.adm = adm;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Boolean getAccess_() {
		return access_;
	}

	public void setAccess_(Boolean access_) {
		this.access_ = access_;
	}

	public Boolean getViewAll() {
		return viewAll;
	}

	public void setViewAll(Boolean viewAll) {
		this.viewAll = viewAll;
	}

	public Boolean getInsert_() {
		return insert_;
	}

	public void setInsert_(Boolean insert_) {
		this.insert_ = insert_;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public Boolean getDelete_() {
		return delete_;
	}

	public void setDelete_(Boolean delete_) {
		this.delete_ = delete_;
	}

	public Boolean getPrint() {
		return print;
	}

	public void setPrint(Boolean print) {
		this.print = print;
	}

	public Boolean getReview() {
		return review;
	}

	public void setReview(Boolean review) {
		this.review = review;
	}

	public Boolean getApprove() {
		return approve;
	}

	public void setApprove(Boolean approve) {
		this.approve = approve;
	}
}
