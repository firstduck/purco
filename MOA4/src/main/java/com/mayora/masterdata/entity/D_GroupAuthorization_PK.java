package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class D_GroupAuthorization_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="ID")
	private Integer Id;
	
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="MODULECODE")
	private String moduleCode;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
}
