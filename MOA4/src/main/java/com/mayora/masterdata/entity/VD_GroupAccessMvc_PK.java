package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VD_GroupAccessMvc_PK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="GROUPCODE")
	private String groupCode;

	@Column(name="MODULECODE")
	private String moduleCode;	
	
	@Column(name="MENU")
	private String actionMenu;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	

	public String getActionMenu() {
		return actionMenu;
	}

	public void setActionMenu(String actionMenu) {
		this.actionMenu = actionMenu;
	}
}
