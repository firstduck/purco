package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_USER")
public class User {
	
	@Id
	@Column(name="USERNAME")
	@NotBlank(message="Please Input Username")
	private String username;
	
	@Column(name="PASSWORD") 
	@NotBlank(message="Please Input Password")
	private String password;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="EMPID")
	private String empId;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="ISDELETED")
	private Boolean isDeleted;
	
	@Column(name="USINGLDAP")
	private Boolean usingLDAP;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Boolean getUsingLDAP() {
		return usingLDAP;
	}
	public void setUsingLDAP(Boolean usingLDAP) {
		this.usingLDAP = usingLDAP;
	}
}
