package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class M_ServerMessage_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="SYSTEMCODE")
	private String systemCode;
	
	@Column(name="ID")
	private Integer index;

	public M_ServerMessage_PK (){
		
	}
	
	public M_ServerMessage_PK (Integer index){
		this.index = index;
	}
	
	public M_ServerMessage_PK (Integer index, String systemCode){
		this.index = index;
		this.systemCode = systemCode;
	}
	
	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
}
