package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_PLANT")
public class Plant {
	
	@Id	
	@Column(name="PLANTCODE")
	private String plantCode;
	
	@Column(name="PLANTNAME")
	private String plantName;
	
	@Column(name="COMPANYCODE")
	private String companyCode;
	
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="TYPE")
	private String type;
	
	@Column(name="PIC")
	private String pic;
	
	@Column(name="ITSUPPORTGROUP")
	private String itSupportGroup;
	
	@Column(name="PLANTGROUP")
	private String plantGroup;
	
	@Column(name="FACTORY")
	private String factory;
	
	@Column(name="PUZZLECODE")
	private String puzzleCode;
	
	public String getPlantCode() {
		return plantCode;
	}
	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}
	public String getPlantName() {
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getItSupportGroup() {
		return itSupportGroup;
	}
	public void setItSupportGroup(String itSupportGroup) {
		this.itSupportGroup = itSupportGroup;
	}
	public String getPlantGroup() {
		return plantGroup;
	}
	public void setPlantGroup(String plantGroup) {
		this.plantGroup = plantGroup;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public String getPuzzleCode() {
		return puzzleCode;
	}
	public void setPuzzleCode(String puzzleCode) {
		this.puzzleCode = puzzleCode;
	}
}
