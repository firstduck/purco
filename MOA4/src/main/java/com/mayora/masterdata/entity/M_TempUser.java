package com.mayora.masterdata.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="MYRMD.M_TEMPORARYUSER")
public class M_TempUser {

	@Id
	@Column(name="USERNAME")
	@NotBlank(message="Please Input Username")
	private String username;
	
	@Column(name="PASSWORD") 
	@NotBlank(message="Please Input Password")
	private String password;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="EMPID")
	private String empId;
	
	@Column(name="STARTDATE")
	private Date startDate;
	
	@Column(name="ENDDATE")
	private Date endDate;
	
	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="GROUPACCESSCODE")
	private String groupAccessCode;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupAccessCode() {
		return groupAccessCode;
	}

	public void setGroupAccessCode(String groupAccessCode) {
		this.groupAccessCode = groupAccessCode;
	}
	
	
}
