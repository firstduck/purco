package com.mayora.masterdata.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="V_DELEGATION")
public class V_Delegation {
	
	@EmbeddedId
	private V_Delegation_PK vdelPK;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="NAME")
	private String name;
	 
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="DELEGATEAS")
	private String delegateAs;
	
	@Column(name="DELEGATEASEMAIL")
	private String delegateAsEmail;
	
	@Column(name="STARTDATE")
	private Timestamp startDate;
	
	@Column(name="ENDDATE")
	private Timestamp endDate;

	public V_Delegation_PK getVdelPK() {
		return vdelPK;
	}

	public void setVdelPK(V_Delegation_PK vdelPK) {
		this.vdelPK = vdelPK;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDelegateAs() {
		return delegateAs;
	}

	public void setDelegateAs(String delegateAs) {
		this.delegateAs = delegateAs;
	}

	public String getDelegateAsEmail() {
		return delegateAsEmail;
	}

	public void setDelegateAsEmail(String delegateAsEmail) {
		this.delegateAsEmail = delegateAsEmail;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
}
