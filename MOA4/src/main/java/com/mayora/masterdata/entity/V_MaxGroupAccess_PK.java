package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_MaxGroupAccess_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="USERNAME")
	private String username;

	@Column(name="GROUPCODE")
	private String groupCode;
	
	@Column(name="MODULECODE")
	private String moduleCode;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
}
