package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.M_JOBTITLEACCESS")
public class M_JobTitleAccess {
	
	public M_JobTitleAccess(M_JobTitleAccess_PK pk, String groupName,
			String jobTitleName) {
		super();
		this.pk = pk;
		this.groupName = groupName;
		this.jobTitleName = jobTitleName;
	}
	
	public M_JobTitleAccess(){
		
	}

	@EmbeddedId
	M_JobTitleAccess_PK pk;
	
	@Column(name="GROUPNAME")
	public String groupName;
	
	@Column(name="ISDEFAULT")
	public Integer isDefault;
	
	@Column(name="JOBTITLENAME")
	public String jobTitleName;

	@Column(name="PLANTCODE")
	public String plantCode;
	
	public M_JobTitleAccess_PK getPk() {
		return pk;
	}

	public void setPk(M_JobTitleAccess_PK pk) {
		this.pk = pk;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getJobTitleName() {
		return jobTitleName;
	}

	public void setJobTitleName(String jobTitleName) {
		this.jobTitleName = jobTitleName;
	}

	public String getPlantCode() {
		return plantCode;
	}

	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	
}
