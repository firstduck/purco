package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Delegation_PK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="MODULECODE")
	private String moduleCode;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="DELEGATEAS") 
	private String delegateas;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDelegateas() {
		return delegateas;
	}

	public void setDelegateas(String delegateas) {
		this.delegateas = delegateas;
	}
}
