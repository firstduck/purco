package com.mayora.masterdata.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class V_Delegation_PK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="USERNAME")
	private String userName;

	@Column(name="DELEGATEASUSERNAME")
	private String delegateAsUsername;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDelegateAsUsername() {
		return delegateAsUsername;
	}

	public void setDelegateAsUsername(String delegateAsUsername) {
		this.delegateAsUsername = delegateAsUsername;
	}
}
