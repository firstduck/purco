package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_MAXGROUPACCESS")
public class MaxGroupAccess {

	@EmbeddedId
	private V_MaxGroupAccess_PK mgaPK;
	
	@Column(name="MODULENAME")
	private String moduleName;
	
	@Column(name="APPLICATIONCODE")
	private String applicationCode;
	
	@Column(name="APPLICATIONNAME")
	private String applicationName;
	
	@Column(name="ACCESS_")
	private Integer access_;
	
	@Column(name="VIEWALL")
	private Integer viewAll;
	
	@Column(name="INSERT_")
	private Integer insert_;
	
	@Column(name="EDIT")
	private Integer edit;
	
	@Column(name="DELETE_")
	private Integer delete_;
	
	@Column(name="PRINT")
	private Integer print;
	
	@Column(name="REVIEW")
	private Integer review;
	
	@Column(name="APPROVE")
	private Integer approve;
	
	@Column(name="ADM")
	private Integer adm;
	
	@Column(name="ID")
	private Integer id;
	
	@Column(name="IDAPPLICATION")
	private Integer idApplication;
	
	@Column(name="WEBNAME")
	private String webName;
	
	@Column(name="CODENAME")
	private String codeName;
	
	public Integer getAdm() {
		return adm;
	}

	public void setAdm(Integer adm) {
		this.adm = adm;
	}

	public V_MaxGroupAccess_PK getMgaPK() {
		return mgaPK;
	}
	
	public void setMgaPK(V_MaxGroupAccess_PK mgaPK) {
		this.mgaPK = mgaPK;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public Integer getAccess_() {
		return access_;
	}
	public void setAccess_(Integer access_) {
		this.access_ = access_;
	}
	public Integer getViewAll() {
		return viewAll;
	}
	public void setViewAll(Integer viewAll) {
		this.viewAll = viewAll;
	}
	public Integer getInsert_() {
		return insert_;
	}
	public void setInsert_(Integer insert_) {
		this.insert_ = insert_;
	}
	public Integer getEdit() {
		return edit;
	}
	public void setEdit(Integer edit) {
		this.edit = edit;
	}
	public Integer getDelete_() {
		return delete_;
	}
	public void setDelete_(Integer delete_) {
		this.delete_ = delete_;
	}
	public Integer getPrint() {
		return print;
	}
	public void setPrint(Integer print) {
		this.print = print;
	}
	public Integer getReview() {
		return review;
	}
	public void setReview(Integer review) {
		this.review = review;
	}
	public Integer getApprove() {
		return approve;
	}
	public void setApprove(Integer approve) {
		this.approve = approve;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdApplication() {
		return idApplication;
	}
	public void setIdApplication(Integer idApplication) {
		this.idApplication = idApplication;
	}
	public String getWebName() {
		return webName;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
}
