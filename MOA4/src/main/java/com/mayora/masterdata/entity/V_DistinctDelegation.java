package com.mayora.masterdata.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="MYRMD.V_DISTINCTDELEGATION")
public class V_DistinctDelegation {
	
	@EmbeddedId
	private V_DistinctDelegation_PK disDelPK;
	
	@Column(name="DELEGATEAS")
	private String delegateAs;

	public String getDelegateAs() {
		return delegateAs;
	}

	public void setDelegateAs(String delegateAs) {
		this.delegateAs = delegateAs;
	}

	public V_DistinctDelegation_PK getDisDelPK() {
		return disDelPK;
	}

	public void setDisDelPK(V_DistinctDelegation_PK disDelPK) {
		this.disDelPK = disDelPK;
	}
}
