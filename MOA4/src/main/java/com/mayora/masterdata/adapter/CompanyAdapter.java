package com.mayora.masterdata.adapter;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.Company;

public class CompanyAdapter extends MasterDataBaseAdapter {
	
	public List<Company> getAllCompany() {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Company where isDeleted = '0' order by ID asc"
		);
		List<Company> companyList = query.list();
		session.close();
		return companyList;
    }
	
	public String getCompanyGroupCode(String companyCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Company where companyCode = '" + companyCode + "'"
		);
		List<Company> companyList = query.list();
		String companyGroupCode = companyList.get(0).getGroupCode();
		session.close();
		return companyGroupCode;
	}
	
	public List<Company> getVendorByCompany(String companyCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Company where companyCode = '" + companyCode + "'"
		);
		List<Company> plantList = query.list(); 
		
		session.close();
		return plantList;
    }
	
	public String getCompanyName(String companyCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Company where companyCode = '" + companyCode + "'"
		);
		List<Company> companyList = query.list();
		String companyName = companyList.get(0).getCompanyName();
		session.close();
		return companyName;
	}
}
