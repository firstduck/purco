package com.mayora.masterdata.adapter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import com.mayora.masterdata.entity.D_GroupAccess;
import com.mayora.masterdata.entity.D_GroupAccess_PK;
import com.mayora.masterdata.entity.D_GroupAccessMvc;
import com.mayora.masterdata.entity.D_GroupAccessMvc_PK;
import com.mayora.masterdata.entity.VD_GroupAccessMvc;
import com.mayora.masterdata.entity.VTD_GroupAccessMvc;
import com.mayora.masterdata.entity.D_GroupAuthorization;
import com.mayora.masterdata.entity.D_GroupAuthorization_PK;
import com.mayora.masterdata.entity.M_GroupAccess;
import com.mayora.masterdata.entity.Module;
import com.mayora.masterdata.entity.VD_GroupAccess;
import com.mayora.masterdata.entity.VD_GroupAuthorization;
import com.mayora.masterdata.entity.VTD_GroupAccess;
import com.mayora.masterdata.entity.MaxGroupAccess;
import com.mayora.masterdata.entity.V_DelegatedGroupAccess;
import com.mayora.masterdata.entity.V_GroupAccess;
import com.mayora.masterdata.model.KendoWindowBrowse;
import com.mayora.moa.session.ValueAuthorization;

public class GroupAccessAdapter extends MasterDataBaseAdapter {

	public String setModule() {
		return "M-GACC";
	}
	
	public V_DelegatedGroupAccess getDelegatedGroupAccess(String moduleCode, String username, String delegatedBy){
		Session session = sfMD.openSession();
		System.out.println("from V_DelegatedGroupAccess ga where ga.vdgaPK.username = '" + username + "' and ga.vdgaPK.delegated = '"+delegatedBy+"' and ga.vdgaPK.moduleCode = '" + moduleCode + "'");
		Query query = session.createQuery(
				"from V_DelegatedGroupAccess ga where ga.vdgaPK.username = '" + username + "' and ga.vdgaPK.delegated = '"+delegatedBy+"' and ga.vdgaPK.moduleCode = '" + moduleCode + "'"
		);

		List<V_DelegatedGroupAccess> groupAccessList = query.list();

		V_DelegatedGroupAccess groupAccess = groupAccessList.get(0);
//		System.out.println(groupAccess.getAccess_() + " " + groupAccess.getViewAll() + " " + groupAccess.getInsert_() + " " + groupAccess.getEdit() + " " + groupAccess.getDelete_() + " " + groupAccess.getPrint() + " " + groupAccess.getReview() + " " + groupAccess.getApprove() + " ");
		session.close();
		return groupAccess;
	}
	
	public MaxGroupAccess getMaxGroupAccess(String moduleCode, String username) {
		Session session = sfMD.openSession();

		Query query = session.createQuery(
				"from MaxGroupAccess mga where mga.mgaPK.username = '" + username + "' and mga.mgaPK.moduleCode = '" + moduleCode + "'"
		);

		List<MaxGroupAccess> groupAccessList = query.list();

		MaxGroupAccess groupAccess = groupAccessList.get(0);
		
		session.close();
		return groupAccess;
	}

	public List<MaxGroupAccess> getMaxGroupAccessByAppCode(String username, String appCode) {
		Session session = sfMD.openSession();
		String query = "";
		if(appCode.contains("__")){
			String[] tmp = appCode.split("__");
			String where = "(";
			for (int i = 0; i < tmp.length; i++) {
				where += "'" + tmp[i] + "',";
			}
			where = where.substring(0, where.length()-1);
			where += ")";
			
			query = "from MaxGroupAccess where (access_ = 1 or viewall = 1 or insert_ = 1 or edit = 1 or delete_ = 1 or print = 1 or review = 1 or approve = 1) " +
				"AND username ='" +  username + "' AND applicationCode in " + where + " ";
		}else{
			query = "from MaxGroupAccess where (access_ = 1 or viewall = 1 or insert_ = 1 or edit = 1 or delete_ = 1 or print = 1 or review = 1 or approve = 1) " +
				"AND username ='" +  username + "' AND applicationCode = '" + appCode + "' ";
		}
		
		List<MaxGroupAccess> result = session.createQuery(query).list(); 
		session.close();
		return result;
	} 

	
	//tabel VD_GroupAccess
	public List<VD_GroupAccess> get_VD_GroupAccessByGroupCode(String groupCode) {
		Session session = sfMD.openSession();
		List<VD_GroupAccess> mbr = session.createQuery("from VD_GroupAccess sub where groupCode ='" + groupCode + "'")
										  .list(); 
		session.close();
		return mbr;
	}
	
	public List<V_GroupAccess> get_V_GroupAccessByGroupCode(String username) {
		Session session = sfMD.openSession();
		List<V_GroupAccess> mbr = session.createQuery("from V_GroupAccess where (access_ = 1 or viewall = 1 or insert_ = 1 or edit = 1 or delete_ = 1 or print = 1 or review = 1 or approve = 1) and username ='" +  username + "'")
										  .list(); 
		session.close();
		return mbr;
	} 
	
	public List<MaxGroupAccess> getMaxGroupAccessByGroupCode(String username) {
		Session session = sfMD.openSession();
		List<MaxGroupAccess> mbr = session.createQuery("from MaxGroupAccess where (access_ = 1 or viewall = 1 or insert_ = 1 or edit = 1 or delete_ = 1 or print = 1 or review = 1 or approve = 1) and username ='" +  username + "'")
										  .list(); 
		session.close();
		return mbr;
	} 
	
	//tabel VTD_GroupAccess
	public List<VTD_GroupAccess> getAll_VTD_GroupAccess() {
		Session session = sfMD.openSession();
		List<VTD_GroupAccess> mbr = session.createQuery("from VTD_GroupAccess")
								          .list(); 
		session.close();
		return mbr;
	}
	
	//tabel VD_GroupAccessMvc
		public List<VD_GroupAccessMvc> get_VD_GroupAccessMvcByGroupCode(String groupCode) {
			Session session = sfMD.openSession();
			List<VD_GroupAccessMvc> mbrm = session.createQuery("from VD_GroupAccessMvc where pk.groupCode ='" + groupCode + "'")
											  .list(); 
//			List<VD_GroupAccessMvc> mbrm = session.createQuery("from VD_GroupAccessMvc")
//					  .list(); 
			session.close();
			System.out.println("ini size listnya : " +mbrm.size());
			return mbrm;
		}
		
		//tabel VTD_GroupAccessMvc
		//tabel VTD_GroupAccessMvc
		public List<VTD_GroupAccessMvc> getAll_VTD_GroupAccessMvc() {
			Session session = sfMD.openSession();
			List<VTD_GroupAccessMvc> mbr = session.createQuery("from VTD_GroupAccessMvc")
									          .list(); 
			session.close();
			return mbr;
		}
		
	//return single record M_GroupAccess
	public List<M_GroupAccess> getM_GroupAccessByGroupCode(String groupCode){
		Session session = sfMD.openSession();
		List<M_GroupAccess> mga = session.createQuery("from M_GroupAccess where groupCode ='" + groupCode + "'")
								         .list(); 
		
		session.close();
		return mga;
	}
	
	//return semua record M_GroupAccess yg sesuai search 
	public List<M_GroupAccess> getAll_M_GroupAccess(String search){
		Session session = sfMD.openSession();
		List<M_GroupAccess> mga = session.createQuery("from M_GroupAccess where upper(groupCode) like :search or upper(groupName) like :search")
										 .setString("search", "%" + search.toUpperCase() + "%")
								         .list(); 
		
		session.close();
		return mga;
	}
	
	public List<KendoWindowBrowse> getGroupAccessWindowData(String search){
		Session session = sfMD.openSession();
		List<M_GroupAccess> mga = session.createQuery("from M_GroupAccess where upper(groupCode) like :search or upper(groupName) like :search")
										 .setString("search", "%" + search.toUpperCase() + "%")
								         .list(); 
		session.close();
		List<KendoWindowBrowse> list = new ArrayList<KendoWindowBrowse>();
		for (int i = 0; i < mga.size(); i++) {
			String groupCode = mga.get(i).getGroupCode();
			String groupName = mga.get(i).getGroupName();
			list.add(new KendoWindowBrowse(groupCode, groupName));
		}
		return list;
	}
	
	//cek eksistensi grupAkses
	public Integer checkGroupAccessExist(String groupCode){
		Session session = sfMD.openSession();
		List<M_GroupAccess> mga = session.createQuery("from M_GroupAccess where groupCode ='" + groupCode + "'")
								         .list(); 
		session.close();
		
		if(mga.size() > 0){
			return 1;
		}
		else{
			return 2;
		}
	}
	
	//untuk looping untuk create GroupAccess
	public Integer getModuleListSize(){
		Session session = sfMD.openSession();
		
		Query query = session.createQuery("select count(*) from VTD_GroupAccess");
		
		Integer size =  Integer.parseInt(query.list().get(0).toString());
		
		session.close();
		return size;
	}
	
	//masukkan M_groupAccess
	public void insert_M_GroupAccess(String groupCode, String groupName, String description){
		M_GroupAccess mga = new M_GroupAccess();
		mga.setGroupCode(groupCode);
		mga.setGroupName(groupName);
		mga.setDescription(description);
		this.addObjectMD(mga);
	}
	
	//masukkan D_GroupAccess
	public void insert_D_GroupAccess(Vector<D_GroupAccess> allGA, Integer moduleSize) {
		for(Integer i = 0; i < moduleSize; i++){
//			if(allGA.get(i).getAccess_()){
				this.addObjectMD(allGA.get(i));
//			}
		}
	}
	
	//hapus M_GroupAccess
	public void deleteGroupAccess(M_GroupAccess mga, String groupCode){
		this.deleteObjectMD(mga, groupCode);
	}
	
	//hapus D_GroupAccess
	public void delete_D_GroupAccess(String groupCode){
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE D_GroupAccess sub where sub.dgaPK.groupCode ='"+ groupCode +"'");
			query.executeUpdate();
			session.getTransaction().commit();
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
	}

	//membuat template D_GroupAccess dg value 0 -- template digunakan di kodingan 
	public  Vector<D_GroupAccess> setAllGA_DefaultValues(Vector<D_GroupAccess> allGA, List<VTD_GroupAccess> tempGA, Integer moduleSize, String groupCode){
		
		for(int i = 0; i < moduleSize; i++){
			D_GroupAccess dga = new D_GroupAccess();
			D_GroupAccess_PK dgapk = new D_GroupAccess_PK();
			
			dgapk.setGroupCode(groupCode);
			dgapk.setModuleCode(tempGA.get(i).getModuleCode());
			dga.setDgaPK(dgapk);
			dga.setAccess_(false);
			dga.setViewAll(false);
			dga.setInsert_(false);
			dga.setEdit(false);
			dga.setDelete_(false);
			dga.setPrint(false);
			dga.setReview(false);
			dga.setApprove(false);
			dga.setAdm(false);
			allGA.addElement(dga);
		 }
		return allGA;
	}

	//edit record M_groupAccess
	public void edit_M_GroupAccess(M_GroupAccess mga, String groupName, String description){
		mga.setGroupName(groupName);
		mga.setDescription(description);
		this.updateObjectMD(mga);
	}

	//get single D_groupAccess
	public List<D_GroupAccess> getD_GroupAccessByGroupCode(String groupCode) {
		Session session = sfMD.openSession();
		List<D_GroupAccess> listDga = session.createQuery("from D_GroupAccess dga where dga.dgaPK.groupCode ='" + groupCode + "'")
											 .list();
		session.close();
		return listDga;
	}
	
	//set value D_groupAccess
	public Vector<D_GroupAccess> setModuleAccess(Integer moduleSize, 
												String[] listAccess, String[] listViewAll,
												String[] listInsert, String[] listEdit,
												String[] listDelete, String[] listPrint,
												String[] listReview, String[] listApprove, 
												String[] listADM,
												Vector<D_GroupAccess> allGA){
		if(listAccess != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listAccess.length ; j++){
					 if(listAccess[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setAccess_(true);
						 break;
					}
				 }
			 }
		}
		
		if(listViewAll != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listViewAll.length ; j++){
					 if(listViewAll[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setViewAll(true);
						 break;
					}
				 }
			 }
		}
		
		 if(listInsert != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listInsert.length ; j++){
					 if(listInsert[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setInsert_(true);
						 break;
					 }
				 }
			 }
		 }
		 
		 if(listEdit != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listEdit.length ; j++){
					 if(listEdit[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setEdit(true);
						 break;
					 }
				 }
			 }
		 }
		 
		 if(listDelete != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listDelete.length ; j++){
					 if(listDelete[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setDelete_(true);
						 break;
					 }
				 }
			 }
		 }
		 
		 if(listPrint != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listPrint.length ; j++){
					 if(listPrint[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setPrint(true);
						 break;
					 }
				 }
			 }
		 }
		 
		 if(listReview != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listReview.length ; j++){
					 if(listReview[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setReview(true);
						 break;
					 }
				 }
			 }
		 }
		 
		 if(listApprove != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listApprove.length ; j++){
					 if(listApprove[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setApprove(true);
						 break;
					 }
				 }
			 }
		 }
		 
		if(listADM != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listADM.length ; j++){
					 if(listADM[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setAdm(true);
						 break;
					}
				 }
			 }
		}
	return allGA;
	}
	
	//set ViewAll
	public Vector<D_GroupAccess> setListViewAll(Integer moduleSize, String[] listViewAll, Vector<D_GroupAccess> allGA){
		if(listViewAll != null){
			 for (Integer i = 0;i < moduleSize ; i++){
				 for(Integer j = 0 ; j < listViewAll.length ; j++){
					 if(listViewAll[j].equals(allGA.get(i).getDgaPK().getModuleCode())){
						 allGA.get(i).setViewAll(true);
						 break;
					}
				 }
			 }
		}
		return allGA;
	}
	
	//return grid detailGroupAuthorization berdasar modulecode
	public List<ValueAuthorization> getListUVA(final Module module) throws SQLException{

		String moduleCode = module.getModuleCode();
		
		Session session = sfMD.openSession();
		List<ValueAuthorization> listUVA = new ArrayList<ValueAuthorization>();
		
		ResultSet rs = session.doReturningWork(new ReturningWork<ResultSet>(){
            @Override
            public ResultSet execute(Connection connection)throws SQLException {
                String sql = "select * from " + module.getDatabaseName()+"."+module.getTableName();
                Statement stmt = connection.createStatement();
                ResultSet resultSet = stmt.executeQuery(sql);
                return resultSet;
            }
        });
		
		while(rs.next()){
			ValueAuthorization UVA = new ValueAuthorization();
			UVA.setModuleCode(moduleCode);
			UVA.setCode(rs.getString(module.getPrimaryKey()));
			UVA.setValue(rs.getString(module.getDisplayMember()));
			listUVA.add(UVA);
		}

		session.close();
		return listUVA;
	}
	
	//masukkan D_groupauthorization
	public Integer insert_D_GroupAuthorization(List<ValueAuthorization> sesListUVA, String groupCode){
		Integer size = sesListUVA.size();
		Integer flag = 0;

		for(int i = 0 ; i < size;i++){
				if(size == 1){
					flag = 1;
					this.addDGAuth(groupCode, sesListUVA.get(i).getModuleCode(), flag, sesListUVA.get(i).getCode());
				}
				else if(size > 1 && i < (size-1)){
					if(sesListUVA.get(i).getModuleCode().equals(sesListUVA.get(i+1).getModuleCode())){
						flag++;
						this.addDGAuth(groupCode, sesListUVA.get(i).getModuleCode(), flag, sesListUVA.get(i).getCode());
					}
					else{
						flag++;
						this.addDGAuth(groupCode, sesListUVA.get(i).getModuleCode(), flag, sesListUVA.get(i).getCode());
						flag = 0;
					}
				}
				else if(size > 1 && i == (size-1)){
					if(sesListUVA.get(i).getModuleCode().equals(sesListUVA.get(i-1).getModuleCode())){
						flag++;
						this.addDGAuth(groupCode, sesListUVA.get(i).getModuleCode(), flag, sesListUVA.get(i).getCode());
					}
					else{
						flag = 1;
						this.addDGAuth(groupCode, sesListUVA.get(i).getModuleCode(), flag, sesListUVA.get(i).getCode());
					}
				}
			}
		
		return 1;
	}
	
	//masukkan D_groupauthorization
	public void addDGAuth(String groupCode, String moduleCode, Integer flag, String code){
		D_GroupAuthorization dGAuth = new D_GroupAuthorization();
		D_GroupAuthorization_PK dgauthPK = new D_GroupAuthorization_PK();
		dgauthPK.setGroupCode(groupCode);
		dgauthPK.setModuleCode(moduleCode);
		dgauthPK.setId(flag);
		dGAuth.setDgauthPK(dgauthPK);
		dGAuth.setValue(code);
		this.addObjectMD(dGAuth);
	}
	
	//get VD_GroupAuthorization
	public List<ValueAuthorization> getVD_GroupAuthorizationByGroupCode(String groupCode){
		Session session = sfMD.openSession();

		Query query = session.createQuery("from VD_GroupAuthorization vdga where vdga.vd_gauthPK.groupCode ='" + groupCode + "'");
		List<VD_GroupAuthorization> listVDGA = query.list();
	
		List<ValueAuthorization> listUVA = new ArrayList<ValueAuthorization>();
		for(int i = 0 ; i < listVDGA.size(); i++){
			ValueAuthorization UVA = new ValueAuthorization();
			UVA.setModuleCode(listVDGA.get(i).getVd_gauthPK().getModuleCode());
			UVA.setCode(listVDGA.get(i).getValue());
			UVA.setSelected(true);
			listUVA.add(UVA);
		}
	
		session.close();
		return listUVA;
	}

	//delete D_GroupAuth
	public Integer delete_D_GroupAuthorization(String groupCode) {
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE D_GroupAuthorization sub where sub.dgauthPK.groupCode ='"+ groupCode +"'");
			query.executeUpdate();
			session.getTransaction().commit();
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
		return 1;
	}
	
	//masukkan D_GroupAccessMvc
		public void insert_D_GroupAccessMvc(String groupCode, String[] listMenu) {
			String[] temp;
			
			//cek checkbox menu
			try {
				for(int i=0;i<listMenu.length;i++)
				{
					System.out.println(listMenu[i]);
					temp =  listMenu[i].split("__");
					D_GroupAccessMvc mvc = new D_GroupAccessMvc();
					D_GroupAccessMvc_PK pk = new D_GroupAccessMvc_PK();
					pk.setGroupCode(groupCode);
					pk.setModuleCode(temp[1]);
					pk.setActionMenu(temp[3]);
					mvc.setPk(pk);
					mvc.setAccess_(true);
					mvc.setController(temp[2]);
					mvc.setId(Integer.valueOf(temp[0]));
					
					this.addObjectMD(mvc);				
				
				}	
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("kosong nih");
			}
				
			
		}
		
		//hapus D_GroupAccessMvc
		public void delete_D_GroupAccessMvc(String groupCode){
			Session session = sfMD.openSession();
			try{
				session.beginTransaction();
				Query query = session.createQuery("DELETE D_GroupAccessMvc where pk.groupCode ='"+ groupCode +"'");
				query.executeUpdate();
				session.getTransaction().commit();
			}
			catch(HibernateException e) { 
	        	session.getTransaction().rollback(); 
			}
			finally{ 
				session.close();
			}
		}
		
		public List<VD_GroupAccessMvc> getVD_GroupAccessMvcByGroupCode(String groupCode) {
			// TODO Auto-generated method stub
			Session session = sfMD.openSession();
			List<VD_GroupAccessMvc> mbr = session.createQuery("from VD_GroupAccessMvc where pk.groupCode='"+groupCode+"'")
									          .list(); 
			session.close();
			return mbr;
		}
	
}
