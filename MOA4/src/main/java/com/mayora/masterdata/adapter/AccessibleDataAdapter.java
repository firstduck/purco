package com.mayora.masterdata.adapter;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.VD_User;

public class AccessibleDataAdapter extends MasterDataBaseAdapter {

	public List<VD_User> accessibleData(String groupCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from VD_User where groupCode = '" + groupCode + "'"
		);
		List<VD_User> accessibleModule = query.list();
		session.close();
		return accessibleModule;
	}
}
