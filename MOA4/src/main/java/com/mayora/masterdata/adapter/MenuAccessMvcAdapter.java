package com.mayora.masterdata.adapter;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.MenuAccessMvc;
import com.mayora.masterdata.entity.MenuAccessMvcWithoutMenu;

public class MenuAccessMvcAdapter extends MasterDataBaseAdapter {

	public List<MenuAccessMvc> getMenuAccessMvc(String controller, String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from MenuAccessMvc where username = '" + username + "' and controller = '" + controller + "'"
		);
		List<MenuAccessMvc> menuAccessMvcList = query.list();
		session.close(); 
		return menuAccessMvcList;
	}
	
	public MenuAccessMvc getPageMenuAccessMvc(String moduleCode, String action, String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from MenuAccessMvc where username = '" + username + "' and moduleCode = '" + moduleCode + "' and action = '" + action + "'"
		);
		List<MenuAccessMvc> menuAccessMvcList = query.list();
		session.close(); 
		if (menuAccessMvcList.size() > 0) {
			return menuAccessMvcList.get(0);
		} else {
			return new MenuAccessMvc();
		}
	}
	
	public MenuAccessMvcWithoutMenu getPageMenuAccessMvcWithoutMenu(String moduleCode, String action, String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from MenuAccessMvcWithoutMenu where username = '" + username + "' and moduleCode = '" + moduleCode + "' and action = '" + action + "'"
		);
		List<MenuAccessMvcWithoutMenu> menuAccessMvcList = query.list();
		session.close(); 
		if (menuAccessMvcList.size() > 0) {
			return menuAccessMvcList.get(0);
		} else {
			return new MenuAccessMvcWithoutMenu();
		}
	}
	
	public String getTCode(String moduleCode, String action){
		Session session = sfMD.openSession();
		List<BigDecimal> list = session.createSQLQuery("select id from myrmd.m_menumvc where modulecode='"+moduleCode+"' and action='"+action+"'").list();
		session.close();
		
		if(list.size()>0){
			return moduleCode+list.get(0).toString();
		}
		return "";
	}
	
	public List<MenuAccessMvc> getMenuAccessMvcNew(String moduleCode, String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from MenuAccessMvc where moduleCode = '"+moduleCode+"' and username = '" + username + "'"
		);
		List<MenuAccessMvc> menuAccessMvcList = query.list();
		session.close(); 
		return menuAccessMvcList;
	}
}
