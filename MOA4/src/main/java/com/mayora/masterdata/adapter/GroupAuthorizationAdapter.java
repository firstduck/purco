package com.mayora.masterdata.adapter;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.D_Branch;
import com.mayora.masterdata.entity.D_GroupAuthorization;
import com.mayora.masterdata.entity.UserAuthorization;

public class GroupAuthorizationAdapter extends MasterDataBaseAdapter{
	
	public List<D_GroupAuthorization> getPlant(String groupCode){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
					"from D_GroupAuthorization dga where dga.dgauthPK.groupCode = '"+groupCode+"'"
				);
		List<D_GroupAuthorization> groupList = query.list();
		session.close();
		return groupList;
	}
	
	public List<D_GroupAuthorization> getAuthorization(String groupCode, String moduleCode){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
					"from D_GroupAuthorization dga where dga.dgauthPK.groupCode = '"+groupCode+"' AND dga.dgauthPK.moduleCode = '"+moduleCode+"'"
				);
		List<D_GroupAuthorization> groupList = query.list();
		session.close();
		return groupList;
	}
	
	public List<UserAuthorization> getUserAuthorization(String username){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
					"from UserAuthorization dga where username = '" + username + "' ");
		List<UserAuthorization> groupList = query.list();
		session.close();
		return groupList;
	}
	
	public List<UserAuthorization> getUserAuthByModule(String username, String moduleCode){
		Session session = sfMD.openSession();
		String query = "";
		
		if(moduleCode.contains("__")){
			String[] tmp = moduleCode.split("__");
			String where = "(";
			for (int i = 0; i < tmp.length; i++) {
				where += "'" + tmp[i] + "',";
			}
			where = where.substring(0, where.length()-1);
			where += ")";
			
			query = "FROM UserAuthorization dga WHERE username = '" + username + "' AND moduleCode IN " + where + " ";
		}else{
			query = "FROM UserAuthorization dga WHERE username = '" + username + "' AND moduleCode = '" + moduleCode + "' ";
		}
		
		List<UserAuthorization> groupList = session.createQuery(query).list();
		session.close();
		return groupList;
	}

	
	public List<D_GroupAuthorization> getValuesByModuleCode(String moduleCode, String username){
		Session session = sfMD.openSession();
		
		List<String> list = session.createSQLQuery("select groupcode from MYRMD.v_groupaccess where access_ = 1 and moduleCode='"+moduleCode+"' and username='"+username+"'").list();
		
		if(list.size()>0){
			List<D_GroupAuthorization> groupList = getListGroupAuthorization(list.get(0));
			if(groupList.size()>0){
				return groupList;
			}
		}
		session.close();
		return null;
	}
	
	public List<D_GroupAuthorization> getValuesByModuleCode(String moduleCode, String username, String groupAuthModuleCode){
		Session session = sfMD.openSession();
		
		List<String> list = session.createSQLQuery("select groupcode from MYRMD.v_groupaccess where access_ = 1 and moduleCode='"+moduleCode+"' and username='"+username+"'").list();
		
		if(list.size()>0){
			List<D_GroupAuthorization> groupList = getListGroupAuthorization(list.get(0),groupAuthModuleCode);
			if(groupList.size()>0){
				return groupList;
			}
		}
		session.close();
		return null;
	}
	
	public List<D_GroupAuthorization> getListGroupAuthorization(String groupCode){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
					"from D_GroupAuthorization dga where dga.dgauthPK.groupCode = '"+groupCode+"'"
				);
		List<D_GroupAuthorization> groupList = query.list();
		session.close();
		return groupList;
	}
	
	public List<D_GroupAuthorization> getListGroupAuthorization(String groupCode, String modulecode){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
					"from D_GroupAuthorization dga where dga.dgauthPK.groupCode = '"+groupCode+"' and dga.dgauthPK.moduleCode = '"+modulecode+"'"
				);
		List<D_GroupAuthorization> groupList = query.list();
		session.close();
		return groupList;
	}

	public List<D_Branch> getBranch(String branchCode, String subBranchCode){
		Session session = sfMD.openSession();
		String qry = "";
		
		if(branchCode.equals("") && subBranchCode.equals(""))
		{	qry = "from D_Branch";
		}
		else if(!branchCode.equals("") && subBranchCode.equals("")) 
		{
			qry = "from D_Branch where branchCode in ("+ branchCode +")";
		}
		else if(branchCode.equals("") && !subBranchCode.equals("")) 
		{
			qry = "from D_Branch where subBranchCode in ("+ subBranchCode +")";
		}
		else if(!branchCode.equals("") && !subBranchCode.equals("")) 
		{
			qry = "from D_Branch where branchCode in ("+ branchCode +") or subBranchCode in ("+ subBranchCode +")";
		}
		
		Query query = session.createQuery(qry);
		List<D_Branch> branchList = query.list();
		session.close();
		return branchList;
	}
}
