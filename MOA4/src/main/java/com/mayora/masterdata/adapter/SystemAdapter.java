package com.mayora.masterdata.adapter;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.mayora.masterdata.entity.M_System;
import com.mayora.masterdata.adapter.MasterDataBaseAdapter;

public class SystemAdapter extends MasterDataBaseAdapter {

	public String setModule() {
		return "M-SYS";
	}
	
	public List<M_System> BrowseSystem(String search) {
		Session session = sfMD.openSession();
		List<M_System> mbs = session.createQuery("from M_System where (upper(systemCode) like :search or upper(systemName) like :search or upper(description) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		session.close();
		return mbs;
	}
	
	public List<M_System> getSystemBySystemCode(String systemCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from M_System where systemCode = '" + systemCode + "'"
		);
		
		List<M_System> mbs = query.list();
		session.close();
		return mbs;
	}
	
	public void deleteSystem(M_System system, String systemCode){
		this.deleteObjectMD(system, systemCode);
	}
	
	public void editSystem(M_System system){
		this.updateObjectMD(system);
	}
	
	public void insertSystem(M_System system){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from M_System"
		);
		List<M_System> mbs = query.list();
		
		Integer i = 0;
		for(M_System sys:mbs)
			 if(sys.getID()!=null)
				 if(sys.getID() > i) i = sys.getID();
		system.setID(i+1);
		this.addObjectMD(system);
		session.close();
	}
}

