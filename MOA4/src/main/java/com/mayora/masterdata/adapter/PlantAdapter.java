package com.mayora.masterdata.adapter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.Plant;
import com.mayora.masterdata.entity.UserAuthorization;

public class PlantAdapter extends MasterDataBaseAdapter{
	
	public List<Plant> getAllPlant() {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Plant where upper(type) = 'FACTORY' or upper(type) = 'FACTORY2' order by plantCode"
		);
		List<Plant> plantList = query.list();
		session.close();
		return plantList;
    }
	
	public List<Plant> getCompanyByPlant(String plantCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Plant where PlantCode = '" + plantCode + "'"
		);
		List<Plant> plantList = query.list(); 
		
		session.close();
		return plantList;
    }
	
	@SuppressWarnings("unchecked")
	public List <Plant> getAllPlantName(String search){ 

		Session session = sfMD.openSession();
		if(search == null || search.equals("")){
			List<Plant> plant = session.createQuery("FROM Plant ORDER BY plantName").setMaxResults(100).list(); 
			session.close();
			return plant;
		}
		else{
			List<Plant> plant = session.createQuery("from Plant where (upper(plantName) like :search)")
					 .setString("search", "%" + search.toUpperCase() + "%").setMaxResults(100)
			         .list(); 
			session.close();
			return plant;
		}
	}
	
	public List<UserAuthorization> getAllViewUserAuthor(String moduleCode, String userName){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from UserAuthorization where moduleCode = '" + moduleCode + "' and userName = '" + userName + "'"
		);
		List<UserAuthorization> test = query.list();
		session.close();
		return test;
	}
	
	public List<Plant> getAllPlantAuthor(HttpServletRequest request, String value){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
			"from Plant where " + this.getWhereClauseForGroupAuthorization("M-PLT", request, value, "plantCode") + " and type = 'Factory'"
		);
		List<Plant> test = query.list();
		session.close();
		return test;
	}
	
	public String getWhereClauseForGroupAuthorization(String ModuleCode, HttpServletRequest request, String value, String PK){
		String whereClause;
		whereClause = PK + " in (";
		whereClause = whereClause + value;
		whereClause = whereClause + ")";
		return whereClause;
	}
	
	public Boolean cekPlant(String plant) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Plant where plantCode = :plant"
		).setString("plant", plant);
		if(query.list().size() == 0) {
			session.close();
			return false;
		} else {
			session.close();
			return true;
		}
	}
	
	
}
