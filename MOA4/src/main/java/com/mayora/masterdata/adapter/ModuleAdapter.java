package com.mayora.masterdata.adapter;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.M_MenuMvc;
import com.mayora.masterdata.entity.Module;
import com.mayora.masterdata.entity.VD_ModuleAuthor;
import com.mayora.masterdata.model.KendoWindowBrowse;
import com.mayora.masterdata.model.ModuleModel;

public class ModuleAdapter extends MasterDataBaseAdapter {
	
	public String setModule() {
		return "M-MOD";
	}
	
	public List<Module> getAllModules(String search){
		Session session = sfMD.openSession();
		List<Module> listMod = session.createQuery("from Module where (upper(moduleName) like :search or upper(moduleCode) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		
		session.close();
		
		return listMod;
	}
	
	public List<Module> getAllModulesIn(String in, String search){
		Session session = sfMD.openSession();
		List<Module> listMod = session.createQuery("from Module where moduleCode in ('"+in+"') and  (upper(moduleName) like :search or upper(moduleCode) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		
		session.close();
		
		return listMod;
	}
	
	public List<M_MenuMvc> getAllMenus(String moduleCode1)
	{
		Session session = sfMD.openSession();
		List<M_MenuMvc> listMen = session.createQuery("from M_MenuMvc where moduleCode = '"+moduleCode1+"' ")
				.list(); 
		session.close();
		return listMen;
	}
	
	public List<Module> getModulePaging(String search, Integer size, Integer skip){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from Module where (upper(moduleName) like :search or upper(moduleCode) like :search)")
				.setString("search", "%" + search.toUpperCase() + "%");
		query.setFirstResult(skip);
		query.setMaxResults(size);
		
		List<Module> listMod = query.list(); 

		session.close();
		
		return listMod;
	}
	
	public List<KendoWindowBrowse> getModuleWindowData(String search){
		Session session = sfMD.openSession();
		List<Module> listMod = session.createQuery("from Module where (upper(moduleName) like :search or upper(moduleCode) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		
		session.close();
		
		List<KendoWindowBrowse> list = new ArrayList<KendoWindowBrowse>();
		for (int i = 0; i < listMod.size(); i++) {
			String moduleCode = listMod.get(i).getModuleCode();
			String moduleName = listMod.get(i).getModuleName();
			list.add(new KendoWindowBrowse(moduleCode, moduleName));
		}
		
		return list;
	}
	
	
	public Module getModuleByModuleCode(String moduleCode){
		
		Session session = sfMD.openSession();
		Query query = session.createQuery("from Module where isActive = 1 and moduleCode='" + moduleCode +"'");
		List<Module> listMod = query.list();
		session.close();
		Module module = listMod.get(0);
		return module;
	}
	
	public List<ModuleModel> getModuleModelByGroupCode(String groupCode){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from VD_ModuleAuthor vdma where vdma.vd_maPK.groupCode='" + groupCode +"'");
	
		List<VD_ModuleAuthor> listVDMA = query.list();
		List<ModuleModel> listMM = new ArrayList<ModuleModel>();
		session.close();
		if(listVDMA.size() > 0){
				for(int i = 0 ; i < listVDMA.size(); i++){
					String moduleCode = listVDMA.get(i).getVd_maPK().getModuleCode();
					String moduleName = listVDMA.get(i).getModuleName();
					
					ModuleModel MM = new ModuleModel();
					MM.setModuleCode(moduleCode);
					MM.setModuleName(moduleName);
					MM.setIndex(i);
					MM.setIsDeleted(false);
					
					listMM.add(MM);
			}
		}
		return listMM;
	}
	
	public void deleteModule(Module module, String moduleCode){
		this.deleteObjectMD(module, moduleCode);
	}
	
	public void editModule(Module module){
		this.updateObjectMD(module);
	}
	
	public void insertModule(Module module){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Module"
		);
		List<Module> mba = query.list();
		
//		Integer i = 0;
//		for(Module mod:mba)
//			 if(mod.getId()!=null)
//				 if(mod.getId() > i) i = mod.getId();
//		module.setId(i+1);
		this.addObjectMD(module);
		session.close();
	}
	
	public void editMenuMvc(M_MenuMvc menumvc){
		this.updateObjectMD(menumvc);
	}
	public void insertMenuMvc(M_MenuMvc menumvc)
	{
		Session session = sfMD.openSession();
//		Query query = session.createQuery(
//				"from M_MENUMVC"
//		);
		this.addObjectMD(menumvc);
		session.close();
	}
	
//	public void deleteMenuMvc(String moduleCode)
//	{
//		Session session = sfMD.openSession();
//		Query query = session.createQuery("delete from M_MenuMvc where pk.moduleCode = '"+moduleCode+"' ");
//		query.executeUpdate();
//		session.close();
//	}
	
	public void deleteMenuMvc(String moduleCode){
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE M_MenuMvc where pk.moduleCode ='"+ moduleCode +"'");
			query.executeUpdate();
			session.getTransaction().commit();
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
	}
	
	public void deleteMenuMvcOneRow(String moduleCode, Integer id){
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE M_MenuMvc where pk.moduleCode ='"+ moduleCode +"' and pk.id = '"+id+"'");
			query.executeUpdate();
			session.getTransaction().commit();
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
	}
}
