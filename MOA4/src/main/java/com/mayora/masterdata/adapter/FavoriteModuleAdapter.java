package com.mayora.masterdata.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import com.mayora.masterdata.entity.M_FavoriteModule;
import com.mayora.masterdata.entity.V_FavoriteModule;
import com.mayora.masterdata.model.FavoriteMenu;
import com.mayora.masterdata.model.FavoriteModule;

public class FavoriteModuleAdapter extends MasterDataBaseAdapter{
	public List<FavoriteModule> getFavorite(String username) {
		Session session = sfMD.openSession();
		List<V_FavoriteModule> list = session.createQuery("from V_FavoriteModule where username = '" + username + "'")
									.list();
		
		session.close();
		
		String mod = "";
		
		List<FavoriteModule> listFav = new ArrayList<FavoriteModule>();
		
		FavoriteModule fm = null;
		for(V_FavoriteModule v : list){
			if(!v.getModuleCode().equals(mod)){
				fm = new FavoriteModule();
				fm.setModuleCode(v.getModuleCode());
				fm.setModuleName(v.getModuleName());
				fm.setList(new ArrayList<FavoriteMenu>());
				listFav.add(fm);
				mod = v.getModuleCode();
			}
			FavoriteMenu fe = new FavoriteMenu();
			fe.setMenu(v.getMenu());
			fe.setAction(v.getAction());
			fe.setWebname(v.getWebName());
			fm.getList().add(fe);
			
		}
		
		return listFav;
	}
	
	public Integer getMaxFavorite(String username, String moduleCode) {
		Session session = sfMD.openSession();
		
		List<BigDecimal> list = session.createSQLQuery("select max(id_) from M_FavoriteModule where username = '" + username + "'").list();
		List<M_FavoriteModule> list2 = session.createQuery("from M_FavoriteModule where username = '"+username+"' and moduleCode = '"+moduleCode+"'").list();
		session.close();
		
		if(list.get(0) != null  && list2.size() == 0){
			return Integer.valueOf(list.get(0).toString());
			
		}
		else if(list.get(0) == null){
			return 0;
		}

		return null;

	} 
}
