package com.mayora.masterdata.adapter;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.AccessibleApplication;
import com.mayora.masterdata.entity.AccessibleModule;
import com.mayora.masterdata.entity.V_DelegationApplication;
import com.mayora.masterdata.entity.V_DelegationModule;

public class AccessibleModuleAdapter extends MasterDataBaseAdapter {
	
	public List<AccessibleModule> accessibleModule(String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleModule where username = '" + username + "'"
		);
		List<AccessibleModule> accessibleModule = query.list();
		session.close();
		return accessibleModule;
	}
	
	public List<AccessibleModule> accessibleModule(String username, String menuCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleModule where username = '" + username + "' and menuCode = '"+menuCode+"'"
		);
		List<AccessibleModule> accessibleModule = query.list();
		session.close();
		return accessibleModule;
	}
	
	public List<AccessibleModule> accessibleModuleWebName(String username, String webName) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleModule where username = '" + username + "' and '" + webName + "' like ('%' || webName || '%')"
		);
		List<AccessibleModule> accessibleModule = query.list();
		session.close();
		return accessibleModule;
	}
	
	public List<AccessibleModule> accessibleModuleLink(String username, String link) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleModule where username = '" + username + "' and link = '"+link+"'"
		);
		List<AccessibleModule> accessibleModule = query.list();
		session.close();
		return accessibleModule;
	}
	
	public List<AccessibleModule> getAccessibleModuleDelegation(String username, String delegateAs) {
		Session session = sfMD.openSession();
		
		List<V_DelegationModule> del = session.createQuery("from V_DelegationModule where username='"+username+"' AND delegateAsUsername = '"+ delegateAs+"'")
												.list();
		
		List<AccessibleModule> list = new ArrayList<AccessibleModule>();
		
		for(V_DelegationModule vdm : del){
			AccessibleModule am = new AccessibleModule();
			am.setModuleName(vdm.getModuleName());
			am.setApplicationCode(vdm.getApplicationCode());
			am.setApplicationName(vdm.getApplicationName());
			am.setModuleCode(vdm.getModuleCode());
			am.setWebName(vdm.getWebName());
			am.setUsername(vdm.getDelegateAsUsername());
			am.setLink(vdm.getLink());
			am.setMenuCode(vdm.getMenuCode());
			list.add(am);
		}
		session.close();
		return list;
	}
	
	public List<AccessibleModule> getAccessibleModuleDelegationLink(String username, String delegateAs, String link) {
		Session session = sfMD.openSession();
		
		List<AccessibleModule> del = (List<AccessibleModule>)session.createQuery("select new AccessibleModule(moduleName, applicationCode, applicationName, moduleCode, webName, delegateAsUsername, link, menuCode) from V_DelegationModule where username='"+username+"' AND delegateAsUsername = '"+ delegateAs+"' and link = '"+link+"'")
												.list();
		
//		List<AccessibleModule> list = new ArrayList<AccessibleModule>();
//		
//		for(V_DelegationModule vdm : del){
//			AccessibleModule am = new AccessibleModule();
//			am.setModuleName(vdm.getModuleName());
//			am.setApplicationCode(vdm.getApplicationCode());
//			am.setApplicationName(vdm.getApplicationName());
//			am.setModuleCode(vdm.getModuleCode());
//			am.setWebName(vdm.getWebName());
//			am.setUsername(vdm.getDelegateAsUsername());
//			am.setLink(vdm.getLink());
//			am.setMenuCode(vdm.getMenuCode());
//			list.add(am);
//		}
		session.close();
		return del;
	}

	public List<AccessibleApplication> getAccessibleApplicationDelegation(String username, String maskUsername) {
		Session session = sfMD.openSession();
		
		List<V_DelegationApplication> del = session.createQuery("from V_DelegationApplication where username='"+username+"' AND delegateAsUsername = '"+ maskUsername+"'")
												.list();
		
		List<AccessibleApplication> list = new ArrayList<AccessibleApplication>();
		
		for(V_DelegationApplication vdm : del){
			AccessibleApplication am = new AccessibleApplication();
			am.setApplicationCode(vdm.getApplicationCode());
			am.setApplicationName(vdm.getApplicationName());
			am.setUsername(vdm.getDelegateAsUsername());
			am.setMenuCode(vdm.getMenuCode());
			am.setLink(vdm.getLink());
			list.add(am);
		}
		session.close();
		return list;
	}
}
