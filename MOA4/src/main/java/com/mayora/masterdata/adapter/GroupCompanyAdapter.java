package com.mayora.masterdata.adapter;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.GroupCompany;

public class GroupCompanyAdapter extends MasterDataBaseAdapter {
	
	public String setModule() {
		return "M-GC";
	}
	
	public List<GroupCompany> getAllCompanyList() {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from GroupCompany"
		);
		List<GroupCompany> companyList = query.list();
		session.close();
		return companyList;
    }
	
	public List<GroupCompany> BrowseGroupCompany(String search) {
		Session session = sfMD.openSession();
		List<GroupCompany> mba = session.createQuery("from GroupCompany where (upper(groupCompanyCode) like :search or upper(groupName) like :search or shortID like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		session.close();
		return mba;
	}
	
	public GroupCompany getGroupCompanyByGroupCompanyCode(String groupCompanyCode){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from GroupCompany where groupCompanyCode='" + groupCompanyCode +"'");
		List<GroupCompany> listGroupCompany = query.list();
		session.close();
		if(!listGroupCompany.isEmpty())
		{
			GroupCompany groupCompany = listGroupCompany.get(0);
			return groupCompany;
		}
		else return null;
	}
	
	public void deleteGroupCompany(GroupCompany groupCompany, String groupCompanyCode){
		this.deleteObjectMD(groupCompany, groupCompanyCode);
	}
	
	public void editGroupCompany(GroupCompany groupCompany){
		this.updateObjectMD(groupCompany);
	}
	
	public void insertGroupCompany(GroupCompany groupCompany){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from GroupCompany"
		);
		List<GroupCompany> mbg = query.list();
		
		Integer i = 0;
		for(GroupCompany gc:mbg)
			 if(gc.getShortId()!=null)
				 if(gc.getShortId() > i) i = gc.getShortId();
		groupCompany.setShortId(i+1);
		this.addObjectMD(groupCompany);
		session.close();
	}
	
}
