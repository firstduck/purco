package com.mayora.masterdata.adapter;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.mayora.masterdata.entity.M_Application;
import com.mayora.masterdata.adapter.MasterDataBaseAdapter;

public class ApplicationAdapter extends MasterDataBaseAdapter {

	public String setModule() {
		return "M-APP";
	}
	
	public List<M_Application> BrowseApplication(String search) {
		Session session = sfMD.openSession();
		List<M_Application> mba = session.createQuery("from M_Application where (upper(applicationCode) like :search or upper(applicationName) like :search or upper(description) like :search or upper(developmentLanguage) like :search or upper(type) like :search or upper(systemCode) like :search or id like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		session.close();
		return mba;
	}
	
	public List<M_Application> getApplicationByApplicationCode(String applicationCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from M_Application where applicationCode = '" + applicationCode + "'"
		);
		
		List<M_Application> mba = query.list();
		session.close();
		return mba;
	}
	
	public void deleteApplication(M_Application application, String applicationCode){
		this.deleteObjectMD(application, applicationCode);
	}
	
	public void editApplication(M_Application application){
		this.updateObjectMD(application);
	}
	
	public void insertApplication(M_Application application){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from M_Application"
		);
		List<M_Application> mba = query.list();
		
		Integer i = 0;
		for(M_Application app:mba)
			 if(app.getId()!=null)
				 if(app.getId() > i) i = app.getId();
		application.setId(i+1);
		this.addObjectMD(application);
		session.close();
	}
}

