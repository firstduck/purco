package com.mayora.masterdata.adapter;
import java.util.ArrayList;
import org.hibernate.Session;
import com.mayora.masterdata.entity.M_CURRENCY_OPTION;
import java.util.List;

public class CurrencyAdapter extends MasterDataBaseAdapter {
	
	public List<M_CURRENCY_OPTION>getCurrencyOption()
	{
		Session session = sfMD.openSession();
		List<M_CURRENCY_OPTION> list = new ArrayList<M_CURRENCY_OPTION>();
		try
		{
			list = session.createQuery("FROM M_CURRENCY_OPTION").list();
		}
		catch(Exception e)
		{
			
		}
		finally
		{
			session.close();
		}
		return list;
	}
}
