package com.mayora.masterdata.adapter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mayora.moa.model.JobList;

public class MasterDataBaseAdapter {

	@Autowired  
	@Qualifier("sfMD")  
	protected SessionFactory sfMD;       
	
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sfMD = sessionFactory;
    }
	
    public boolean addObjectMD(Object object) {
        Session session = sfMD.openSession();
        try {
	        session.beginTransaction();
	        session.save(object);
	        session.getTransaction().commit();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	return false;
        } finally {
        	session.close();
        }
        return true;
    }
    
    public boolean deleteObjectMD(Object object, String docNo) {
    	Session session = sfMD.openSession();
        try {
	        session.beginTransaction();
	        session.load(object, docNo);
	        session.delete(object);
	        session.getTransaction().commit();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	return false;
        } finally {
        	session.close();
        }
        return true;
    }
    
    public boolean updateObjectMD(Object object) {
    	Session session = sfMD.openSession();
        try {
	        session.beginTransaction();
	        session.update(object);
	        session.getTransaction().commit();
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	return false;
        } finally {
        	session.close();
        }
        return true;
    }
 
}
