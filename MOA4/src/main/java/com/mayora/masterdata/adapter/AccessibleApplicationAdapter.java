package com.mayora.masterdata.adapter;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.AccessibleApplication;
import com.mayora.masterdata.entity.AccessiblePortalMenu;
import com.mayora.masterdata.entity.M_PortalMenu;

public class AccessibleApplicationAdapter extends MasterDataBaseAdapter {

	public List<AccessibleApplication> accessibleApplication(String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleApplication where username = '" + username + "'"
		);
		List<AccessibleApplication> accessibleApplication = query.list();
		session.close();
		return accessibleApplication;
	}
	public List<AccessiblePortalMenu> getAccessiblePortalMenu(String username)
	{
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessiblePortalMenu where username = '" + username + "' order by applicationName asc, portalMenuID asc"
		);
		List<AccessiblePortalMenu> accessibleApplication = query.list();
		session.close();
		return accessibleApplication;
	}
	
	public List<AccessibleApplication> accessibleApplication(String username,String menuCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleApplication where username = '" + username + "' and menuCode = '"+menuCode+"'"
		);
		List<AccessibleApplication> accessibleApplication = query.list();
		session.close();
		return accessibleApplication;
	}
	
	public List<AccessibleApplication> accessibleApplicationGG(String username,String menuCode) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from AccessibleApplication where username = '" + username + "' and menuCode = '"+menuCode+"'"
		);
		List<AccessibleApplication> accessibleApplication = query.list();
		session.close();
		return accessibleApplication;
	}
	
	public List<M_PortalMenu> getPortalMenuByLink(String link) 
	{
		Session session  = sfMD.openSession();
		List<M_PortalMenu> listPortalMenu = session.createQuery("from M_PortalMenu where link = '"+link+"'").list();
		session.close();
		return listPortalMenu;
	}
	
	public List<String> executeSQL(String qry) {
    	Session session = sfMD.openSession();
		List<String> list = (List<String>) session.createSQLQuery(qry).list();
		session.close();
		return list;
    }
}
