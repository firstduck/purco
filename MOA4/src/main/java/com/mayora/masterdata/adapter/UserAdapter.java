package com.mayora.masterdata.adapter;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.mayora.masterdata.entity.D_User;
import com.mayora.masterdata.entity.D_User_PK;
import com.mayora.masterdata.entity.Delegation;
import com.mayora.masterdata.entity.M_GroupAccess;
import com.mayora.masterdata.entity.M_JOBTITLEACCESSPK;
import com.mayora.masterdata.entity.M_JOBTITLEGROUPWORKFLOW;
import com.mayora.masterdata.entity.M_JOBTITLEGROUPWORKFLOWPK;
import com.mayora.masterdata.entity.M_TempUser;
import com.mayora.masterdata.entity.M_UserMobile;
import com.mayora.masterdata.entity.M_UserMobile_PK;
import com.mayora.masterdata.entity.V_USER_EDIT;
import com.mayora.masterdata.entity.User;
import com.mayora.masterdata.entity.VD_User;
import com.mayora.masterdata.entity.V_Delegation;
import com.mayora.masterdata.model.KendoWindowBrowse;
import com.mayora.masterdata.model.UserChangePassword;
import com.mayora.masterdata.model.UserCreate;
import com.mayora.moa.session.UserGroupAccess;
import com.mayora.masterdata.adapter.MasterDataBaseAdapter;

public class UserAdapter extends MasterDataBaseAdapter {

	public String setModule() {
		return "M-USR";
	}
	
	//username nya sudah to upper
	public Integer checkPassword(String UserName, String Password) throws Exception {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from User where upper(username) = '" + UserName + "'"
		);
		List<User> userList = query.list();
		
		if (userList.size() == 0) {
			session.close();
			return 1;
		} else {
			String hashPassword = this.gen_SHA256_Hash(Password);
			
			query = session.createQuery(
					"from User where upper(username) = :username and password = :hashPassword"
			).setString("username", UserName).setString("hashPassword", hashPassword);
			userList = query.list();
			session.close();
			if(userList.size() == 0) {
				return 2;
			} else {
				return 3;
			}
		}
	}
	
	public User getUser(User user) throws Exception {
		
		String hashPassword = this.gen_SHA256_Hash(user.getPassword());

		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from User where upper(username) = :username and password = :hashPassword"
		).setString("username", user.getUsername().toUpperCase()).setString("hashPassword", hashPassword);
	
		List<User> userList = query.list();
		session.close();
		user = userList.get(0);
		user.setUsername(user.getUsername().toUpperCase());
		
		return user;
    }
	
	public Integer ChangePassword(UserChangePassword changePassword, User user) throws Exception {
		Session session = sfMD.openSession();
		String hashPass = this.gen_SHA256_Hash(changePassword.getPassword());
		Query query = session.createQuery(
				"from User where username = '" + user.getUsername() + "' and password = '" + hashPass + "'"
		);
		List<User> userList = query.list();
		session.close();
		if (userList.size() == 0) {
			return 1;
		} else if (changePassword.getNewPassword().equals(changePassword.getConfirmPassword())) {
			String newHashPass =  this.gen_SHA256_Hash(changePassword.getNewPassword());
			User userUpdate = userList.get(0);
			userUpdate.setPassword(newHashPass);
			this.updateObjectMD(userUpdate);
			return 3;
		} else {
			return 2;
		}
    }
	
//	public boolean isValidEmail(String email){  
//		String  expression="^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";  
//		CharSequence inputStr = email;   
//		Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);  
//		Matcher matcher = pattern.matcher(inputStr);  
//		return matcher.matches();  
//	}
	
    public String gen_SHA256_Hash(String password)throws Exception
	    {

	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update(password.getBytes("UTF-8")); //getbytes(encoding type) supaya lebih aman karena kalau tanpa encoding katanya pakai platform-default dan itu beresiko hasil hash nya beda2
	 
	        byte byteData[] = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        return sb.toString();
	    }
	
	public Integer validateCreateUser(UserCreate userCreate, List<UserGroupAccess> listUga, String type) throws Exception {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from User where username = '" + userCreate.getUsername() + "'");
		List<User> userList = query.list();
		if (userList.size() > 0) {
			session.close();
			return 1;
		} else if(userCreate.getPassword().length() < 6){
			session.close();
			return 9;
		}else if (!userCreate.getPassword().equals(userCreate.getConfirmPassword())) {
			session.close();
			return 2;	
		}
//		else if (!this.isValidEmail(userCreate.getEmail())) {
//			session.close();
//			return 3;
//		}
		  else{ //commit
			  Query checkSize = session.createQuery(
						"select count(*) from User"
				);
			  	
				
				String hashPassword = this.gen_SHA256_Hash(userCreate.getPassword());
				
				if(type.equals("Vendor") || type.equals("Customer"))
				{
					User newUser = new User();
					newUser.setUsername(userCreate.getUsername());
					newUser.setPassword(hashPassword);
					newUser.setEmpId(userCreate.getEmpId());
					newUser.setEmail(userCreate.getEmail());
					newUser.setName(userCreate.getName());
					newUser.setGroupCode(userCreate.getGroupCompCode());
					newUser.setIsDeleted(false);
					newUser.setUsingLDAP(false);
					
					this.addObjectMD(newUser);
					
					for(int i = 0;i<listUga.size();i++){
						if(!listUga.get(i).getIsDeleted()){
							D_User_PK userPK = new D_User_PK();
							userPK.setGroupCode(listUga.get(i).getGroupCode());
							userPK.setUsername(userCreate.getUsername());
							
							D_User d_user = new D_User();
							d_user.setUserPK(userPK);
							this.addObjectMD(d_user);
						}
					}
				}
				else{
					User newUser = new User();
					newUser.setUsername(userCreate.getUsername());
					newUser.setPassword(hashPassword);
					if(userCreate.getUsername().contains("MG")) newUser.setEmpId("000"+userCreate.getUsername().substring(2));
					else newUser.setEmpId(userCreate.getEmpId());
					newUser.setEmail(userCreate.getEmail());
					newUser.setName(userCreate.getName());
					newUser.setGroupCode(userCreate.getGroupCompCode());
					newUser.setIsDeleted(false);
					newUser.setUsingLDAP(false);
					
					this.addObjectMD(newUser);
					
					for(int i = 0;i<listUga.size();i++){
						if(!listUga.get(i).getIsDeleted()){
							D_User_PK userPK = new D_User_PK();
							userPK.setGroupCode(listUga.get(i).getGroupCode());
							userPK.setUsername(userCreate.getUsername());
							
							D_User d_user = new D_User();
							d_user.setUserPK(userPK);
							this.addObjectMD(d_user);
						}
					}
				}
				session.close();
				return 4;

		}
	}


	public List<User> BrowseUser(String search) {
		Session session = sfMD.openSession();
		List<User> mbr = session.createQuery("from User where isDeleted = 0 and (upper(username) like :search or upper(name) like :search or upper(empid) like :search or upper(email) like :search or upper(groupcode) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		session.close();
		return mbr;
	}
	
	public List<User> BrowseUserByGroupCompany(String search, List<String> otorisasi) {
		Session session = sfMD.openSession();
		String gc="";
		for(int i=0;i<otorisasi.size();i++)
		{
			if(i!=0)
			{
				gc=gc+",";
			}
			gc=gc+"'"+otorisasi.get(i).toString()+"'";
		}
		System.out.println(gc);
		List<User> mbr = session.createQuery("from User where isDeleted = 0 and (upper(username) like :search or upper(name) like :search or upper(empid) like :search or upper(email) like :search or upper(groupcode) like :search) and groupCode in("+gc+")")
								.setString("search", "%" + search.toUpperCase() + "%")
								.list(); 
		session.close();
		return mbr;
	}
	
	public List<User> BrowseUserDelegation(String search) {
		Session session = sfMD.openSession();
		List<User> mbr = session.createQuery("from User").list(); 
		session.close();
		return mbr;
	}
	
	public List<KendoWindowBrowse> BrowseUserAndName(String search) {
		Session session = sfMD.openSession();
		List<User> mbr = session.createQuery("from User where isDeleted = 0 and (upper(username) like :search or upper(name) like :search)")
								.setString("search", "%" + search.toUpperCase() + "%")
								.setMaxResults(100).list();
		
		List<KendoWindowBrowse> listKendo = new ArrayList<KendoWindowBrowse>();
		for (int i = 0; i < mbr.size(); i++) {
			listKendo.add(new KendoWindowBrowse(mbr.get(i).getUsername(), mbr.get(i).getName()));
		}
		
		session.close();
		return listKendo;
	}
	
	
	public List<User> getUserByUsername(String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from User where upper(username) = '" + username.toUpperCase() + "' and isDeleted = 0"
		);
		
		List<User> mbr = query.list();
		session.close();
		return mbr;
	}
	
	public List<M_TempUser> getUserPAByUsername(String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from M_TempUser where upper(username) = '" + username.toUpperCase() + "'"
		);
		
		List<M_TempUser> mbr = query.list();
		session.close();
		return mbr;
	}
	
	public Integer validateEditUser(UserCreate userEdit, List<UserGroupAccess> listUga) throws Exception
	{
		String username =  userEdit.getUsername();
		String password = userEdit.getPassword();
		String email = userEdit.getEmail();
		String name = userEdit.getName();
		String groupCode = userEdit.getGroupCompCode(); 
		
		if (!password.equals(userEdit.getConfirmPassword())) {
			return 1;
		}
//		else if (!isValidEmail(email)){
//			return 2;
//		}
		else{
			Session session = sfMD.openSession();
			Query query = session.createQuery(
					"from User where username = '" + username + "'"
			);
			
			List<User> userList = query.list();
			session.close();
			
			User userUpdate = (User)userList.get(0);
			
			
			if(password.equals("*#$%^&@#")){
				//do Nothing
			}
			else{
					String hashPassword = this.gen_SHA256_Hash(password);
					userUpdate.setPassword(hashPassword);
			}
			
			userUpdate.setName(name);
			userUpdate.setEmail(email);
			userUpdate.setGroupCode(groupCode);
			
			this.updateObjectMD(userUpdate);
			
			this.reset_D_User(username);
			
//			for(int i = 0;i<listUga.size();i++){
//				if(!listUga.get(i).getIsDeleted()){
//					
//					D_User_PK userPK = new D_User_PK();
//					userPK.setGroupCode(listUga.get(i).getGroupCode());
//					userPK.setUsername(username);
//					
//					D_User d_user = new D_User();
//					d_user.setUserPK(userPK);
//					this.addObjectMD(d_user);
//
//				}
//			}
			
			return 3;
		}
	}
	
	public void deleteUser(String username){
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from User where username = '" + username + "'"
		);
		
		List<User> userList = query.list();
		
		User userDelete = (User)userList.get(0);
		
		userDelete.setIsDeleted(true);
		this.updateObjectMD(userDelete);
		session.close();
	}
	
	public void reset_D_User(String username){
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE D_User user where user.userPK.username ='"+ username +"'");
			query.executeUpdate();
			session.getTransaction().commit();
		}
		catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
		}
		finally{
			session.close();
		}
	}

	public List<UserGroupAccess> getUserGroupAccessByUsername(String username) {
		Session session = sfMD.openSession();
		Query query = session.createQuery("from VD_User user where user.vd_userPK.username ='"+ username +"'");
		List<VD_User> listVDU = query.list();
		List<UserGroupAccess> listUga = new ArrayList<UserGroupAccess>();
		
		Integer i = 0;
		for(VD_User vdu : listVDU){
			UserGroupAccess uga = new UserGroupAccess();
			uga.setIndex(i);
			uga.setGroupCode(vdu.getVd_userPK().getGroupCode());
			uga.setGroupName(vdu.getGroupName());
			uga.setIsDeleted(false);
			listUga.add(uga);
			i++;
		}
		session.close();
		return listUga;
	}
	
	public List<D_User> get_D_UserByUsername(){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from D_User");
		List<D_User> listDU = query.list();
		session.close();
		return listDU;
	}
	
	public Integer checkUser(Delegation d)
	{
		if(getUserByUsername(d.getDelegationPK().getDelegateas()).size() == 0)  return -1;
		else if (getUserByUsername(d.getDelegationPK().getUsername()).size() == 0) return -2;
		else if(d.getDelegationPK().getDelegateas().equals(d.getDelegationPK().getUsername())) return -3;
		else 
		{
			if(d.getEndDate().compareTo(d.getStartDate())<0) return 0;
			else return 1;
		}
	}
	
	public boolean DelegateUser(Delegation d)
	{
		Session session = sfMD.openSession();
		Query query = session.createQuery(
				"from Delegation del where del.delegationPK.username = '" + d.getDelegationPK().getUsername() + "' and del.delegationPK.delegateas = '"+ d.getDelegationPK().getDelegateas()+"'"
		);
		
		List<Delegation> delegationList = query.list();
		if(!delegationList.isEmpty())
		{
			try
			{
				session.beginTransaction();
				query = session.createQuery("DELETE Delegation del where del.delegationPK.username ='"+ d.getDelegationPK().getUsername() + "' and del.delegationPK.delegateas = '"+ d.getDelegationPK().getDelegateas()+"'");
				query.executeUpdate();
				session.getTransaction().commit();
			}
			catch(HibernateException e) { 
	        	session.getTransaction().rollback(); 
			}
		}		

		try	{	
			this.addObjectMD(d);
			session.close();
			return true;
		}
		catch(Exception e) {
			session.close();
			return false;
		}
	}
	
	public List<String> get_GroupCodeByUsername(String UserName){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from D_User user where user.userPK.username = '" + UserName + "'");
		List<D_User> listDU = query.list();
		List<String> listString = new ArrayList<String>();
		for(D_User d_user:listDU){
			listString.add(d_user.getUserPK().getGroupCode());
		}
		session.close();
		return listString;
	}
	
	public List<V_Delegation> get_DelegationByUsername(String UserName){
		Session session = sfMD.openSession();
		Query query = session.createQuery("from V_Delegation where username = '" + UserName + "'");
		List<V_Delegation> listVD = query.list();
		session.close();
		return listVD;
	}
	
	public List<VD_User> getNameUserByGroupCode(String groupCode,String search){
		Session session = sfMD.openSession();
		List<VD_User> listVU = session.createQuery("from VD_User vdu where vdu.vd_userPK.groupCode = '" + groupCode + "' and (upper(vdu.vd_userPK.username) like :search or upper(name) like :search) order by name")
				.setString("search", "%" + search.toUpperCase() + "%")
				.list();
		return listVU;
	}
	
	public Boolean getTokenByAppCode(String appCode)
	{
		Session session = sfMD.openSession();
		try
		{
			SQLQuery query = session.createSQLQuery("select CHECKTOKEN('"+appCode+"') from dual");
			List<BigDecimal> listDec = query.list();
			
			if(listDec.get(0) != null) return true;
			else return false;
		}
		catch(HibernateException e) { System.out.println(e); }
		return false;
	}

	public String getEmployeeName(String username) {
		
		Session session = sfMD.openSession();
		List<String> listString = session.createSQLQuery("select name from MYRHR.M_EMPLOYEE where EMPID = '" + username + "'").list();
		if(listString.size()>0) return listString.get(0);
		return "";
	}

	public void updateTestGcm(String username) {
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			SQLQuery sql  = session.createSQLQuery("UPDATE T_GCMLOG SET RECEIVEDATE=systimestamp where username = '"+username+"'");
			sql.executeUpdate();
			session.getTransaction().commit();
		}catch(Exception e){
			System.out.println(e);
			session.getTransaction().rollback();
		}finally{
			session.close();
		}
	}
	
	public String addTestGcm(String username) {
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery("delete t_gcmlog where username='"+username+"'");
			sqlQuery.executeUpdate();
			
			sqlQuery = session.createSQLQuery("INSERT INTO T_GCMLOG(USERNAME,REQUESTDATE,RECEIVEDATE) VALUES ('"+username+"',systimestamp,null)");
			sqlQuery.executeUpdate();
			String temp = "Request : " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
			session.getTransaction().commit();
			return temp;
		}catch(Exception e){
			System.out.println(e);
			session.getTransaction().rollback();
		}finally{
			session.close();
		}

		return "Gagal";
	}

	public String getGcmLog(String username) {
		Session session = sfMD.openSession();
		List<String> list = session.createSQLQuery("select 'Request : ' || to_char(requestdate,'DD-MM-YYYY HH24:MI:SS') || ' , Received : ' || to_char(receivedate,'DD-MM-YYYY HH24:MI:SS') from t_gcmlog").list();
		
		session.close();
		if(list.size()>0)return list.get(0);
		return "Gagal";
	}
	
	public String getUserClient(String access){
		Session session = sfMD.openSession();
		List<String> list = session.createSQLQuery("select useraccess from t_clientsession where accesscode='"+access+"'").list();
		session.close();
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	public List<M_UserMobile> getUserMobileByUsername(String username) {
		
		Session session = sfMD.openSession();
		List <M_UserMobile> list = new ArrayList<M_UserMobile>();
		try
		{
			list = session.createQuery("from M_UserMobile where pk.username = '"+username+"'").list();
		}
		finally
		{
			session.close();
		}
		
		return list;
	}
	

	public boolean deleteTempData(String username) {
		Session session = sfMD.openSession();
        	try {
	             session.beginTransaction();
	             Query query = null;
	             query = session.createSQLQuery("delete from MYRMD.M_USER_MOBILE where USERNAME = '"+username+"' ");
	             System.out.println(query.getQueryString());
	             query.executeUpdate();
	             session.getTransaction().commit();
	             session.close();
	             return true;
	         } catch(HibernateException e) {
	             session.getTransaction().rollback();
	             session.close();
	             return false;
	         }
	         
	}

//	public List<M_JOBTITLEACCESSPK> getJobTitleAcces(String empId) {
//		
//		Session session = sfMD.openSession();
//		List <M_JOBTITLEACCESSPK> list = new ArrayList<M_JOBTITLEACCESSPK>();
//		try
//		{
//			list = session.createQuery("from MYRHR").list();
//		}
//		
//		
//		return null;
//	}
	
	
	public List<Map<String,Object>> getAjaxAllJobTitleAccessByEmpId(String empId)
	{
		Session session = sfMD.openSession();
		Query query = session.createSQLQuery("select JOBTITLENAME from MYRHR.M_EMPLOYEE where EMPID = '" + empId + "'");
		
		System.out.println(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		
		List<Map<String,Object>> aliasToValueMapList = new ArrayList<Map<String,Object>>();
		aliasToValueMapList = query.list();
		return aliasToValueMapList;
	}

	public List<M_JOBTITLEACCESSPK> getGroupCodeByJobTitle(String jobTitleName) {
		
		Session session = sfMD.openSession();
		List <M_JOBTITLEACCESSPK> list = new ArrayList<M_JOBTITLEACCESSPK>();
		try
		{
			list = session.createQuery("from M_JOBTITLEACCESSPK where jobTitleName = '"+jobTitleName+"'").list();
		}
		finally
		{
			session.close();
		}
		return list;
	}

//	public List<V_USER_EDIT> getVUserEditByUsername(String userName) {
//		
//		Session session = sfMD.openSession();
//		List <V_USER_EDIT> list = new ArrayList<V_USER_EDIT>();
//		try
//		{
//			list = session.createQuery("from V_USER_EDIT where userName = '"+userName+"'").list();
//		}
//		finally
//		{
//			session.close();
//		}
//		
//		return list;
//	}
	
	public List<Map<String,Object>> getVUserEditByUsername(String userName)
	{
		Session session = sfMD.openSession();
		Query query = session.createSQLQuery("select * from V_USER_EDIT where USERNAME = '" + userName + "'");
		
		System.out.println(query.getQueryString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		
		List<Map<String,Object>> aliasToValueMapList = new ArrayList<Map<String,Object>>();
		aliasToValueMapList = query.list();
		return aliasToValueMapList;
	}

	 public boolean deleteAll(String table,String variable, String value) {
			Session session = sfMD.openSession();
		  try {
		      session.beginTransaction();	
		      Query query = session.createSQLQuery("Delete from "+table+" where "+variable+"='"+value+"'");
		      System.out.println(query.getQueryString());
		      query.executeUpdate();
		      session.getTransaction().commit();
		      session.close();
			  return true;
		  } catch(HibernateException e) { 
		  	session.getTransaction().rollback(); 
		  	session.close();
		  	return false;
		  }
	    }

	public List<M_JOBTITLEGROUPWORKFLOWPK> getAllJobTitleGroupWF(
			String jobTitleCode) {
		
		Session session = sfMD.openSession();
		List <M_JOBTITLEGROUPWORKFLOWPK> list = new ArrayList<M_JOBTITLEGROUPWORKFLOWPK>();
		try
		{
			list = session.createQuery("from M_JOBTITLEGROUPWORKFLOWPK where jobTitleCode = '"+jobTitleCode+"' order by jobTitleCode asc" ).list();
		}
		finally
		{
			session.close();
		}
		
		return list;
	}
	
	public void deleteGridJobTitleWF(String groupWorkflowCode, String jobTitleCode){
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createSQLQuery("DELETE from MYRMD.M_JOBTITLEGROUPWORKFLOW where groupWorkfLowCode='"+groupWorkflowCode+"' "+
					"AND jobTitleCode='"+jobTitleCode+"'");
			query.executeUpdate();
			System.out.println(query);
			session.getTransaction().commit();
		}catch(Exception e){
			session.getTransaction().rollback();
		}finally{
			session.close();
		}
	}
	
	public boolean saveObject(Object object) {
    	Session session = sfMD.openSession();
        try {
	        session.beginTransaction();
	        session.save(object);
	        session.getTransaction().commit();
	        session.close();
	        
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }

	public boolean saveupdateObject(Object object) {
    	Session session = sfMD.openSession();
        try {
	        session.beginTransaction();
	        session.saveOrUpdate(object);
	        session.getTransaction().commit();
	        session.close();
	        
	        return true;
        } catch(HibernateException e) { 
        	session.getTransaction().rollback(); 
        	session.close();
        	return false;
        } 
      
    }


}

