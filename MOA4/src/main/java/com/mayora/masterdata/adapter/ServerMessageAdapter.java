package com.mayora.masterdata.adapter;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.mayora.masterdata.entity.M_ServerMessage;
import com.mayora.masterdata.entity.V_ServerMessage;
import com.mayora.masterdata.model.KendoWindowBrowse;
import com.mayora.masterdata.adapter.MasterDataBaseAdapter;

public class ServerMessageAdapter extends MasterDataBaseAdapter{
	public List<M_ServerMessage> getServerMessageListBySystemCode(String sysCode){
		Session session = sfMD.openSession();
		List<M_ServerMessage> listSM = session.createQuery("from M_ServerMessage sub where sub.serverMessagePK.systemCode='"+sysCode+"'").list();
		session.close();
		return listSM;
	}

	public void removeServerMessage(String sysCode) {
		Session session = sfMD.openSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("DELETE FROM M_ServerMessage sub where sub.serverMessagePK.systemCode='"+sysCode+"'");
			query.executeUpdate();
			session.getTransaction().commit();
		} 
		catch(HibernateException e) { 
        	session.getTransaction().rollback();  
		}
		finally{ 
			session.close();
		}
	}
	
	public List<KendoWindowBrowse> getServerMessageWindowData(){
		Session session = sfMD.openSession();
		List<V_ServerMessage> mga = session.createQuery("from V_ServerMessage where isDeleted = 0")
								         .list(); 
		session.close();
		List<KendoWindowBrowse> list = new ArrayList<KendoWindowBrowse>();
		for (int i = 0; i < mga.size(); i++) {
			Integer no = (i+1);
			String message = mga.get(i).getMessage();
			list.add(new KendoWindowBrowse(no.toString(), message));
		}
		
		return list;
	}

	public String setModule() {
		return "M-SVM";
	}

}
