package com.mayora.masterdata.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import com.mayora.masterdata.adapter.MasterDataBaseAdapter;
import com.mayora.masterdata.entity.V_OPERATION_CALENDER;

public class OperationCalenderAdapter extends MasterDataBaseAdapter {

	@SuppressWarnings("unchecked")
	public List <V_OPERATION_CALENDER> getCalender(String search){ 

		Session session = sfMD.openSession();
		if(search == null || search.equals("")){
			List<V_OPERATION_CALENDER> calender = session.createQuery("FROM V_OPERATION_CALENDER where year = '"+new SimpleDateFormat("yyyy").format(new Date())+"'").setMaxResults(100).list(); 
			session.close();
			return calender;
		}
		else{
			List<V_OPERATION_CALENDER> calender = session.createQuery("from V_OPERATION_CALENDER where (upper(month) like :search)")
					 .setString("search", "%" + search.toUpperCase() + "%").setMaxResults(100)
			         .list(); 
			session.close();
			return calender;
		}
	}
}
