package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class UserCreate{
	
	@Id
	@NotBlank(message="Username tidak boleh kosong!")
	private String username;
	
	@NotBlank(message="Password tidak boleh kosong!")
	private String password;
	
	@NotBlank(message="Confirm Password tidak boleh kosong!")
	private String confirmPassword;
	
	@NotBlank(message="Nama tidak boleh kosong!")
	private String name;
	
	private String email;

	@NotBlank(message="Group Code tidak boleh kosong!")
	private String groupCompCode;
	
	private String empId;
	
	private Boolean usingLDAP;
	
	private Boolean isDeleted;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
	public String getGroupCompCode() {
		return groupCompCode;
	}

	public void setGroupCompCode(String groupCompCode) {
		this.groupCompCode = groupCompCode;
	}

	public Boolean getUsingLDAP() {
		return usingLDAP;
	}

	public void setUsingLDAP(Boolean usingLDAP) {
		this.usingLDAP = usingLDAP;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}

