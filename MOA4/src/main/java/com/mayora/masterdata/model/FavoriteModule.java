package com.mayora.masterdata.model;

import java.util.List;

public class FavoriteModule {

	private String moduleCode;
	
	private String moduleName;
	
	List<FavoriteMenu> list;

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public List<FavoriteMenu> getList() {
		return list;
	}

	public void setList(List<FavoriteMenu> list) {
		this.list = list;
	}
}
