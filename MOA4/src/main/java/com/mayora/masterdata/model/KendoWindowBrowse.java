package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KendoWindowBrowse {

	@Id
	private String id;
	private String value;
	
	public KendoWindowBrowse(String id, String value) {
		setId(id);
		setValue(value);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
