package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PanelButtonEntry {
	
	@Id
	private String id;
	private String send;
	private String cancel;
	private String pending;
	private String return_;
	private String saveAndSend;
	private String saveAndClose;
	private String saveAndApprove;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSend() {
		return send;
	}
	public void setSend(String send) {
		this.send = send;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getPending() {
		return pending;
	}
	public void setPending(String pending) {
		this.pending = pending;
	}
	public String getReturn_() {
		return return_;
	}
	public void setReturn_(String return_) {
		this.return_ = return_;
	}
	public String getSaveAndSend() {
		return saveAndSend;
	}
	public void setSaveAndSend(String saveAndSend) {
		this.saveAndSend = saveAndSend;
	}
	public String getSaveAndClose() {
		return saveAndClose;
	}
	public void setSaveAndClose(String saveAndClose) {
		this.saveAndClose = saveAndClose;
	}
	public String getSaveAndApprove() {
		return saveAndApprove;
	}
	public void setSaveAndApprove(String saveAndApprove) {
		this.saveAndApprove = saveAndApprove;
	}
}
