package com.mayora.masterdata.model;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.mayora.masterdata.entity.Module;

public class ListPaging {

	private Integer total;
	private List<Object> data;
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public List<?> getData() {
		return data;
	}
	public void setData(List<Object> data) {
		this.data = data;
	}
	
	public ListPaging(Integer total,List<Object> data) {
		this.data = data;
		this.total = total;
	}
	
}
