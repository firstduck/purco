package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class UserChangePassword {
	
	@Id
	@NotBlank(message="Password tidak boleh kosong!")
	private String password;
	
	@NotBlank(message="New Password tidak boleh kosong!")
	private String newPassword;
	
	@NotBlank(message="Confirm Password tidak boleh kosong!")
	private String confirmPassword;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
