package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class KendoWindowBrowse3KPI {

	@Id
	private String value1;
	private String value2;
	private String value3;
	
	public KendoWindowBrowse3KPI(String value1, String value2, String value3) {
		setValue1(value1);
		setValue2(value2);
		setValue3(value3);
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		if(value2==null){
			this.value2="---";
		}else{
			this.value2 = value2;
		}
		
	}
	
	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		if(value3==null){
			this.value3="---";
		}else{
			this.value3 = value3;
		}		
	}
}
