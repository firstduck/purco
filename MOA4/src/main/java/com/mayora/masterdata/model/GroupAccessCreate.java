package com.mayora.masterdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class GroupAccessCreate {

	@Id
	@NotBlank(message="groupCode tidak boleh kosong!")
	private String groupCode;
	
	@NotBlank(message="Group Name tidak boleh kosong!")
	private String groupName;
	
	@NotBlank(message="Description tidak boleh kosong!")
	private String description;

	private String moduleCode;
	 
	private String moduleName;
	
	private String applicationCode;
	
	private String applicationName;
	
	private Boolean access_; 
	
	private Boolean viewAll;
	
	private Boolean insert_;
	
	private Boolean edit;
	
	private Boolean delete_;
	
	private Boolean print;
	
	private Boolean review;
	
	private Boolean approve;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Boolean getAccess_() {
		return access_;
	}

	public void setAccess_(Boolean access_) {
		this.access_ = access_;
	}

	public Boolean getViewAll() {
		return viewAll;
	}

	public void setViewAll(Boolean viewAll) {
		this.viewAll = viewAll;
	}

	public Boolean getInsert_() {
		return insert_;
	}

	public void setInsert_(Boolean insert_) {
		this.insert_ = insert_;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public Boolean getDelete_() {
		return delete_;
	}

	public void setDelete_(Boolean delete_) {
		this.delete_ = delete_;
	}

	public Boolean getPrint() {
		return print;
	}

	public void setPrint(Boolean print) {
		this.print = print;
	}

	public Boolean getReview() {
		return review;
	}

	public void setReview(Boolean review) {
		this.review = review;
	}

	public Boolean getApprove() {
		return approve;
	}

	public void setApprove(Boolean approve) {
		this.approve = approve;
	}
}
