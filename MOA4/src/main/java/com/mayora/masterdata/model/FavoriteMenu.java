package com.mayora.masterdata.model;

public class FavoriteMenu {
	
	private String menu;
	
	private String action;
	
	private String webname;

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getWebname() {
		return webname;
	}

	public void setWebname(String webname) {
		this.webname = webname;
	}
	
}
